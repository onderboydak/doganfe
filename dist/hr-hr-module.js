(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["hr-hr-module"],{

/***/ "./src/app/hr/hr.module.ts":
/*!*********************************!*\
  !*** ./src/app/hr/hr.module.ts ***!
  \*********************************/
/*! exports provided: HrModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HrModule", function() { return HrModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _hr_routing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./hr.routing */ "./src/app/hr/hr.routing.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _member_member_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./member/member.component */ "./src/app/hr/member/member.component.ts");
/* harmony import */ var _payroll_payroll_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./payroll/payroll.component */ "./src/app/hr/payroll/payroll.component.ts");
/* harmony import */ var _team_team_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./team/team.component */ "./src/app/hr/team/team.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var HrModule = /** @class */ (function () {
    function HrModule() {
    }
    HrModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_hr_routing__WEBPACK_IMPORTED_MODULE_4__["HrRoutes"]),
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"]
            ],
            declarations: [
                _member_member_component__WEBPACK_IMPORTED_MODULE_6__["MemberComponent"],
                _payroll_payroll_component__WEBPACK_IMPORTED_MODULE_7__["PayrollComponent"],
                _team_team_component__WEBPACK_IMPORTED_MODULE_8__["TeamComponent"]
            ]
        })
    ], HrModule);
    return HrModule;
}());



/***/ }),

/***/ "./src/app/hr/hr.routing.ts":
/*!**********************************!*\
  !*** ./src/app/hr/hr.routing.ts ***!
  \**********************************/
/*! exports provided: HrRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HrRoutes", function() { return HrRoutes; });
/* harmony import */ var _member_member_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./member/member.component */ "./src/app/hr/member/member.component.ts");
/* harmony import */ var _payroll_payroll_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./payroll/payroll.component */ "./src/app/hr/payroll/payroll.component.ts");
/* harmony import */ var _team_team_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./team/team.component */ "./src/app/hr/team/team.component.ts");
/* harmony import */ var _auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth/auth-guard.service */ "./src/app/auth/auth-guard.service.ts");




var HrRoutes = [
    {
        path: '',
        children: [{
                path: 'team',
                component: _team_team_component__WEBPACK_IMPORTED_MODULE_2__["TeamComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]]
            }]
    },
    {
        path: '',
        children: [{
                path: 'member',
                component: _member_member_component__WEBPACK_IMPORTED_MODULE_0__["MemberComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]]
            }]
    },
    {
        path: 'member/:id',
        component: _member_member_component__WEBPACK_IMPORTED_MODULE_0__["MemberComponent"],
        canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]]
    },
    {
        path: '',
        children: [{
                path: 'payroll',
                component: _payroll_payroll_component__WEBPACK_IMPORTED_MODULE_1__["PayrollComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]]
            }]
    },
    {
        path: 'payroll/:id',
        component: _payroll_payroll_component__WEBPACK_IMPORTED_MODULE_1__["PayrollComponent"],
        canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuardService"]]
    },
];


/***/ }),

/***/ "./src/app/hr/member/member.component.html":
/*!*************************************************!*\
  !*** ./src/app/hr/member/member.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <form method=\"post\" class=\"form-horizontal\">\n                    <h4 class=\"title\">New/Edit Employee</h4>\n                    <div class=\"card\">\n                        <!-- mains will be here -->\n                        <div class=\"header\">\n                            <legend>Basics</legend>\n                        </div>\n                        <div class=\"content\">\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Name</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"name\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"employee.name\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Last Name</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"lastName\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"employee.lastName\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">TCKN</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" [(ngModel)]=\"employee.TCKN\"\n                                            name=\"TCKN\" [ngModelOptions]=\"{standalone: true}\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Email</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" [(ngModel)]=\"employee.email\"\n                                            name=\"email\" [ngModelOptions]=\"{standalone: true}\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Position/Title</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" [(ngModel)]=\"employee.title\"\n                                            name=\"title\" [ngModelOptions]=\"{standalone: true}\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Department</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\"\n                                            [(ngModel)]=\"employee.department\" name=\"department\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">StartDate</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"employee.startDate\"\n                                            name=\"startDate\" class=\"form-control datetimepicker pickerStartDate\"\n                                            placeholder=\"Pick Date Time\" />\n                                    </div>\n\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">SSN</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" [(ngModel)]=\"employee.SSKNo\"\n                                            name=\"SSKNo\" [ngModelOptions]=\"{standalone: true}\">\n                                    </div>\n                                </div>\n                            </fieldset>\n\n                            <div class=\"header\">\n                                <legend>Personal</legend>\n                            </div>\n                            <br>\n\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Home Phone</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\"\n                                            [(ngModel)]=\"employee.homePhone\" name=\"homePhone\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Cell Phone</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\"\n                                            [(ngModel)]=\"employee.cellPhone\" name=\"cellPhone\">\n                                    </div>\n                                </div>\n                            </fieldset>\n\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">School</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\"\n                                            [(ngModel)]=\"employee.school\" name=\"school\">\n                                    </div>\n\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Birthday</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"employee.birthday\"\n                                            name=\"birthday\" class=\"form-control datetimepicker pickerBirthday\"\n                                            placeholder=\"Pick Date Time\" />\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <div class=\"header\">\n                                <legend>Compensation Related</legend>\n                            </div>\n                            <br>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Salary</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"number\" placeholder=\"\" class=\"form-control\" [ngModelOptions]=\"{standalone: true}\"\n                                            [(ngModel)]=\"employee.salary\" name=\"salary\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Net/Gross Salary?</label>\n                                    <div class=\"col-sm-10\">\n                                        <div class=\"radio\">\n                                            <input type=\"radio\" name=\"isNet\" id=\"radioNet\" value=\"1\" (change)=\"onInChange(1)\"\n                                                [checked]=\"employee.isNet==1 || employee.ID==0\">\n                                            <label for=\"radioNet\">\n                                                Net\n                                            </label>\n                                        </div>\n                                        <div class=\"radio\">\n                                            <input type=\"radio\" name=\"isNet\" id=\"radioGross\" value=\"0\" (change)=\"onInChange(0)\"\n                                                [checked]=\"employee.isNet==0\">\n                                            <label for=\"radioGross\">\n                                                Gross\n                                            </label>\n                                        </div>\n                                    </div>\n                                </div>\n                            </fieldset>\n\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Is Married?</label>\n                                    <div class=\"col-sm-10\">\n                                        <div class=\"radio\">\n                                            <input type=\"radio\" name=\"isMarried\" id=\"radioMarried\" value=\"1\" (change)=\"onImChange(1)\"\n                                                [checked]=\"employee.isMarried==1 || employee.ID==0\">\n                                            <label for=\"radioMarried\">\n                                                Married\n                                            </label>\n                                        </div>\n                                        <div class=\"radio\">\n                                            <input type=\"radio\" name=\"isMarried\" id=\"radioBachelor\" value=\"0\" (change)=\"onImChange(0)\"\n                                                [checked]=\"employee.isMarried==0\">\n                                            <label for=\"radioBachelor\">\n                                                Not Married\n                                            </label>\n                                        </div>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <!--Burası eger radıo 1 secılırse cıkacak-->\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Is Partner Working?</label>\n                                    <div class=\"col-sm-10\">\n                                        <div class=\"radio\">\n                                            <input type=\"radio\" name=\"isSpouseWorking\" id=\"radioWorking\" value=\"1\"\n                                                (change)=\"onIsChange(1)\" [checked]=\"employee.isSpouseWorking==1 || employee.ID==0\">\n                                            <label for=\"radioWorking\">\n                                                Working\n                                            </label>\n                                        </div>\n                                        <div class=\"radio\">\n                                            <input type=\"radio\" name=\"isSpouseWorking\" id=\"radioNotWorking\" value=\"0\"\n                                                (change)=\"onIsChange(0)\" [checked]=\"employee.isSpouseWorking==0 || employee.isMarried==0\">\n                                            <label for=\"radioNotWorking\">\n                                                Not Working\n                                            </label>\n                                        </div>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <!--Burası eger radıo 1 secılırse cıkacak-->\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Number of Children</label>\n                                    <div class=\"col-sm-10\">\n                                        <select name=\"numberOfKids\" [(ngModel)]=\"employee.numberOfKids\" class=\"selectpicker\"\n                                            data-title=\"\" data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                            <option selected [value]=\"1\">1</option>\n                                            <option selected [value]=\"2\">2</option>\n                                            <option selected [value]=\"3\">3</option>\n                                            <option selected [value]=\"4\">4</option>\n                                            <option selected [value]=\"5\">5</option>\n                                            <option selected [value]=\"6\">6</option>\n                                            <option selected [value]=\"7\">7</option>\n                                            <option selected [value]=\"8\">8</option>\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <div class=\"header\">\n                                <legend>Files</legend>\n                                <span style='float:right;font-size:16px;'>\n                                    <a (click)='addFile()' title='Add File' style='color:gray;margin-right:10px;cursor:pointer;'><i\n                                            class='fa fa-plus-square-o'></i></a>\n                                </span>\n                            </div>\n                            <br>\n                            <div class=\"content\" style=\"padding-top: 30px;\">\n                                    <table class=\"table table-hover\">\n                                        <thead>\n                                            <tr>\n                                                <th>#</th>\n                                                <th class=\"text-left\">Description</th>\n                                                <th class=\"text-center\">Date</th>\n                                                <th class=\"text-center\"></th>\n                                            </tr>\n                                        </thead>\n                                        <tbody>\n                                            <tr *ngFor=\"let file of files;let i = index\">\n                                                <td class=\"text-left\">{{i+1}}</td>\n                                                <td class=\"text-left\"><a href=\"https://crmapi.efdigitalcodes.com/Uploads/files/{{file.fileName}}\" target=\"_blank\">{{file.description}}</a></td>\n                                                <td class=\"text-center\">{{file.date_}}</td>\n                                                <td class=\"text-right\"><a (click)=\"deleteFile(file.ID,i)\"><i class=\"fa fa-trash\"> </i></a></td>\n                                            </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n\n\n\n                        </div>\n                    </div> <!-- end card -->\n\n                    <div class=\"content\">\n                        <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" *ngIf=\"memberID>0 && (userType==1 || userType==3)\"\n                            (click)=\"deleteEmployee()\">DELETE</button>\n\n                        <div style='float:right;'>\n                            <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                            <button class=\"btn btn-success btn-fill btn-wd\" (click)=\"saveEmployee()\">SAVE</button>\n                        </div>\n                    </div>\n                </form>\n            </div> <!-- end col-md-12 -->\n        </div> <!-- end row -->\n\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/hr/member/member.component.scss":
/*!*************************************************!*\
  !*** ./src/app/hr/member/member.component.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hyL21lbWJlci9tZW1iZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/hr/member/member.component.ts":
/*!***********************************************!*\
  !*** ./src/app/hr/member/member.component.ts ***!
  \***********************************************/
/*! exports provided: MemberComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberComponent", function() { return MemberComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var app_pages_upload_upload_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/pages/upload/upload.component */ "./src/app/pages/upload/upload.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MemberComponent = /** @class */ (function () {
    function MemberComponent(apiService, activeRoute, router, modalService) {
        this.apiService = apiService;
        this.activeRoute = activeRoute;
        this.router = router;
        this.modalService = modalService;
        this.employee = [];
        this.files = [];
        var routeParams = this.activeRoute.snapshot.params;
        this.memberID = routeParams.id;
        console.log(this.memberID, "memberID");
        this.businessID = localStorage.getItem("businessID");
        this.userType = localStorage.getItem("userType");
        console.log(this.userType, "userType");
        if (this.memberID > 0) {
            this.getMember(this.memberID);
            this.getFiles(this.memberID);
        }
        else {
            this.employee = { "ID": "0", "TCKN": "", "name": "", "lastName": "", "email": "", "homePhone": "", "cellPhone": "", "birthday": "", "title": "", "startDate": "", "salary": "0", "isNet": "1", "school": "", "department": "", "socialSecDoc": "", "SSKNo": "", "idDoc": "", "gradDoc": "", "asID": this.businessID, "userID": "0", "isMarried": "0", "numberOfKids": "0", "isSpouseWorking": "0", "payrollInfo": "" };
        }
    }
    MemberComponent.prototype.getFiles = function (memberID) {
        var _this = this;
        this.apiService.getData(memberID + "/members", "files/").then(function (result) {
            _this.files = result;
            console.log(_this.files, "files");
        });
    };
    MemberComponent.prototype.getMember = function (memberID) {
        var _this = this;
        if (this.memberID > 0) {
            this.apiService.getData(this.businessID + "/" + memberID, "members/").then(function (result) {
                _this.employee = result[0];
                console.log(_this.employee, "member");
                $(".selectpicker").selectpicker('val', _this.employee.numberOfKids);
            });
        }
        else {
            this.employee.ID = 0;
        }
    };
    MemberComponent.prototype.onInChange = function (ct) {
        this.employee.isNet = ct;
    };
    MemberComponent.prototype.addFile = function () {
        var modalRef = this.modalService.open(app_pages_upload_upload_component__WEBPACK_IMPORTED_MODULE_4__["UploadComponent"], { "backdrop": "static" });
        modalRef.componentInstance.ID = this.memberID;
        modalRef.componentInstance.table = "members";
        modalRef.result.then(function (data) {
            // on close
            console.log(data, "result data");
            if (data > 0) {
                //this.invoices[index].paymentID = data;
            }
        }, function () {
            // on dismiss
        });
    };
    MemberComponent.prototype.deleteFile = function (fileID, index) {
        var _this = this;
        if (confirm("Are you sure ?")) {
            this.apiService.deleteData(fileID, "files/delete/").then(function (result) {
                console.log(result);
                _this.files.splice(index, 1);
            });
        }
        ;
    };
    MemberComponent.prototype.onImChange = function (ct) {
        this.employee.isMarried = ct;
    };
    MemberComponent.prototype.onIsChange = function (ct) {
        this.employee.isSpouseWorking = ct;
    };
    MemberComponent.prototype.ngOnInit = function () {
        this.spInit();
    };
    MemberComponent.prototype.clickCancel = function () {
        this.router.navigateByUrl("/hr/team");
    };
    MemberComponent.prototype.saveEmployee = function () {
        var _this = this;
        console.log(this.employee, "employee");
        this.employee.asID = this.businessID;
        this.employee.startDate = $(".pickerStartDate").val();
        this.employee.birthday = $(".pickerBirthday").val();
        this.apiService.postData(this.memberID, "member/save/", JSON.stringify(this.employee)).then(function (result) {
            console.log(result);
            _this.router.navigateByUrl("/hr/team");
        });
    };
    MemberComponent.prototype.deleteEmployee = function () {
        var _this = this;
        this.apiService.deleteData(this.memberID, "/member/delete/").then(function (result) {
            console.log(result);
            _this.router.navigateByUrl("/hr/team");
        });
    };
    MemberComponent.prototype.spInit = function () {
        $('.datetimepicker').datetimepicker({
            locale: 'tr',
            format: 'DD-MM-YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], MemberComponent.prototype, "fileInput", void 0);
    MemberComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-member',
            template: __webpack_require__(/*! ./member.component.html */ "./src/app/hr/member/member.component.html"),
            styles: [__webpack_require__(/*! ./member.component.scss */ "./src/app/hr/member/member.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]])
    ], MemberComponent);
    return MemberComponent;
}());



/***/ }),

/***/ "./src/app/hr/payroll/payroll.component.html":
/*!***************************************************!*\
  !*** ./src/app/hr/payroll/payroll.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <div class=\"card\">\n          <div class=\"content\">\n            <h4 class=\"title\">{{\"payroll\"| translate |titlecase}} </h4>\n            <p class=\"category\">{{\"Your payroll here\"| translate |titlecase}}</p>\n            <br>\n            <div class=\"toolbar\">\n              <!--        Here you can write extra buttons/actions for the toolbar              -->\n            </div>\n            <div class=\"fresh-datatables\">\n              <table id=\"datatables\" class=\"table table-no-bordered table-hover text-center\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                <thead>\n                  <tr>\n                    <th hidden>{{ dataTable.headerRow[0] }}</th>\n                    <th>Ay</th>\n                    <th>SSK Isci</th>\n                    <th>Issizlik Isci</th>\n                    <th>Gelir Vergisi</th>\n                    <th>Damga Vergisi</th>\n                    <th>Kumulatif Matrah</th>\n                    <th>Brut</th>\n                    <th>AGI</th>\n                    <th>Ele Gecen</th>\n                    <th>SSK Isveren</th>\n                    <th>Issizlik Isveren</th>\n                    <th>Maliyet</th>\n                  \n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                    <td hidden>{{i+1}}</td>\n                    <td><b>{{row.MonthName}}</b></td>\n                    <td>{{row.A2}}</td>\n                    <td>{{row.A3}}</td>\n                    <td>{{row.A4}}</td>\n                    <td>{{row.A5}}</td>\n                    <td>{{row.A8}}</td>\n                    <td>{{row.A1}}</td>\n                    <td>{{row.A10}}</td>\n                    <td><b>{{row.A11}}</b></td>\n                    <td>{{row.A12}}</td>\n                    <td>{{row.A13}}</td>  \n                    <td>{{row.A14}}</td> \n                   \n                  </tr>\n                </tbody>\n              </table>\n            </div>\n          </div>\n          <!-- end content-->\n        </div>\n        <!--  end card  -->\n      </div>\n      <!-- end col-md-12 -->\n    </div>\n    <!-- end row -->\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/hr/payroll/payroll.component.scss":
/*!***************************************************!*\
  !*** ./src/app/hr/payroll/payroll.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hyL3BheXJvbGwvcGF5cm9sbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/hr/payroll/payroll.component.ts":
/*!*************************************************!*\
  !*** ./src/app/hr/payroll/payroll.component.ts ***!
  \*************************************************/
/*! exports provided: PayrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayrollComponent", function() { return PayrollComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PayrollComponent = /** @class */ (function () {
    function PayrollComponent(apiService, activeRoute) {
        this.apiService = apiService;
        this.activeRoute = activeRoute;
        this.payroll = [];
        this.ep = [];
        this.semployeeID = localStorage.getItem("employeeID");
        this.businessID = localStorage.getItem("businessID");
        this.userType = localStorage.getItem("userType");
        var routeParams = this.activeRoute.snapshot.params;
        this.employeeID = routeParams.id;
        if (this.userType == 1 || this.userType == 3 || this.employeeID == this.semployeeID) {
            this.getPayroll();
        }
    }
    PayrollComponent.prototype.ngOnInit = function () {
    };
    PayrollComponent.prototype.getPayroll = function () {
        var _this = this;
        var months = new Array(12);
        months[0] = "January";
        months[1] = "February";
        months[2] = "March";
        months[3] = "April";
        months[4] = "May";
        months[5] = "June";
        months[6] = "July";
        months[7] = "August";
        months[8] = "September";
        months[9] = "October";
        months[10] = "November";
        months[11] = "December";
        this.apiService.getData("0/" + this.employeeID, "members/").then(function (result) {
            _this.payroll = result[0];
            //console.log(this.payroll, 'payroll');
            var pi = _this.payroll.payrollInfo;
            var piArray = pi.split(";");
            var rowc = 0;
            var mc = 1;
            var eps = [];
            piArray.forEach(function (element) {
                rowc++;
                if (rowc == 1) {
                    eps = [];
                    eps["Month"] = mc;
                    eps["MonthName"] = months[mc - 1];
                    eps["Salary"] = _this.payroll.salary;
                }
                eps["A" + rowc] = element;
                if (rowc == 14) {
                    rowc = 0;
                    mc++;
                    _this.ep.push(eps);
                }
                console.log(element, 'element');
            });
            console.log(_this.ep);
            _this.ep[12]["MonthName"] = "Total";
            _this.dataTable = {
                headerRow: ['#', 'Net', 'SSK Isci', 'Issizlik Isci', 'Aylik Gelir Vergisi', 'Damga Vergisi', 'Kumulatif Vergi Matrahi', 'Brut', 'Asgari Gecim Indirimi', 'Toplam Ele Gecen', 'SSK Isveren', 'Issizlik Isveren', 'Toplam Maliyet'],
                footerRow: ['#', 'Net', 'SSK Isci', 'Issizlik Isci', 'Aylik Gelir Vergisi', 'Damga Vergisi', 'Kumulatif Vergi Matrahi', 'Brut', 'Asgari Gecim Indirimi', 'Toplam Ele Gecen', 'SSK Isveren', 'Issizlik Isveren', 'Toplam Maliyet'],
                dataRows: _this.ep
            };
            //console.log(this.dataTable);
        });
    };
    PayrollComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }
        });
    };
    PayrollComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-payroll',
            template: __webpack_require__(/*! ./payroll.component.html */ "./src/app/hr/payroll/payroll.component.html"),
            styles: [__webpack_require__(/*! ./payroll.component.scss */ "./src/app/hr/payroll/payroll.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], PayrollComponent);
    return PayrollComponent;
}());



/***/ }),

/***/ "./src/app/hr/team/team.component.html":
/*!*********************************************!*\
  !*** ./src/app/hr/team/team.component.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        \n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"content\">\n                          <h4 class=\"title\">{{\"team\"| translate |titlecase}} <span style='float:right;font-size:16px;'>\n                                  <a routerLink='/hr/member/0' style='color:gray;margin-right:10px;'><i class='fa fa-plus-square-o'></i></a>\n                          </span></h4>\n                          <p class=\"category\">{{\"Your team here\"| translate |titlecase}}</p>\n                          <br>\n                      <div class=\"toolbar\">\n                          <!--        Here you can write extra buttons/actions for the toolbar              -->\n                      </div>\n                      <div class=\"fresh-datatables\">\n                          <table id=\"datatablesIn\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                              <thead>\n                                  <tr>\n                                      <th>{{ dataTable.headerRow[0] }}</th>\n                                      <th class='text-left'>{{\"name\"| translate |titlecase}}</th>\n                                      <th class='text-left'>{{\"lastName\"| translate |titlecase}}</th>\n                                      <th class=\"text-left\">{{\"TCKN\"| translate |titlecase}}</th>\n                                      <th>{{\"Title\"| translate |titlecase}}</th>\n                                      <th>{{\"Department\"| translate |titlecase}}</th>\n                                      <th>{{\"cellPhone\"| translate |titlecase}}</th>\n                                      <th class=\"text-right\">{{\"Birthday\"| translate |titlecase}}</th>\n                                      <th class=\"text-center\">{{\"Salary\"| translate |titlecase}}</th>\n                                      <th style=\"width:10%;\" class=\"text-right\"></th>\n                                  </tr>\n                              </thead>\n                              <tbody>\n                                  <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                      <td>{{i+1}}</td>\n                                      <td class='text-left'>{{row.name}}</td>\n                                      <td class='text-left'>{{row.lastName}}</td>\n                                      <td class=\"text-left\">{{row.TCKN}}</td>\n                                      <td>{{row.title}}</td>\n                                      <td>{{row.department}}</td>\n                                      <td>{{row.cellPhone }}</td>\n                                      <td class='text-right'>{{row.birthday | date:'dd-MM-yyyy'}}</td>\n                                      <td class=\"text-center\">\n                                            <span *ngIf=\"row.userID==userID\">{{row.salary | currency:'TRY '}}</span>\n                                      </td>\n                                      <td style=\"width:10%;\" class=\"text-right tableicon\">\n                                        <a *ngIf=\"(row.userID==userID || userType==1 || userType==3)\" routerLink='/hr/payroll/{{row.ID}}'><i class=\"fa fa-bars\"> </i>&nbsp;</a>\n                                        <a *ngIf=\"row.userID==userID || userType==1 || userType==3\"  routerLink='/hr/member/{{row.ID}}'><i class=\"fa fa-search\"> </i>&nbsp;</a>\n                                      </td>\n                                  </tr>\n                              </tbody>\n                          </table>\n                      </div>\n                  </div>\n                  <!-- end content-->\n              </div>\n              <!--  end card  -->\n          </div>\n          <!-- end col-md-12 -->\n      </div>\n      <!-- end row -->\n  </div>\n  </div>\n  \n  "

/***/ }),

/***/ "./src/app/hr/team/team.component.scss":
/*!*********************************************!*\
  !*** ./src/app/hr/team/team.component.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hyL3RlYW0vdGVhbS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/hr/team/team.component.ts":
/*!*******************************************!*\
  !*** ./src/app/hr/team/team.component.ts ***!
  \*******************************************/
/*! exports provided: TeamComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamComponent", function() { return TeamComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TeamComponent = /** @class */ (function () {
    function TeamComponent(apiService, router, modalService) {
        this.apiService = apiService;
        this.router = router;
        this.modalService = modalService;
        this.members = [];
        this.userType = localStorage.getItem("userType");
        /*
        this.dataTable = {
          headerRow: ['#', 'Name', 'LastName', 'TCKN', 'Title', 'Department', 'cellPhone','Birthday','Salary'],
          footerRow: ['Row Number', 'Name', 'LastName', 'TCKN', 'Title', 'Department', 'cellPhone','Birthday','Salary'],
          dataRows: []
        };
        */
    }
    TeamComponent.prototype.ngOnInit = function () {
        this.businessID = localStorage.getItem("businessID");
        this.userID = localStorage.getItem("userID");
        this.getMembers(this.businessID + '/0');
    };
    TeamComponent.prototype.getMembers = function (employeeID) {
        var _this = this;
        this.apiService.getData(employeeID, "members/").then(function (result) {
            _this.members = result;
            _this.dataTable = {
                headerRow: ['#', 'Name', 'LastName', 'TCKN', 'Title', 'Department', 'cellPhone', 'Birthday', 'Salary'],
                footerRow: ['Row Number', 'Name', 'LastName', 'TCKN', 'Title', 'Department', 'cellPhone', 'Birthday', 'Salary'],
                dataRows: _this.members
            };
            //console.log(this.dataTable);
        });
    };
    /*
    callInvoice(invoiceType:any) {
      this.router.navigate(["/sales/invoice"],{ queryParams: { invoiceType: invoiceType } });
    }
  
  */
    TeamComponent.prototype.ngAfterViewInit = function () {
        $('#datatablesIn').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }
        });
    };
    TeamComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-team',
            template: __webpack_require__(/*! ./team.component.html */ "./src/app/hr/team/team.component.html"),
            styles: [__webpack_require__(/*! ./team.component.scss */ "./src/app/hr/team/team.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]])
    ], TeamComponent);
    return TeamComponent;
}());



/***/ })

}]);
//# sourceMappingURL=hr-hr-module.js.map