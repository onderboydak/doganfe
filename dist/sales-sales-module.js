(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sales-sales-module"],{

/***/ "./node_modules/ng2-currency-mask/index.js":
/*!*************************************************!*\
  !*** ./node_modules/ng2-currency-mask/index.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(/*! ./src/currency-mask.directive */ "./node_modules/ng2-currency-mask/src/currency-mask.directive.js"));
__export(__webpack_require__(/*! ./src/currency-mask.module */ "./node_modules/ng2-currency-mask/src/currency-mask.module.js"));
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/ng2-currency-mask/src/currency-mask.config.js":
/*!********************************************************************!*\
  !*** ./node_modules/ng2-currency-mask/src/currency-mask.config.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
exports.CURRENCY_MASK_CONFIG = new core_1.InjectionToken("currency.mask.config");
//# sourceMappingURL=currency-mask.config.js.map

/***/ }),

/***/ "./node_modules/ng2-currency-mask/src/currency-mask.directive.js":
/*!***********************************************************************!*\
  !*** ./node_modules/ng2-currency-mask/src/currency-mask.directive.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var currency_mask_config_1 = __webpack_require__(/*! ./currency-mask.config */ "./node_modules/ng2-currency-mask/src/currency-mask.config.js");
var input_handler_1 = __webpack_require__(/*! ./input.handler */ "./node_modules/ng2-currency-mask/src/input.handler.js");
exports.CURRENCYMASKDIRECTIVE_VALUE_ACCESSOR = {
    provide: forms_1.NG_VALUE_ACCESSOR,
    useExisting: core_1.forwardRef(function () { return CurrencyMaskDirective; }),
    multi: true
};
var CurrencyMaskDirective = (function () {
    function CurrencyMaskDirective(currencyMaskConfig, elementRef, keyValueDiffers) {
        this.currencyMaskConfig = currencyMaskConfig;
        this.elementRef = elementRef;
        this.keyValueDiffers = keyValueDiffers;
        this.options = {};
        this.optionsTemplate = {
            align: "right",
            allowNegative: true,
            decimal: ".",
            precision: 2,
            prefix: "$ ",
            suffix: "",
            thousands: ","
        };
        if (currencyMaskConfig) {
            this.optionsTemplate = currencyMaskConfig;
        }
        this.keyValueDiffer = keyValueDiffers.find({}).create();
    }
    CurrencyMaskDirective.prototype.ngAfterViewInit = function () {
        this.elementRef.nativeElement.style.textAlign = this.options.align ? this.options.align : this.optionsTemplate.align;
    };
    CurrencyMaskDirective.prototype.ngDoCheck = function () {
        if (this.keyValueDiffer.diff(this.options)) {
            this.elementRef.nativeElement.style.textAlign = this.options.align ? this.options.align : this.optionsTemplate.align;
            this.inputHandler.updateOptions(Object.assign({}, this.optionsTemplate, this.options));
        }
    };
    CurrencyMaskDirective.prototype.ngOnInit = function () {
        this.inputHandler = new input_handler_1.InputHandler(this.elementRef.nativeElement, Object.assign({}, this.optionsTemplate, this.options));
    };
    CurrencyMaskDirective.prototype.handleBlur = function (event) {
        this.inputHandler.getOnModelTouched().apply(event);
    };
    CurrencyMaskDirective.prototype.handleClick = function (event) {
        this.inputHandler.handleClick(event, this.isChromeAndroid());
    };
    CurrencyMaskDirective.prototype.handleCut = function (event) {
        if (!this.isChromeAndroid()) {
            this.inputHandler.handleCut(event);
        }
    };
    CurrencyMaskDirective.prototype.handleInput = function (event) {
        if (this.isChromeAndroid()) {
            this.inputHandler.handleInput(event);
        }
    };
    CurrencyMaskDirective.prototype.handleKeydown = function (event) {
        if (!this.isChromeAndroid()) {
            this.inputHandler.handleKeydown(event);
        }
    };
    CurrencyMaskDirective.prototype.handleKeypress = function (event) {
        if (!this.isChromeAndroid()) {
            this.inputHandler.handleKeypress(event);
        }
    };
    CurrencyMaskDirective.prototype.handleKeyup = function (event) {
        if (!this.isChromeAndroid()) {
            this.inputHandler.handleKeyup(event);
        }
    };
    CurrencyMaskDirective.prototype.handlePaste = function (event) {
        if (!this.isChromeAndroid()) {
            this.inputHandler.handlePaste(event);
        }
    };
    CurrencyMaskDirective.prototype.isChromeAndroid = function () {
        return /chrome/i.test(navigator.userAgent) && /android/i.test(navigator.userAgent);
    };
    CurrencyMaskDirective.prototype.registerOnChange = function (callbackFunction) {
        this.inputHandler.setOnModelChange(callbackFunction);
    };
    CurrencyMaskDirective.prototype.registerOnTouched = function (callbackFunction) {
        this.inputHandler.setOnModelTouched(callbackFunction);
    };
    CurrencyMaskDirective.prototype.setDisabledState = function (value) {
        this.elementRef.nativeElement.disabled = value;
    };
    CurrencyMaskDirective.prototype.validate = function (abstractControl) {
        var result = {};
        if (abstractControl.value > this.max) {
            result.max = true;
        }
        if (abstractControl.value < this.min) {
            result.min = true;
        }
        return result != {} ? result : null;
    };
    CurrencyMaskDirective.prototype.writeValue = function (value) {
        this.inputHandler.setValue(value);
    };
    return CurrencyMaskDirective;
}());
CurrencyMaskDirective.decorators = [
    { type: core_1.Directive, args: [{
                selector: "[currencyMask]",
                providers: [
                    exports.CURRENCYMASKDIRECTIVE_VALUE_ACCESSOR,
                    { provide: forms_1.NG_VALIDATORS, useExisting: CurrencyMaskDirective, multi: true }
                ]
            },] },
];
/** @nocollapse */
CurrencyMaskDirective.ctorParameters = function () { return [
    { type: undefined, decorators: [{ type: core_1.Optional }, { type: core_1.Inject, args: [currency_mask_config_1.CURRENCY_MASK_CONFIG,] },] },
    { type: core_1.ElementRef, },
    { type: core_1.KeyValueDiffers, },
]; };
CurrencyMaskDirective.propDecorators = {
    'max': [{ type: core_1.Input },],
    'min': [{ type: core_1.Input },],
    'options': [{ type: core_1.Input },],
    'handleBlur': [{ type: core_1.HostListener, args: ["blur", ["$event"],] },],
    'handleClick': [{ type: core_1.HostListener, args: ["click", ["$event"],] },],
    'handleCut': [{ type: core_1.HostListener, args: ["cut", ["$event"],] },],
    'handleInput': [{ type: core_1.HostListener, args: ["input", ["$event"],] },],
    'handleKeydown': [{ type: core_1.HostListener, args: ["keydown", ["$event"],] },],
    'handleKeypress': [{ type: core_1.HostListener, args: ["keypress", ["$event"],] },],
    'handleKeyup': [{ type: core_1.HostListener, args: ["keyup", ["$event"],] },],
    'handlePaste': [{ type: core_1.HostListener, args: ["paste", ["$event"],] },],
};
exports.CurrencyMaskDirective = CurrencyMaskDirective;
//# sourceMappingURL=currency-mask.directive.js.map

/***/ }),

/***/ "./node_modules/ng2-currency-mask/src/currency-mask.module.js":
/*!********************************************************************!*\
  !*** ./node_modules/ng2-currency-mask/src/currency-mask.module.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var currency_mask_directive_1 = __webpack_require__(/*! ./currency-mask.directive */ "./node_modules/ng2-currency-mask/src/currency-mask.directive.js");
var CurrencyMaskModule = (function () {
    function CurrencyMaskModule() {
    }
    return CurrencyMaskModule;
}());
CurrencyMaskModule.decorators = [
    { type: core_1.NgModule, args: [{
                imports: [
                    common_1.CommonModule,
                    forms_1.FormsModule
                ],
                declarations: [
                    currency_mask_directive_1.CurrencyMaskDirective
                ],
                exports: [
                    currency_mask_directive_1.CurrencyMaskDirective
                ]
            },] },
];
/** @nocollapse */
CurrencyMaskModule.ctorParameters = function () { return []; };
exports.CurrencyMaskModule = CurrencyMaskModule;
//# sourceMappingURL=currency-mask.module.js.map

/***/ }),

/***/ "./node_modules/ng2-currency-mask/src/input.handler.js":
/*!*************************************************************!*\
  !*** ./node_modules/ng2-currency-mask/src/input.handler.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var input_service_1 = __webpack_require__(/*! ./input.service */ "./node_modules/ng2-currency-mask/src/input.service.js");
var InputHandler = (function () {
    function InputHandler(htmlInputElement, options) {
        this.inputService = new input_service_1.InputService(htmlInputElement, options);
        this.htmlInputElement = htmlInputElement;
    }
    InputHandler.prototype.handleClick = function (event, chromeAndroid) {
        var selectionRangeLength = Math.abs(this.inputService.inputSelection.selectionEnd - this.inputService.inputSelection.selectionStart);
        //if there is no selection and the value is not null, the cursor position will be fixed. if the browser is chrome on android, the cursor will go to the end of the number.
        if (selectionRangeLength == 0 && !isNaN(this.inputService.value)) {
            this.inputService.fixCursorPosition(chromeAndroid);
        }
    };
    InputHandler.prototype.handleCut = function (event) {
        var _this = this;
        if (this.isReadOnly()) {
            return;
        }
        setTimeout(function () {
            _this.inputService.updateFieldValue();
            _this.setValue(_this.inputService.value);
            _this.onModelChange(_this.inputService.value);
        }, 0);
    };
    InputHandler.prototype.handleInput = function (event) {
        if (this.isReadOnly()) {
            return;
        }
        var keyCode = this.getNewKeyCode(this.inputService.storedRawValue, this.inputService.rawValue);
        var rawValueLength = this.inputService.rawValue.length;
        var rawValueSelectionEnd = this.inputService.inputSelection.selectionEnd;
        var rawValueWithoutSuffixEndPosition = this.inputService.getRawValueWithoutSuffixEndPosition();
        var storedRawValueLength = this.inputService.storedRawValue.length;
        this.inputService.rawValue = this.inputService.storedRawValue;
        if ((rawValueSelectionEnd != rawValueWithoutSuffixEndPosition || Math.abs(rawValueLength - storedRawValueLength) != 1) && storedRawValueLength != 0) {
            this.setCursorPosition(event);
            return;
        }
        if (rawValueLength < storedRawValueLength) {
            if (this.inputService.value != 0) {
                this.inputService.removeNumber(8);
            }
            else {
                this.setValue(null);
            }
        }
        if (rawValueLength > storedRawValueLength) {
            switch (keyCode) {
                case 43:
                    this.inputService.changeToPositive();
                    break;
                case 45:
                    this.inputService.changeToNegative();
                    break;
                default:
                    if (!this.inputService.canInputMoreNumbers || (isNaN(this.inputService.value) && String.fromCharCode(keyCode).match(/\d/) == null)) {
                        return;
                    }
                    this.inputService.addNumber(keyCode);
            }
        }
        this.setCursorPosition(event);
        this.onModelChange(this.inputService.value);
    };
    InputHandler.prototype.handleKeydown = function (event) {
        if (this.isReadOnly()) {
            return;
        }
        var keyCode = event.which || event.charCode || event.keyCode;
        if (keyCode == 8 || keyCode == 46 || keyCode == 63272) {
            event.preventDefault();
            var selectionRangeLength = Math.abs(this.inputService.inputSelection.selectionEnd - this.inputService.inputSelection.selectionStart);
            if (selectionRangeLength == this.inputService.rawValue.length || this.inputService.value == 0) {
                this.setValue(null);
                this.onModelChange(this.inputService.value);
            }
            if (selectionRangeLength == 0 && !isNaN(this.inputService.value)) {
                this.inputService.removeNumber(keyCode);
                this.onModelChange(this.inputService.value);
            }
            if ((keyCode === 8 || keyCode === 46) && selectionRangeLength != 0 && !isNaN(this.inputService.value)) {
                this.inputService.removeNumber(keyCode);
                this.onModelChange(this.inputService.value);
            }
        }
    };
    InputHandler.prototype.handleKeypress = function (event) {
        if (this.isReadOnly()) {
            return;
        }
        var keyCode = event.which || event.charCode || event.keyCode;
        if (keyCode == undefined || [9, 13].indexOf(keyCode) != -1 || this.isArrowEndHomeKeyInFirefox(event)) {
            return;
        }
        switch (keyCode) {
            case 43:
                this.inputService.changeToPositive();
                break;
            case 45:
                this.inputService.changeToNegative();
                break;
            default:
                if (this.inputService.canInputMoreNumbers && (!isNaN(this.inputService.value) || String.fromCharCode(keyCode).match(/\d/) != null)) {
                    this.inputService.addNumber(keyCode);
                }
        }
        event.preventDefault();
        this.onModelChange(this.inputService.value);
    };
    InputHandler.prototype.handleKeyup = function (event) {
        this.inputService.fixCursorPosition();
    };
    InputHandler.prototype.handlePaste = function (event) {
        var _this = this;
        if (this.isReadOnly()) {
            return;
        }
        setTimeout(function () {
            _this.inputService.updateFieldValue();
            _this.setValue(_this.inputService.value);
            _this.onModelChange(_this.inputService.value);
        }, 1);
    };
    InputHandler.prototype.updateOptions = function (options) {
        this.inputService.updateOptions(options);
    };
    InputHandler.prototype.getOnModelChange = function () {
        return this.onModelChange;
    };
    InputHandler.prototype.setOnModelChange = function (callbackFunction) {
        this.onModelChange = callbackFunction;
    };
    InputHandler.prototype.getOnModelTouched = function () {
        return this.onModelTouched;
    };
    InputHandler.prototype.setOnModelTouched = function (callbackFunction) {
        this.onModelTouched = callbackFunction;
    };
    InputHandler.prototype.setValue = function (value) {
        this.inputService.value = value;
    };
    InputHandler.prototype.getNewKeyCode = function (oldString, newString) {
        if (oldString.length > newString.length) {
            return null;
        }
        for (var x = 0; x < newString.length; x++) {
            if (oldString.length == x || oldString[x] != newString[x]) {
                return newString.charCodeAt(x);
            }
        }
    };
    InputHandler.prototype.isArrowEndHomeKeyInFirefox = function (event) {
        if ([35, 36, 37, 38, 39, 40].indexOf(event.keyCode) != -1 && (event.charCode == undefined || event.charCode == 0)) {
            return true;
        }
        return false;
    };
    InputHandler.prototype.isReadOnly = function () {
        return this.htmlInputElement && this.htmlInputElement.readOnly;
    };
    InputHandler.prototype.setCursorPosition = function (event) {
        var rawValueWithoutSuffixEndPosition = this.inputService.getRawValueWithoutSuffixEndPosition();
        setTimeout(function () {
            event.target.setSelectionRange(rawValueWithoutSuffixEndPosition, rawValueWithoutSuffixEndPosition);
        }, 0);
    };
    return InputHandler;
}());
exports.InputHandler = InputHandler;
//# sourceMappingURL=input.handler.js.map

/***/ }),

/***/ "./node_modules/ng2-currency-mask/src/input.manager.js":
/*!*************************************************************!*\
  !*** ./node_modules/ng2-currency-mask/src/input.manager.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var InputManager = (function () {
    function InputManager(htmlInputElement) {
        this.htmlInputElement = htmlInputElement;
    }
    InputManager.prototype.setCursorAt = function (position) {
        if (this.htmlInputElement.setSelectionRange) {
            this.htmlInputElement.focus();
            this.htmlInputElement.setSelectionRange(position, position);
        }
        else if (this.htmlInputElement.createTextRange) {
            var textRange = this.htmlInputElement.createTextRange();
            textRange.collapse(true);
            textRange.moveEnd("character", position);
            textRange.moveStart("character", position);
            textRange.select();
        }
    };
    InputManager.prototype.updateValueAndCursor = function (newRawValue, oldLength, selectionStart) {
        this.rawValue = newRawValue;
        var newLength = newRawValue.length;
        selectionStart = selectionStart - (oldLength - newLength);
        this.setCursorAt(selectionStart);
    };
    Object.defineProperty(InputManager.prototype, "canInputMoreNumbers", {
        get: function () {
            var haventReachedMaxLength = !(this.rawValue.length >= this.htmlInputElement.maxLength && this.htmlInputElement.maxLength >= 0);
            var selectionStart = this.inputSelection.selectionStart;
            var selectionEnd = this.inputSelection.selectionEnd;
            var haveNumberSelected = (selectionStart != selectionEnd && this.htmlInputElement.value.substring(selectionStart, selectionEnd).match(/\d/)) ? true : false;
            var startWithZero = (this.htmlInputElement.value.substring(0, 1) == "0");
            return haventReachedMaxLength || haveNumberSelected || startWithZero;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputManager.prototype, "inputSelection", {
        get: function () {
            var selectionStart = 0;
            var selectionEnd = 0;
            if (typeof this.htmlInputElement.selectionStart == "number" && typeof this.htmlInputElement.selectionEnd == "number") {
                selectionStart = this.htmlInputElement.selectionStart;
                selectionEnd = this.htmlInputElement.selectionEnd;
            }
            else {
                var range = document.getSelection().baseNode;
                if (range && range.firstChild == this.htmlInputElement) {
                    var lenght = this.htmlInputElement.value.length;
                    var normalizedValue = this.htmlInputElement.value.replace(/\r\n/g, "\n");
                    var startRange = this.htmlInputElement.createTextRange();
                    var endRange = this.htmlInputElement.createTextRange();
                    endRange.collapse(false);
                    if (startRange.compareEndPoints("StartToEnd", endRange) > -1) {
                        selectionStart = selectionEnd = lenght;
                    }
                    else {
                        selectionStart = -startRange.moveStart("character", -lenght);
                        selectionStart += normalizedValue.slice(0, selectionStart).split("\n").length - 1;
                        if (startRange.compareEndPoints("EndToEnd", endRange) > -1) {
                            selectionEnd = lenght;
                        }
                        else {
                            selectionEnd = -startRange.moveEnd("character", -lenght);
                            selectionEnd += normalizedValue.slice(0, selectionEnd).split("\n").length - 1;
                        }
                    }
                }
            }
            return {
                selectionStart: selectionStart,
                selectionEnd: selectionEnd
            };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputManager.prototype, "rawValue", {
        get: function () {
            return this.htmlInputElement && this.htmlInputElement.value;
        },
        set: function (value) {
            this._storedRawValue = value;
            if (this.htmlInputElement) {
                this.htmlInputElement.value = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputManager.prototype, "storedRawValue", {
        get: function () {
            return this._storedRawValue;
        },
        enumerable: true,
        configurable: true
    });
    return InputManager;
}());
exports.InputManager = InputManager;
//# sourceMappingURL=input.manager.js.map

/***/ }),

/***/ "./node_modules/ng2-currency-mask/src/input.service.js":
/*!*************************************************************!*\
  !*** ./node_modules/ng2-currency-mask/src/input.service.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var input_manager_1 = __webpack_require__(/*! ./input.manager */ "./node_modules/ng2-currency-mask/src/input.manager.js");
var InputService = (function () {
    function InputService(htmlInputElement, options) {
        this.htmlInputElement = htmlInputElement;
        this.options = options;
        this.inputManager = new input_manager_1.InputManager(htmlInputElement);
    }
    InputService.prototype.addNumber = function (keyCode) {
        if (!this.rawValue) {
            this.rawValue = this.applyMask(false, "0");
        }
        var keyChar = String.fromCharCode(keyCode);
        var selectionStart = this.inputSelection.selectionStart;
        var selectionEnd = this.inputSelection.selectionEnd;
        this.rawValue = this.rawValue.substring(0, selectionStart) + keyChar + this.rawValue.substring(selectionEnd, this.rawValue.length);
        this.updateFieldValue(selectionStart + 1);
    };
    InputService.prototype.applyMask = function (isNumber, rawValue) {
        var _a = this.options, allowNegative = _a.allowNegative, decimal = _a.decimal, precision = _a.precision, prefix = _a.prefix, suffix = _a.suffix, thousands = _a.thousands;
        rawValue = isNumber ? new Number(rawValue).toFixed(precision) : rawValue;
        var onlyNumbers = rawValue.replace(/[^0-9]/g, "");
        if (!onlyNumbers) {
            return "";
        }
        var integerPart = onlyNumbers.slice(0, onlyNumbers.length - precision).replace(/^0*/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, thousands);
        if (integerPart == "") {
            integerPart = "0";
        }
        var newRawValue = integerPart;
        var decimalPart = onlyNumbers.slice(onlyNumbers.length - precision);
        if (precision > 0) {
            decimalPart = "0".repeat(precision - decimalPart.length) + decimalPart;
            newRawValue += decimal + decimalPart;
        }
        var isZero = parseInt(integerPart) == 0 && (parseInt(decimalPart) == 0 || decimalPart == "");
        var operator = (rawValue.indexOf("-") > -1 && allowNegative && !isZero) ? "-" : "";
        return operator + prefix + newRawValue + suffix;
    };
    InputService.prototype.clearMask = function (rawValue) {
        if (rawValue == null || rawValue == "") {
            return null;
        }
        var value = rawValue.replace(this.options.prefix, "").replace(this.options.suffix, "");
        if (this.options.thousands) {
            value = value.replace(new RegExp("\\" + this.options.thousands, "g"), "");
        }
        if (this.options.decimal) {
            value = value.replace(this.options.decimal, ".");
        }
        return parseFloat(value);
    };
    InputService.prototype.changeToNegative = function () {
        if (this.options.allowNegative && this.rawValue != "" && this.rawValue.charAt(0) != "-" && this.value != 0) {
            var selectionStart = this.inputSelection.selectionStart;
            this.rawValue = "-" + this.rawValue;
            this.updateFieldValue(selectionStart + 1);
        }
    };
    InputService.prototype.changeToPositive = function () {
        var selectionStart = this.inputSelection.selectionStart;
        this.rawValue = this.rawValue.replace("-", "");
        this.updateFieldValue(selectionStart - 1);
    };
    InputService.prototype.fixCursorPosition = function (forceToEndPosition) {
        var currentCursorPosition = this.inputSelection.selectionStart;
        //if the current cursor position is after the number end position, it is moved to the end of the number, ignoring the prefix or suffix. this behavior can be forced with forceToEndPosition flag
        if (currentCursorPosition > this.getRawValueWithoutSuffixEndPosition() || forceToEndPosition) {
            this.inputManager.setCursorAt(this.getRawValueWithoutSuffixEndPosition());
            //if the current cursor position is before the number start position, it is moved to the start of the number, ignoring the prefix or suffix
        }
        else if (currentCursorPosition < this.getRawValueWithoutPrefixStartPosition()) {
            this.inputManager.setCursorAt(this.getRawValueWithoutPrefixStartPosition());
        }
    };
    InputService.prototype.getRawValueWithoutSuffixEndPosition = function () {
        return this.rawValue.length - this.options.suffix.length;
    };
    InputService.prototype.getRawValueWithoutPrefixStartPosition = function () {
        return this.value != null && this.value < 0 ? this.options.prefix.length + 1 : this.options.prefix.length;
    };
    InputService.prototype.removeNumber = function (keyCode) {
        var _a = this.options, decimal = _a.decimal, thousands = _a.thousands;
        var selectionEnd = this.inputSelection.selectionEnd;
        var selectionStart = this.inputSelection.selectionStart;
        if (selectionStart > this.rawValue.length - this.options.suffix.length) {
            selectionEnd = this.rawValue.length - this.options.suffix.length;
            selectionStart = this.rawValue.length - this.options.suffix.length;
        }
        //there is no selection
        if (selectionEnd == selectionStart) {
            //delete key and the target digit is a number
            if ((keyCode == 46 || keyCode == 63272) && /^\d+$/.test(this.rawValue.substring(selectionStart, selectionEnd + 1))) {
                selectionEnd = selectionEnd + 1;
            }
            //delete key and the target digit is the decimal or thousands divider
            if ((keyCode == 46 || keyCode == 63272) && (this.rawValue.substring(selectionStart, selectionEnd + 1) == decimal || this.rawValue.substring(selectionStart, selectionEnd + 1) == thousands)) {
                selectionEnd = selectionEnd + 2;
                selectionStart = selectionStart + 1;
            }
            //backspace key and the target digit is a number
            if (keyCode == 8 && /^\d+$/.test(this.rawValue.substring(selectionStart - 1, selectionEnd))) {
                selectionStart = selectionStart - 1;
            }
            //backspace key and the target digit is the decimal or thousands divider
            if (keyCode == 8 && (this.rawValue.substring(selectionStart - 1, selectionEnd) == decimal || this.rawValue.substring(selectionStart - 1, selectionEnd) == thousands)) {
                selectionStart = selectionStart - 2;
                selectionEnd = selectionEnd - 1;
            }
        }
        this.rawValue = this.rawValue.substring(0, selectionStart) + this.rawValue.substring(selectionEnd, this.rawValue.length);
        this.updateFieldValue(selectionStart);
    };
    InputService.prototype.updateFieldValue = function (selectionStart) {
        var newRawValue = this.applyMask(false, this.rawValue || "");
        selectionStart = selectionStart == undefined ? this.rawValue.length : selectionStart;
        this.inputManager.updateValueAndCursor(newRawValue, this.rawValue.length, selectionStart);
    };
    InputService.prototype.updateOptions = function (options) {
        var value = this.value;
        this.options = options;
        this.value = value;
    };
    Object.defineProperty(InputService.prototype, "canInputMoreNumbers", {
        get: function () {
            return this.inputManager.canInputMoreNumbers;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputService.prototype, "inputSelection", {
        get: function () {
            return this.inputManager.inputSelection;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputService.prototype, "rawValue", {
        get: function () {
            return this.inputManager.rawValue;
        },
        set: function (value) {
            this.inputManager.rawValue = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputService.prototype, "storedRawValue", {
        get: function () {
            return this.inputManager.storedRawValue;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InputService.prototype, "value", {
        get: function () {
            return this.clearMask(this.rawValue);
        },
        set: function (value) {
            this.rawValue = this.applyMask(true, "" + value);
        },
        enumerable: true,
        configurable: true
    });
    return InputService;
}());
exports.InputService = InputService;
//# sourceMappingURL=input.service.js.map

/***/ }),

/***/ "./src/app/sales/customer/customer.component.html":
/*!********************************************************!*\
  !*** ./src/app/sales/customer/customer.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div *ngIf=\"customer.ID!=0\" class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card \">\n                    <div class=\"header\">\n                        <h4 class=\"title\">{{\"Customer Summary\"| translate |titlecase}}\n                            <!-- Burada eger tedarikci ise Customer>Vendor olacak -->\n                            <span style='float:right;'><a data-target=\"#collapseOne\" data-toggle=\"collapse\"><i style='color:gray;margin-right:10px;'\n                                        class=\"fa fa-ellipsis-h\"> </i>&nbsp;</a></span>\n                        </h4>\n                        <p class=\"category\"></p>\n                    </div>\n                    <div class=\"content\">\n                        <div class=\"panel-group\" id=\"accordion\">\n                            <div class=\"panel panel-default\">\n                                <div id=\"collapseOne\" class=\"panel-collapse collapse\">\n\n                                    <div class=\"panel-body\">\n                                        <div class=\"col-md-6\">\n                                            Graph\n                                        </div>\n                                        <div class=\"col-md-6\">\n                                            <table class=\"table table-hover\">\n                                                <thead>\n                                                    <tr>\n                                                        <th></th>\n                                                        <th>3 Months Ago</th>\n                                                        <th>2 Months Ago</th>\n                                                        <th>Last Month</th>\n                                                        <th>Cumulative</th>\n                                                    </tr>\n                                                </thead>\n                                                <tbody>\n                                                    <tr *ngFor=\"let ci of customerInvoices\">\n                                                        <td>{{ci.title}}</td>\n                                                        <td style='text-align: center;'>{{ci.M3}}</td>\n                                                        <td style='text-align: center;'>{{ci.M2}}</td>\n                                                        <td style='text-align: center;'>{{ci.M1}}</td>\n                                                        <td style='text-align: center;'>{{ci.M0}}</td>\n                                                    </tr>\n                                                </tbody>\n                                            </table>\n                                        </div>\n\n\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <form class=\"form-horizontal\" [formGroup]=\"customerForm\" (ngSubmit)=\"saveCustomer()\">\n                    <h4 class=\"title\">New/Edit Customer</h4>\n                    <div class=\"card\">\n                        <!-- mains will be here -->\n                        <div class=\"header\">\n                            <legend>{{\"Customer Card\" | translate}}</legend>\n                        </div>\n                        <div class=\"content\">\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Short Name</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" formControlName=\"customerShortName\" \n                                            ngClass=\"{{f.customerShortName.invalid ? 'error' : ''}}\"\n                                            [(ngModel)]=\"customer.customerShortName\" >\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Official Name</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" formControlName=\"customer\" \n                                            ngClass=\"{{f.customer.invalid ? 'error' : ''}}\"\n                                            [(ngModel)]=\"customer.customer\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Payment Terms</label>\n                                    <div class=\"col-sm-10\">\n                                        <select formControlName=\"dueDateOptions\" [(ngModel)]=\"customer.dueDateOptions\" class=\"selectpicker selectdueDateOptions\"\n                                            data-title=\"\" data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                            <option [value]=\"1\">Same Day</option>\n                                            <option [value]=\"2\">7</option>\n                                            <option [value]=\"3\">14</option>\n                                            <option [value]=\"4\">30</option>\n                                            <option [value]=\"5\">60</option>\n                                            <option [value]=\"6\">90</option>\n                                            <option [value]=\"7\">180</option>\n                                            <option [value]=\"8\">360</option>\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Type</label>\n                                    <div class=\"col-sm-10\">\n                                        <div class=\"radio\">\n                                            <input type=\"radio\" #radioCorp name=\"radio\" id=\"radioCorp\" value=\"1\" (change)=\"onCtChange(1)\" \n                                                [checked]=\"customer.customerType==1 || customer.ID==0\">\n                                            <label for=\"radioCorp\">\n                                                Corporate\n                                            </label>\n                                        </div>\n                                        <div class=\"radio\">\n                                            <input type=\"radio\" #radioInd name=\"radio\" id=\"radioInd\" value=\"2\" (change)=\"onCtChange(2)\"\n                                                [checked]=\"customer.customerType==2\">\n                                            <label for=\"radioInd\">\n                                                Individual\n                                            </label>\n                                        </div>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Customer and Supplier</label>\n                                    <div class=\"col-sm-10 checkbox\" style=\"margin-top:5px;\">    \n                                        <input id=\"isVendor2\" type=\"checkbox\"  name=\"isVendor2\" [checked]=\"isVendor2\"> \n                                        <label for=\"isVendor2\">\n\n                                        </label>\n                                    </div>\n                                </div>   \n                            </fieldset>\n                            <fieldset>\n                                <!--Burası eger radıo 1 secılırse cıkacak-->\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Tax Authority</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" formControlName=\"vatAuthority\" \n                                            ngClass=\"{{f.vatAuthority.invalid ? 'error' : ''}}\"\n                                            [(ngModel)]=\"customer.vatAuthority\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <!--Burası eger radıo 1 secılırse cıkacak-->\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Tax Number</label>\n                                    <div class=\"col-sm-10\"> \n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" formControlName=\"vatNumber\" \n                                            ngClass=\"{{f.vatNumber.invalid ? 'error' : ''}}\"\n                                            [(ngModel)]=\"customer.vatNumber\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <!--Burası eger radıo 1 secılırse cıkacak-->\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Commerce Authority Number</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" [(ngModel)]=\"customer.commerceAuthorityNo\"  formControlName=\"commerceAuthorityNo\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                        </div>\n                        <div class=\"header\">\n                            <legend></legend>\n\n                        </div>\n                        <br>\n                        <div class=\"content\">\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Building No</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" [(ngModel)]=\"customer.buildingNumber\" formControlName=\"buildingNumber\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Address</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" formControlName=\"address\" \n                                             [(ngModel)]=\"customer.address\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">District</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\"  formControlName=\"district\" [(ngModel)]=\"customer.district\" ngClass=\"{{f.district.invalid ? 'error' : ''}}\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">City</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" formControlName=\"city\" ngClass=\"{{f.city.invalid ? 'error' : ''}}\"\n                                             [(ngModel)]=\"customer.city\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Country</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" formControlName=\"country\" ngClass=\"{{f.country.invalid ? 'error' : ''}}\"\n                                             [(ngModel)]=\"customer.country\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">\n                                        <i class='fa fa-envelope-square'></i>\n                                        Email</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" formControlName=\"email\" \n                                            [(ngModel)]=\"customer.email\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">\n                                        <i class='fa fa-phone-square'></i>\n                                        Phone Number</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\"  formControlName=\"phone\"\n                                            [(ngModel)]=\"customer.phone\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">URN</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\"  formControlName=\"pbox\"\n                                             [(ngModel)]=\"customer.pbox\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                        </div>\n                        <div class=\"header\">\n                            <legend>Contact</legend>\n                        </div>\n                        <br>\n                        <div class=\"content\">\n                            <!-- detail will be here -->\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">First/Last Name</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" formControlName=\"customerContact\" \n                                             [(ngModel)]=\"customer.customerContact\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">\n                                        <i class='fa fa-envelope-square'></i>\n                                        Email</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" formControlName=\"customerContactEmail\" ngClass=\"{{f.customerContactEmail.invalid ? 'error' : ''}}\"\n                                            [(ngModel)]=\"customer.customerContactEmail\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                        </div>\n                        <br>\n\n                    </div> <!-- end card -->\n\n                    <div class=\"content\">\n                        <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"deleteCustomer()\">DELETE</button>\n\n                        <div style='float:right;'>\n                            <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                            <button type=\"submit\" class=\"btn btn-success btn-fill btn-wd\" >SAVE</button>\n                        </div>\n                    </div>\n                </form>\n            </div> <!-- end col-md-12 -->\n        </div> <!-- end row -->\n\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/sales/customer/customer.component.scss":
/*!********************************************************!*\
  !*** ./src/app/sales/customer/customer.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NhbGVzL2N1c3RvbWVyL2N1c3RvbWVyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/sales/customer/customer.component.ts":
/*!******************************************************!*\
  !*** ./src/app/sales/customer/customer.component.ts ***!
  \******************************************************/
/*! exports provided: CustomerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerComponent", function() { return CustomerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CustomerComponent = /** @class */ (function () {
    function CustomerComponent(apiService, activeRoute, router, fb) {
        var _this = this;
        this.apiService = apiService;
        this.activeRoute = activeRoute;
        this.router = router;
        this.fb = fb;
        this.customer = [];
        this.customerInvoices = [];
        this.isVendor2 = false;
        var routeParams = this.activeRoute.snapshot.params;
        this.customerID = routeParams.id;
        this.activeRoute.queryParams
            .subscribe(function (params) {
            // Defaults to 0 if no query param provided.
            _this.isVendor = +params['isVendor'] || 0;
        });
        console.log(this.customerID, "customerID");
        this.businessID = localStorage.getItem("businessID");
        this.customer = { "ID": 0, "address": "", "buildingNumber": "", "city": "", "commerceAuthorityNo": "", "isVendor": this.isVendor, "asID": this.businessID,
            "country": "", "customer": "", "customerContact": "", "customerContactEmail": "", "customerShortName": "",
            "customerType": 1, "district": "", "dueDateOptions": null, "email": "", "phone": "", "vatAuthority": "", "vatNumber": "", "pbox": "" };
        // form validation
        this.customerForm = this.fb.group({
            customerShortName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            customer: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            dueDateOptions: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            vatAuthority: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            vatNumber: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            commerceAuthorityNo: [],
            buildingNumber: [],
            address: [],
            pbox: [],
            district: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            country: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            email: [],
            phone: [],
            customerContact: [],
            customerContactEmail: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    }
    Object.defineProperty(CustomerComponent.prototype, "f", {
        get: function () {
            return this.customerForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    CustomerComponent.prototype.getCustomerInvoices = function () {
        var _this = this;
        this.apiService.getData(this.customer.asID + "/" + this.customerID, "invoiceCustomerSummary/").then(function (result) {
            _this.customerInvoices = result;
            var m3Total = _this.customerInvoices[0].M3 * 1 - _this.customerInvoices[1].M3 * 1;
            var m2Total = _this.customerInvoices[0].M2 * 1 - _this.customerInvoices[1].M2 * 1;
            var m1Total = _this.customerInvoices[0].M1 * 1 - _this.customerInvoices[1].M1 * 1;
            var m0Total = _this.customerInvoices[0].M0 * 1 - _this.customerInvoices[1].M0 * 1;
            _this.customerInvoices[2].M3 = m3Total.toFixed(2);
            _this.customerInvoices[2].M2 = m2Total.toFixed(2);
            _this.customerInvoices[2].M1 = m1Total.toFixed(2);
            _this.customerInvoices[2].M0 = m0Total.toFixed(2);
        });
    };
    CustomerComponent.prototype.onCtChange = function (ct) {
        this.customer.customerType = ct;
    };
    CustomerComponent.prototype.ngOnInit = function () {
        if (this.customerID > 0) {
            this.getCustomer(this.businessID + "/0/" + this.customerID);
        }
        else {
            $(".selectdueDateOptions").selectpicker();
        }
        this.getCustomerInvoices();
    };
    CustomerComponent.prototype.getCustomer = function (customerID) {
        var _this = this;
        if (this.customerID > 0) {
            this.apiService.getData(customerID, "customers/").then(function (result) {
                _this.customer = result[0];
                console.log(_this.customer, "customer");
                _this.isVendor2 = (_this.customer.isVendor == 3) ? true : false;
                console.log(_this.isVendor2);
                $(".selectdueDateOptions").selectpicker('val', _this.customer.dueDateOptions);
            });
        }
        else {
            this.customer.ID = 0;
        }
    };
    CustomerComponent.prototype.saveCustomer = function () {
        var _this = this;
        console.log(this.customerForm.invalid);
        if (this.customerForm.invalid) {
            return;
        }
        // console.log(this.isVendor2,"isVendor2"); 
        // console.log($("#isVendor2").val(),"isVendor2");
        this.isVendor2 = $("#isVendor2").prop('checked');
        console.log(this.isVendor2, "isVendor2");
        if (this.isVendor2 === true) {
            this.customer.isVendor = 3;
        }
        else {
            this.customer.isVendor = this.isVendor;
        }
        this.apiService.postData(this.customer.ID, "/customer/save/", JSON.stringify(this.customer)).then(function (result) {
            console.log(result);
            if (_this.customer.isVendor == 1 || _this.customer.isVendor == 3) {
                _this.router.navigateByUrl("/sales/customers");
            }
            else {
                _this.router.navigateByUrl("/costs/suppliers");
            }
        });
    };
    CustomerComponent.prototype.deleteCustomer = function () {
        var _this = this;
        this.apiService.deleteData(this.customer.ID, "/customer/delete/").then(function (result) {
            if (_this.customer.isVendor == 1) {
                _this.router.navigateByUrl("/sales/customers");
            }
            else {
                _this.router.navigateByUrl("/costs/suppliers");
            }
        });
    };
    CustomerComponent.prototype.clickCancel = function () {
        if (this.customer.isVendor == 1) {
            this.router.navigateByUrl("/sales/customers");
        }
        else {
            this.router.navigateByUrl("/costs/suppliers");
        }
    };
    CustomerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-customer',
            template: __webpack_require__(/*! ./customer.component.html */ "./src/app/sales/customer/customer.component.html"),
            styles: [__webpack_require__(/*! ./customer.component.scss */ "./src/app/sales/customer/customer.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], CustomerComponent);
    return CustomerComponent;
}());



/***/ }),

/***/ "./src/app/sales/customers/customers.component.html":
/*!**********************************************************!*\
  !*** ./src/app/sales/customers/customers.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"content\">\n                        <h4 class=\"title\">{{\"customers\" | translate |titlecase}}\n                            <span style='float:right;'>\n                                <a routerLink='/sales/customer' [queryParams]=\"{isVendor:1}\" style='color:gray;margin-right:10px;font-size:16px;'><i class='fa fa-plus-square-o'></i></a>\n                                <a routerLink='/finance/aggr' [queryParams]=\"{customerID: 0}\" style='color:gray;margin-right:10px;font-size:16px;'><i class=\"fa fa-handshake-o\"></i></a>\n        \n                            </span>\n                        </h4>\n                        <p class=\"category\">{{\"These are your customers\" | translate |titlecase}}</p>\n                        <!-- <legend></legend> -->\n                        <br>\n                        <div class=\"toolbar\">\n\n                            <!--        Here you can write extra buttons/actions for the toolbar              -->\n                        </div>\n                        <div class=\"fresh-datatables\">\n                            <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\"\n                                style=\"width:100%\">\n                                <thead>\n                                    <tr>\n                                        <th>#</th>\n                                        <th>{{ 'Name' | translate }}</th>\n                                        <th>{{ 'Country' | translate }}</th>\n                                        <th>{{ 'Phone' | translate }}</th>\n                                        <th>{{ 'Contact' | translate }}</th>\n                                        <th class='text-right'>{{ 'Type' | translate }}</th>\n                                        <th class='text-right'></th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                        <td><span class=\"dot {{row.color}}\"></span></td>\n                                        <td><a routerLink='/sales/customer/{{row.ID}}' [queryParams]=\"{isVendor: 1}\">{{row.customerShortName}}</a></td>\n                                        <td>{{row.country}}</td>\n                                        <td>{{row.phone}}</td>\n                                        <td>{{row.customerContact}}</td>\n                                        <td class='text-right'><button class=\"btn btn-xs btn-fill\">{{row.cType | uppercase}}</button></td>\n                                        <td class=\"text-right tableicon\">\n                                            <a href='mailto:{{row.customerContactEmail}}'><i class=\"fa fa-envelope-o\"></i>&nbsp;</a>\n                                            <a routerLink='/finance/aggr' [queryParams]=\"{customerID: row.ID}\"><i class=\"fa fa-handshake-o\"></i>&nbsp;</a>\n                                            <a title=\"Create Invoice\" routerLink=\"/sales/invoice\" [queryParams]=\"{invoiceType:1,customerID:row.ID}\"><i\n                                                    class=\"fa fa-file-text-o\"></i>&nbsp;</a>\n                                            <a routerLink='/sales/customer/{{row.ID}}'><i class=\"fa fa-search\"></i>&nbsp;</a>\n                                        </td>\n                                    </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                    <!-- end content-->\n                </div>\n                <!--  end card  -->\n            </div>\n            <!-- end col-md-12 -->\n        </div>\n        <!-- end row -->\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/sales/customers/customers.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/sales/customers/customers.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NhbGVzL2N1c3RvbWVycy9jdXN0b21lcnMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/sales/customers/customers.component.ts":
/*!********************************************************!*\
  !*** ./src/app/sales/customers/customers.component.ts ***!
  \********************************************************/
/*! exports provided: CustomersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersComponent", function() { return CustomersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CustomersComponent = /** @class */ (function () {
    function CustomersComponent(apiService) {
        /*
          this.dataTable = {
              headerRow: ['#', 'Name', 'Country', 'Type'],
              footerRow: ['#', 'Name', 'Country', 'Type'],
              dataRows: []
          };
      */
        this.apiService = apiService;
        this.customers = [];
    }
    CustomersComponent.prototype.ngOnInit = function () {
        this.businessID = localStorage.getItem("businessID");
        this.getCustomers(this.businessID + "/1/0");
    };
    CustomersComponent.prototype.getCustomers = function (customerID) {
        var _this = this;
        this.apiService.getData(customerID, "customers/").then(function (result) {
            _this.customers = result;
            _this.dataTable = {
                headerRow: ['#', 'Name', 'Country', 'Type'],
                footerRow: ['#', 'Name', 'Country', 'Type'],
                dataRows: _this.customers
            };
            console.log(_this.dataTable);
        });
    };
    CustomersComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }
        });
    };
    CustomersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-customers',
            template: __webpack_require__(/*! ./customers.component.html */ "./src/app/sales/customers/customers.component.html"),
            styles: [__webpack_require__(/*! ./customers.component.scss */ "./src/app/sales/customers/customers.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], CustomersComponent);
    return CustomersComponent;
}());



/***/ }),

/***/ "./src/app/sales/invoice/invoice.component.html":
/*!******************************************************!*\
  !*** ./src/app/sales/invoice/invoice.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <form method=\"post\" class=\"form-horizontal\">\n                    <h4 class=\"title\">New/Edit Invoice</h4>\n                    <div class=\"card\">\n                        <!-- mains will be here -->\n                        <div class=\"header\">\n                            <legend>Invoice Card\n                                <span style='float:right;'>\n                                    <span\n                                        class=\"btn btn-xs btn-primary\">{{((invoiceType==1) ? \"outgoing\" : \"incoming\" )| uppercase}}</span>\n                                    {{invoice.invoiceNumber}}\n\n                                </span>\n\n                            </legend>\n                        </div>\n                        <div class=\"content\">\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Is Proforma?</label>\n                                    <div class=\"col-sm-10 checkbox\" style=\"padding-top: 10px;\">\n                                        <input id=\"isProforma\" type=\"checkbox\" [(ngModel)]=\"invoice.isProforma\"\n                                            name=\"isProforma\" [disabled]=\"invoice.ID>0\">\n                                        <label for=\"isProforma\">\n\n                                        </label>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">PO Number</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"Po Number\" class=\"form-control\"\n                                            [(ngModel)]=\"invoice.po\" name=\"po\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Description</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"Description\" class=\"form-control\"\n                                            [(ngModel)]=\"invoice.description\" name=\"description\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset *ngIf=\"invoiceType==2 || invoice.invoiceType==2\">\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Invoice Number</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"Invoice Number\" class=\"form-control\"\n                                            [(ngModel)]=\"invoice.invoiceNumber\" name=\"invoiceNumber\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Customer</label>\n                                    <div class=\"col-sm-10\">\n                                        <select name=\"customerID\" [(ngModel)]=\"invoice.customerID\"\n                                            class=\"selectpicker selectCustomer\"\n                                            (change)=\"getCustomer(invoice.customerID)\" data-title=\"Select To\"\n                                            data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                            <option *ngFor=\"let customer of customers\" [value]=\"customer.ID\">\n                                                {{customer.customer}}</option>\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Invoice Date</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" [(ngModel)]=\"invoice.invoiceDate\" name=\"invoiceDate\"\n                                            class=\"form-control datetimepicker\" placeholder=\"Pick Date Time\" />\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Payment Due</label>\n                                    <div class=\"col-sm-10\">\n                                        <select name=\"dueDateOptions\" [(ngModel)]=\"invoice.dueDateOptions\"\n                                            class=\"selectpicker selectdueDateOptions\" data-title=\"Payment Due\"\n                                            data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                            <option selected [value]=\"1\">Same Day</option>\n                                            <option selected [value]=\"2\">7</option>\n                                            <option selected [value]=\"3\">14</option>\n                                            <option selected [value]=\"4\">30</option>\n                                            <option selected [value]=\"5\">60</option>\n                                            <option selected [value]=\"6\">90</option>\n                                            <option selected [value]=\"7\">180</option>\n                                            <option selected [value]=\"8\">360</option>\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Currency</label>\n                                    <div class=\"col-sm-10\">\n                                        <select name=\"invoiceCurrency\" [(ngModel)]=\"invoice.invoiceCurrency\"\n                                            (change)=\"changeCurrency($event)\" class=\"selectpicker selectCurrency\"\n                                            data-title=\"Currency\" data-style=\"btn-default btn-block\"\n                                            data-menu-style=\"dropdown-blue\">\n                                            <option [value]=\"1\">TRY</option>\n                                            <option [value]=\"2\">USD</option>\n                                            <option [value]=\"3\">EUR</option>\n                                            <option [value]=\"4\">GBP</option>\n\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Invoice Type</label>\n                                    <div class=\"col-sm-2\">\n                                        <select name=\"invoiceClass\" [(ngModel)]=\"invoice.invoiceClass\"\n                                            class=\"selectpicker selectinvoiceClass\" data-title=\"Invoice Type\"\n                                            data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                            <option [value]=\"1\">Satış</option>\n                                            <option [value]=\"2\">İade</option>\n                                            <option [value]=\"3\">Tevkifat</option>\n                                            <option [value]=\"4\">Istisna</option>\n                                            <option [value]=\"5\">Ozel Matrah</option>\n                                            <option [value]=\"6\">Ihracat</option>\n                                        </select>\n                                    </div>\n                                    <label class=\"col-sm-2 control-label\">Auto Generate?</label>\n                                    <div class=\"col-sm-1 checkbox\" style=\"padding-top: 10px;\">\n                                        <input id=\"autoGenerate\" type=\"checkbox\" name=\"autoGenerate\" [(ngModel)]=\"invoice.autoGenerate\">\n                                        <label for=\"autoGenerate\">\n\n                                        </label>\n                                    </div>\n                                    <div class=\"form-group\" [hidden]=\"invoice.autoGenerate==0\">\n                                        <label class=\"col-sm-1 control-label\">Period</label>\n                                        <div class=\"col-sm-2\">\n                                            <select name=\"invoicePeriod\" [(ngModel)]=\"invoice.invoicePeriod\"\n                                                class=\"selectpicker selectInvoicePeriod\" data-title=\"Invoice Period\"\n                                                data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                                <option [value]=\"1\">Aylık</option>\n                                                <option [value]=\"2\">Yıllık</option>\n                                            </select>\n                                        </div>\n                                        <div class=\"col-sm-2\">\n                                            <input type=\"text\" [(ngModel)]=\"invoice.periodLastDate\"\n                                                name=\"periodLastDate\" class=\"form-control selectperiodLastDate\"\n                                                placeholder=\"Last Date\" />\n                                        </div>\n                                    </div>\n                                </div>\n                            </fieldset>\n\n                            <fieldset *ngIf=\"invoice.ID>0\">\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Invoice Image</label>\n                                    <div class=\"col-sm-10\" style=\"padding-top:10px;\">\n                                        <input type=\"file\" name=\"uf\" style=\"display:none;\" #fileInput\n                                            (change)=\"onFileChange($event)\" />\n                                        <a *ngIf=\"invoice.ID>0 && invoice.pdfPath==null\" title=\"Upload Invoice\"\n                                            (click)=\"uploadInvoice(invoice.ID)\">Upload Invoice Image\n                                            <span style=\"float:right;\"><i class=\"fa fa-upload\"></i></span>\n                                        </a>\n                                        {{invoice.pdfPath}}\n                                        <span style=\"float:right;\">\n                                            <a style=\"margin-top:10px;\" *ngIf=\"invoice.pdfPath!=null\" title=\"Get File\"\n                                                target=\"_blank\"\n                                                href=\"https://crmapi.efdigitalcodes.com{{invoice.pdfPath}}\"><i\n                                                    class=\"fa fa-search\"></i></a>\n                                            &nbsp;<a style=\"margin-top:10px;cursor: pointer;\"\n                                                *ngIf=\"invoice.pdfPath!=null\" title=\"Delete File\"\n                                                (click)=\"deleteFile(invoice.fileID)\">\n                                                <i class=\"fa fa-trash\"> </i>&nbsp;</a>\n                                        </span>\n\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Notes</label>\n                                    <div class=\"col-sm-10\">\n                                        <textarea rows=\"3\" class=\"form-control\" placeholder=\"Notes\"\n                                            [(ngModel)]=\"invoice.notes\" name=\"notes\"></textarea>\n\n                                    </div>\n                                </div>\n                            </fieldset>\n                        </div>\n                        <div class=\"header\">\n                            <legend>Details</legend>\n                            <span style='float:right;'>\n                                <a (click)='addInvoiceDetail()' #addDetail *ngIf=\"invoice.statusID<=3\"\n                                    style='color:gray;margin-right:10px;'><i class='fa fa-plus-square-o'></i></a>\n                            </span>\n                        </div>\n                        <br>\n                        <div class=\"content\" #idContent>\n                            <!-- detail will be here -->\n                            <fieldset>\n                                <div class=\"col-md-5\">\n                                    <div class=\"form-group\">\n                                        <label>Product/Service</label>\n                                    </div>\n                                </div>\n                                <div class=\"col-md-1\">\n                                    <div class=\"form-group\">\n                                        <label>Unit</label>\n                                    </div>\n                                </div>\n                                <div class=\"col-md-1\">\n                                    <div class=\"form-group\">\n                                        <label>Unit Price</label>\n                                    </div>\n                                </div>\n                                <div class=\"col-md-1\">\n                                    <div class=\"form-group\">\n                                        <label>Discount</label>\n                                    </div>\n                                </div>\n\n                                <div class=\"col-md-1\">\n                                    <div class=\"form-group\">\n                                        <label>Tax Rate</label>\n                                    </div>\n                                </div>\n                                <div class=\"col-md-1\">\n                                    <div class=\"form-group\">\n                                        <label>Tax</label>\n                                    </div>\n                                </div>\n                                <div class=\"col-md-2\">\n                                    <div class=\"form-group\">\n                                        <label>Total</label>\n\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset *ngFor=\"let invoiceD of invoice.invoiceDetails\">\n                                <div class=\"col-md-5\">\n                                    <div class=\"form-group\">\n                                        <select name=\"item\" [(ngModel)]=\"invoiceD.item\"\n                                            class=\"selectpicker selectProduct rowd{{invoiceD.rowNumber}}\"\n                                            data-title=\"Select Product\" data-style=\"btn-default btn-block\"\n                                            data-menu-style=\"dropdown-blue\">\n                                            <option selected *ngFor=\"let product of products\" [value]=\"product.product\">\n                                                {{product.product}}</option>\n                                        </select>\n\n                                        <!-- <input type=\"text\" disabled placeholder=\"\" [ngModelOptions]=\"{standalone: true}\" class=\"form-control\" *ngIf=\"invoice.invoiceType==2\"\n                                            name=\"item\" [value]=\"invoiceD.item\" [(ngModel)]=\"invoiceD.item\">  -->\n                                    </div>\n                                </div>\n                                <div class=\"col-md-1\">\n                                    <div class=\"form-group\">\n                                        <input type=\"number\" placeholder=\"\" class=\"form-control\" name=\"unit\"\n                                            (input)=\"unitChanged($event.target.value,invoiceD.rowNumber)\"\n                                            [ngModelOptions]=\"{standalone: true}\" [value]=\"invoiceD.unit\"\n                                            [(ngModel)]=\"invoiceD.unit\">\n                                    </div>\n                                </div>\n                                <div class=\"col-md-1\">\n                                    <div class=\"form-group\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"amount\"\n                                            (input)=\"amountChanged($event.target.value,invoiceD.rowNumber)\"\n                                            [ngModelOptions]=\"{standalone: true}\" [value]=\"invoiceD.amount\"\n                                            [(ngModel)]=\"invoiceD.amount\">\n                                    </div>\n                                </div>\n                                <div class=\"col-md-1\">\n                                    <div class=\"form-group\">\n                                        <input type=\"number\" placeholder=\"\" class=\"form-control text-right\"\n                                            name=\"discountRate\"\n                                            (input)=\"discountChanged($event.target.value,invoiceD.rowNumber)\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"invoiceD.discountRate\">\n                                    </div>\n                                </div>\n\n                                <div class=\"col-md-1\">\n                                    <div class=\"form-group\">\n                                        <select class=\"selectpicker selectVatRate\" data-title=\"Rate\"\n                                            data-style=\"btn-default btn-block\" [ngModelOptions]=\"{standalone: true}\"\n                                            name=\"vatRate\" [(ngModel)]=\"invoiceD.vatRate\"\n                                            (change)=\"vatChanged($event.target.value,invoiceD.rowNumber)\"\n                                            data-menu-style=\"dropdown-blue\">\n                                            <option selected value=\"18\">%18</option>\n                                            <option selected value=\"8\">%8</option>\n                                            <option selected value=\"1\">%1</option>\n                                            <option selected value=\"0\">%0</option>\n                                        </select>\n                                    </div>\n                                </div>\n                                <div class=\"col-md-1\">\n                                    <div class=\"form-group\">\n                                        <input disabled=\"\" type=\"item\" placeholder=\"\"\n                                            [ngModelOptions]=\"{standalone: true}\" class=\"form-control text-right\"\n                                            name=\"vatAmount\" [(ngModel)]=\"invoiceD.vatAmount\">\n                                    </div>\n                                </div>\n                                <div class=\"col-md-2\">\n                                    <div class=\"form-group\">\n                                        <input currencyMask [options]=\"{ prefix: invoice.currency+' ',  decimal: '.' }\"\n                                            disabled=\"\" type=\"text\" placeholder=\"\" class=\"form-control text-right\"\n                                            style=\"width:80%;\" [ngModelOptions]=\"{standalone: true}\" name=\"totalAmount\"\n                                            [(ngModel)]=\"invoiceD.totalAmount\">\n                                        <a (click)=\"removeItem(invoiceD.rowNumber)\"\n                                            style='color:gray;float:right;margin-top:-30px;'><i\n                                                class='fa fa-remove'></i></a>\n                                    </div>\n\n                                </div>\n                            </fieldset>\n                        </div>\n                        <br>\n                        <legend></legend>\n                        <fieldset>\n                            <div class=\"form-group\">\n                                <label class=\"col-sm-8 control-label\">Total Units</label>\n                                <div class=\"col-sm-4\">\n                                    <input type=\"text\" disabled placeholder=\"Units\" class=\"form-control text-right\"\n                                        [(ngModel)]=\"totals.unit\" name=\"unitTotal\">\n                                </div>\n                            </div>\n                        </fieldset>\n                        <fieldset>\n                            <div class=\"form-group\">\n                                <label class=\"col-sm-8 control-label\">Sub Total</label>\n                                <div class=\"col-sm-4\">\n                                    <input currencyMask [options]=\"{ prefix: invoice.currency+' ',  decimal: '.' }\"\n                                        type=\"text\" disabled placeholder=\"SubTotal\" class=\"form-control text-right\"\n                                        [(ngModel)]=\"totals.amount\" name=\"subTotal\">\n                                </div>\n                            </div>\n                        </fieldset>\n                        <fieldset>\n                            <div class=\"form-group\">\n                                <label class=\"col-sm-8 control-label\">Vat Total</label>\n                                <div class=\"col-sm-4\">\n                                    <input currencyMask [options]=\"{ prefix: invoice.currency+' ', decimal: '.' }\"\n                                        type=\"text\" disabled placeholder=\"VAT\" class=\"form-control text-right\"\n                                        [(ngModel)]=\"totals.vatAmount\" name=\"vatTotal\">\n                                </div>\n                            </div>\n                        </fieldset>\n                        <fieldset>\n                            <div class=\"form-group\">\n                                <label class=\"col-sm-8 control-label\">Total</label>\n                                <div class=\"col-sm-4\">\n                                    <input currencyMask [options]=\"{ prefix: invoice.currency+' ', decimal: '.' }\"\n                                        type=\"text\" disabled placeholder=\"Total\" class=\"form-control text-right\"\n                                        [(ngModel)]=\"totals.total\" name=\"total\">\n                                </div>\n                            </div>\n                        </fieldset>\n\n                        <br>\n                    </div> <!-- end card -->\n                    <div class=\"content\">\n                        <button #btnSave\n                            *ngIf=\"(invoice.statusID<=3 && invoice.invoiceType==1) || (invoice.merchantInvoiceID=='' && invoice.invoiceType==2) && invoice.ID>0\"\n                            class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickDelete(content)\">DELETE</button>\n                        <button #btnSave *ngIf=\"(invoice.statusID==1000 && invoice.invoiceType==2) && invoice.ID>0\"\n                            class=\"btn btn-default btn-fill btn-wd\" (click)=\"postAccount()\">POST ACCOUNT</button>\n\n                        <div style='float:right;'>\n                            <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\"\n                                (click)=\"clickCancel()\">CANCEL</button>\n                            <button style='margin-right:5px;' #btnSave\n                                *ngIf=\"(invoice.statusID<=3 && invoiceType==1) || ( invoiceType==2 && invoice.statusID<=3)\"\n                                class=\"btn btn-success btn-fill btn-wd\" (click)=\"clickSave()\">SAVE</button>\n                            <button #btnSaveAsInvoice *ngIf=\"invoice.ID>0 && invoice.isProforma==1\"\n                                class=\"btn btn-success btn-fill btn-wd\" (click)=\"clickSaveAsInvoice()\">SAVE AS\n                                INVOICE</button>\n\n                        </div>\n                    </div>\n                </form>\n            </div> <!-- end col-md-12 -->\n        </div> <!-- end row -->\n\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/sales/invoice/invoice.component.scss":
/*!******************************************************!*\
  !*** ./src/app/sales/invoice/invoice.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-horizontal .form-group {\n  margin-right: 0px !important;\n  margin-left: 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9kZXZlbG9wZXIyMDIvRG9jdW1lbnRzL2lvbmljcHJvamVjdHMvZWZjcm0vc3JjL2FwcC9zYWxlcy9pbnZvaWNlL2ludm9pY2UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw2QkFBNEI7RUFDNUIsNEJBQTJCLEVBQzlCIiwiZmlsZSI6InNyYy9hcHAvc2FsZXMvaW52b2ljZS9pbnZvaWNlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcm0taG9yaXpvbnRhbCAuZm9ybS1ncm91cCB7XG4gICAgbWFyZ2luLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcbiAgICBtYXJnaW4tbGVmdDogMHB4ICFpbXBvcnRhbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/sales/invoice/invoice.component.ts":
/*!****************************************************!*\
  !*** ./src/app/sales/invoice/invoice.component.ts ***!
  \****************************************************/
/*! exports provided: InvoiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceComponent", function() { return InvoiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var app_costs_accounting_accounting_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/costs/accounting/accounting.component */ "./src/app/costs/accounting/accounting.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var InvoiceComponent = /** @class */ (function () {
    //invoiceForm: FormGroup;
    function InvoiceComponent(apiService, activeRoute, router, render, modalService) {
        var _this = this;
        this.apiService = apiService;
        this.activeRoute = activeRoute;
        this.router = router;
        this.render = render;
        this.modalService = modalService;
        this.customers = [];
        this.accountingSuppliers = [];
        this.invoice = [];
        this.totals = [];
        this.invoiceType = 0;
        this.selectedCustomer = [];
        this.customerID = 0;
        this.products = [];
        this.masterProducts = [];
        var routeParams = this.activeRoute.snapshot.params;
        this.activeRoute.queryParams
            .subscribe(function (params) {
            // Defaults to 0 if no query param provided.
            _this.invoiceType = +params['invoiceType'] || 0;
            _this.customerID = +params['customerID'] || 0;
        });
        this.invoiceID = routeParams.id;
        this.businessID = localStorage.getItem("businessID");
        console.log(routeParams);
        console.log(this.invoiceType, "invoiceType");
        console.log(this.customerID, "customerID");
        if (!this.invoiceID) {
            this.invoiceID = 0;
        }
        this.getProducts();
        this.invoice = { "ID": 0, "asID": this.businessID, "invoiceDate": "", "customerID": this.customerID, "description": "", "notes": "", "tags": "", "statusID": 0, "dueDateOptions": 0 };
        /*
        this.invoiceForm = this.fb.group({
          invoiceNumber: ['', Validators.required],
          customerID:['', Validators.required],
          invoiceDate:['', Validators.required],
          dueDateOptions:['', Validators.required],
          invoiceCurrency:['', Validators.required],
          invoiceClass:['', Validators.required],
          isProforma:[],
          po:[],
          description:['', Validators.required],
          notes:[],
          uf:[]
        });
        */
    }
    InvoiceComponent.prototype.resolveAfter2Seconds = function (x) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                resolve(x);
            }, 2000);
        });
    };
    InvoiceComponent.prototype.ngOnInit = function () {
        var _this = this;
        /*
        this.invoiceForm = this.fb.group({
          customerID: ['', Validators.min(1)]
        });
        */
        this.getCustomers(0);
        if (this.invoiceID > 0) {
            console.log(this.invoiceID);
            this.getInvoice(this.invoiceID, 0);
        }
        else {
            this.invoice.invoiceDetails = [];
            this.invoice.statusID = 0;
        }
        this.resolveAfter2Seconds(40).then(function (value) {
            _this.spInit();
            $(".selectCustomer").selectpicker('refresh');
        });
        $('.datetimepicker').datetimepicker({
            locale: 'tr',
            format: 'DD-MM-YYYY HH:mm',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
        $('.selectperiodLastDate').datetimepicker({
            locale: 'tr',
            format: 'DD-MM-YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    };
    InvoiceComponent.prototype.openModal = function (content) {
        var _this = this;
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(function (result) {
            _this.closeResult = 'Closed with: ${result}';
        }, function (reason) {
            _this.closeResult = 'Dismissed ';
        });
        console.log(this.closeResult);
    };
    InvoiceComponent.prototype.getTotals = function () {
        this.totals.unit = this.invoice.invoiceDetails.reduce(function (sum, item) { return sum + (item.unit * 1); }, 0);
        this.totals.amount = this.invoice.invoiceDetails.reduce(function (sum, item) { return sum + (item.totalAmount * 1); }, 0);
        this.totals.vatAmount = this.invoice.invoiceDetails.reduce(function (sum, item) { return sum + (item.vatAmount * 1); }, 0);
        this.totals.total = (this.totals.amount * 1) + (this.totals.vatAmount * 1);
        console.log(this.totals.total, ",", this.totals.amount, ",", this.totals.vatAmount, "total amounts");
        /*this.invoice.invoiceDetails.forEach((e:any) => {
          console.log(e.amount);
        
          this.totals.amount =  Number(this.totals.amount) + Number(e.amount);
          console.log( this.totals.amount);
          this.totals.vatAmount =  this.totals.vatAmount + Number(e.vatAmount);
          this.totals.total =  this.totals.total + Number(e.totalAmount);
          this.totals.discount =  this.totals.discount + Number(e.discountAmount);
      });
      */
    };
    InvoiceComponent.prototype.getCustomer = function (customerID) {
        var _this = this;
        this.apiService.getData(this.businessID + "/1/" + customerID, "customers/").then(function (result) {
            _this.selectedCustomer = result[0];
            $(".selectdueDateOptions").selectpicker('val', _this.selectedCustomer.dueDateOptions);
            _this.invoice.dueDateOptions = _this.selectedCustomer.dueDateOptions;
            _this.getSuppliers(0);
        });
    };
    InvoiceComponent.prototype.getCustomers = function (customerID) {
        var _this = this;
        if (this.invoiceType > 0) {
            this.invoice.invoiceType = this.invoiceType; // Satıs Faturası
        }
        this.apiService.getData(this.businessID + "/" + this.invoice.invoiceType + "/" + customerID, "customers/").then(function (result) {
            _this.customers = result;
        });
    };
    InvoiceComponent.prototype.getSuppliers = function (supplierID) {
        var _this = this;
        this.apiService.getData(supplierID, "accountingSuppliers/").then(function (result) {
            _this.accountingSuppliers = result;
        });
    };
    InvoiceComponent.prototype.ngAfterViewInit = function () {
    };
    InvoiceComponent.prototype.getInvoice = function (invoiceID, invoiceType) {
        var _this = this;
        this.apiService.getData((invoiceType + "/" + this.businessID + "/" + invoiceID), "invoices/").then(function (result) {
            _this.invoice = result[0];
            _this.invoice.autoGenerate = parseInt(_this.invoice.autoGenerate);
            _this.invoice.isProforma = parseInt(_this.invoice.isProforma);
            _this.invoice.invoiceDetails = JSON.parse(_this.invoice.invoiceDetails);
            if (_this.invoice.invoiceDetails.length > 0) {
                _this.getTotals();
                console.log(_this.invoice.currency, "currency");
                if (_this.invoice.currency != "") {
                    var pf = _this.masterProducts.filter(function (x) { return x.currency == _this.invoice.currency; });
                    console.log(pf);
                    _this.products = pf;
                    _this.resolveAfter2Seconds(20).then(function (value) {
                        $(".selectProduct").selectpicker('refresh');
                    });
                }
            }
        });
    };
    InvoiceComponent.prototype.spInit = function () {
        console.log("Init Select");
        $(".selectpicker").selectpicker();
        $(".selectAS").selectpicker('val', this.invoice.asID);
        $(".selectCustomer").selectpicker('val', this.invoice.customerID);
        $(".selectCurrency").selectpicker('val', this.invoice.invoiceCurrency);
        $(".selectinvoiceClass").selectpicker('val', this.invoice.invoiceClass);
        $(".selectdueDateOptions").selectpicker('val', this.invoice.dueDateOptions);
        $(".selectInvoicePeriod").selectpicker('val', this.invoice.invoicePeriod);
        $(".selectVatRate").selectpicker();
        //$(".selectProduct").selectpicker();
        this.invoice.invoiceDetails.forEach(function (element) {
            var si = ".rowd" + element.rowNumber;
            //console.log(si, element.item);
            $(".rowd" + element.rowNumber).selectpicker('val', element.item);
        });
        //$(".selectProduct").selectpicker("val", "GP Manganez AA Kalem Pil 12 Adet (GP15G R6)");
    };
    InvoiceComponent.prototype.addInvoiceDetail = function () {
        console.log(this.invoice, "invoice");
        if (this.invoice.invoiceDetails === null) {
            this.invoice.invoiceDetails = [];
        }
        var rn = this.invoice.invoiceDetails.length + 1;
        var id = { "rowNumber": rn, "item": "", "unit": 1, "amount": 0, "discountRate": 0, "vatRate": 18, "vatAmount": 0, "totalAmount": 0, "discountAmount": 0 };
        this.invoice.invoiceDetails.push(id);
        this.resolveAfter2Seconds(20).then(function (value) {
            $(".selectVatRate").selectpicker();
            $(".selectProduct").selectpicker();
        });
    };
    InvoiceComponent.prototype.uploadInvoice = function (invoiceID) {
        var event = new MouseEvent('click', { bubbles: false });
        this.invoiceID = invoiceID;
        this.fileInput.nativeElement.dispatchEvent(event);
    };
    InvoiceComponent.prototype.onFileChange = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (this.fileInput.nativeElement.files && this.fileInput.nativeElement.files.length > 0) {
            var file = this.fileInput.nativeElement.files[0];
            reader.readAsDataURL(file);
            reader.onload = function () {
                var file = {
                    "fileName": _this.fileInput.nativeElement.files[0].name,
                    "fileType": _this.fileInput.nativeElement.files[0].type,
                    "table": "invoices",
                    "userID": localStorage.getItem("userID"),
                    "description": "Invoice",
                    "file": reader.result.toString().split(",")[1]
                };
                _this.apiService.postData("invoices/" + _this.invoiceID, "files/upload/", JSON.stringify(file)).then(function (result) {
                    console.log(result);
                });
                /*
                   console.log(this.fileInput.nativeElement.files[0].name);
                 console.log(this.fileInput.nativeElement.files[0].type);
                 console.log(reader.result.toString().split(",")[1]);
                */
            };
        }
    };
    InvoiceComponent.prototype.removeItem = function (rowNumber) {
        var ndx = this.invoice.invoiceDetails.findIndex(function (x) { return x.rowNumber == rowNumber; });
        this.invoice.invoiceDetails.splice(ndx, 1);
    };
    InvoiceComponent.prototype.clickCancel = function () {
        if (this.invoice.invoiceType == 2) {
            this.router.navigateByUrl("/costs/invoices");
        }
        else {
            this.router.navigateByUrl("/sales/invoices");
        }
    };
    InvoiceComponent.prototype.clickSave = function () {
        /*
        console.log("Click Save");
        if (this.invoiceForm.invalid) {
          return;
        }
        */
        var _this = this;
        if (this.invoiceType > 0) {
            this.invoice.invoiceType = this.invoiceType; // Satıs Faturası
        }
        this.invoice.asID = this.businessID;
        this.invoice.tags = this.invoice.supplier;
        this.invoice.invoiceDate = $('.datetimepicker').val();
        this.invoice.periodLastDate = $('.selectperiodLastDate').val();
        this.apiService.postData(this.invoice.ID, "/invoice/save/", JSON.stringify(this.invoice)).then(function (result) {
            console.log(result);
            if (_this.invoice.invoiceType == 2) {
                _this.router.navigateByUrl("/costs/invoices");
            }
            else {
                _this.router.navigateByUrl("/sales/invoices");
            }
        });
    };
    InvoiceComponent.prototype.clickSaveAsInvoice = function () {
        var _this = this;
        console.log("Click Save");
        if (this.invoiceType > 0) {
            this.invoice.invoiceType = this.invoiceType; // Satıs Faturası
        }
        this.invoice.isProforma = 0;
        this.invoice.asID = this.businessID;
        this.invoice.tags = this.invoice.supplier;
        this.invoice.invoiceDate = $('.datetimepicker').val();
        this.apiService.postData(this.invoice.ID, "/invoice/save/", JSON.stringify(this.invoice)).then(function (result) {
            console.log(result);
            if (_this.invoice.invoiceType == 2) {
                _this.router.navigateByUrl("/costs/invoices");
            }
            else {
                _this.router.navigateByUrl("/sales/invoices");
            }
        });
    };
    InvoiceComponent.prototype.deleteFile = function (fileID) {
        var _this = this;
        this.apiService.deleteData(fileID, "/files/delete/").then(function (result) {
            console.log(result);
            _this.invoice.pdfPath = null;
        });
    };
    InvoiceComponent.prototype.clickDelete = function (content) {
        //this.openModal(content);
        var _this = this;
        this.apiService.deleteData(this.invoice.ID, "/invoice/delete/").then(function (result) {
            console.log(result);
            if (_this.invoice.invoiceType == 2) {
                _this.router.navigateByUrl("/costs/invoices");
            }
            else {
                _this.router.navigateByUrl("/sales/invoices");
            }
        });
    };
    InvoiceComponent.prototype.changeCurrency = function (e) {
        this.invoice.currency = e.target['options'][e.target['options'].selectedIndex].text;
        console.log(e.target['options'][e.target['options'].selectedIndex].value, "selected currency");
        var pf = this.masterProducts.filter(function (x) { return x.currency == e.target['options'][e.target['options'].selectedIndex].text; });
        console.log(pf);
        this.products = pf;
        this.resolveAfter2Seconds(20).then(function (value) {
            $(".selectProduct").selectpicker('refresh');
        });
    };
    InvoiceComponent.prototype.unitChanged = function (e, rowNumber) {
        console.log("unit changed", e, rowNumber);
        this.calculate(rowNumber);
    };
    InvoiceComponent.prototype.amountChanged = function (e, rowNumber) {
        console.log("amount changed", e);
        this.calculate(rowNumber);
    };
    InvoiceComponent.prototype.discountChanged = function (e, rowNumber) {
        console.log("discount changed", e);
        this.calculate(rowNumber);
    };
    InvoiceComponent.prototype.vatChanged = function (e, rowNumber) {
        console.log("vat changed", e);
        this.calculate(rowNumber);
    };
    InvoiceComponent.prototype.calculate = function (rowNumber) {
        var idr = this.invoice.invoiceDetails.filter(function (x) { return x.rowNumber == rowNumber; })[0];
        idr.vatAmount = (idr.unit * idr.amount * idr.vatRate / 100).toFixed(2);
        if (idr.discount > 0) {
            idr.totalAmount = (idr.unit * idr.amount * ((100 - idr.discount) / 100)).toFixed(2);
        }
        else {
            idr.totalAmount = (idr.unit * idr.amount).toFixed(2);
        }
        this.getTotals();
    };
    InvoiceComponent.prototype.getProducts = function () {
        //console.log(this.businessID + "/3/0/" + this.invoiceID,"prodicys");
        var _this = this;
        this.apiService.getData((this.businessID + "/" + this.invoiceType + "/0/" + this.invoiceID), "products/").then(function (result) {
            _this.products = result;
            _this.masterProducts = result;
        });
    };
    InvoiceComponent.prototype.postAccount = function () {
        console.log(this.invoiceID, "invoice ID");
        var modalRef = this.modalService.open(app_costs_accounting_accounting_component__WEBPACK_IMPORTED_MODULE_4__["AccountingComponent"], { "backdrop": "static" });
        modalRef.componentInstance.invoiceID = this.invoiceID;
        modalRef.result.then(function (data) {
            console.log(data, "result data");
        }, function (reason) {
            // on dismiss
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('idContent'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], InvoiceComponent.prototype, "idContent", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], InvoiceComponent.prototype, "fileInput", void 0);
    InvoiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-invoice',
            template: __webpack_require__(/*! ./invoice.component.html */ "./src/app/sales/invoice/invoice.component.html"),
            styles: [__webpack_require__(/*! ./invoice.component.scss */ "./src/app/sales/invoice/invoice.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]])
    ], InvoiceComponent);
    return InvoiceComponent;
}());



/***/ }),

/***/ "./src/app/sales/invoices-out/invoices-out.component.html":
/*!****************************************************************!*\
  !*** ./src/app/sales/invoices-out/invoices-out.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card \">\n                    <div class=\"header\">\n                        <h4 class=\"title\">{{\"salesinvoicessummary\"| translate | titlecase}}\n                            <span style='float:right;font-size:16px;'><a (click)=\"showAccord()\"><i\n                                        style='color:gray;margin-right:10px;' class=\"fa fa-ellipsis-h\">\n                                    </i>&nbsp;</a></span>\n                        </h4>\n                        <p class=\"category\"></p>\n                    </div>\n                    <div class=\"content\">\n                        <div class=\"panel panel-default\" *ngIf=\"showHideAccor\">\n                            <div class=\"panel-body\">\n                                <div class=\"col-md-6\">\n                                    <lbd-chart [title]=\"'Payables'\" [subtitle]=\"'All products including Taxes'\"\n                                        [chartType]=\"activityChartType\" [chartData]=\"activityChartData\"\n                                        [chartOptions]=\"activityChartOptions\"\n                                        [chartResponsive]=\"activityChartResponsive\"\n                                        [legendItems]=\"activityChartLegendItems\" [withHr]=\"false\" [noCard]=\"false\"\n                                        [footerIconClass]=\"'fa fa-check'\" [footerText]=\"'Data information certified'\">\n                                    </lbd-chart>\n                                </div>\n                                <div class=\"col-md-6\">\n                                    <div class=\"card\">\n                                        <div class=\"content\" style=\"padding-top: 30px;\">\n                                                <table class=\"table table-hover\">\n                                                    <thead>\n                                                        <tr>\n                                                            <th></th>\n                                                            <th class=\"text-center\">TRY</th>\n                                                            <th class=\"text-center\">USD</th>\n                                                            <th class=\"text-center\">EUR</th>\n                                                            <th class=\"text-center\">GBP</th>\n                                                           \n                                                        </tr>\n                                                    </thead>\n                                                    <tbody>\n                                                        <tr *ngFor=\"let dd of invoiceDueDates\">\n                                                            <td class=\"text-right\">{{dd.dayBlock | translate | uppercase}}</td>\n                                                            <td class=\"text-center\">{{dd.TRY | number}}</td>\n                                                            <td class=\"text-center\">{{dd.USD | number}}</td>\n                                                            <td class=\"text-center\">{{dd.EUR | number}}</td>\n                                                            <td class=\"text-center\">{{dd.GBP | number}}</td>\n                                                        </tr>\n                                                        <tr>\n                                                            <td class=\"text-right\">Total</td>\n                                                            <td class=\"text-center\">{{totals.totalTRY | number}}</td>\n                                                            <td class=\"text-center\">{{totals.totalUSD | number}}</td>\n                                                            <td class=\"text-center\">{{totals.totalEUR | number}}</td>\n                                                            <td class=\"text-center\">{{totals.totalGBP | number}}</td>\n                                                        </tr>\n                                                    </tbody>\n                                                </table>\n                                            </div>\n                                            <div class=\"footer\">\n                                                <div class=\"legend\">\n                                                    \n                                                </div>\n                                        </div>\n\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-md-12\">\n                        <div class=\"card\">\n                            <div class=\"content\">\n                                <h4 class=\"title\">{{\"invoices\"| translate |titlecase}} <span style='float:right;'>\n                                    <div class=\"button\">\n                                        <div (click)=\"callInvoice(1)\"\n                                            style='color:gray;margin-right:10px;font-size:16px;cursor:pointer;'><i\n                                                class='fa fa-plus-square-o'></i></div></div>\n                                    </span></h4>\n                                <p class=\"category\">{{\"These are your outgoing invoices\"| translate | titlecase}}</p>\n                                <!-- <legend></legend> -->\n                                <br>\n\n                                <div class=\"toolbar\">\n                                    <!--        Here you can write extra buttons/actions for the toolbar              -->\n                                </div>\n                                <div class=\"fresh-datatables\">\n                                    <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\"\n                                        width=\"100%\" style=\"width:100%\">\n                                        <thead>\n                                            <tr>\n                                                <th>#</th>\n                                                <th class='text-center'>{{\"Status\" | translate | uppercase}}</th>\n                                                <th class='text-center'></th>\n                                                <th class=\"text-left\">{{\"Invoice No\" | translate | uppercase}}</th>\n                                                <th>{{\"Customer\" | translate | uppercase}}</th>\n                                                <th>{{\"Issued\" | translate | uppercase}}</th>\n                                                <th class=\"text-center\"></th>\n                                                <th>{{\"Due\" | translate | uppercase}}</th>\n                                                <th class=\"text-right\">{{\"Amount\" | translate | uppercase}}</th>\n                                                <th class=\"text-center\"></th>\n                                                <th class=\"text-center\"></th>\n                                                <th style=\"width:10%;\" class=\"text-right\">{{\"Action\" | translate | uppercase}}</th>\n\n                                            </tr>\n                                        </thead>\n                                        <tbody>\n                                            <tr *ngFor=\"let row of dataTable.dataRows; let i = index\" ngClass=\"{{(row.statusID==2000) ? 'strikeout' : ''}}\" [ngStyle]=\"{'background-color':(row.statusID==2000) ? 'lightgray' : ''}\">\n                                                <td><span class=\"dot {{row.color}}\"></span></td>\n                                                <td class='text-center'>\n                                                    <span *ngIf=\"!(row.statusID<=1 && row.isProforma!=1)\" [ngStyle]=\"{'color':(row.statusID==1200) ? 'red' : '','border-color':(row.statusID==1200) ? 'red' : ''}\"\n                                                         title=\"{{row.statusDesc}}\" class=\"btn btn-xs \">{{row.status | uppercase}}</span>\n                                                    <a *ngIf=\"(row.statusID==0 && row.isProforma!=1) || (row.statusID==1 && userType==1 && row.isProforma!=1)\"\n                                                        (click)=\"approveInvoice(row.ID,i)\"\n                                                        class=\"btn btn-xs btn-success btn-fill\"><b>{{\"Approve\" | uppercase}}</b></a>\n                                                </td>\n                                                <td class='text-center'><span\n                                                        class=\"btn btn-primary btn-xs\">{{row.invoiceClassName | uppercase}}</span>\n                                                </td>\n                                                <td class=\"text-left\"><a routerLink='/sales/invoice/{{row.ID}}' title=\"{{row.description}}\"\n                                                        [queryParams]=\"{invoiceType: 1}\">{{row.invoiceNumber}}</a></td>\n                                                <td><a routerLink='/sales/customer/{{row.customerID}}' target=\"_blank\">{{row.customerShortName}}</a></td>\n                                                <td><span\n                                                        *ngIf=\"row.paymentID==0\">{{row.invoiceDate}}</span>\n                                                </td>\n                                                <td class=\"text-center\">\n                                                    <span\n                                                        *ngIf=\"calculateDays(row.invoiceDate,row.dueDate) != 9999 && row.paymentID==0\"\n                                                        ngClass=\"btn btn-xs {{calculateDays(row.invoiceDate,row.dueDate) >= 0 ? 'btn-fill btn-info' : 'btn-fill btn-danger'}}\">\n                                                        {{((calculateDays(row.invoiceDate,row.dueDate)))}} days</span>\n                                                    <!-- if calculateDays = 9999 then it means either Due or Invoice Date is NULL, so dont show any -->\n                                                </td>\n                                                <td>{{row.dueDate | date:'dd-MM-yyyy'}}</td>\n                                                <td class='text-right'>{{row.totalAmount | number}}\n                                                </td>\n                                                <td class='text-center'><span\n                                                        class=\"btn btn-success btn-xs\">{{row.currency }}</span></td>\n                                                <td class=\"text-center\">\n                                                    <span *ngIf=\"row.paymentID>0\"\n                                                        class=\"btn btn-success btn-xs btn-fill\">{{\"PAID\" | uppercase}}</span>\n                                                    <span *ngIf=\"row.isProforma==1\"\n                                                        class=\"btn btn-xs\">{{\"PROFORMA\" | uppercase}}</span>\n                                                </td>\n                                                \n                                                <td style=\"width:10%;\" class=\"text-right tableicon\">\n\n                                                    <a *ngIf=\"row.paymentID==0 && row.isProforma==0  && userType==1\"\n                                                        alt='Received Payment' (click)=\"openModal(row.ID,i)\"><i\n                                                            class=\"fa fa-credit-card\"></i>&nbsp;</a>\n                                                    <a target=\"_blank\" *ngIf=\"row.pdfPath!=null\"\n                                                        href=\"https://crmapi.efdigitalcodes.com{{row.pdfPath}}\"><i\n                                                            class=\"fa fa-file-pdf-o\"> </i>&nbsp;</a>\n                                                    <a *ngIf=\"row.statusID<=1\" (click)=\"deleteInvoice(row.ID,i)\"><i\n                                                            class=\"fa fa-trash\"> </i>&nbsp;</a>\n                                                    <a (click)=\"cloneInvoice(row.ID)\"><i class=\"fa fa-clone\">\n                                                        </i>&nbsp;</a>\n                                                    <a routerLink='/sales/invoice/{{row.ID}}'\n                                                        [queryParams]=\"{invoiceType: 1}\"><i\n                                                            class=\"fa fa-search\"></i></a>\n                                                </td>\n                                            </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n                            </div>\n                            <!-- end content-->\n                        </div>\n                        <!--  end card  -->\n\n                    </div>\n                    <!-- end col-md-12 -->\n                </div>\n                <!-- end row -->\n            </div>\n        </div>"

/***/ }),

/***/ "./src/app/sales/invoices-out/invoices-out.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/sales/invoices-out/invoices-out.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ct-chart {\n  margin: 30px 0 30px !important;\n  height: 245px !important; }\n\n.ct-label {\n  fill: rgba(0, 0, 0, 0.4);\n  color: rgba(0, 0, 0, 0.4);\n  font-size: 1.3rem;\n  line-height: 1; }\n\n.ct-chart-line .ct-label,\n.ct-chart-bar .ct-label {\n  display: block;\n  display: flex; }\n\n.ct-label.ct-horizontal.ct-start {\n  align-items: flex-end;\n  justify-content: flex-start;\n  text-align: left;\n  text-anchor: start; }\n\n.ct-label.ct-horizontal.ct-end {\n  align-items: flex-start;\n  justify-content: flex-start;\n  text-align: left;\n  text-anchor: start; }\n\n.ct-label.ct-vertical.ct-start {\n  align-items: flex-end;\n  justify-content: flex-end;\n  text-align: right;\n  text-anchor: end; }\n\n.ct-label.ct-vertical.ct-end {\n  align-items: flex-end;\n  justify-content: flex-start;\n  text-align: left;\n  text-anchor: start; }\n\n.ct-chart-bar .ct-label.ct-horizontal.ct-start {\n  align-items: flex-end;\n  justify-content: center;\n  text-align: center;\n  text-anchor: start; }\n\n.ct-chart-bar .ct-label.ct-horizontal.ct-end {\n  align-items: flex-start;\n  justify-content: center;\n  text-align: center;\n  text-anchor: start; }\n\n.ct-chart-bar.ct-horizontal-bars .ct-label.ct-horizontal.ct-start {\n  align-items: flex-end;\n  justify-content: flex-start;\n  text-align: left;\n  text-anchor: start; }\n\n.ct-chart-bar.ct-horizontal-bars .ct-label.ct-horizontal.ct-end {\n  align-items: flex-start;\n  justify-content: flex-start;\n  text-align: left;\n  text-anchor: start; }\n\n.ct-chart-bar.ct-horizontal-bars .ct-label.ct-vertical.ct-start {\n  align-items: center;\n  justify-content: flex-end;\n  text-align: right;\n  text-anchor: end; }\n\n.ct-chart-bar.ct-horizontal-bars .ct-label.ct-vertical.ct-end {\n  align-items: center;\n  justify-content: flex-start;\n  text-align: left;\n  text-anchor: end; }\n\n.ct-grid {\n  stroke: rgba(0, 0, 0, 0.2);\n  stroke-width: 1px;\n  stroke-dasharray: 2px; }\n\n.ct-point {\n  stroke-width: 8px;\n  stroke-linecap: round; }\n\n.ct-line {\n  fill: none;\n  stroke-width: 3px; }\n\n.ct-area {\n  stroke: none;\n  fill-opacity: 0.8; }\n\n.ct-bar {\n  fill: none;\n  stroke-width: 10px; }\n\n.ct-slice-donut {\n  fill: none;\n  stroke-width: 60px; }\n\n.ct-series-a .ct-point, .ct-series-a .ct-line, .ct-series-a .ct-bar, .ct-series-a .ct-slice-donut {\n  stroke: #23CCEF; }\n\n.ct-series-a .ct-slice-pie, .ct-series-a .ct-area {\n  fill: #23CCEF; }\n\n.ct-series-b .ct-point, .ct-series-b .ct-line, .ct-series-b .ct-bar, .ct-series-b .ct-slice-donut {\n  stroke: #FB404B; }\n\n.ct-series-b .ct-slice-pie, .ct-series-b .ct-area {\n  fill: #FB404B; }\n\n.ct-series-c .ct-point, .ct-series-c .ct-line, .ct-series-c .ct-bar, .ct-series-c .ct-slice-donut {\n  stroke: #FFA534; }\n\n.ct-series-c .ct-slice-pie, .ct-series-c .ct-area {\n  fill: #FFA534; }\n\n.ct-series-d .ct-point, .ct-series-d .ct-line, .ct-series-d .ct-bar, .ct-series-d .ct-slice-donut {\n  stroke: #9368E9; }\n\n.ct-series-d .ct-slice-pie, .ct-series-d .ct-area {\n  fill: #9368E9; }\n\n.ct-series-e .ct-point, .ct-series-e .ct-line, .ct-series-e .ct-bar, .ct-series-e .ct-slice-donut {\n  stroke: #87CB16; }\n\n.ct-series-e .ct-slice-pie, .ct-series-e .ct-area {\n  fill: #87CB16; }\n\n.ct-series-f .ct-point, .ct-series-f .ct-line, .ct-series-f .ct-bar, .ct-series-f .ct-slice-donut {\n  stroke: #447DF7; }\n\n.ct-series-f .ct-slice-pie, .ct-series-f .ct-area {\n  fill: #447DF7; }\n\n.ct-series-g .ct-point, .ct-series-g .ct-line, .ct-series-g .ct-bar, .ct-series-g .ct-slice-donut {\n  stroke: #5e5e5e; }\n\n.ct-series-g .ct-slice-pie, .ct-series-g .ct-area {\n  fill: #5e5e5e; }\n\n.ct-series-h .ct-point, .ct-series-h .ct-line, .ct-series-h .ct-bar, .ct-series-h .ct-slice-donut {\n  stroke: #dd4b39; }\n\n.ct-series-h .ct-slice-pie, .ct-series-h .ct-area {\n  fill: #dd4b39; }\n\n.ct-series-i .ct-point, .ct-series-i .ct-line, .ct-series-i .ct-bar, .ct-series-i .ct-slice-donut {\n  stroke: #35465c; }\n\n.ct-series-i .ct-slice-pie, .ct-series-i .ct-area {\n  fill: #35465c; }\n\n.ct-series-j .ct-point, .ct-series-j .ct-line, .ct-series-j .ct-bar, .ct-series-j .ct-slice-donut {\n  stroke: #e52d27; }\n\n.ct-series-j .ct-slice-pie, .ct-series-j .ct-area {\n  fill: #e52d27; }\n\n.ct-series-k .ct-point, .ct-series-k .ct-line, .ct-series-k .ct-bar, .ct-series-k .ct-slice-donut {\n  stroke: #55acee; }\n\n.ct-series-k .ct-slice-pie, .ct-series-k .ct-area {\n  fill: #55acee; }\n\n.ct-series-l .ct-point, .ct-series-l .ct-line, .ct-series-l .ct-bar, .ct-series-l .ct-slice-donut {\n  stroke: #cc2127; }\n\n.ct-series-l .ct-slice-pie, .ct-series-l .ct-area {\n  fill: #cc2127; }\n\n.ct-series-m .ct-point, .ct-series-m .ct-line, .ct-series-m .ct-bar, .ct-series-m .ct-slice-donut {\n  stroke: #1769ff; }\n\n.ct-series-m .ct-slice-pie, .ct-series-m .ct-area {\n  fill: #1769ff; }\n\n.ct-series-n .ct-point, .ct-series-n .ct-line, .ct-series-n .ct-bar, .ct-series-n .ct-slice-donut {\n  stroke: #6188e2; }\n\n.ct-series-n .ct-slice-pie, .ct-series-n .ct-area {\n  fill: #6188e2; }\n\n.ct-series-o .ct-point, .ct-series-o .ct-line, .ct-series-o .ct-bar, .ct-series-o .ct-slice-donut {\n  stroke: #a748ca; }\n\n.ct-series-o .ct-slice-pie, .ct-series-o .ct-area {\n  fill: #a748ca; }\n\n.ct-square {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-square:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 100%; }\n\n.ct-square:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-square > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-minor-second {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-minor-second:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 93.75%; }\n\n.ct-minor-second:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-minor-second > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-second {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-second:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 88.88889%; }\n\n.ct-major-second:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-second > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-minor-third {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-minor-third:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 83.33333%; }\n\n.ct-minor-third:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-minor-third > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-third {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-third:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 80%; }\n\n.ct-major-third:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-third > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-perfect-fifth {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-perfect-fifth:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 75%; }\n\n.ct-perfect-fifth:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-perfect-fifth > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-minor-sixth {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-minor-sixth:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 66.66667%; }\n\n.ct-minor-sixth:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-minor-sixth > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-golden-section {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-golden-section:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 62.5%; }\n\n.ct-golden-section:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-golden-section > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-sixth {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-sixth:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 61.8047%; }\n\n.ct-major-sixth:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-sixth > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-minor-seventh {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-minor-seventh:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 60%; }\n\n.ct-minor-seventh:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-minor-seventh > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-seventh {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-seventh:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 56.25%; }\n\n.ct-major-seventh:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-seventh > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-octave {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-octave:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 53.33333%; }\n\n.ct-octave:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-octave > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-tenth {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-tenth:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 50%; }\n\n.ct-major-tenth:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-tenth > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-eleventh {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-eleventh:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 40%; }\n\n.ct-major-eleventh:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-eleventh > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-major-twelfth {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-major-twelfth:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 37.5%; }\n\n.ct-major-twelfth:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-major-twelfth > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-double-octave {\n  display: block;\n  position: relative;\n  width: 100%; }\n\n.ct-double-octave:before {\n  display: block;\n  float: left;\n  content: \"\";\n  width: 0;\n  height: 0;\n  padding-bottom: 33.33333%; }\n\n.ct-double-octave:after {\n  content: \"\";\n  display: table;\n  clear: both; }\n\n.ct-double-octave > svg {\n  display: block;\n  position: absolute;\n  top: 0;\n  left: 0; }\n\n.ct-blue {\n  stroke: #447DF7 !important; }\n\n.ct-azure {\n  stroke: #23CCEF !important; }\n\n.ct-green {\n  stroke: #87CB16 !important; }\n\n.ct-orange {\n  stroke: #FFA534 !important; }\n\n.ct-red {\n  stroke: #FB404B !important; }\n\ntr.strikeout td:before {\n  content: \" \";\n  position: absolute;\n  top: 50%;\n  left: 0;\n  border-bottom: 1px solid #111;\n  width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9kZXZlbG9wZXIyMDIvRG9jdW1lbnRzL2lvbmljcHJvamVjdHMvZWZjcm0vc3JjL2FwcC9zYWxlcy9pbnZvaWNlcy1vdXQvaW52b2ljZXMtb3V0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksK0JBQThCO0VBQzlCLHlCQUF3QixFQUFHOztBQUUvQjtFQUNJLHlCQUF3QjtFQUN4QiwwQkFBeUI7RUFDekIsa0JBQWlCO0VBQ2pCLGVBQWMsRUFBRzs7QUFFbkI7O0VBRUUsZUFBYztFQUtkLGNBQWEsRUFBRzs7QUFFbEI7RUFJRSxzQkFBcUI7RUFJckIsNEJBQTJCO0VBQzNCLGlCQUFnQjtFQUNoQixtQkFBa0IsRUFBRzs7QUFFdkI7RUFJRSx3QkFBdUI7RUFJdkIsNEJBQTJCO0VBQzNCLGlCQUFnQjtFQUNoQixtQkFBa0IsRUFBRzs7QUFFdkI7RUFJRSxzQkFBcUI7RUFJckIsMEJBQXlCO0VBQ3pCLGtCQUFpQjtFQUNqQixpQkFBZ0IsRUFBRzs7QUFFckI7RUFJRSxzQkFBcUI7RUFJckIsNEJBQTJCO0VBQzNCLGlCQUFnQjtFQUNoQixtQkFBa0IsRUFBRzs7QUFFdkI7RUFJRSxzQkFBcUI7RUFJckIsd0JBQXVCO0VBQ3ZCLG1CQUFrQjtFQUNsQixtQkFBa0IsRUFBRzs7QUFFdkI7RUFJRSx3QkFBdUI7RUFJdkIsd0JBQXVCO0VBQ3ZCLG1CQUFrQjtFQUNsQixtQkFBa0IsRUFBRzs7QUFFdkI7RUFJRSxzQkFBcUI7RUFJckIsNEJBQTJCO0VBQzNCLGlCQUFnQjtFQUNoQixtQkFBa0IsRUFBRzs7QUFFdkI7RUFJRSx3QkFBdUI7RUFJdkIsNEJBQTJCO0VBQzNCLGlCQUFnQjtFQUNoQixtQkFBa0IsRUFBRzs7QUFFdkI7RUFJRSxvQkFBbUI7RUFJbkIsMEJBQXlCO0VBQ3pCLGtCQUFpQjtFQUNqQixpQkFBZ0IsRUFBRzs7QUFFckI7RUFJRSxvQkFBbUI7RUFJbkIsNEJBQTJCO0VBQzNCLGlCQUFnQjtFQUNoQixpQkFBZ0IsRUFBRzs7QUFFckI7RUFDRSwyQkFBMEI7RUFDMUIsa0JBQWlCO0VBQ2pCLHNCQUFxQixFQUFHOztBQUUxQjtFQUNFLGtCQUFpQjtFQUNqQixzQkFBcUIsRUFBRzs7QUFFMUI7RUFDRSxXQUFVO0VBQ1Ysa0JBQWlCLEVBQUc7O0FBRXRCO0VBQ0UsYUFBWTtFQUNaLGtCQUFpQixFQUFHOztBQUV0QjtFQUNFLFdBQVU7RUFDVixtQkFBa0IsRUFBRzs7QUFFdkI7RUFDRSxXQUFVO0VBQ1YsbUJBQWtCLEVBQUc7O0FBRXZCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZ0JBQWUsRUFBRzs7QUFDcEI7RUFDRSxjQUFhLEVBQUc7O0FBRWxCO0VBQ0UsZUFBYztFQUNkLG1CQUFrQjtFQUNsQixZQUFXLEVBQUc7O0FBQ2Q7RUFDRSxlQUFjO0VBQ2QsWUFBVztFQUNYLFlBQVc7RUFDWCxTQUFRO0VBQ1IsVUFBUztFQUNULHFCQUFvQixFQUFHOztBQUN6QjtFQUNFLFlBQVc7RUFDWCxlQUFjO0VBQ2QsWUFBVyxFQUFHOztBQUNoQjtFQUNFLGVBQWM7RUFDZCxtQkFBa0I7RUFDbEIsT0FBTTtFQUNOLFFBQU8sRUFBRzs7QUFFZDtFQUNFLGVBQWM7RUFDZCxtQkFBa0I7RUFDbEIsWUFBVyxFQUFHOztBQUNkO0VBQ0UsZUFBYztFQUNkLFlBQVc7RUFDWCxZQUFXO0VBQ1gsU0FBUTtFQUNSLFVBQVM7RUFDVCx1QkFBc0IsRUFBRzs7QUFDM0I7RUFDRSxZQUFXO0VBQ1gsZUFBYztFQUNkLFlBQVcsRUFBRzs7QUFDaEI7RUFDRSxlQUFjO0VBQ2QsbUJBQWtCO0VBQ2xCLE9BQU07RUFDTixRQUFPLEVBQUc7O0FBRWQ7RUFDRSxlQUFjO0VBQ2QsbUJBQWtCO0VBQ2xCLFlBQVcsRUFBRzs7QUFDZDtFQUNFLGVBQWM7RUFDZCxZQUFXO0VBQ1gsWUFBVztFQUNYLFNBQVE7RUFDUixVQUFTO0VBQ1QsMEJBQXlCLEVBQUc7O0FBQzlCO0VBQ0UsWUFBVztFQUNYLGVBQWM7RUFDZCxZQUFXLEVBQUc7O0FBQ2hCO0VBQ0UsZUFBYztFQUNkLG1CQUFrQjtFQUNsQixPQUFNO0VBQ04sUUFBTyxFQUFHOztBQUVkO0VBQ0UsZUFBYztFQUNkLG1CQUFrQjtFQUNsQixZQUFXLEVBQUc7O0FBQ2Q7RUFDRSxlQUFjO0VBQ2QsWUFBVztFQUNYLFlBQVc7RUFDWCxTQUFRO0VBQ1IsVUFBUztFQUNULDBCQUF5QixFQUFHOztBQUM5QjtFQUNFLFlBQVc7RUFDWCxlQUFjO0VBQ2QsWUFBVyxFQUFHOztBQUNoQjtFQUNFLGVBQWM7RUFDZCxtQkFBa0I7RUFDbEIsT0FBTTtFQUNOLFFBQU8sRUFBRzs7QUFFZDtFQUNFLGVBQWM7RUFDZCxtQkFBa0I7RUFDbEIsWUFBVyxFQUFHOztBQUNkO0VBQ0UsZUFBYztFQUNkLFlBQVc7RUFDWCxZQUFXO0VBQ1gsU0FBUTtFQUNSLFVBQVM7RUFDVCxvQkFBbUIsRUFBRzs7QUFDeEI7RUFDRSxZQUFXO0VBQ1gsZUFBYztFQUNkLFlBQVcsRUFBRzs7QUFDaEI7RUFDRSxlQUFjO0VBQ2QsbUJBQWtCO0VBQ2xCLE9BQU07RUFDTixRQUFPLEVBQUc7O0FBRWQ7RUFDRSxlQUFjO0VBQ2QsbUJBQWtCO0VBQ2xCLFlBQVcsRUFBRzs7QUFDZDtFQUNFLGVBQWM7RUFDZCxZQUFXO0VBQ1gsWUFBVztFQUNYLFNBQVE7RUFDUixVQUFTO0VBQ1Qsb0JBQW1CLEVBQUc7O0FBQ3hCO0VBQ0UsWUFBVztFQUNYLGVBQWM7RUFDZCxZQUFXLEVBQUc7O0FBQ2hCO0VBQ0UsZUFBYztFQUNkLG1CQUFrQjtFQUNsQixPQUFNO0VBQ04sUUFBTyxFQUFHOztBQUVkO0VBQ0UsZUFBYztFQUNkLG1CQUFrQjtFQUNsQixZQUFXLEVBQUc7O0FBQ2Q7RUFDRSxlQUFjO0VBQ2QsWUFBVztFQUNYLFlBQVc7RUFDWCxTQUFRO0VBQ1IsVUFBUztFQUNULDBCQUF5QixFQUFHOztBQUM5QjtFQUNFLFlBQVc7RUFDWCxlQUFjO0VBQ2QsWUFBVyxFQUFHOztBQUNoQjtFQUNFLGVBQWM7RUFDZCxtQkFBa0I7RUFDbEIsT0FBTTtFQUNOLFFBQU8sRUFBRzs7QUFFZDtFQUNFLGVBQWM7RUFDZCxtQkFBa0I7RUFDbEIsWUFBVyxFQUFHOztBQUNkO0VBQ0UsZUFBYztFQUNkLFlBQVc7RUFDWCxZQUFXO0VBQ1gsU0FBUTtFQUNSLFVBQVM7RUFDVCxzQkFBcUIsRUFBRzs7QUFDMUI7RUFDRSxZQUFXO0VBQ1gsZUFBYztFQUNkLFlBQVcsRUFBRzs7QUFDaEI7RUFDRSxlQUFjO0VBQ2QsbUJBQWtCO0VBQ2xCLE9BQU07RUFDTixRQUFPLEVBQUc7O0FBRWQ7RUFDRSxlQUFjO0VBQ2QsbUJBQWtCO0VBQ2xCLFlBQVcsRUFBRzs7QUFDZDtFQUNFLGVBQWM7RUFDZCxZQUFXO0VBQ1gsWUFBVztFQUNYLFNBQVE7RUFDUixVQUFTO0VBQ1QseUJBQXdCLEVBQUc7O0FBQzdCO0VBQ0UsWUFBVztFQUNYLGVBQWM7RUFDZCxZQUFXLEVBQUc7O0FBQ2hCO0VBQ0UsZUFBYztFQUNkLG1CQUFrQjtFQUNsQixPQUFNO0VBQ04sUUFBTyxFQUFHOztBQUVkO0VBQ0UsZUFBYztFQUNkLG1CQUFrQjtFQUNsQixZQUFXLEVBQUc7O0FBQ2Q7RUFDRSxlQUFjO0VBQ2QsWUFBVztFQUNYLFlBQVc7RUFDWCxTQUFRO0VBQ1IsVUFBUztFQUNULG9CQUFtQixFQUFHOztBQUN4QjtFQUNFLFlBQVc7RUFDWCxlQUFjO0VBQ2QsWUFBVyxFQUFHOztBQUNoQjtFQUNFLGVBQWM7RUFDZCxtQkFBa0I7RUFDbEIsT0FBTTtFQUNOLFFBQU8sRUFBRzs7QUFFZDtFQUNFLGVBQWM7RUFDZCxtQkFBa0I7RUFDbEIsWUFBVyxFQUFHOztBQUNkO0VBQ0UsZUFBYztFQUNkLFlBQVc7RUFDWCxZQUFXO0VBQ1gsU0FBUTtFQUNSLFVBQVM7RUFDVCx1QkFBc0IsRUFBRzs7QUFDM0I7RUFDRSxZQUFXO0VBQ1gsZUFBYztFQUNkLFlBQVcsRUFBRzs7QUFDaEI7RUFDRSxlQUFjO0VBQ2QsbUJBQWtCO0VBQ2xCLE9BQU07RUFDTixRQUFPLEVBQUc7O0FBRWQ7RUFDRSxlQUFjO0VBQ2QsbUJBQWtCO0VBQ2xCLFlBQVcsRUFBRzs7QUFDZDtFQUNFLGVBQWM7RUFDZCxZQUFXO0VBQ1gsWUFBVztFQUNYLFNBQVE7RUFDUixVQUFTO0VBQ1QsMEJBQXlCLEVBQUc7O0FBQzlCO0VBQ0UsWUFBVztFQUNYLGVBQWM7RUFDZCxZQUFXLEVBQUc7O0FBQ2hCO0VBQ0UsZUFBYztFQUNkLG1CQUFrQjtFQUNsQixPQUFNO0VBQ04sUUFBTyxFQUFHOztBQUVkO0VBQ0UsZUFBYztFQUNkLG1CQUFrQjtFQUNsQixZQUFXLEVBQUc7O0FBQ2Q7RUFDRSxlQUFjO0VBQ2QsWUFBVztFQUNYLFlBQVc7RUFDWCxTQUFRO0VBQ1IsVUFBUztFQUNULG9CQUFtQixFQUFHOztBQUN4QjtFQUNFLFlBQVc7RUFDWCxlQUFjO0VBQ2QsWUFBVyxFQUFHOztBQUNoQjtFQUNFLGVBQWM7RUFDZCxtQkFBa0I7RUFDbEIsT0FBTTtFQUNOLFFBQU8sRUFBRzs7QUFFZDtFQUNFLGVBQWM7RUFDZCxtQkFBa0I7RUFDbEIsWUFBVyxFQUFHOztBQUNkO0VBQ0UsZUFBYztFQUNkLFlBQVc7RUFDWCxZQUFXO0VBQ1gsU0FBUTtFQUNSLFVBQVM7RUFDVCxvQkFBbUIsRUFBRzs7QUFDeEI7RUFDRSxZQUFXO0VBQ1gsZUFBYztFQUNkLFlBQVcsRUFBRzs7QUFDaEI7RUFDRSxlQUFjO0VBQ2QsbUJBQWtCO0VBQ2xCLE9BQU07RUFDTixRQUFPLEVBQUc7O0FBRWQ7RUFDRSxlQUFjO0VBQ2QsbUJBQWtCO0VBQ2xCLFlBQVcsRUFBRzs7QUFDZDtFQUNFLGVBQWM7RUFDZCxZQUFXO0VBQ1gsWUFBVztFQUNYLFNBQVE7RUFDUixVQUFTO0VBQ1Qsc0JBQXFCLEVBQUc7O0FBQzFCO0VBQ0UsWUFBVztFQUNYLGVBQWM7RUFDZCxZQUFXLEVBQUc7O0FBQ2hCO0VBQ0UsZUFBYztFQUNkLG1CQUFrQjtFQUNsQixPQUFNO0VBQ04sUUFBTyxFQUFHOztBQUVkO0VBQ0UsZUFBYztFQUNkLG1CQUFrQjtFQUNsQixZQUFXLEVBQUc7O0FBQ2Q7RUFDRSxlQUFjO0VBQ2QsWUFBVztFQUNYLFlBQVc7RUFDWCxTQUFRO0VBQ1IsVUFBUztFQUNULDBCQUF5QixFQUFHOztBQUM5QjtFQUNFLFlBQVc7RUFDWCxlQUFjO0VBQ2QsWUFBVyxFQUFHOztBQUNoQjtFQUNFLGVBQWM7RUFDZCxtQkFBa0I7RUFDbEIsT0FBTTtFQUNOLFFBQU8sRUFBRzs7QUFFZDtFQUNFLDJCQUEwQixFQUFHOztBQUUvQjtFQUNFLDJCQUEwQixFQUFHOztBQUUvQjtFQUNFLDJCQUEwQixFQUFHOztBQUUvQjtFQUNFLDJCQUEwQixFQUFHOztBQUUvQjtFQUNFLDJCQUEwQixFQUFHOztBQUU3QjtFQUNFLGFBQVk7RUFDWixtQkFBa0I7RUFDbEIsU0FBUTtFQUNSLFFBQU87RUFDUCw4QkFBNkI7RUFDN0IsWUFBVyxFQUNkIiwiZmlsZSI6InNyYy9hcHAvc2FsZXMvaW52b2ljZXMtb3V0L2ludm9pY2VzLW91dC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jdC1jaGFydCB7XG4gICAgbWFyZ2luOiAzMHB4IDAgMzBweCAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMjQ1cHggIWltcG9ydGFudDsgfVxuXG4uY3QtbGFiZWwge1xuICAgIGZpbGw6IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAgIGZvbnQtc2l6ZTogMS4zcmVtO1xuICAgIGxpbmUtaGVpZ2h0OiAxOyB9XG4gIFxuICAuY3QtY2hhcnQtbGluZSAuY3QtbGFiZWwsXG4gIC5jdC1jaGFydC1iYXIgLmN0LWxhYmVsIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcbiAgICBkaXNwbGF5OiAtbW96LWJveDtcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gICAgZGlzcGxheTogZmxleDsgfVxuICBcbiAgLmN0LWxhYmVsLmN0LWhvcml6b250YWwuY3Qtc3RhcnQge1xuICAgIC13ZWJraXQtYm94LWFsaWduOiBmbGV4LWVuZDtcbiAgICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgICAtbXMtZmxleC1hbGlnbjogZmxleC1lbmQ7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgIC13ZWJraXQtYm94LXBhY2s6IGZsZXgtc3RhcnQ7XG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgLW1zLWZsZXgtcGFjazogZmxleC1zdGFydDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB0ZXh0LWFuY2hvcjogc3RhcnQ7IH1cbiAgXG4gIC5jdC1sYWJlbC5jdC1ob3Jpem9udGFsLmN0LWVuZCB7XG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGZsZXgtc3RhcnQ7XG4gICAgLXdlYmtpdC1hbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICAtbXMtZmxleC1hbGlnbjogZmxleC1zdGFydDtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICAtd2Via2l0LWJveC1wYWNrOiBmbGV4LXN0YXJ0O1xuICAgIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgIC1tcy1mbGV4LXBhY2s6IGZsZXgtc3RhcnQ7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgdGV4dC1hbmNob3I6IHN0YXJ0OyB9XG4gIFxuICAuY3QtbGFiZWwuY3QtdmVydGljYWwuY3Qtc3RhcnQge1xuICAgIC13ZWJraXQtYm94LWFsaWduOiBmbGV4LWVuZDtcbiAgICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgICAtbXMtZmxleC1hbGlnbjogZmxleC1lbmQ7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgIC13ZWJraXQtYm94LXBhY2s6IGZsZXgtZW5kO1xuICAgIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICAtbXMtZmxleC1wYWNrOiBmbGV4LWVuZDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHRleHQtYW5jaG9yOiBlbmQ7IH1cbiAgXG4gIC5jdC1sYWJlbC5jdC12ZXJ0aWNhbC5jdC1lbmQge1xuICAgIC13ZWJraXQtYm94LWFsaWduOiBmbGV4LWVuZDtcbiAgICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgICAtbXMtZmxleC1hbGlnbjogZmxleC1lbmQ7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgIC13ZWJraXQtYm94LXBhY2s6IGZsZXgtc3RhcnQ7XG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgLW1zLWZsZXgtcGFjazogZmxleC1zdGFydDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB0ZXh0LWFuY2hvcjogc3RhcnQ7IH1cbiAgXG4gIC5jdC1jaGFydC1iYXIgLmN0LWxhYmVsLmN0LWhvcml6b250YWwuY3Qtc3RhcnQge1xuICAgIC13ZWJraXQtYm94LWFsaWduOiBmbGV4LWVuZDtcbiAgICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgICAtbXMtZmxleC1hbGlnbjogZmxleC1lbmQ7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcbiAgICAtd2Via2l0LWp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgdGV4dC1hbmNob3I6IHN0YXJ0OyB9XG4gIFxuICAuY3QtY2hhcnQtYmFyIC5jdC1sYWJlbC5jdC1ob3Jpem9udGFsLmN0LWVuZCB7XG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGZsZXgtc3RhcnQ7XG4gICAgLXdlYmtpdC1hbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICAtbXMtZmxleC1hbGlnbjogZmxleC1zdGFydDtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHRleHQtYW5jaG9yOiBzdGFydDsgfVxuICBcbiAgLmN0LWNoYXJ0LWJhci5jdC1ob3Jpem9udGFsLWJhcnMgLmN0LWxhYmVsLmN0LWhvcml6b250YWwuY3Qtc3RhcnQge1xuICAgIC13ZWJraXQtYm94LWFsaWduOiBmbGV4LWVuZDtcbiAgICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgICAtbXMtZmxleC1hbGlnbjogZmxleC1lbmQ7XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICAgIC13ZWJraXQtYm94LXBhY2s6IGZsZXgtc3RhcnQ7XG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgLW1zLWZsZXgtcGFjazogZmxleC1zdGFydDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB0ZXh0LWFuY2hvcjogc3RhcnQ7IH1cbiAgXG4gIC5jdC1jaGFydC1iYXIuY3QtaG9yaXpvbnRhbC1iYXJzIC5jdC1sYWJlbC5jdC1ob3Jpem9udGFsLmN0LWVuZCB7XG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGZsZXgtc3RhcnQ7XG4gICAgLXdlYmtpdC1hbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICAtbXMtZmxleC1hbGlnbjogZmxleC1zdGFydDtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICAtd2Via2l0LWJveC1wYWNrOiBmbGV4LXN0YXJ0O1xuICAgIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgIC1tcy1mbGV4LXBhY2s6IGZsZXgtc3RhcnQ7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgdGV4dC1hbmNob3I6IHN0YXJ0OyB9XG4gIFxuICAuY3QtY2hhcnQtYmFyLmN0LWhvcml6b250YWwtYmFycyAuY3QtbGFiZWwuY3QtdmVydGljYWwuY3Qtc3RhcnQge1xuICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XG4gICAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAtd2Via2l0LWJveC1wYWNrOiBmbGV4LWVuZDtcbiAgICAtd2Via2l0LWp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gICAgLW1zLWZsZXgtcGFjazogZmxleC1lbmQ7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICB0ZXh0LWFuY2hvcjogZW5kOyB9XG4gIFxuICAuY3QtY2hhcnQtYmFyLmN0LWhvcml6b250YWwtYmFycyAuY3QtbGFiZWwuY3QtdmVydGljYWwuY3QtZW5kIHtcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xuICAgIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgLXdlYmtpdC1ib3gtcGFjazogZmxleC1zdGFydDtcbiAgICAtd2Via2l0LWp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICAtbXMtZmxleC1wYWNrOiBmbGV4LXN0YXJ0O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIHRleHQtYW5jaG9yOiBlbmQ7IH1cbiAgXG4gIC5jdC1ncmlkIHtcbiAgICBzdHJva2U6IHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgICBzdHJva2Utd2lkdGg6IDFweDtcbiAgICBzdHJva2UtZGFzaGFycmF5OiAycHg7IH1cbiAgXG4gIC5jdC1wb2ludCB7XG4gICAgc3Ryb2tlLXdpZHRoOiA4cHg7XG4gICAgc3Ryb2tlLWxpbmVjYXA6IHJvdW5kOyB9XG4gIFxuICAuY3QtbGluZSB7XG4gICAgZmlsbDogbm9uZTtcbiAgICBzdHJva2Utd2lkdGg6IDNweDsgfVxuICBcbiAgLmN0LWFyZWEge1xuICAgIHN0cm9rZTogbm9uZTtcbiAgICBmaWxsLW9wYWNpdHk6IDAuODsgfVxuICBcbiAgLmN0LWJhciB7XG4gICAgZmlsbDogbm9uZTtcbiAgICBzdHJva2Utd2lkdGg6IDEwcHg7IH1cbiAgXG4gIC5jdC1zbGljZS1kb251dCB7XG4gICAgZmlsbDogbm9uZTtcbiAgICBzdHJva2Utd2lkdGg6IDYwcHg7IH1cbiAgXG4gIC5jdC1zZXJpZXMtYSAuY3QtcG9pbnQsIC5jdC1zZXJpZXMtYSAuY3QtbGluZSwgLmN0LXNlcmllcy1hIC5jdC1iYXIsIC5jdC1zZXJpZXMtYSAuY3Qtc2xpY2UtZG9udXQge1xuICAgIHN0cm9rZTogIzIzQ0NFRjsgfVxuICAuY3Qtc2VyaWVzLWEgLmN0LXNsaWNlLXBpZSwgLmN0LXNlcmllcy1hIC5jdC1hcmVhIHtcbiAgICBmaWxsOiAjMjNDQ0VGOyB9XG4gIFxuICAuY3Qtc2VyaWVzLWIgLmN0LXBvaW50LCAuY3Qtc2VyaWVzLWIgLmN0LWxpbmUsIC5jdC1zZXJpZXMtYiAuY3QtYmFyLCAuY3Qtc2VyaWVzLWIgLmN0LXNsaWNlLWRvbnV0IHtcbiAgICBzdHJva2U6ICNGQjQwNEI7IH1cbiAgLmN0LXNlcmllcy1iIC5jdC1zbGljZS1waWUsIC5jdC1zZXJpZXMtYiAuY3QtYXJlYSB7XG4gICAgZmlsbDogI0ZCNDA0QjsgfVxuICBcbiAgLmN0LXNlcmllcy1jIC5jdC1wb2ludCwgLmN0LXNlcmllcy1jIC5jdC1saW5lLCAuY3Qtc2VyaWVzLWMgLmN0LWJhciwgLmN0LXNlcmllcy1jIC5jdC1zbGljZS1kb251dCB7XG4gICAgc3Ryb2tlOiAjRkZBNTM0OyB9XG4gIC5jdC1zZXJpZXMtYyAuY3Qtc2xpY2UtcGllLCAuY3Qtc2VyaWVzLWMgLmN0LWFyZWEge1xuICAgIGZpbGw6ICNGRkE1MzQ7IH1cbiAgXG4gIC5jdC1zZXJpZXMtZCAuY3QtcG9pbnQsIC5jdC1zZXJpZXMtZCAuY3QtbGluZSwgLmN0LXNlcmllcy1kIC5jdC1iYXIsIC5jdC1zZXJpZXMtZCAuY3Qtc2xpY2UtZG9udXQge1xuICAgIHN0cm9rZTogIzkzNjhFOTsgfVxuICAuY3Qtc2VyaWVzLWQgLmN0LXNsaWNlLXBpZSwgLmN0LXNlcmllcy1kIC5jdC1hcmVhIHtcbiAgICBmaWxsOiAjOTM2OEU5OyB9XG4gIFxuICAuY3Qtc2VyaWVzLWUgLmN0LXBvaW50LCAuY3Qtc2VyaWVzLWUgLmN0LWxpbmUsIC5jdC1zZXJpZXMtZSAuY3QtYmFyLCAuY3Qtc2VyaWVzLWUgLmN0LXNsaWNlLWRvbnV0IHtcbiAgICBzdHJva2U6ICM4N0NCMTY7IH1cbiAgLmN0LXNlcmllcy1lIC5jdC1zbGljZS1waWUsIC5jdC1zZXJpZXMtZSAuY3QtYXJlYSB7XG4gICAgZmlsbDogIzg3Q0IxNjsgfVxuICBcbiAgLmN0LXNlcmllcy1mIC5jdC1wb2ludCwgLmN0LXNlcmllcy1mIC5jdC1saW5lLCAuY3Qtc2VyaWVzLWYgLmN0LWJhciwgLmN0LXNlcmllcy1mIC5jdC1zbGljZS1kb251dCB7XG4gICAgc3Ryb2tlOiAjNDQ3REY3OyB9XG4gIC5jdC1zZXJpZXMtZiAuY3Qtc2xpY2UtcGllLCAuY3Qtc2VyaWVzLWYgLmN0LWFyZWEge1xuICAgIGZpbGw6ICM0NDdERjc7IH1cbiAgXG4gIC5jdC1zZXJpZXMtZyAuY3QtcG9pbnQsIC5jdC1zZXJpZXMtZyAuY3QtbGluZSwgLmN0LXNlcmllcy1nIC5jdC1iYXIsIC5jdC1zZXJpZXMtZyAuY3Qtc2xpY2UtZG9udXQge1xuICAgIHN0cm9rZTogIzVlNWU1ZTsgfVxuICAuY3Qtc2VyaWVzLWcgLmN0LXNsaWNlLXBpZSwgLmN0LXNlcmllcy1nIC5jdC1hcmVhIHtcbiAgICBmaWxsOiAjNWU1ZTVlOyB9XG4gIFxuICAuY3Qtc2VyaWVzLWggLmN0LXBvaW50LCAuY3Qtc2VyaWVzLWggLmN0LWxpbmUsIC5jdC1zZXJpZXMtaCAuY3QtYmFyLCAuY3Qtc2VyaWVzLWggLmN0LXNsaWNlLWRvbnV0IHtcbiAgICBzdHJva2U6ICNkZDRiMzk7IH1cbiAgLmN0LXNlcmllcy1oIC5jdC1zbGljZS1waWUsIC5jdC1zZXJpZXMtaCAuY3QtYXJlYSB7XG4gICAgZmlsbDogI2RkNGIzOTsgfVxuICBcbiAgLmN0LXNlcmllcy1pIC5jdC1wb2ludCwgLmN0LXNlcmllcy1pIC5jdC1saW5lLCAuY3Qtc2VyaWVzLWkgLmN0LWJhciwgLmN0LXNlcmllcy1pIC5jdC1zbGljZS1kb251dCB7XG4gICAgc3Ryb2tlOiAjMzU0NjVjOyB9XG4gIC5jdC1zZXJpZXMtaSAuY3Qtc2xpY2UtcGllLCAuY3Qtc2VyaWVzLWkgLmN0LWFyZWEge1xuICAgIGZpbGw6ICMzNTQ2NWM7IH1cbiAgXG4gIC5jdC1zZXJpZXMtaiAuY3QtcG9pbnQsIC5jdC1zZXJpZXMtaiAuY3QtbGluZSwgLmN0LXNlcmllcy1qIC5jdC1iYXIsIC5jdC1zZXJpZXMtaiAuY3Qtc2xpY2UtZG9udXQge1xuICAgIHN0cm9rZTogI2U1MmQyNzsgfVxuICAuY3Qtc2VyaWVzLWogLmN0LXNsaWNlLXBpZSwgLmN0LXNlcmllcy1qIC5jdC1hcmVhIHtcbiAgICBmaWxsOiAjZTUyZDI3OyB9XG4gIFxuICAuY3Qtc2VyaWVzLWsgLmN0LXBvaW50LCAuY3Qtc2VyaWVzLWsgLmN0LWxpbmUsIC5jdC1zZXJpZXMtayAuY3QtYmFyLCAuY3Qtc2VyaWVzLWsgLmN0LXNsaWNlLWRvbnV0IHtcbiAgICBzdHJva2U6ICM1NWFjZWU7IH1cbiAgLmN0LXNlcmllcy1rIC5jdC1zbGljZS1waWUsIC5jdC1zZXJpZXMtayAuY3QtYXJlYSB7XG4gICAgZmlsbDogIzU1YWNlZTsgfVxuICBcbiAgLmN0LXNlcmllcy1sIC5jdC1wb2ludCwgLmN0LXNlcmllcy1sIC5jdC1saW5lLCAuY3Qtc2VyaWVzLWwgLmN0LWJhciwgLmN0LXNlcmllcy1sIC5jdC1zbGljZS1kb251dCB7XG4gICAgc3Ryb2tlOiAjY2MyMTI3OyB9XG4gIC5jdC1zZXJpZXMtbCAuY3Qtc2xpY2UtcGllLCAuY3Qtc2VyaWVzLWwgLmN0LWFyZWEge1xuICAgIGZpbGw6ICNjYzIxMjc7IH1cbiAgXG4gIC5jdC1zZXJpZXMtbSAuY3QtcG9pbnQsIC5jdC1zZXJpZXMtbSAuY3QtbGluZSwgLmN0LXNlcmllcy1tIC5jdC1iYXIsIC5jdC1zZXJpZXMtbSAuY3Qtc2xpY2UtZG9udXQge1xuICAgIHN0cm9rZTogIzE3NjlmZjsgfVxuICAuY3Qtc2VyaWVzLW0gLmN0LXNsaWNlLXBpZSwgLmN0LXNlcmllcy1tIC5jdC1hcmVhIHtcbiAgICBmaWxsOiAjMTc2OWZmOyB9XG4gIFxuICAuY3Qtc2VyaWVzLW4gLmN0LXBvaW50LCAuY3Qtc2VyaWVzLW4gLmN0LWxpbmUsIC5jdC1zZXJpZXMtbiAuY3QtYmFyLCAuY3Qtc2VyaWVzLW4gLmN0LXNsaWNlLWRvbnV0IHtcbiAgICBzdHJva2U6ICM2MTg4ZTI7IH1cbiAgLmN0LXNlcmllcy1uIC5jdC1zbGljZS1waWUsIC5jdC1zZXJpZXMtbiAuY3QtYXJlYSB7XG4gICAgZmlsbDogIzYxODhlMjsgfVxuICBcbiAgLmN0LXNlcmllcy1vIC5jdC1wb2ludCwgLmN0LXNlcmllcy1vIC5jdC1saW5lLCAuY3Qtc2VyaWVzLW8gLmN0LWJhciwgLmN0LXNlcmllcy1vIC5jdC1zbGljZS1kb251dCB7XG4gICAgc3Ryb2tlOiAjYTc0OGNhOyB9XG4gIC5jdC1zZXJpZXMtbyAuY3Qtc2xpY2UtcGllLCAuY3Qtc2VyaWVzLW8gLmN0LWFyZWEge1xuICAgIGZpbGw6ICNhNzQ4Y2E7IH1cbiAgXG4gIC5jdC1zcXVhcmUge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogMTAwJTsgfVxuICAgIC5jdC1zcXVhcmU6YmVmb3JlIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgd2lkdGg6IDA7XG4gICAgICBoZWlnaHQ6IDA7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogMTAwJTsgfVxuICAgIC5jdC1zcXVhcmU6YWZ0ZXIge1xuICAgICAgY29udGVudDogXCJcIjtcbiAgICAgIGRpc3BsYXk6IHRhYmxlO1xuICAgICAgY2xlYXI6IGJvdGg7IH1cbiAgICAuY3Qtc3F1YXJlID4gc3ZnIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgdG9wOiAwO1xuICAgICAgbGVmdDogMDsgfVxuICBcbiAgLmN0LW1pbm9yLXNlY29uZCB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlOyB9XG4gICAgLmN0LW1pbm9yLXNlY29uZDpiZWZvcmUge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBmbG9hdDogbGVmdDtcbiAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICB3aWR0aDogMDtcbiAgICAgIGhlaWdodDogMDtcbiAgICAgIHBhZGRpbmctYm90dG9tOiA5My43NSU7IH1cbiAgICAuY3QtbWlub3Itc2Vjb25kOmFmdGVyIHtcbiAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICBkaXNwbGF5OiB0YWJsZTtcbiAgICAgIGNsZWFyOiBib3RoOyB9XG4gICAgLmN0LW1pbm9yLXNlY29uZCA+IHN2ZyB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIHRvcDogMDtcbiAgICAgIGxlZnQ6IDA7IH1cbiAgXG4gIC5jdC1tYWpvci1zZWNvbmQge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogMTAwJTsgfVxuICAgIC5jdC1tYWpvci1zZWNvbmQ6YmVmb3JlIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgd2lkdGg6IDA7XG4gICAgICBoZWlnaHQ6IDA7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogODguODg4ODklOyB9XG4gICAgLmN0LW1ham9yLXNlY29uZDphZnRlciB7XG4gICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgZGlzcGxheTogdGFibGU7XG4gICAgICBjbGVhcjogYm90aDsgfVxuICAgIC5jdC1tYWpvci1zZWNvbmQgPiBzdmcge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB0b3A6IDA7XG4gICAgICBsZWZ0OiAwOyB9XG4gIFxuICAuY3QtbWlub3ItdGhpcmQge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogMTAwJTsgfVxuICAgIC5jdC1taW5vci10aGlyZDpiZWZvcmUge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBmbG9hdDogbGVmdDtcbiAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICB3aWR0aDogMDtcbiAgICAgIGhlaWdodDogMDtcbiAgICAgIHBhZGRpbmctYm90dG9tOiA4My4zMzMzMyU7IH1cbiAgICAuY3QtbWlub3ItdGhpcmQ6YWZ0ZXIge1xuICAgICAgY29udGVudDogXCJcIjtcbiAgICAgIGRpc3BsYXk6IHRhYmxlO1xuICAgICAgY2xlYXI6IGJvdGg7IH1cbiAgICAuY3QtbWlub3ItdGhpcmQgPiBzdmcge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB0b3A6IDA7XG4gICAgICBsZWZ0OiAwOyB9XG4gIFxuICAuY3QtbWFqb3ItdGhpcmQge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogMTAwJTsgfVxuICAgIC5jdC1tYWpvci10aGlyZDpiZWZvcmUge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBmbG9hdDogbGVmdDtcbiAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICB3aWR0aDogMDtcbiAgICAgIGhlaWdodDogMDtcbiAgICAgIHBhZGRpbmctYm90dG9tOiA4MCU7IH1cbiAgICAuY3QtbWFqb3ItdGhpcmQ6YWZ0ZXIge1xuICAgICAgY29udGVudDogXCJcIjtcbiAgICAgIGRpc3BsYXk6IHRhYmxlO1xuICAgICAgY2xlYXI6IGJvdGg7IH1cbiAgICAuY3QtbWFqb3ItdGhpcmQgPiBzdmcge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB0b3A6IDA7XG4gICAgICBsZWZ0OiAwOyB9XG4gIFxuICAuY3QtcGVyZmVjdC1maWZ0aCB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlOyB9XG4gICAgLmN0LXBlcmZlY3QtZmlmdGg6YmVmb3JlIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgd2lkdGg6IDA7XG4gICAgICBoZWlnaHQ6IDA7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogNzUlOyB9XG4gICAgLmN0LXBlcmZlY3QtZmlmdGg6YWZ0ZXIge1xuICAgICAgY29udGVudDogXCJcIjtcbiAgICAgIGRpc3BsYXk6IHRhYmxlO1xuICAgICAgY2xlYXI6IGJvdGg7IH1cbiAgICAuY3QtcGVyZmVjdC1maWZ0aCA+IHN2ZyB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIHRvcDogMDtcbiAgICAgIGxlZnQ6IDA7IH1cbiAgXG4gIC5jdC1taW5vci1zaXh0aCB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlOyB9XG4gICAgLmN0LW1pbm9yLXNpeHRoOmJlZm9yZSB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgY29udGVudDogXCJcIjtcbiAgICAgIHdpZHRoOiAwO1xuICAgICAgaGVpZ2h0OiAwO1xuICAgICAgcGFkZGluZy1ib3R0b206IDY2LjY2NjY3JTsgfVxuICAgIC5jdC1taW5vci1zaXh0aDphZnRlciB7XG4gICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgZGlzcGxheTogdGFibGU7XG4gICAgICBjbGVhcjogYm90aDsgfVxuICAgIC5jdC1taW5vci1zaXh0aCA+IHN2ZyB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIHRvcDogMDtcbiAgICAgIGxlZnQ6IDA7IH1cbiAgXG4gIC5jdC1nb2xkZW4tc2VjdGlvbiB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlOyB9XG4gICAgLmN0LWdvbGRlbi1zZWN0aW9uOmJlZm9yZSB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgY29udGVudDogXCJcIjtcbiAgICAgIHdpZHRoOiAwO1xuICAgICAgaGVpZ2h0OiAwO1xuICAgICAgcGFkZGluZy1ib3R0b206IDYyLjUlOyB9XG4gICAgLmN0LWdvbGRlbi1zZWN0aW9uOmFmdGVyIHtcbiAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICBkaXNwbGF5OiB0YWJsZTtcbiAgICAgIGNsZWFyOiBib3RoOyB9XG4gICAgLmN0LWdvbGRlbi1zZWN0aW9uID4gc3ZnIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgdG9wOiAwO1xuICAgICAgbGVmdDogMDsgfVxuICBcbiAgLmN0LW1ham9yLXNpeHRoIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6IDEwMCU7IH1cbiAgICAuY3QtbWFqb3Itc2l4dGg6YmVmb3JlIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgd2lkdGg6IDA7XG4gICAgICBoZWlnaHQ6IDA7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogNjEuODA0NyU7IH1cbiAgICAuY3QtbWFqb3Itc2l4dGg6YWZ0ZXIge1xuICAgICAgY29udGVudDogXCJcIjtcbiAgICAgIGRpc3BsYXk6IHRhYmxlO1xuICAgICAgY2xlYXI6IGJvdGg7IH1cbiAgICAuY3QtbWFqb3Itc2l4dGggPiBzdmcge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB0b3A6IDA7XG4gICAgICBsZWZ0OiAwOyB9XG4gIFxuICAuY3QtbWlub3Itc2V2ZW50aCB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlOyB9XG4gICAgLmN0LW1pbm9yLXNldmVudGg6YmVmb3JlIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgd2lkdGg6IDA7XG4gICAgICBoZWlnaHQ6IDA7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogNjAlOyB9XG4gICAgLmN0LW1pbm9yLXNldmVudGg6YWZ0ZXIge1xuICAgICAgY29udGVudDogXCJcIjtcbiAgICAgIGRpc3BsYXk6IHRhYmxlO1xuICAgICAgY2xlYXI6IGJvdGg7IH1cbiAgICAuY3QtbWlub3Itc2V2ZW50aCA+IHN2ZyB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIHRvcDogMDtcbiAgICAgIGxlZnQ6IDA7IH1cbiAgXG4gIC5jdC1tYWpvci1zZXZlbnRoIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6IDEwMCU7IH1cbiAgICAuY3QtbWFqb3Itc2V2ZW50aDpiZWZvcmUge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBmbG9hdDogbGVmdDtcbiAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICB3aWR0aDogMDtcbiAgICAgIGhlaWdodDogMDtcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1Ni4yNSU7IH1cbiAgICAuY3QtbWFqb3Itc2V2ZW50aDphZnRlciB7XG4gICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgZGlzcGxheTogdGFibGU7XG4gICAgICBjbGVhcjogYm90aDsgfVxuICAgIC5jdC1tYWpvci1zZXZlbnRoID4gc3ZnIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgdG9wOiAwO1xuICAgICAgbGVmdDogMDsgfVxuICBcbiAgLmN0LW9jdGF2ZSB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlOyB9XG4gICAgLmN0LW9jdGF2ZTpiZWZvcmUge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBmbG9hdDogbGVmdDtcbiAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICB3aWR0aDogMDtcbiAgICAgIGhlaWdodDogMDtcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1My4zMzMzMyU7IH1cbiAgICAuY3Qtb2N0YXZlOmFmdGVyIHtcbiAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICBkaXNwbGF5OiB0YWJsZTtcbiAgICAgIGNsZWFyOiBib3RoOyB9XG4gICAgLmN0LW9jdGF2ZSA+IHN2ZyB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIHRvcDogMDtcbiAgICAgIGxlZnQ6IDA7IH1cbiAgXG4gIC5jdC1tYWpvci10ZW50aCB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlOyB9XG4gICAgLmN0LW1ham9yLXRlbnRoOmJlZm9yZSB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgY29udGVudDogXCJcIjtcbiAgICAgIHdpZHRoOiAwO1xuICAgICAgaGVpZ2h0OiAwO1xuICAgICAgcGFkZGluZy1ib3R0b206IDUwJTsgfVxuICAgIC5jdC1tYWpvci10ZW50aDphZnRlciB7XG4gICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgZGlzcGxheTogdGFibGU7XG4gICAgICBjbGVhcjogYm90aDsgfVxuICAgIC5jdC1tYWpvci10ZW50aCA+IHN2ZyB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIHRvcDogMDtcbiAgICAgIGxlZnQ6IDA7IH1cbiAgXG4gIC5jdC1tYWpvci1lbGV2ZW50aCB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlOyB9XG4gICAgLmN0LW1ham9yLWVsZXZlbnRoOmJlZm9yZSB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgY29udGVudDogXCJcIjtcbiAgICAgIHdpZHRoOiAwO1xuICAgICAgaGVpZ2h0OiAwO1xuICAgICAgcGFkZGluZy1ib3R0b206IDQwJTsgfVxuICAgIC5jdC1tYWpvci1lbGV2ZW50aDphZnRlciB7XG4gICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgZGlzcGxheTogdGFibGU7XG4gICAgICBjbGVhcjogYm90aDsgfVxuICAgIC5jdC1tYWpvci1lbGV2ZW50aCA+IHN2ZyB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIHRvcDogMDtcbiAgICAgIGxlZnQ6IDA7IH1cbiAgXG4gIC5jdC1tYWpvci10d2VsZnRoIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6IDEwMCU7IH1cbiAgICAuY3QtbWFqb3ItdHdlbGZ0aDpiZWZvcmUge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBmbG9hdDogbGVmdDtcbiAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICB3aWR0aDogMDtcbiAgICAgIGhlaWdodDogMDtcbiAgICAgIHBhZGRpbmctYm90dG9tOiAzNy41JTsgfVxuICAgIC5jdC1tYWpvci10d2VsZnRoOmFmdGVyIHtcbiAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICBkaXNwbGF5OiB0YWJsZTtcbiAgICAgIGNsZWFyOiBib3RoOyB9XG4gICAgLmN0LW1ham9yLXR3ZWxmdGggPiBzdmcge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB0b3A6IDA7XG4gICAgICBsZWZ0OiAwOyB9XG4gIFxuICAuY3QtZG91YmxlLW9jdGF2ZSB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlOyB9XG4gICAgLmN0LWRvdWJsZS1vY3RhdmU6YmVmb3JlIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgd2lkdGg6IDA7XG4gICAgICBoZWlnaHQ6IDA7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogMzMuMzMzMzMlOyB9XG4gICAgLmN0LWRvdWJsZS1vY3RhdmU6YWZ0ZXIge1xuICAgICAgY29udGVudDogXCJcIjtcbiAgICAgIGRpc3BsYXk6IHRhYmxlO1xuICAgICAgY2xlYXI6IGJvdGg7IH1cbiAgICAuY3QtZG91YmxlLW9jdGF2ZSA+IHN2ZyB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIHRvcDogMDtcbiAgICAgIGxlZnQ6IDA7IH1cbiAgXG4gIC5jdC1ibHVlIHtcbiAgICBzdHJva2U6ICM0NDdERjcgIWltcG9ydGFudDsgfVxuICBcbiAgLmN0LWF6dXJlIHtcbiAgICBzdHJva2U6ICMyM0NDRUYgIWltcG9ydGFudDsgfVxuICBcbiAgLmN0LWdyZWVuIHtcbiAgICBzdHJva2U6ICM4N0NCMTYgIWltcG9ydGFudDsgfVxuICBcbiAgLmN0LW9yYW5nZSB7XG4gICAgc3Ryb2tlOiAjRkZBNTM0ICFpbXBvcnRhbnQ7IH1cbiAgXG4gIC5jdC1yZWQge1xuICAgIHN0cm9rZTogI0ZCNDA0QiAhaW1wb3J0YW50OyB9XG4gIFxuICAgIHRyLnN0cmlrZW91dCB0ZDpiZWZvcmUge1xuICAgICAgY29udGVudDogXCIgXCI7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB0b3A6IDUwJTtcbiAgICAgIGxlZnQ6IDA7XG4gICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzExMTtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICB9Il19 */"

/***/ }),

/***/ "./src/app/sales/invoices-out/invoices-out.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/sales/invoices-out/invoices-out.component.ts ***!
  \**************************************************************/
/*! exports provided: InvoicesOutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicesOutComponent", function() { return InvoicesOutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var app_sales_invoice_payment_invoice_payment_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/sales/invoice-payment/invoice-payment.component */ "./src/app/sales/invoice-payment/invoice-payment.component.ts");
/* harmony import */ var _approve_modal_approve_modal_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../approve-modal/approve-modal.component */ "./src/app/sales/approve-modal/approve-modal.component.ts");
/* harmony import */ var app_lbd_lbd_chart_lbd_chart_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/lbd/lbd-chart/lbd-chart.component */ "./src/app/lbd/lbd-chart/lbd-chart.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var InvoicesOutComponent = /** @class */ (function () {
    function InvoicesOutComponent(apiService, router, modalService) {
        this.apiService = apiService;
        this.router = router;
        this.modalService = modalService;
        this.invoices = [];
        this.invoiceDueDates = [];
        this.showHideAccor = 0;
        this.series = [];
        this.totals = [];
        this.businessID = localStorage.getItem("businessID");
        this.userType = localStorage.getItem("userType");
        /*
          this.dataTable = {
              headerRow: ['#', 'Due', 'Invoice Number', 'Customer', 'Issued', 'Amount', 'Status', 'Action'],
              footerRow: ['Row Number', 'Due Date', 'Description', 'Customer', 'ßIssue Date', 'Amount', 'Status', 'Action'],
              dataRows: []
          };
          */
    }
    InvoicesOutComponent.prototype.showAccord = function () {
        this.showHideAccor = (this.showHideAccor == 1) ? 0 : 1;
    };
    InvoicesOutComponent.prototype.ngOnInit = function () {
        this.getInvoices('1/' + this.businessID + '/0');
        this.getInvoiceDueDates();
        // graph
        this.activityChartType = app_lbd_lbd_chart_lbd_chart_component__WEBPACK_IMPORTED_MODULE_6__["ChartType"].Bar;
        this.activityChartData = {
            labels: ['Upcoming', '1-30 days', '31-60 days', '61-90 days', '90 days'],
            series: this.series
        };
        this.activityChartOptions = {
            seriesBarDistance: 10,
            axisX: {
                showGrid: true
            },
            height: '240px'
        };
        this.activityChartResponsive = [
            ['screen and (max-width: 640px)', {
                    seriesBarDistance: 10,
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
        ];
        this.activityChartLegendItems = [
            { title: 'TRY', imageClass: 'fa fa-circle text-info' },
            { title: 'USD', imageClass: 'fa fa-circle text-danger' },
            { title: 'EUR', imageClass: 'fa fa-circle text-warning' },
            { title: 'GBP', imageClass: 'fa fa-circle text-success' }
        ];
    };
    InvoicesOutComponent.prototype.calculateDays = function (startTime, endTime) {
        var date2 = endTime;
        var diffInMs = Date.parse(date2) - Date.parse(Date());
        var diffInHours = diffInMs / 1000 / 60 / 60 / 24;
        if (isNaN(diffInHours)) {
            diffInHours = 9999;
        }
        return Math.round(diffInHours);
    };
    InvoicesOutComponent.prototype.getInvoices = function (invoiceID) {
        var _this = this;
        this.apiService.getData(invoiceID, "invoices/").then(function (result) {
            _this.invoices = result;
            _this.dataTable = {
                headerRow: ['#', 'Due', 'Invoice Number', 'Customer', 'Issued', 'Amount', 'Status', 'Action'],
                footerRow: ['Row Number', 'Due Date', 'Description', 'Customer', 'ßIssue Date', 'Amount', 'Status', 'Action'],
                dataRows: _this.invoices
            };
            //console.log(this.dataTable);
        });
    };
    InvoicesOutComponent.prototype.getInvoiceDueDates = function () {
        var _this = this;
        this.apiService.getData(this.businessID + "/1", "invoiceDueDates/").then(function (result) {
            _this.invoiceDueDates = result;
            var vlTRY = [_this.invoiceDueDates[0].TRY * 1, _this.invoiceDueDates[1].TRY * 1, _this.invoiceDueDates[2].TRY * 1, _this.invoiceDueDates[3].TRY * 1];
            var vlUSD = [_this.invoiceDueDates[0].USD * 1, _this.invoiceDueDates[1].USD * 1, _this.invoiceDueDates[2].USD * 1, _this.invoiceDueDates[3].USD * 1];
            var vlEUR = [_this.invoiceDueDates[0].EUR * 1, _this.invoiceDueDates[1].EUR * 1, _this.invoiceDueDates[2].EUR * 1, _this.invoiceDueDates[3].EUR * 1];
            var vlGBP = [_this.invoiceDueDates[0].GBP * 1, _this.invoiceDueDates[1].GBP * 1, _this.invoiceDueDates[2].GBP * 1, _this.invoiceDueDates[3].GBP * 1];
            _this.totals = { "totalTRY": _this.invoiceDueDates[0].TRY * 1 + _this.invoiceDueDates[1].TRY * 1 + _this.invoiceDueDates[2].TRY * 1 + _this.invoiceDueDates[3].TRY * 1,
                "totalUSD": _this.invoiceDueDates[0].USD * 1 + _this.invoiceDueDates[1].USD * 1 + _this.invoiceDueDates[2].USD * 1 + _this.invoiceDueDates[3].USD * 1,
                "totalEUR": _this.invoiceDueDates[0].EUR * 1 + _this.invoiceDueDates[1].EUR * 1 + _this.invoiceDueDates[2].EUR * 1 + _this.invoiceDueDates[3].EUR * 1,
                "totalGBP": _this.invoiceDueDates[0].GBP * 1 + _this.invoiceDueDates[1].GBP * 1 + _this.invoiceDueDates[2].GBP * 1 + _this.invoiceDueDates[3].GBP * 1 };
            console.log(_this.totals, "inv due dates");
            _this.series.push(vlTRY);
            _this.series.push(vlUSD);
            _this.series.push(vlEUR);
            _this.series.push(vlGBP);
            console.log(_this.series, "series");
            console.log(vlTRY, "total");
        });
    };
    InvoicesOutComponent.prototype.cloneInvoice = function (invoiceID) {
        var _this = this;
        this.apiService.postData(invoiceID, "invoice/clone/", null).then(function (result) {
            console.log(result, "clone");
            var nInvoiceID = result;
            if (nInvoiceID > 0) {
                _this.getInvoices('1/' + _this.businessID + '/0');
            }
        });
    };
    InvoicesOutComponent.prototype.deleteInvoice = function (invoiceID, index) {
        var _this = this;
        if (confirm("Are you sure ?")) {
            this.apiService.deleteData(invoiceID, "/invoice/delete/").then(function (result) {
                console.log(result);
                _this.invoices.splice(index, 1);
            });
        }
        ;
    };
    InvoicesOutComponent.prototype.approveInvoice = function (invoiceID, index) {
        var _this = this;
        var modalRef = this.modalService.open(_approve_modal_approve_modal_component__WEBPACK_IMPORTED_MODULE_5__["ApproveModalComponent"], { "backdrop": "static" });
        modalRef.componentInstance.invoiceID = invoiceID;
        modalRef.result.then(function (data) {
            // on close
            console.log(data, "result data");
            if (data) {
                if (data.statusID > 0) {
                    _this.invoices[index].statusID = data.statusID;
                    _this.invoices[index].status = data.status;
                }
            }
        }, function () {
            // on dismiss
        });
        /*
         if (confirm("Are you sure ?")) {
             this.apiService.getData('1/' + this.businessID + '/' + invoiceID, "invoices/").then((result) => {
                 let inv = result[0];
                 inv.status = 3;
                 this.apiService.postData(invoiceID, "/invoice/save/", JSON.stringify(inv)).then((result) => {
                     console.log(result);
                     this.invoices[index].statusID = 3;
                     this.invoices[index].status = "Approved";
                     alert("Approved");
                 });
             });
         }
         */
    };
    InvoicesOutComponent.prototype.callInvoice = function (invoiceType) {
        this.router.navigate(["/sales/invoice"], { queryParams: { invoiceType: invoiceType } });
    };
    InvoicesOutComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                paginate: {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                }
            }
        });
    };
    InvoicesOutComponent.prototype.openModal = function (invoiceID, index) {
        var _this = this;
        console.log(index, "ondex ID");
        var modalRef = this.modalService.open(app_sales_invoice_payment_invoice_payment_component__WEBPACK_IMPORTED_MODULE_4__["InvoicePaymentComponent"], { "backdrop": "static" });
        modalRef.componentInstance.invoiceID = invoiceID;
        modalRef.componentInstance.paymentType = "Received";
        modalRef.result.then(function (data) {
            // on close
            console.log(data, "result data");
            if (data > 0) {
                _this.invoices[index].paymentID = data;
            }
        }, function () {
            // on dismiss
        });
    };
    InvoicesOutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-invoices-out',
            template: __webpack_require__(/*! ./invoices-out.component.html */ "./src/app/sales/invoices-out/invoices-out.component.html"),
            styles: [__webpack_require__(/*! ./invoices-out.component.scss */ "./src/app/sales/invoices-out/invoices-out.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]])
    ], InvoicesOutComponent);
    return InvoicesOutComponent;
}());



/***/ }),

/***/ "./src/app/sales/product/product.component.html":
/*!******************************************************!*\
  !*** ./src/app/sales/product/product.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <form method=\"post\" class=\"form-horizontal\">\n                    <h4 class=\"title\">New/Edit Product & Service</h4>\n                    <div class=\"card\">\n                        <!-- mains will be here -->\n                        <div class=\"header\">\n                            <legend>Product & Service Card</legend>\n                        </div>\n                        <div class=\"content\">\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Name</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"product\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"product.product\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Description</label>\n                                    <div class=\"col-sm-10\">\n                                        <textarea class=\"form-control\" placeholder=\"Description\" rows=\"3\" name=\"description\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"product.description\"></textarea>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Price</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"price\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"product.price\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Currency</label>\n                                    <div class=\"col-sm-10\">\n                                        <select name=\"currency\" [(ngModel)]=\"product.currency\" \n                                            class=\"selectpicker selectCurrency\" data-title=\"Currency\" data-style=\"btn-default btn-block\"\n                                            data-menu-style=\"dropdown-blue\">\n                                            <option [value]=\"1\">TRY</option>\n                                            <option [value]=\"2\">USD</option>\n                                            <option [value]=\"3\">EUR</option>\n                                            <option [value]=\"4\">GBP</option>\n\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Buy / Sell This\n                                    </label>\n                                    <div class=\"col-sm-10\">\n                                        <select name=\"isVendor\" [(ngModel)]=\"product.isVendor\" class=\"selectpicker selectisVendor\"\n                                            (change)=\"clearValues()\" data-title=\"Buy / Sell\" data-style=\"btn-default btn-block\"\n                                            data-menu-style=\"dropdown-blue\">\n                                            <option [value]=\"2\">Buy</option>\n                                            <option [value]=\"1\">Sell</option>\n                                            <option [value]=\"3\">Both</option>\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">\n                                    </label>\n                                    <div class=\"col-sm-10\">\n                                        <span class=\"col-md-12\" style='font-size: 10px;color:gray;'>Allow the above\n                                            product/service to be added to Invoices/Bills</span>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Expense Account</label>\n                                    <div class=\"col-sm-4\">\n                                        <input type=\"text\" #accountBuy placeholder=\"\" class=\"form-control\" name=\"accountBuy\"\n                                            disabled [ngModelOptions]=\"{standalone: true}\"\n                                            [(ngModel)]=\"product.accountBuy\">\n                                    </div>\n                                    <label class=\"col-sm-2 control-label\">Income Account</label>\n                                    <div class=\"col-sm-4\">\n                                            <select #accountSell name=\"accountSell\" [(ngModel)]=\"product.accountSell\" class=\"selectpicker selectAccountSell\" \n                                            data-title=\"Select To\" data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                            <option *ngFor=\"let coa of chartOfAccounts\" [value]=\"(coa.account+'.'+coa.subAccount)\">{{coa.account+\".\"+coa.subAccount+\" \"+coa.accName+\" (\"+coa.masterAccName+\")\"}}</option>\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Tax</label>\n                                    <div class=\"col-sm-10\">\n                                        <select name=\"taxRate\" [(ngModel)]=\"product.taxRate\" class=\"selectpicker selecttaxRate\"\n                                            data-title=\"Tax\" data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                            <option [value]=0>0%</option>\n                                            <option [value]=1>1%</option>\n                                            <option [value]=8>8%</option>\n                                            <option [value]=18>18%</option>\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n                        </div>\n                    </div>\n                    <div class=\"content\">\n                        <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"deleteProduct()\">DELETE</button>\n                        <div style='float:right;'>\n                            <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                            <button class=\"btn btn-success btn-fill btn-wd\" (click)=\"saveProduct()\">SAVE</button>\n                        </div>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>"

/***/ }),

/***/ "./src/app/sales/product/product.component.scss":
/*!******************************************************!*\
  !*** ./src/app/sales/product/product.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NhbGVzL3Byb2R1Y3QvcHJvZHVjdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/sales/product/product.component.ts":
/*!****************************************************!*\
  !*** ./src/app/sales/product/product.component.ts ***!
  \****************************************************/
/*! exports provided: ProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductComponent", function() { return ProductComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductComponent = /** @class */ (function () {
    function ProductComponent(apiService, router, activeRoute) {
        var _this = this;
        this.apiService = apiService;
        this.router = router;
        this.activeRoute = activeRoute;
        this.product = [];
        this.chartOfAccounts = [];
        var routeParams = this.activeRoute.snapshot.params;
        this.productID = routeParams.id;
        this.activeRoute.queryParams
            .subscribe(function (params) {
            // Defaults to 0 if no query param provided.
            _this.isVendor = +params['isVendor'] || 0;
        });
        console.log(this.productID, "productID");
        this.businessID = localStorage.getItem("businessID");
        this.getChartOfAccounts();
        this.getProduct(this.businessID + "/" + this.isVendor + "/" + this.productID);
    }
    ProductComponent.prototype.ngOnInit = function () {
        $(".selectpicker").selectpicker();
    };
    ProductComponent.prototype.getProduct = function (productID) {
        var _this = this;
        if (this.productID > 0) {
            this.apiService.getData(productID, "products/").then(function (result) {
                _this.product = result[0];
                console.log(_this.product, "product");
                _this.spInit();
            });
        }
        else {
            this.product = { "ID": 0, "asID": this.businessID, "product": "", "price": 0, "description": "", "account": "", "taxRate": 0, "isVendor": 0, "currency": 0 };
            console.log(this.product);
        }
    };
    ProductComponent.prototype.getChartOfAccounts = function () {
        var _this = this;
        this.apiService.getData(this.businessID + "/600,601", "chartOfAccounts/").then(function (result) {
            _this.chartOfAccounts = result;
            console.log(_this.chartOfAccounts, "chartofaccounts");
        });
    };
    ProductComponent.prototype.saveProduct = function () {
        var _this = this;
        this.apiService.postData(this.product.ID, "product/save/", JSON.stringify(this.product)).then(function (result) {
            console.log(result);
            if (_this.isVendor == 1) {
                _this.router.navigateByUrl("/sales/products");
            }
            else {
                _this.router.navigateByUrl("/costs/products");
            }
        });
    };
    ProductComponent.prototype.deleteProduct = function () {
        var _this = this;
        this.apiService.deleteData(this.product.ID, "product/delete/").then(function (result) {
            console.log(result);
            if (_this.isVendor == 1) {
                _this.router.navigateByUrl("/sales/products");
            }
            else {
                _this.router.navigateByUrl("/costs/products");
            }
        });
    };
    ProductComponent.prototype.clickCancel = function () {
        if (this.isVendor == 1) {
            this.router.navigateByUrl("/sales/products");
        }
        else {
            this.router.navigateByUrl("/costs/products");
        }
    };
    ProductComponent.prototype.spInit = function () {
        console.log("Init Select");
        $(".selectisVendor").selectpicker('val', this.product.isVendor);
        $(".selecttaxRate").selectpicker('val', this.product.taxRate);
        $(".selectCurrency").selectpicker('val', this.product.currency);
        $(".selectAccountSell").selectpicker('val', this.product.accountSell);
        $(".selectAccountSell").selectpicker('refresh');
    };
    ProductComponent.prototype.clearValues = function () {
        console.log(this.product.isVendor);
        if (this.product.isVendor == 2) {
            this.accountBuy.nativeElement.value = "";
        }
        if (this.product.isVendor == 1) {
            this.accountSell.nativeElement.value = "";
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('accountBuy'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ProductComponent.prototype, "accountBuy", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('accountSell'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ProductComponent.prototype, "accountSell", void 0);
    ProductComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product',
            template: __webpack_require__(/*! ./product.component.html */ "./src/app/sales/product/product.component.html"),
            styles: [__webpack_require__(/*! ./product.component.scss */ "./src/app/sales/product/product.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], ProductComponent);
    return ProductComponent;
}());



/***/ }),

/***/ "./src/app/sales/products/products.component.html":
/*!********************************************************!*\
  !*** ./src/app/sales/products/products.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n\n  \n              <div class=\"card\">\n                  <div class=\"content\">\n                        <h4 class=\"title\">{{\"products\" | translate |titlecase}} \n                                <span style='float:right;'>\n                                        <a routerLink='/sales/product' [queryParams]=\"{isVendor:1}\" style='color:gray;margin-right:10px;font-size:16px;'><i class='fa fa-plus-square-o'></i></a>\n                                </span>\n                            </h4>\n                            <p class=\"category\">{{\"These are the products you can sell\"| translate |titlecase}}</p>\n                        <!-- <legend></legend> -->\n                        <br>\n                      <div class=\"toolbar\">\n                          \n                          <!--        Here you can write extra buttons/actions for the toolbar              -->\n                      </div>\n                      <div class=\"fresh-datatables\">\n                          <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                              <thead>\n                                  <tr>\n                                    <th style=\"width:5%;\">#</th>\n                                    <th >{{\"Name\" | translate |titlecase}}</th>\n                                    <th style=\"width:5%;\" class=\"text-center \"></th>\n                                    <th style=\"width:5%;\" class=\"text-center \">{{\"Tax\" | translate |titlecase}}</th>\n                                    <th style=\"width:10%;\" class=\"text-right \">{{\"Price\" | translate |titlecase}}</th>\n                                    <th style=\"width:10%;\" class=\"text-right\">{{\"Actions\" | translate |titlecase}}</th>\n                                  </tr>\n                              </thead>\n                              <tbody>\n                                      <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                          <td>{{i+1}}</td>\n                                          <td><a routerLink='/sales/product/{{row.ID}}' [queryParams]=\"{isVendor:1}\">{{row.product}}</a></td>\n                                          <td class=\"text-center\"><span class=\"btn btn-success btn-xs\">{{row.currency| uppercase}}</span></td>\n                                          <td class=\"text-center \">%{{row.taxRate}}</td>\n                                          <td class=\"text-right\">{{row.price | currency : row.currency}}</td>\n                                          <td class=\"text-right tableicon\">\n                                            <a routerLink='/sales/product/{{row.ID}}' [queryParams]=\"{isVendor:1}\"><i class=\"fa fa-search\"> </i>&nbsp;</a>\n                                          </td>\n                                          \n                                      </tr>\n                                  </tbody>\n                          </table>\n                      </div>\n                  </div>\n                  <!-- end content-->\n              </div>\n              <!--  end card  -->\n          </div>\n          <!-- end col-md-12 -->\n      </div>\n      <!-- end row -->\n  </div>\n  </div>\n  "

/***/ }),

/***/ "./src/app/sales/products/products.component.scss":
/*!********************************************************!*\
  !*** ./src/app/sales/products/products.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NhbGVzL3Byb2R1Y3RzL3Byb2R1Y3RzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/sales/products/products.component.ts":
/*!******************************************************!*\
  !*** ./src/app/sales/products/products.component.ts ***!
  \******************************************************/
/*! exports provided: ProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsComponent", function() { return ProductsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductsComponent = /** @class */ (function () {
    function ProductsComponent(apiService) {
        this.apiService = apiService;
        this.products = [];
        /*
         this.dataTable = {
             headerRow: ['#', 'Name', 'Price'],
             footerRow: ['#', 'Name', 'Price'],
             dataRows: []
         };
         */
    }
    ProductsComponent.prototype.ngOnInit = function () {
        this.businessID = localStorage.getItem("businessID");
        this.getProducts(this.businessID + "/1/0");
    };
    ProductsComponent.prototype.getProducts = function (productID) {
        var _this = this;
        this.apiService.getData(productID, "products/").then(function (result) {
            _this.products = result;
            _this.dataTable = {
                headerRow: ['#', 'Name', 'Price'],
                footerRow: ['#', 'Name', 'Price'],
                dataRows: _this.products
            };
            console.log(_this.dataTable);
        });
    };
    ProductsComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }
        });
        /*
        var table = $('#datatables').DataTable();
    
        // Edit record
        table.on( 'click', '.edit', function () {
            var $tr = $(this).closest('tr');
    
            var data = table.row($tr).data();
            alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
        } );
    
        // Delete a record
        table.on( 'click', '.remove', function (e) {
            var $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        } );
    
        //Like record
        table.on( 'click', '.like', function () {
            alert('You clicked on Like button');
        });
        */
    };
    ProductsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-products',
            template: __webpack_require__(/*! ./products.component.html */ "./src/app/sales/products/products.component.html"),
            styles: [__webpack_require__(/*! ./products.component.scss */ "./src/app/sales/products/products.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], ProductsComponent);
    return ProductsComponent;
}());



/***/ }),

/***/ "./src/app/sales/sales.module.ts":
/*!***************************************!*\
  !*** ./src/app/sales/sales.module.ts ***!
  \***************************************/
/*! exports provided: SalesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalesModule", function() { return SalesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _sales_routing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./sales.routing */ "./src/app/sales/sales.routing.ts");
/* harmony import */ var _customers_customers_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./customers/customers.component */ "./src/app/sales/customers/customers.component.ts");
/* harmony import */ var _customer_customer_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./customer/customer.component */ "./src/app/sales/customer/customer.component.ts");
/* harmony import */ var _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./invoice/invoice.component */ "./src/app/sales/invoice/invoice.component.ts");
/* harmony import */ var ng2_currency_mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-currency-mask */ "./node_modules/ng2-currency-mask/index.js");
/* harmony import */ var ng2_currency_mask__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_currency_mask__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _invoices_out_invoices_out_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./invoices-out/invoices-out.component */ "./src/app/sales/invoices-out/invoices-out.component.ts");
/* harmony import */ var _products_products_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./products/products.component */ "./src/app/sales/products/products.component.ts");
/* harmony import */ var _product_product_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./product/product.component */ "./src/app/sales/product/product.component.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var app_lbd_lbd_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! app/lbd/lbd.module */ "./src/app/lbd/lbd.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var SalesModule = /** @class */ (function () {
    function SalesModule() {
    }
    SalesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_sales_routing__WEBPACK_IMPORTED_MODULE_4__["SalesRoutes"]),
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                ng2_currency_mask__WEBPACK_IMPORTED_MODULE_8__["CurrencyMaskModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__["TranslateModule"],
                app_lbd_lbd_module__WEBPACK_IMPORTED_MODULE_13__["LbdModule"]
            ],
            declarations: [
                _customers_customers_component__WEBPACK_IMPORTED_MODULE_5__["CustomersComponent"],
                _customer_customer_component__WEBPACK_IMPORTED_MODULE_6__["CustomerComponent"],
                _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_7__["InvoiceComponent"],
                _invoices_out_invoices_out_component__WEBPACK_IMPORTED_MODULE_9__["InvoicesOutComponent"],
                _products_products_component__WEBPACK_IMPORTED_MODULE_10__["ProductsComponent"],
                _product_product_component__WEBPACK_IMPORTED_MODULE_11__["ProductComponent"]
            ]
        })
    ], SalesModule);
    return SalesModule;
}());



/***/ }),

/***/ "./src/app/sales/sales.routing.ts":
/*!****************************************!*\
  !*** ./src/app/sales/sales.routing.ts ***!
  \****************************************/
/*! exports provided: SalesRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalesRoutes", function() { return SalesRoutes; });
/* harmony import */ var _customers_customers_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./customers/customers.component */ "./src/app/sales/customers/customers.component.ts");
/* harmony import */ var _customer_customer_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./customer/customer.component */ "./src/app/sales/customer/customer.component.ts");
/* harmony import */ var _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./invoice/invoice.component */ "./src/app/sales/invoice/invoice.component.ts");
/* harmony import */ var _products_products_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./products/products.component */ "./src/app/sales/products/products.component.ts");
/* harmony import */ var _invoices_out_invoices_out_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./invoices-out/invoices-out.component */ "./src/app/sales/invoices-out/invoices-out.component.ts");
/* harmony import */ var _product_product_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product/product.component */ "./src/app/sales/product/product.component.ts");
/* harmony import */ var _auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../auth/auth-guard.service */ "./src/app/auth/auth-guard.service.ts");







var SalesRoutes = [
    {
        path: '',
        children: [{
                path: 'invoices',
                component: _invoices_out_invoices_out_component__WEBPACK_IMPORTED_MODULE_4__["InvoicesOutComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    }, {
        path: 'invoice/:id',
        component: _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_2__["InvoiceComponent"],
        canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
    }, {
        path: '',
        children: [{
                path: 'invoice',
                component: _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_2__["InvoiceComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    }, {
        path: '',
        children: [{
                path: 'customers',
                component: _customers_customers_component__WEBPACK_IMPORTED_MODULE_0__["CustomersComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    }, {
        path: 'customer/:id',
        component: _customer_customer_component__WEBPACK_IMPORTED_MODULE_1__["CustomerComponent"],
        canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
    }, {
        path: '',
        children: [{
                path: 'customer',
                component: _customer_customer_component__WEBPACK_IMPORTED_MODULE_1__["CustomerComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    },
    {
        path: '',
        children: [{
                path: 'products',
                component: _products_products_component__WEBPACK_IMPORTED_MODULE_3__["ProductsComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    }, {
        path: 'product/:id',
        component: _product_product_component__WEBPACK_IMPORTED_MODULE_5__["ProductComponent"],
        canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
    }, {
        path: '',
        children: [{
                path: 'product',
                component: _product_product_component__WEBPACK_IMPORTED_MODULE_5__["ProductComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    },
];


/***/ })

}]);
//# sourceMappingURL=sales-sales-module.js.map