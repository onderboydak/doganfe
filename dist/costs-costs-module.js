(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["costs-costs-module"],{

/***/ "./src/app/costs/costs.module.ts":
/*!***************************************!*\
  !*** ./src/app/costs/costs.module.ts ***!
  \***************************************/
/*! exports provided: CostsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CostsModule", function() { return CostsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _costs_routing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./costs.routing */ "./src/app/costs/costs.routing.ts");
/* harmony import */ var _suppliers_suppliers_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./suppliers/suppliers.component */ "./src/app/costs/suppliers/suppliers.component.ts");
/* harmony import */ var _expenses_expenses_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./expenses/expenses.component */ "./src/app/costs/expenses/expenses.component.ts");
/* harmony import */ var _supplier_supplier_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./supplier/supplier.component */ "./src/app/costs/supplier/supplier.component.ts");
/* harmony import */ var _expense_expense_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./expense/expense.component */ "./src/app/costs/expense/expense.component.ts");
/* harmony import */ var _expense_tab_expense_tab_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./expense-tab/expense-tab.component */ "./src/app/costs/expense-tab/expense-tab.component.ts");
/* harmony import */ var _products_products_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./products/products.component */ "./src/app/costs/products/products.component.ts");
/* harmony import */ var _invoices_in_invoices_in_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./invoices-in/invoices-in.component */ "./src/app/costs/invoices-in/invoices-in.component.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var app_lbd_lbd_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! app/lbd/lbd.module */ "./src/app/lbd/lbd.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var CostsModule = /** @class */ (function () {
    function CostsModule() {
    }
    CostsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_costs_routing__WEBPACK_IMPORTED_MODULE_4__["CostsRoutes"]),
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__["TranslateModule"],
                app_lbd_lbd_module__WEBPACK_IMPORTED_MODULE_13__["LbdModule"]
            ],
            declarations: [
                _suppliers_suppliers_component__WEBPACK_IMPORTED_MODULE_5__["SuppliersComponent"],
                _expenses_expenses_component__WEBPACK_IMPORTED_MODULE_6__["ExpensesComponent"],
                _supplier_supplier_component__WEBPACK_IMPORTED_MODULE_7__["SupplierComponent"],
                _expense_expense_component__WEBPACK_IMPORTED_MODULE_8__["ExpenseComponent"],
                _expense_tab_expense_tab_component__WEBPACK_IMPORTED_MODULE_9__["ExpenseTabComponent"],
                _products_products_component__WEBPACK_IMPORTED_MODULE_10__["ProductsComponent"],
                _invoices_in_invoices_in_component__WEBPACK_IMPORTED_MODULE_11__["InvoicesInComponent"]
            ]
        })
    ], CostsModule);
    return CostsModule;
}());



/***/ }),

/***/ "./src/app/costs/costs.routing.ts":
/*!****************************************!*\
  !*** ./src/app/costs/costs.routing.ts ***!
  \****************************************/
/*! exports provided: CostsRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CostsRoutes", function() { return CostsRoutes; });
/* harmony import */ var _expenses_expenses_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./expenses/expenses.component */ "./src/app/costs/expenses/expenses.component.ts");
/* harmony import */ var _suppliers_suppliers_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./suppliers/suppliers.component */ "./src/app/costs/suppliers/suppliers.component.ts");
/* harmony import */ var _expense_expense_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./expense/expense.component */ "./src/app/costs/expense/expense.component.ts");
/* harmony import */ var _supplier_supplier_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./supplier/supplier.component */ "./src/app/costs/supplier/supplier.component.ts");
/* harmony import */ var _products_products_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./products/products.component */ "./src/app/costs/products/products.component.ts");
/* harmony import */ var _invoices_in_invoices_in_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./invoices-in/invoices-in.component */ "./src/app/costs/invoices-in/invoices-in.component.ts");
/* harmony import */ var _auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../auth/auth-guard.service */ "./src/app/auth/auth-guard.service.ts");







var CostsRoutes = [
    {
        path: '',
        children: [{
                path: 'invoices',
                component: _invoices_in_invoices_in_component__WEBPACK_IMPORTED_MODULE_5__["InvoicesInComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    },
    {
        path: '',
        children: [{
                path: 'expenses',
                component: _expenses_expenses_component__WEBPACK_IMPORTED_MODULE_0__["ExpensesComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    }, {
        path: 'expense/:id',
        component: _expense_expense_component__WEBPACK_IMPORTED_MODULE_2__["ExpenseComponent"]
    }, {
        path: '',
        children: [{
                path: 'expense',
                component: _expense_expense_component__WEBPACK_IMPORTED_MODULE_2__["ExpenseComponent"]
            }]
    }, {
        path: '',
        children: [{
                path: 'suppliers',
                component: _suppliers_suppliers_component__WEBPACK_IMPORTED_MODULE_1__["SuppliersComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    }, {
        path: '',
        children: [{
                path: 'supplier',
                component: _supplier_supplier_component__WEBPACK_IMPORTED_MODULE_3__["SupplierComponent"]
            }]
    },
    {
        path: '',
        children: [{
                path: 'products',
                component: _products_products_component__WEBPACK_IMPORTED_MODULE_4__["ProductsComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    }
];


/***/ }),

/***/ "./src/app/costs/expense-tab/expense-tab.component.html":
/*!**************************************************************!*\
  !*** ./src/app/costs/expense-tab/expense-tab.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n\n  <div class=\"container-fluid\">\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"nav-container\" class='float:left;'>\n                  <ul class=\"nav nav-icons\" role=\"tablist\">\n                      <li class=\"active\">\n                          <a href=\"#description-logo\" role=\"tab\" data-toggle=\"tab\">\n                              ALBUMS\n                          </a>\n                      </li>\n                      <li>\n                          <a href=\"#map-logo\" role=\"tab\" data-toggle=\"tab\">\n                              TRACKS\n                          </a>\n                        </li>\n                      <li class=\"\">\n                          <a href=\"#legal-logo\" role=\"tab\" data-toggle=\"tab\">\n                              VIDEOS\n                          </a>\n                      </li>\n                  </ul>\n              </div>\n  \n              <div class=\"tab-content\">\n                  <div class=\"tab-pane active\" id=\"description-logo\">\n                      <div class=\"card\">\n                          <div class=\"header\">\n                              <h4 class=\"title\">Description about product</h4>\n                              <p class=\"category\">More information here</p>\n                          </div>\n  \n                          <div class=\"content\">\n                              <p>Larger, yet dramatically thinner. More powerful, but remarkably power efficient. With a smooth metal surface that seamlessly meets the new Retina HD display.</p>\n                              <p>The first thing you notice when you hold the phone is how great it feels in your hand. There are no distinct edges. No gaps. Just a smooth, seamless bond of metal and glass that feels like one continuous surface.</p>\n                          </div>\n                      </div>\n                  </div>\n  \n  \n                  <div class=\"tab-pane\" id=\"map-logo\">\n                      <div class=\"card\">\n                          <div class=\"header\">\n                              <h4 class=\"title\">Location of product</h4>\n                              <p class=\"category\">Here is some text</p>\n                          </div>\n  \n                          <div class=\"content\">\n                              <p>Another Text. The first thing you notice when you hold the phone is how great it feels in your hand. The cover glass curves down around the sides to meet the anodized aluminum enclosure in a remarkable, simplified design.</p>\n                              <p>Larger, yet dramatically thinner.It’s one continuous form where hardware and software function in perfect unison, creating a new generation of phone that’s better by any measure.</p>\n                          </div>\n                      </div>\n                  </div>\n  \n  \n                  <div class=\"tab-pane\" id=\"legal-logo\">\n                      <div class=\"card\">\n                          <div class=\"header\">\n                              <h4 class=\"title\">Legal items</h4>\n                              <p class=\"category\">More information here</p>\n                          </div>\n  \n                          <div class=\"content\">\n                              <p>The first thing you notice when you hold the phone is how great it feels in your hand. The cover glass curves down around the sides to meet the anodized aluminum enclosure in a remarkable, simplified design.</p>\n                              <p>Larger, yet dramatically thinner.It’s one continuous form where hardware and software function in perfect unison, creating a new generation of phone that’s better by any measure.</p>\n                          </div>\n                      </div>\n                  </div>\n  \n                  <div class=\"tab-pane\" id=\"help-logo\">\n                      <div class=\"card\">\n                          <div class=\"header\">\n                              <h4 class=\"title\">Help center</h4>\n                              <p class=\"category\">More information here</p>\n                          </div>\n  \n                          <div class=\"content\">\n                              <p>From the seamless transition of glass and metal to the streamlined profile, every detail was carefully considered to enhance your experience. So while its display is larger, the phone feels just right.</p>\n                              <p>Another Text. The first thing you notice when you hold the phone is how great it feels in your hand. The cover glass curves down around the sides to meet the anodized aluminum enclosure in a remarkable, simplified design.</p>\n                          </div>\n                      </div>\n                  </div>\n  \n              </div> <!-- end tab content -->\n  \n          </div> <!-- end col-md-8 -->\n  \n      </div> <!-- end row -->\n  \n  </div>\n  </div>\n  "

/***/ }),

/***/ "./src/app/costs/expense-tab/expense-tab.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/costs/expense-tab/expense-tab.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Nvc3RzL2V4cGVuc2UtdGFiL2V4cGVuc2UtdGFiLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/costs/expense-tab/expense-tab.component.ts":
/*!************************************************************!*\
  !*** ./src/app/costs/expense-tab/expense-tab.component.ts ***!
  \************************************************************/
/*! exports provided: ExpenseTabComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpenseTabComponent", function() { return ExpenseTabComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ExpenseTabComponent = /** @class */ (function () {
    function ExpenseTabComponent() {
    }
    ExpenseTabComponent.prototype.ngOnInit = function () {
        $('[data-toggle="collapse-hover"]').each(function () {
            var thisdiv = $(this).attr("data-target");
            $(thisdiv).addClass("collapse-hover");
        });
        $('[data-toggle="collapse-hover"]').hover(function () {
            var thisdiv = $(this).attr("data-target");
            if (!$(this).hasClass('state-open')) {
                $(this).addClass('state-hover');
                $(thisdiv).css({
                    'height': '30px'
                });
            }
        }, function () {
            var thisdiv = $(this).attr("data-target");
            $(this).removeClass('state-hover');
            if (!$(this).hasClass('state-open')) {
                $(thisdiv).css({
                    'height': '0px'
                });
            }
        }).click(function (event) {
            event.preventDefault();
            var thisdiv = $(this).attr("data-target");
            var height = $(thisdiv).children('.panel-body').height();
            if ($(this).hasClass('state-open')) {
                $(thisdiv).css({
                    'height': '0px',
                });
                $(this).removeClass('state-open');
            }
            else {
                $(thisdiv).css({
                    'height': height + 30,
                });
                $(this).addClass('state-open');
            }
        });
    };
    ExpenseTabComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-expense-tab',
            template: __webpack_require__(/*! ./expense-tab.component.html */ "./src/app/costs/expense-tab/expense-tab.component.html"),
            styles: [__webpack_require__(/*! ./expense-tab.component.scss */ "./src/app/costs/expense-tab/expense-tab.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ExpenseTabComponent);
    return ExpenseTabComponent;
}());



/***/ }),

/***/ "./src/app/costs/expense/expense.component.html":
/*!******************************************************!*\
  !*** ./src/app/costs/expense/expense.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <form method=\"post\" class=\"form-horizontal\">\n                    <h4 class=\"title\">New/Edit Expense</h4>\n                    <div class=\"card\">\n                        <!-- mains will be here -->\n                        <div class=\"header\">\n                            <legend>Expense Card</legend>\n                        </div>\n                        <div class=\"content\">\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Merchant</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"merchant\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"expense.merchant\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                    <div class=\"form-group\">\n                                        <label class=\"col-sm-2 control-label\">Receipt Number</label>\n                                        <div class=\"col-sm-10\">\n                                            <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"expenseNumber\"\n                                                [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"expense.expenseNumber\">\n                                        </div>\n                                    </div>\n                                </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Date</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" name=\"expenseDate\" class=\"form-control datetimepicker\"\n                                        [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"expense.expenseDate\" placeholder=\"Pick Date Time\" />\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Payment Account</label>\n                                    <div class=\"col-sm-10\">\n                                        <select name=\"expenseAccount\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"expense.expenseAccount\"\n                                            class=\"selectpicker selectPaymentAccount\" data-title=\"Payment Account\" data-style=\"btn-default btn-block\"\n                                            data-menu-style=\"dropdown-blue\">\n                                            <option value=\"1\">Cash</option>\n                                            <option value=\"2\">Credit</option>\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Category</label>\n                                    <div class=\"col-sm-10\">\n                                        <select name=\"expenseCategory\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"expense.expenseCategory\"\n                                            class=\"selectpicker selectexpenseCategory\" data-title=\"Category\" data-style=\"btn-default btn-block\"\n                                            data-menu-style=\"dropdown-blue\">\n                                            <option *ngFor=\"let ec of expenseCategories\" [value]=\"ec.ID\">{{ec.category | translate}}</option>\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n\n                            <fieldset>\n                                    <div class=\"form-group\">\n                                        <label class=\"col-sm-2 control-label\">Currency</label>\n                                        <div class=\"col-sm-10\">\n                                            <select name=\"currency\" [(ngModel)]=\"expense.currency\" (change)=\"changeCurrency($event)\"\n                                                class=\"selectpicker selectCurrency\" data-title=\"Currency\" data-style=\"btn-default btn-block\"\n                                                data-menu-style=\"dropdown-blue\">\n                                                <option value=\"1\">TRY</option>\n                                                <option value=\"2\">USD</option>\n                                                <option value=\"3\">EUR</option>\n                                                <option value=\"4\">GBP</option>\n    \n                                            </select>\n                                        </div>\n                                    </div>\n                                </fieldset>\n                                <fieldset>\n                                        <div class=\"form-group\">\n                                            <label class=\"col-sm-2 control-label\">VAT</label>\n                                            <div class=\"col-sm-10\">\n                                                <select name=\"vatRate\" [(ngModel)]=\"expense.vatRate\" \n                                                    class=\"selectpicker selectvatRate\" data-title=\"Vat Rate\" data-style=\"btn-default btn-block\"\n                                                    (change)=\"vatChanged($event.target.value)\"\n                                                    data-menu-style=\"dropdown-blue\">\n                                                    <option selected value=\"18\">%18</option>\n                                                    <option selected value=\"8\">%8</option>\n                                                    <option selected value=\"1\">%1</option>\n                                                    <option selected value=\"0\">%0</option>\n                                                </select>\n                                            </div>\n                                        </div>\n                                    </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-8 control-label\">Sub Total</label>\n                                    <div class=\"col-sm-4\">\n                                        <input \n                                            type=\"text\" placeholder=\"Sub Total\" class=\"form-control text-right\"\n                                            name=\"amount\"\n                                             [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"expense.amount\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-8 control-label\">Vat Total</label>\n                                    <div class=\"col-sm-4\">\n                                        <input \n                                            type=\"text\" placeholder=\"Vat Total\" class=\"form-control text-right\" disabled\n                                             name=\"vatAmount\"  [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"expense.vatAmount\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-8 control-label\">Total</label>\n                                    <div class=\"col-sm-4\">\n                                        <input \n                                            type=\"text\" placeholder=\"Total\" class=\"form-control text-right\" disabled\n                                             name=\"totalAmount\" [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"expense.totalAmount\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Notes</label>\n                                    <div class=\"col-sm-10\">\n                                        <textarea class=\"form-control\" placeholder=\"Description\" rows=\"3\" name=\"notes\"  [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"expense.notes\"\n                                           ></textarea>\n                                    </div>\n                                </div>\n                            </fieldset>\n                        </div>\n                    </div>\n                    <div class=\"content\">\n                        <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"deleteExpense()\">DELETE</button>\n\n                        <div style='float:right;'>\n                            <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                            <button class=\"btn btn-success btn-fill btn-wd\" (click)=\"saveExpense()\" >SAVE</button>\n                        </div>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>"

/***/ }),

/***/ "./src/app/costs/expense/expense.component.scss":
/*!******************************************************!*\
  !*** ./src/app/costs/expense/expense.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Nvc3RzL2V4cGVuc2UvZXhwZW5zZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/costs/expense/expense.component.ts":
/*!****************************************************!*\
  !*** ./src/app/costs/expense/expense.component.ts ***!
  \****************************************************/
/*! exports provided: ExpenseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpenseComponent", function() { return ExpenseComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ExpenseComponent = /** @class */ (function () {
    function ExpenseComponent(apiService, activeRoute, router) {
        this.apiService = apiService;
        this.activeRoute = activeRoute;
        this.router = router;
        this.expense = [];
        this.expenseCategories = [];
        var routeParams = this.activeRoute.snapshot.params;
        this.expenseID = routeParams.id;
        console.log(this.expenseID, "expenseID");
        this.businessID = localStorage.getItem("businessID");
        this.getExpenseCategories();
        this.getExpense(this.businessID + "/" + this.expenseID);
    }
    ExpenseComponent.prototype.getExpenseCategories = function () {
        var _this = this;
        this.apiService.getData(this.businessID, "expenseCategories/").then(function (result) {
            _this.expenseCategories = result;
            console.log(_this.expenseCategories, "expenseCategories");
            $(".selectexpenseCategory").selectpicker('refresh');
        });
    };
    ExpenseComponent.prototype.getExpense = function (expenseID) {
        var _this = this;
        if (this.expenseID > 0) {
            this.apiService.getData(expenseID, "expenses/").then(function (result) {
                _this.expense = result[0];
                console.log(_this.expense, "expense");
                _this.spInit();
            });
        }
        else {
            this.expense = { "ID": 0, "merchant": "", "expenseNumber": "", "expenseDate": "", "expenseAccount": 0, "expenseCategory": 1, "currency": 1, "amount": 0, "vatRate": 18, "vatAmount": 0, "totalAmount": 0, "notes": "", "asID": this.businessID, "status": 1 };
            console.log(this.expense);
        }
    };
    ExpenseComponent.prototype.ngOnInit = function () {
        var _this = this;
        $('.datetimepicker').datetimepicker({
            locale: 'tr',
            format: 'DD-MM-YYYY HH:mm',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
        this.resolveAfter2Seconds(20).then(function (value) {
            _this.spInit();
        });
    };
    ExpenseComponent.prototype.ngAfterViewInit = function () {
    };
    ExpenseComponent.prototype.spInit = function () {
        console.log("Sp Init");
        $(".selectpicker").selectpicker('refresh');
        $(".selectPaymentAccount").selectpicker('val', this.expense.expenseAccount);
        $(".selectexpenseCategory").selectpicker('val', this.expense.expenseCategory);
        $(".selectCurrency").selectpicker('val', this.expense.currency);
        $(".selectvatRate").selectpicker('val', this.expense.vatRate);
    };
    ExpenseComponent.prototype.resolveAfter2Seconds = function (x) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                resolve(x);
            }, 1000);
        });
    };
    ExpenseComponent.prototype.saveExpense = function () {
        this.apiService.postData(this.expense.ID, "/expense/save/", JSON.stringify(this.expense)).then(function (result) {
            console.log(result);
        });
    };
    ExpenseComponent.prototype.deleteExpense = function () {
        this.apiService.deleteData(this.expense.ID, "/expense/delete/").then(function (result) {
            console.log(result);
        });
    };
    ExpenseComponent.prototype.clickCancel = function () {
        this.router.navigateByUrl("/costs/expenses");
    };
    ExpenseComponent.prototype.changeCurrency = function (event) {
        console.log("Change Currency");
    };
    ExpenseComponent.prototype.vatChanged = function (event) {
        if (this.businessID == 1 || this.businessID == 2) {
            this.expense.vatAmount = (this.expense.totalAmount - this.expense.totalAmount / ((event / 100) + 1)).toFixed(2);
            this.expense.amount = (this.expense.totalAmount / ((event / 100) + 1)).toFixed(2);
        }
    };
    ExpenseComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-expense',
            template: __webpack_require__(/*! ./expense.component.html */ "./src/app/costs/expense/expense.component.html"),
            styles: [__webpack_require__(/*! ./expense.component.scss */ "./src/app/costs/expense/expense.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ExpenseComponent);
    return ExpenseComponent;
}());



/***/ }),

/***/ "./src/app/costs/expenses/expenses.component.html":
/*!********************************************************!*\
  !*** ./src/app/costs/expenses/expenses.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n            <div class=\"row\">\n                    <div class=\"col-md-12\">\n                        <div class=\"card \">\n                            <div class=\"header\">\n                                <h4 class=\"title\">{{\"Expense Summary\"| translate |titlecase}}\n                                    <!-- Burada eger tedarikci ise Customer>Vendor olacak -->\n                                    <span style='float:right;font-size:16px;'>\n                                        <a data-target=\"#collapseOne\" data-toggle=\"collapse\">\n                                            <i style='color:gray;margin-right:10px;' class=\"fa fa-ellipsis-h\"> </i>&nbsp;\n                                        </a>\n                                    </span>\n                                </h4>\n                                <p class=\"category\"></p>\n                            </div>\n                            <div class=\"content\">\n                                <div class=\"panel-group\" id=\"accordion\">\n                                    <div class=\"panel panel-default\">\n                                        <div id=\"collapseOne\" class=\"panel-collapse collapse\">\n        \n                                            <div class=\"panel-body\">\n                                                <div class=\"col-md-6\">\n                                                    Graph\n                                                </div>\n                                                <div class=\"col-md-6\">\n                                                    <table class=\"table table-hover\">\n                                                        <thead>\n                                                            <tr>\n                                                                <th></th>\n                                                                <th>3 Months Ago</th>\n                                                                <th>2 Months Ago</th>\n                                                                <th>Last Month</th>\n                                                                <th>Cumulative</th>\n                                                            </tr>\n                                                        </thead>\n                                                        <tbody>\n                                                            <tr>\n                                                                <td>Travel Expenses</td>\n                                                                <td>TRY 12,000</td>\n                                                                <td></td>\n                                                                <td>TRY 12,000</td>\n                                                                <td>TRY 12,000</td>\n                                                            </tr>\n                                                            <tr>\n                                                                <td>Meals/Entertainment</td>\n                                                                <td>TRY 12,000</td>\n                                                                <td></td>\n                                                                <td>TRY 12,000</td>\n                                                                <td>TRY 12,000</td>\n                                                            </tr>\n                                                            <tr>\n                                                                <td><b>TOTAL</b></td>\n                                                                <td><b>TRY 12,000</b></td>\n                                                                <td><b></b></td>\n                                                                <td><b>TRY 12,000</b></td>\n                                                                <td><b>TRY 12,000</b></td>\n                                                            </tr>\n                                                        </tbody>\n                                                    </table>\n                                                </div>\n        \n        \n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n            </div> \n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"content\">\n                        <h4 class=\"title\">{{\"expenses\" | translate |titlecase}} \n                                <span style='float:right;font-size:16px;'>\n                                    <input type=\"file\" name=\"uf\" style=\"display:none;\" #fileInput (change)=\"onFileChange($event)\"/>\n                                    <a (click)=\"uploadFile()\" data-target=\"#\" data-toggle=\"collapse\"><i style='color:gray;margin-right:10px;' class=\"fa fa-upload\"> </i>&nbsp;</a>\n                                    <a routerLink='/costs/expense' style='color:gray;margin-right:10px;'><i class='fa fa-plus-square-o'></i></a>\n                                </span>\n                        </h4>\n                        <p class=\"category\">{{\"These are your Receipts and Expenses\" | translate |titlecase}}</p>\n                        <!-- <legend></legend> -->\n                        <br>\n                      <div class=\"toolbar\">\n                          <!--        Here you can write extra buttons/actions for the toolbar              -->\n                      </div>\n                      <div class=\"fresh-datatables\">\n                          <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                              <thead>\n                                  <tr>\n                                    <th>{{ dataTable.headerRow[0] }}</th>\n                                    <th class='text-center'>{{ dataTable.headerRow[1] }}</th>\n                                    <th>{{ dataTable.headerRow[2] }}</th>\n                                    <th>{{ dataTable.headerRow[3] }}</th>\n                                    <th>{{ dataTable.headerRow[4] }}</th>\n                                    <th>{{ dataTable.headerRow[5] }}</th>\n                                    <th class=\"text-center\">{{ dataTable.headerRow[6] }}</th>\n                                    <th class=\"text-center\">{{ dataTable.headerRow[7] }}</th>\n\n                                  </tr>\n                              </thead>\n                              <tbody>\n                                  <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                      <td><span class=\"dot {{row.color}}\"></span></td>\n                                      <td class='text-center'><button class=\"btn btn-xs btn-primary\">{{row.statusName | uppercase}}</button></td>\n                                      <td>{{row.expenseDate | date:'dd-MM-yyyy'}}</td>\n                                      <td><a routerLink='/costs/expense/{{row.ID}}'>{{row.merchant}}</a></td>\n                                      <td>{{row.expenseCategoryName}}</td>\n                                      <td>{{row.expenseAccountName}}</td>\n                                      <td class='text-right'>{{row.totalAmount | number }}</td>\n                                      <td class='text-left'><button class=\"btn btn-xs btn-success\">{{row.currencyName}}</button></td>\n                                  </tr>\n                              </tbody>\n                          </table>\n                      </div>\n                  </div>\n                  <!-- end content-->\n              </div>\n              <!--  end card  -->\n          </div>\n          <!-- end col-md-12 -->\n      </div>\n      <!-- end row -->\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/costs/expenses/expenses.component.scss":
/*!********************************************************!*\
  !*** ./src/app/costs/expenses/expenses.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Nvc3RzL2V4cGVuc2VzL2V4cGVuc2VzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/costs/expenses/expenses.component.ts":
/*!******************************************************!*\
  !*** ./src/app/costs/expenses/expenses.component.ts ***!
  \******************************************************/
/*! exports provided: ExpensesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpensesComponent", function() { return ExpensesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ExpensesComponent = /** @class */ (function () {
    function ExpensesComponent(apiService) {
        this.apiService = apiService;
        this.expenses = [];
        /*
        this.dataTable = {
            headerRow: ['#', 'Status', 'Date', 'Merchant', 'Category', 'Account', 'Total', 'Total Amount'],
            footerRow: ['Row Number', 'Status', 'Date', 'Merchant', 'Category', 'Account', 'Total', 'Total Amount'],
            dataRows:[]
        };
        */
    }
    ExpensesComponent.prototype.ngOnInit = function () {
        this.businessID = localStorage.getItem("businessID");
        this.getExpenses(this.businessID + '/0');
    };
    ExpensesComponent.prototype.uploadFile = function () {
        var event = new MouseEvent('click', { bubbles: false });
        this.fileInput.nativeElement.dispatchEvent(event);
    };
    ExpensesComponent.prototype.onFileChange = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            var file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = function () {
                var file = {
                    "fileName": _this.fileInput.nativeElement.files[0].name,
                    "fileType": _this.fileInput.nativeElement.files[0].type,
                    "file": reader.result.toString().split(",")[1]
                };
                _this.apiService.postData(_this.businessID, "receipts/import/", JSON.stringify(file)).then(function (result) {
                    console.log(result);
                    document.location.reload();
                });
                console.log(_this.fileInput.nativeElement.files[0].name);
                console.log(_this.fileInput.nativeElement.files[0].type);
                console.log(reader.result.toString().split(",")[1]);
                /*
                this.fileInput.nativeElement.setValue({
                  filename: file.name,
                  filetype: file.type,
                  value: reader.result //.split(',')[1]
                })
                */
            };
        }
    };
    ExpensesComponent.prototype.getExpenses = function (expenseID) {
        var _this = this;
        this.apiService.getData(expenseID, "expenses/").then(function (result) {
            _this.expenses = result;
            _this.dataTable = {
                headerRow: ['#', 'Status', 'Date', 'Merchant', 'Category', 'Account', 'Total', 'Total Amount'],
                footerRow: ['Row Number', 'Status', 'Date', 'Merchant', 'Category', 'Account', 'Total', 'Total Amount'],
                dataRows: _this.expenses
            };
            console.log(_this.expenses, "expenses");
        });
    };
    ExpensesComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ExpensesComponent.prototype, "fileInput", void 0);
    ExpensesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-expenses',
            template: __webpack_require__(/*! ./expenses.component.html */ "./src/app/costs/expenses/expenses.component.html"),
            styles: [__webpack_require__(/*! ./expenses.component.scss */ "./src/app/costs/expenses/expenses.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], ExpensesComponent);
    return ExpensesComponent;
}());



/***/ }),

/***/ "./src/app/costs/invoices-in/invoices-in.component.html":
/*!**************************************************************!*\
  !*** ./src/app/costs/invoices-in/invoices-in.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card \">\n                    <div class=\"header\">\n                        <h4 class=\"title\">{{\"costinvoicessummary\"| translate | titlecase}}\n                            <span style='float:right;font-size:16px;'><a (click)=\"showAccord()\"><i style='color:gray;margin-right:10px;'\n                                        class=\"fa fa-ellipsis-h\"> </i>&nbsp;</a></span>\n                        </h4>\n                        <p class=\"category\"></p>\n                    </div>\n                    <div class=\"content\">\n                        <div class=\"panel panel-default\" *ngIf=\"showHideAccor\">\n\n                            <div class=\"panel-body\">\n                                <div class=\"col-md-6\">\n                                        <lbd-chart [title]=\"'Payables'\" [subtitle]=\"'All products including Taxes'\"\n                                        [chartType]=\"activityChartType\" [chartData]=\"activityChartData\"\n                                        [chartOptions]=\"activityChartOptions\"\n                                        [chartResponsive]=\"activityChartResponsive\"\n                                        [legendItems]=\"activityChartLegendItems\" [withHr]=\"false\"\n                                        [noCard]=\"false\" [footerIconClass]=\"'fa fa-check'\"\n                                        [footerText]=\"'Data information certified'\">\n                                    </lbd-chart>\n                                </div>\n                                <div class=\"col-md-6\">\n                                        <div class=\"card\">\n                                                <div class=\"content\" style=\"padding-top: 30px;\">\n                                    <table class=\"table table-hover\">\n                                        <thead>\n                                            <tr>\n                                                <th></th>\n                                                <th class=\"text-center\">TRY</th>\n                                                <th class=\"text-center\">USD</th>\n                                                <th class=\"text-center\">EUR</th>\n                                                <th class=\"text-center\">GBP</th>\n                                            </tr>\n                                        </thead>\n                                        <tbody>\n                                            <tr *ngFor=\"let dd of invoiceDueDates\">\n                                                <td class=\"text-right\">{{dd.dayBlock | translate | uppercase}}</td>\n                                                <td class=\"text-center\">{{dd.TRY | number}}</td>\n                                                <td class=\"text-center\">{{dd.USD | number}}</td>\n                                                <td class=\"text-center\">{{dd.EUR | number}}</td>\n                                                <td class=\"text-center\">{{dd.GBP | number}}</td>\n                                            </tr>\n                                            <tr>\n                                                <td class=\"text-right\">Total</td>\n                                                <td class=\"text-center\">{{totals.totalTRY | number}}</td>\n                                                <td class=\"text-center\">{{totals.totalUSD | number}}</td>\n                                                <td class=\"text-center\">{{totals.totalEUR | number}}</td>\n                                                <td class=\"text-center\">{{totals.totalGBP | number}}</td>\n                                            </tr>\n                                        </tbody>\n                                    </table>\n                                </div>\n                                <div class=\"footer\">\n                                    <div class=\"legend\">\n                                    </div>\n                            </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"content\">\n                        <input type=\"file\" accept=\"application/pdf\" name=\"uf\" style=\"display:none;\" #fileInput (change)=\"onFileChange($event)\"/>\n                        <h4 class=\"title\">{{\"bills\"| translate |titlecase}}\n                            <span style='float:right;font-size:16px;'>\n                                <a (click)=\"callInvoice(2)\" style='color:gray;margin-right:10px;cursor:pointer;'><i class='fa fa-plus-square-o'></i></a>\n                            </span>\n                        </h4>\n                        <p class=\"category\">{{\"These are your incoming bills\"| translate |titlecase}}</p>\n                        <br>\n                        <div class=\"toolbar\">\n                            <!-- Here you can write extra buttons/actions for the toolbar -->\n                        </div>\n                        <div class=\"fresh-datatables\">\n                            <table id=\"datatablesIn\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\"\n                                style=\"width:100%\">\n                                <thead>\n                                    <tr>\n                                        <th>#</th>\n                                        <th class='text-center'>{{\"Status\" | translate | uppercase}}</th>\n                                        <th class='text-center'></th>\n                                        <th class=\"text-left\">{{\"Invoice No\" | translate | uppercase}}</th>\n                                        <th>{{\"Customer\" | translate | uppercase}}</th>\n                                        <th>{{\"Issued\" | translate | uppercase}}</th>\n                                        <th class=\"text-center\"></th>\n                                        <th>{{\"Due\" | translate | uppercase}}</th>\n                                        <th class=\"text-right\">{{\"Amount\" | translate | uppercase}}</th>\n                                        <th class=\"text-center\"></th>\n                                        <th class=\"text-center\"></th>\n                                        <th class=\"text-center\"></th>\n                                        <th style=\"width:10%;\" class=\"text-right\">{{\"Action\" | translate | uppercase}}</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr *ngFor=\"let row of dataTable.dataRows; let i = index\" [ngStyle]=\"{'background-color':(row.statusID==2000) ? 'lightgray' : ''}\">\n                                        <td><span class=\"dot {{row.color}}\"></span></td>\n                                        <td class='text-center'><span class=\"btn btn-xs\" [ngStyle]=\"{'border-color':(row.statusID==1200) ? 'red' : ''}\" *ngIf=\"row.statusID>1\">{{row.status |\n                                                uppercase}}</span>\n                                                <a *ngIf=\"(row.statusID==0 ) || (row.statusID==1 && userType==1)\"\n                                                (click)=\"approveInvoice(row.ID,i)\"\n                                                class=\"btn btn-xs btn-success btn-fill\"><b>{{\"Approve\" | uppercase}}</b></a>\n                                            </td>\n                                        <td class='text-center'><span class=\"btn btn-primary btn-xs\">{{row.invoiceClassName\n                                                | uppercase}}</span></td>\n                                        <td class=\"text-left\"><a routerLink='/sales/invoice/{{row.ID}}' title=\"{{row.description}}\" [queryParams]=\"{invoiceType: 2}\">{{row.invoiceNumber}}</a></td>\n                                        <td><a routerLink='/sales/customer/{{row.customerID}}' target=\"_blank\">{{row.customerShortName}}</a></td>\n                                        <td>{{row.invoiceDate}}</td>\n                                        <td class=\"text-center\">\n                                            <span *ngIf=\"calculateDays(row.invoiceDate,row.dueDate) != 9999 && row.paymentID==0\"\n                                                ngClass=\"btn btn-xs {{calculateDays(row.invoiceDate,row.dueDate) >= 0 ? 'btn-fill btn-info' : 'btn-fill btn-danger'}}\">\n                                                {{((calculateDays(row.invoiceDate,row.dueDate))*1)}} days</span>\n                                        </td>\n                                        <td><span *ngIf=\"row.paymentID==0\">{{row.dueDate | date:'dd-MM-yyyy'}}</span></td>\n                                        <td class='text-right'>{{row.totalAmount | number}}</td>\n                                        <td class='text-center'><span class=\"btn btn-success btn-xs\">{{row.currency }}</span></td>\n\n                                        <td class=\"text-center\">\n                                            <span *ngIf=\"row.paymentID>0\" class=\"btn btn-success btn-xs btn-fill\">{{\"PAID\"\n                                                | uppercase}}</span>\n                                            <span *ngIf=\"row.isProforma==1\" class=\"btn btn-success btn-xs\">{{\"PROFORMA\"\n                                                | uppercase}}</span>\n                                        </td>\n                                        <td class=\"text-center\">\n                                                <span *ngIf=\"row.account\"\n                                                    class=\"btn btn-warning btn-xs btn-fill\">{{\"POSTED\" | uppercase}}</span>\n                                            </td>\n\n                                        <td class=\"text-right tableicon\">\n                                            <a *ngIf=\"row.statusID==1000 && userType==1\" alt='Accounting'  title='Accounting'\n                                                (click)=\"openAccountingModal(row.ID,i)\"><i class=\"fa fa-check\"></i>&nbsp;</a>\n                                            <a *ngIf=\"row.paymentID==0 && row.isProforma==0 && userType==1\" alt='Record Payment'  title='Record Payment'\n                                                (click)=\"openModal(row.ID,i)\"><i class=\"fa fa-credit-card\"></i>&nbsp;</a>\n                                            <a title=\"View Invoice\" target=\"_blank\" *ngIf=\"row.pdfPath!=null\" href=\"https://crmapi.efdigitalcodes.com{{row.pdfPath}}\"><i\n                                                    class=\"fa fa-file-pdf-o\"> </i>&nbsp;</a>\n                                            <a title=\"Upload Invoice\" *ngIf=\"row.pdfPath==null\"  (click)=\"uploadInvoice(row.ID)\"><i class=\"fa fa-upload\"> </i>&nbsp;</a>\n                                            <a title=\"Clone Invoice\" (click)=\"cloneInvoice(row.ID)\"><i class=\"fa fa-clone\"> </i>&nbsp;</a>\n                                            <a title=\"Get Invoice\" routerLink='/sales/invoice/{{row.ID}}' [queryParams]=\"{invoiceType: 2}\"><i\n                                                    class=\"fa fa-search\"></i></a>\n\n                                        </td>\n                                    </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                    <!-- end content-->\n                </div>\n                <!--  end card  -->\n            </div>\n            <!-- end col-md-12 -->\n        </div>\n        <!-- end row -->\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/costs/invoices-in/invoices-in.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/costs/invoices-in/invoices-in.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Nvc3RzL2ludm9pY2VzLWluL2ludm9pY2VzLWluLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/costs/invoices-in/invoices-in.component.ts":
/*!************************************************************!*\
  !*** ./src/app/costs/invoices-in/invoices-in.component.ts ***!
  \************************************************************/
/*! exports provided: InvoicesInComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicesInComponent", function() { return InvoicesInComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var app_sales_invoice_payment_invoice_payment_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/sales/invoice-payment/invoice-payment.component */ "./src/app/sales/invoice-payment/invoice-payment.component.ts");
/* harmony import */ var app_lbd_lbd_chart_lbd_chart_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/lbd/lbd-chart/lbd-chart.component */ "./src/app/lbd/lbd-chart/lbd-chart.component.ts");
/* harmony import */ var app_sales_approve_modal_approve_modal_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/sales/approve-modal/approve-modal.component */ "./src/app/sales/approve-modal/approve-modal.component.ts");
/* harmony import */ var _accounting_accounting_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../accounting/accounting.component */ "./src/app/costs/accounting/accounting.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var InvoicesInComponent = /** @class */ (function () {
    function InvoicesInComponent(apiService, router, modalService) {
        this.apiService = apiService;
        this.router = router;
        this.modalService = modalService;
        this.invoices = [];
        this.invoiceDueDates = [];
        this.showHideAccor = 0;
        this.series = [];
        this.totals = [];
        this.userType = localStorage.getItem("userType");
        this.userID = localStorage.getItem("userID");
        /*
         this.dataTable = {
           headerRow: ['#', 'Due', 'Invoice Number', 'Customer', 'Issued', 'Amount', 'Status','Action'],
           footerRow: ['Row Number', 'Due Date', 'Description', 'Customer', 'Issue Date', 'Amount', 'Status','Action'],
           dataRows: []
         };
     */
    }
    InvoicesInComponent.prototype.showAccord = function () {
        this.showHideAccor = (this.showHideAccor == 1) ? 0 : 1;
    };
    InvoicesInComponent.prototype.ngOnInit = function () {
        this.businessID = localStorage.getItem("businessID");
        this.getInvoices('2/' + this.businessID + '/0');
        this.getInvoiceDueDates();
        // graph
        this.activityChartType = app_lbd_lbd_chart_lbd_chart_component__WEBPACK_IMPORTED_MODULE_5__["ChartType"].Bar;
        this.activityChartData = {
            labels: ['Upcoming', '1-30 days', '31-60 days', '61-90 days', '90 days'],
            series: this.series
        };
        this.activityChartOptions = {
            seriesBarDistance: 10,
            axisX: {
                showGrid: true
            },
            height: '240px'
        };
        this.activityChartResponsive = [
            ['screen and (max-width: 640px)', {
                    seriesBarDistance: 10,
                    axisX: {
                        labelInterpolationFnc: function (value) {
                            return value[0];
                        }
                    }
                }]
        ];
        this.activityChartLegendItems = [
            { title: 'TRY', imageClass: 'fa fa-circle text-info' },
            { title: 'USD', imageClass: 'fa fa-circle text-danger' },
            { title: 'EUR', imageClass: 'fa fa-circle text-warning' },
            { title: 'GBP', imageClass: 'fa fa-circle text-success' }
        ];
    };
    InvoicesInComponent.prototype.calculateDays = function (startTime, endTime) {
        var date1 = startTime;
        var date2 = endTime;
        var diffInMs = Date.parse(date2) - Date.parse(Date());
        var diffInHours = diffInMs / 1000 / 60 / 60 / 24;
        if (isNaN(diffInHours)) {
            diffInHours = 9999;
        }
        return Math.round(diffInHours);
    };
    InvoicesInComponent.prototype.getInvoices = function (invoiceID) {
        var _this = this;
        this.apiService.getData(invoiceID, "invoices/").then(function (result) {
            _this.invoices = result;
            _this.dataTable = {
                headerRow: ['#', 'Due', 'Invoice Number', 'Customer', 'Issued', 'Amount', 'Status', 'Action'],
                footerRow: ['Row Number', 'Due Date', 'Description', 'Customer', 'ßIssue Date', 'Amount', 'Status', 'Action'],
                dataRows: _this.invoices
            };
            //console.log(this.dataTable);
        });
    };
    InvoicesInComponent.prototype.callInvoice = function (invoiceType) {
        this.router.navigate(["/sales/invoice"], { queryParams: { invoiceType: invoiceType } });
    };
    InvoicesInComponent.prototype.ngAfterViewInit = function () {
        $('#datatablesIn').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }
        });
    };
    InvoicesInComponent.prototype.cloneInvoice = function (invoiceID) {
        var _this = this;
        this.apiService.postData(invoiceID, "invoice/clone/", null).then(function (result) {
            console.log(result, "clone");
            var nInvoiceID = result;
            if (nInvoiceID > 0) {
                _this.getInvoices('2/' + _this.businessID + '/0');
            }
        });
    };
    InvoicesInComponent.prototype.getInvoiceDueDates = function () {
        var _this = this;
        this.apiService.getData(this.businessID + "/2", "invoiceDueDates/").then(function (result) {
            _this.invoiceDueDates = result;
            var vlTRY = [_this.invoiceDueDates[0].TRY * 1, _this.invoiceDueDates[1].TRY * 1, _this.invoiceDueDates[2].TRY * 1, _this.invoiceDueDates[3].TRY * 1];
            var vlUSD = [_this.invoiceDueDates[0].USD * 1, _this.invoiceDueDates[1].USD * 1, _this.invoiceDueDates[2].USD * 1, _this.invoiceDueDates[3].USD * 1];
            var vlEUR = [_this.invoiceDueDates[0].EUR * 1, _this.invoiceDueDates[1].EUR * 1, _this.invoiceDueDates[2].EUR * 1, _this.invoiceDueDates[3].EUR * 1];
            var vlGBP = [_this.invoiceDueDates[0].GBP * 1, _this.invoiceDueDates[1].GBP * 1, _this.invoiceDueDates[2].GBP * 1, _this.invoiceDueDates[3].GBP * 1];
            _this.totals = {
                "totalTRY": _this.invoiceDueDates[0].TRY * 1 + _this.invoiceDueDates[1].TRY * 1 + _this.invoiceDueDates[2].TRY * 1 + _this.invoiceDueDates[3].TRY * 1,
                "totalUSD": _this.invoiceDueDates[0].USD * 1 + _this.invoiceDueDates[1].USD * 1 + _this.invoiceDueDates[2].USD * 1 + _this.invoiceDueDates[3].USD * 1,
                "totalEUR": _this.invoiceDueDates[0].EUR * 1 + _this.invoiceDueDates[1].EUR * 1 + _this.invoiceDueDates[2].EUR * 1 + _this.invoiceDueDates[3].EUR * 1,
                "totalGBP": _this.invoiceDueDates[0].GBP * 1 + _this.invoiceDueDates[1].GBP * 1 + _this.invoiceDueDates[2].GBP * 1 + _this.invoiceDueDates[3].GBP * 1
            };
            console.log(_this.totals, "inv due dates");
            _this.series.push(vlTRY);
            _this.series.push(vlUSD);
            _this.series.push(vlEUR);
            _this.series.push(vlGBP);
            console.log(_this.series, "series");
        });
    };
    InvoicesInComponent.prototype.uploadInvoice = function (invoiceID) {
        var event = new MouseEvent('click', { bubbles: false });
        this.invoiceID = invoiceID;
        this.fileInput.nativeElement.dispatchEvent(event);
    };
    InvoicesInComponent.prototype.onFileChange = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (this.fileInput.nativeElement.files && this.fileInput.nativeElement.files.length > 0) {
            var file = this.fileInput.nativeElement.files[0];
            reader.readAsDataURL(file);
            reader.onload = function () {
                var file = {
                    "fileName": _this.fileInput.nativeElement.files[0].name,
                    "fileType": _this.fileInput.nativeElement.files[0].type,
                    "table": "invoices",
                    "userID": _this.userID,
                    "description": "Invoice",
                    "file": reader.result.toString().split(",")[1]
                };
                _this.apiService.postData("invoices/" + _this.invoiceID, "files/upload/", JSON.stringify(file)).then(function (result) {
                    console.log(result);
                });
                /*
                   console.log(this.fileInput.nativeElement.files[0].name);
                 console.log(this.fileInput.nativeElement.files[0].type);
                 console.log(reader.result.toString().split(",")[1]);
                */
            };
        }
    };
    InvoicesInComponent.prototype.openModal = function (invoiceID, index) {
        var _this = this;
        console.log(invoiceID, "invoice ID");
        var modalRef = this.modalService.open(app_sales_invoice_payment_invoice_payment_component__WEBPACK_IMPORTED_MODULE_4__["InvoicePaymentComponent"], { "backdrop": "static" });
        modalRef.componentInstance.title = 'Payment';
        //modalRef.componentInstance.ID = invoiceID;
        modalRef.componentInstance.invoiceID = invoiceID;
        modalRef.componentInstance.paymentType = "Record";
        modalRef.result.then(function (data) {
            // on close
            console.log(data, "result data");
            if (data > 0) {
                _this.invoices[index].paymentID = data;
            }
        }, function (reason) {
            // on dismiss
        });
    };
    InvoicesInComponent.prototype.approveInvoice = function (invoiceID, index) {
        var _this = this;
        var modalRef = this.modalService.open(app_sales_approve_modal_approve_modal_component__WEBPACK_IMPORTED_MODULE_6__["ApproveModalComponent"], { "backdrop": "static" });
        modalRef.componentInstance.invoiceID = invoiceID;
        modalRef.result.then(function (data) {
            // on close
            console.log(data, "result data");
            if (data) {
                if (data.statusID > 0) {
                    _this.invoices[index].statusID = data.statusID;
                    _this.invoices[index].status = data.status;
                }
            }
        }, function () {
            // on dismiss
        });
    };
    InvoicesInComponent.prototype.openAccountingModal = function (invoiceID, index) {
        var _this = this;
        console.log(invoiceID, "invoice ID");
        var modalRef = this.modalService.open(_accounting_accounting_component__WEBPACK_IMPORTED_MODULE_7__["AccountingComponent"], { "backdrop": "static" });
        modalRef.componentInstance.invoiceID = invoiceID;
        modalRef.result.then(function (data) {
            // on close
            //console.log(data, "result data");
            if (data.account != "") {
                _this.invoices[index].account = data.account;
                //console.log(data.account, "result data account");
            }
        }, function (reason) {
            // on dismiss
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], InvoicesInComponent.prototype, "fileInput", void 0);
    InvoicesInComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-invoices-in',
            template: __webpack_require__(/*! ./invoices-in.component.html */ "./src/app/costs/invoices-in/invoices-in.component.html"),
            styles: [__webpack_require__(/*! ./invoices-in.component.scss */ "./src/app/costs/invoices-in/invoices-in.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]])
    ], InvoicesInComponent);
    return InvoicesInComponent;
}());



/***/ }),

/***/ "./src/app/costs/products/products.component.html":
/*!********************************************************!*\
  !*** ./src/app/costs/products/products.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"content\">\n                        <div class=\"content\">\n                                <h4 class=\"title\">{{\"products\" | translate |titlecase}} \n                                        <span style='float:right;'>\n                                                <a routerLink='/sales/product' [queryParams]=\"{isVendor:2}\" style='color:gray;margin-right:10px;font-size:16px;'><i class='fa fa-plus-square-o'></i></a>\n                                        </span>\n                                    </h4>\n                                    <p class=\"category\">{{\"These are the products you can buy\"| translate |titlecase}}</p>\n                                <!-- <legend></legend> -->\n                                <br>\n                      <div class=\"toolbar\">\n                          \n                          <!--        Here you can write extra buttons/actions for the toolbar              -->\n                      </div>\n                      <div class=\"fresh-datatables\">\n                          <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                <thead>\n                                        <tr>\n                                          <th style=\"width:5%;\">#</th>\n                                          <th >{{\"Name\" | translate |titlecase}}</th>\n                                          <th style=\"width:5%;\" class=\"text-center \"></th>\n                                          <th style=\"width:5%;\" class=\"text-center \">{{\"Tax\" | translate |titlecase}}</th>\n                                          <th style=\"width:10%;\" class=\"text-right \">{{\"Price\" | translate |titlecase}}</th>\n                                          <th style=\"width:10%;\" class=\"text-right\">{{\"Actions\" | translate |titlecase}}</th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                            <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                                <td>{{i+1}}</td>\n                                                <td><a routerLink='/sales/product/{{row.ID}}' [queryParams]=\"{isVendor:2}\">{{row.product}}</a></td>\n                                                <td class=\"text-center\"><span class=\"btn btn-success btn-xs\">{{row.currency| uppercase}}</span></td>\n                                                <td class=\"text-center \">%{{row.taxRate}}</td>\n                                                <td class=\"text-right\">{{row.price | currency : row.currency}}</td>\n                                                <td class=\"text-right tableicon\">\n                                                  <a routerLink='/sales/product/{{row.ID}}' [queryParams]=\"{isVendor:2}\"><i class=\"fa fa-search\"> </i>&nbsp;</a>\n                                                </td>\n                                                \n                                            </tr>\n                                        </tbody>\n                          </table>\n                      </div>\n                  </div>\n                  <!-- end content-->\n              </div>\n              <!--  end card  -->\n          </div>\n          <!-- end col-md-12 -->\n      </div>\n      <!-- end row -->\n  </div>\n  </div>\n  "

/***/ }),

/***/ "./src/app/costs/products/products.component.scss":
/*!********************************************************!*\
  !*** ./src/app/costs/products/products.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Nvc3RzL3Byb2R1Y3RzL3Byb2R1Y3RzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/costs/products/products.component.ts":
/*!******************************************************!*\
  !*** ./src/app/costs/products/products.component.ts ***!
  \******************************************************/
/*! exports provided: ProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsComponent", function() { return ProductsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductsComponent = /** @class */ (function () {
    function ProductsComponent(apiService) {
        /*
      this.dataTable = {
          headerRow: ['#', 'Name', 'Price'],
          footerRow: ['#', 'Name', 'Price'],
          dataRows: []
      };
      */
        this.apiService = apiService;
        this.products = [];
    }
    ProductsComponent.prototype.ngOnInit = function () {
        this.businessID = localStorage.getItem("businessID");
        this.getProducts(this.businessID + "/2/0");
    };
    ProductsComponent.prototype.getProducts = function (productID) {
        var _this = this;
        this.apiService.getData(productID, "products/").then(function (result) {
            _this.products = result;
            _this.dataTable = {
                headerRow: ['#', 'Name', 'Price'],
                footerRow: ['#', 'Name', 'Price'],
                dataRows: _this.products
            };
            console.log(_this.dataTable);
        });
    };
    ProductsComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }
        });
        /*
        var table = $('#datatables').DataTable();
    
        // Edit record
        table.on( 'click', '.edit', function () {
            var $tr = $(this).closest('tr');
    
            var data = table.row($tr).data();
            alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
        } );
    
        // Delete a record
        table.on( 'click', '.remove', function (e) {
            var $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        } );
    
        //Like record
        table.on( 'click', '.like', function () {
            alert('You clicked on Like button');
        });
        */
    };
    ProductsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-products',
            template: __webpack_require__(/*! ./products.component.html */ "./src/app/costs/products/products.component.html"),
            styles: [__webpack_require__(/*! ./products.component.scss */ "./src/app/costs/products/products.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], ProductsComponent);
    return ProductsComponent;
}());



/***/ }),

/***/ "./src/app/costs/supplier/supplier.component.html":
/*!********************************************************!*\
  !*** ./src/app/costs/supplier/supplier.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n        <div class=\"container-fluid\">\n          <div class=\"row\">\n              <div class=\"col-md-12\">\n                  <h4 class=\"title\">Vendors \n                      <span style='float:right;'>\n                              <a routerLink='/sales/customer' style='color:gray;margin-right:10px;'><i class='fa fa-plus-square-o'></i></a>\n                      </span>\n                  </h4>\n      \n                  <div class=\"card\">\n                      <div class=\"content\">\n                          <div class=\"toolbar\">\n                              \n                              <!--        Here you can write extra buttons/actions for the toolbar              -->\n                          </div>\n                          <div class=\"fresh-datatables\">\n                              <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                  <thead>\n                                      <tr>\n                                        <th>{{ dataTable.headerRow[0] }}</th>\n                                        <th>{{ dataTable.headerRow[1] }}</th>\n                                        <th>{{ dataTable.headerRow[2] }}</th>\n                                        <th class='text-right'>{{ dataTable.headerRow[3] }}</th>\n                                        <th></th>\n                                      </tr>\n                                  </thead>\n                                  <tbody>\n                                          <tr *ngFor=\"let row of dataTable.dataRows\">\n                                              <td>{{row.ID}}</td>\n                                              <td><a routerLink='/sales/customer/{{row.ID}}'>{{row.customerShortName}}</a></td>\n                                              <td>{{row.country}}</td>\n                                              <td class='text-right'><button class=\"btn btn-xs btn-fill\">{{row.cType | uppercase}}</button></td>\n                                              <td><a title=\"Create Invoice\" routerLink=\"/sales/invoice\" [queryParams]=\"{invoiceType:2}\"><span class=\"fas fa-file-invoice-dollar\"></span></a></td>\n                                          </tr>\n                                      </tbody>\n                              </table>\n                          </div>\n                      </div>\n                      <!-- end content-->\n                  </div>\n                  <!--  end card  -->\n              </div>\n              <!-- end col-md-12 -->\n          </div>\n          <!-- end row -->\n      </div>\n      </div>\n      "

/***/ }),

/***/ "./src/app/costs/supplier/supplier.component.scss":
/*!********************************************************!*\
  !*** ./src/app/costs/supplier/supplier.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Nvc3RzL3N1cHBsaWVyL3N1cHBsaWVyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/costs/supplier/supplier.component.ts":
/*!******************************************************!*\
  !*** ./src/app/costs/supplier/supplier.component.ts ***!
  \******************************************************/
/*! exports provided: SupplierComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupplierComponent", function() { return SupplierComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SupplierComponent = /** @class */ (function () {
    function SupplierComponent(apiService) {
        this.apiService = apiService;
        this.customers = [];
    }
    SupplierComponent.prototype.ngOnInit = function () {
        this.businessID = localStorage.getItem("businessID");
        this.getCustomers(this.businessID + "/2/0");
    };
    SupplierComponent.prototype.getCustomers = function (customerID) {
        var _this = this;
        this.apiService.getData(customerID, "customers/").then(function (result) {
            _this.customers = result;
            _this.dataTable = {
                headerRow: ['#', 'Name', 'Country', 'Type'],
                footerRow: ['#', 'Name', 'Country', 'Type'],
                dataRows: _this.customers
            };
            console.log(_this.dataTable);
        });
    };
    SupplierComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }
        });
    };
    SupplierComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-supplier',
            template: __webpack_require__(/*! ./supplier.component.html */ "./src/app/costs/supplier/supplier.component.html"),
            styles: [__webpack_require__(/*! ./supplier.component.scss */ "./src/app/costs/supplier/supplier.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], SupplierComponent);
    return SupplierComponent;
}());



/***/ }),

/***/ "./src/app/costs/suppliers/suppliers.component.html":
/*!**********************************************************!*\
  !*** ./src/app/costs/suppliers/suppliers.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n        <div class=\"container-fluid\">\n          <div class=\"row\">\n              <div class=\"col-md-12\">\n                  <div class=\"card\">\n                      <div class=\"content\">\n                            <h4 class=\"title\">{{\"vendors\" | translate |titlecase}} \n                                    <span style='float:right;'>\n                                            <a routerLink='/sales/customer' [queryParams]=\"{isVendor:2}\" style='color:gray;margin-right:10px;font-size:16px;'><i class='fa fa-plus-square-o'></i></a>\n                                            <a routerLink='/finance/aggr' [queryParams]=\"{customerID: 0}\" style='color:gray;margin-right:10px;font-size:16px;'><i class=\"fa fa-handshake-o\"></i></a>\n\n                                        </span>\n                                </h4>\n                                <p class=\"category\">{{\"These are your Vendors\" | translate |titlecase}} </p>\n                                <!-- <legend></legend> -->\n                                <br>     \n                          <div class=\"toolbar\">\n                              \n                              <!--        Here you can write extra buttons/actions for the toolbar              -->\n                          </div>\n                          <div class=\"fresh-datatables\">\n                              <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                                  <thead>\n                                      <tr>\n                                        <th>#</th>\n                                        <th>{{ 'Name' | translate }}</th>\n                                        <th>{{ 'Country' | translate }}</th>\n                                        <th>{{ 'Phone' | translate }}</th>\n                                        <th>{{ 'Contact' | translate }}</th>\n                                        <th class='text-right'>{{ 'Type' | translate }}</th>\n                                        <th class='text-right'></th>\n                                      </tr>\n                                  </thead>\n                                  <tbody>\n                                          <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                              <td><span class=\"dot {{row.color}}\"></span></td>\n                                              <td><a routerLink='/sales/customer/{{row.ID}}' [queryParams]=\"{isVendor: 2}\">{{row.customerShortName}}</a></td>\n                                              <td>{{row.country}}</td>\n                                              <td>{{row.phone}}</td>\n                                              <td>{{row.customerContact}}</td>\n                                              <td class='text-right'><button class=\"btn btn-xs btn-fill\">{{row.cType | uppercase}}</button></td>\n                                              <td class=\"text-right tableicon\">\n                                                  <!-- <a title=\"Create Invoice\" routerLink=\"/sales/invoice\" [queryParams]=\"{invoiceType:2}\"><span class=\"fas fa-file-invoice-dollar\"></span></a> -->\n                                            \n                                                  <a href='mailto:{{row.customerContactEmail}}'><i class=\"fa fa-envelope-o\"></i>&nbsp;</a>\n                                                  <a routerLink='/finance/aggr' [queryParams]=\"{customerID: row.ID}\"><i class=\"fa fa-handshake-o\"></i>&nbsp;</a>\n                                                  <a title=\"Create Invoice\" routerLink=\"/sales/invoice\" [queryParams]=\"{invoiceType:2,customerID:row.ID}\"><i class=\"fa fa-file-text-o\"></i>&nbsp;</a>\n                                                  <a routerLink='/sales/customer/{{row.ID}}'><i class=\"fa fa-search\"></i>&nbsp;</a>\n      \n                                              </td>\n                                          </tr>\n                                      </tbody>\n                              </table>\n                          </div>\n                      </div>\n                      <!-- end content-->\n                  </div>\n                  <!--  end card  -->\n              </div>\n              <!-- end col-md-12 -->\n          </div>\n          <!-- end row -->\n      </div>\n      </div>\n      "

/***/ }),

/***/ "./src/app/costs/suppliers/suppliers.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/costs/suppliers/suppliers.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Nvc3RzL3N1cHBsaWVycy9zdXBwbGllcnMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/costs/suppliers/suppliers.component.ts":
/*!********************************************************!*\
  !*** ./src/app/costs/suppliers/suppliers.component.ts ***!
  \********************************************************/
/*! exports provided: SuppliersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuppliersComponent", function() { return SuppliersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SuppliersComponent = /** @class */ (function () {
    function SuppliersComponent(apiService) {
        this.apiService = apiService;
        this.customers = [];
        /*
        this.dataTable = {
            dataRows: []
        };
        */
    }
    SuppliersComponent.prototype.ngOnInit = function () {
        this.businessID = localStorage.getItem("businessID");
        this.getCustomers(this.businessID + "/2/0");
    };
    SuppliersComponent.prototype.getCustomers = function (customerID) {
        var _this = this;
        this.apiService.getData(customerID, "customers/").then(function (result) {
            _this.customers = result;
            _this.dataTable = {
                dataRows: _this.customers
            };
            console.log(_this.dataTable);
        });
    };
    SuppliersComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            pagingType: "full",
            pageLength: 25,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                paginate: {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                }
            }
        });
    };
    SuppliersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-suppliers',
            template: __webpack_require__(/*! ./suppliers.component.html */ "./src/app/costs/suppliers/suppliers.component.html"),
            styles: [__webpack_require__(/*! ./suppliers.component.scss */ "./src/app/costs/suppliers/suppliers.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], SuppliersComponent);
    return SuppliersComponent;
}());



/***/ })

}]);
//# sourceMappingURL=costs-costs-module.js.map