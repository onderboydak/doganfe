(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-dashboard-module"],{

/***/ "./src/app/dashboard/dashboard.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card \">\n                    <div class=\"header\">\n                        <h4 class=\"title\">Income Statement</h4>\n                        <p class=\"category\"></p>\n                    </div>\n                    <div class=\"content\">\n                        <div class=\"row\">\n                            <div class=\"col-md-5\">\n                                <lbd-table\n                                  [data]=\"tableData\">\n                              </lbd-table>\n                            </div>\n                            <div class=\"col-md-6 col-md-offset-1\">\n                                <div id=\"worldMap\" style=\"height: 300px;\"></div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"row\">\n            <div class=\"col-md-6\">\n              <lbd-chart\n                [title]=\"'Sales Invoices'\"\n                [subtitle]=\"''\"\n                [chartType]=\"activityChartType\"\n                [chartData]=\"activityChartData\"\n                [chartOptions]=\"activityChartOptions\"\n                [chartResponsive]=\"activityChartResponsive\"\n                [legendItems]=\"activityChartLegendItems\"\n                [withHr]=\"true\"\n                [noCard]=\"true\"\n                [footerIconClass]=\"'fa fa-check'\"\n                [footerText]=\"'Data information certified'\">\n              </lbd-chart>\n            </div>\n            <div class=\"col-md-6\">\n                <lbd-chart\n                  [title]=\"'Costs Invoices'\"\n                  [subtitle]=\"''\"\n                  [chartType]=\"activityChartType2\"\n                  [chartData]=\"activityChartData2\"\n                  [chartOptions]=\"activityChartOptions\"\n                  [chartResponsive]=\"activityChartResponsive\"\n                  [legendItems]=\"activityChartLegendItems\"\n                  [withHr]=\"true\"\n                  [noCard]=\"true\"\n                  [footerIconClass]=\"'fa fa-check'\"\n                  [footerText]=\"'Data information certified'\">\n                </lbd-chart>\n              </div>\n          </div>\n\n        <div class=\"row\">\n          <div class=\"col-md-4\" >\n            <lbd-chart\n              [title]=\"'Expenses'\"\n              [subtitle]=\"'Last Campaign Performance'\"\n              [chartClass]=\"'ct-perfect-fourth'\"\n              [chartType]=\"emailChartType\"\n              [chartData]=\"emailChartData\"\n              [legendItems]=\"emailChartLegendItems\"\n              [withHr]=\"true\"\n              [noCard]=\"false\"\n              [footerIconClass]=\"'fa fa-clock-o'\"\n              [footerText]=\"'Campaign sent 2 days ago'\">\n            </lbd-chart>\n          </div>\n\n          <div class=\"col-md-8\" >\n            <lbd-chart\n              [title]=\"'Cash Flow'\"\n              [subtitle]=\"'24 Hours performance'\"\n              [chartType]=\"hoursChartType\"\n              [chartData]=\"hoursChartData\"\n              [chartOptions]=\"hoursChartOptions\"\n              [chartResponsive]=\"hoursChartResponsive\"\n              [legendItems]=\"hoursChartLegendItems\"\n              [withHr]=\"true\"\n              [noCard]=\"false\"\n              [footerIconClass]=\"'fa fa-history'\"\n              [footerText]=\"'Updated 3 minutes ago'\">\n            </lbd-chart>\n          </div>\n        </div>\n\n        <div class=\"row\">\n          <div class=\"col-md-6\">\n            <lbd-chart\n              [title]=\"'Sales / Costs '\"\n              [subtitle]=\"'All products including Taxes'\"\n              [chartType]=\"activityChartType3\"\n              [chartData]=\"activityChartData3\"\n              [chartOptions]=\"activityChartOptions\"\n              [chartResponsive]=\"activityChartResponsive\"\n              [legendItems]=\"activityChartLegendItems3\"\n              [withHr]=\"true\"\n              [noCard]=\"true\"\n              [footerIconClass]=\"'fa fa-check'\"\n              [footerText]=\"'Data information certified'\">\n            </lbd-chart>\n          </div>\n\n          <div class=\"col-md-6\" >\n            <lbd-task-list\n              [title]=\"'Tasks'\"\n              [subtitle]=\"'Backend development'\"\n              [tasks]=\"tasks\"\n              [withHr]=\"true\"\n              [footerIconClass]=\"'fa fa-history'\"\n              [footerText]=\"'Updated 3 minutes ago'\">\n            </lbd-task-list>\n          </div>\n        </div>\n\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _lbd_lbd_chart_lbd_chart_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../lbd/lbd-chart/lbd-chart.component */ "./src/app/lbd/lbd-chart/lbd-chart.component.ts");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! chartist */ "./node_modules/chartist/dist/chartist.js");
/* harmony import */ var chartist__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chartist__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(apiService) {
        this.apiService = apiService;
        this.invoiceDueDates = [];
        this.invoiceDueDates3 = [];
        this.series = [];
        this.series2 = [];
        this.series3 = [];
        this.businessID = localStorage.getItem("businessID");
    }
    DashboardComponent.prototype.getCustomerInvoiceDueDates = function () {
        var _this = this;
        this.apiService.getData(this.businessID + "/0", "invoiceCustomerDueDates/").then(function (result) {
            _this.invoiceDueDates3 = result;
            var vlM1 = [_this.invoiceDueDates3[0].M3 * 1, _this.invoiceDueDates3[0].M2 * 1, _this.invoiceDueDates3[0].M1 * 1, _this.invoiceDueDates3[0].M0 * 1];
            var vlM0 = [_this.invoiceDueDates3[1].M3 * 1, _this.invoiceDueDates3[1].M2 * 1, _this.invoiceDueDates3[1].M1 * 1, _this.invoiceDueDates3[1].M0 * 1];
            _this.series3.push(vlM1);
            _this.series3.push(vlM0);
            console.log(_this.series3, "series 3");
        });
    };
    DashboardComponent.prototype.getInvoiceDueDates = function () {
        var _this = this;
        this.apiService.getData(this.businessID + "/1", "invoiceDueDates/").then(function (result) {
            _this.invoiceDueDates = result;
            var vlTRY = [_this.invoiceDueDates[0].TRY * 1, _this.invoiceDueDates[1].TRY * 1, _this.invoiceDueDates[2].TRY * 1, _this.invoiceDueDates[3].TRY * 1];
            var vlUSD = [_this.invoiceDueDates[0].USD * 1, _this.invoiceDueDates[1].USD * 1, _this.invoiceDueDates[2].USD * 1, _this.invoiceDueDates[3].USD * 1];
            var vlEUR = [_this.invoiceDueDates[0].EUR * 1, _this.invoiceDueDates[1].EUR * 1, _this.invoiceDueDates[2].EUR * 1, _this.invoiceDueDates[3].EUR * 1];
            var vlGBP = [_this.invoiceDueDates[0].GBP * 1, _this.invoiceDueDates[1].GBP * 1, _this.invoiceDueDates[2].GBP * 1, _this.invoiceDueDates[3].GBP * 1];
            _this.series.push(vlTRY);
            _this.series.push(vlUSD);
            _this.series.push(vlEUR);
            _this.series.push(vlGBP);
            console.log(_this.series, "series");
        });
    };
    DashboardComponent.prototype.getInvoiceDueDates2 = function () {
        var _this = this;
        this.apiService.getData(this.businessID + "/2", "invoiceDueDates/").then(function (result) {
            _this.invoiceDueDates = result;
            var vlTRY = [_this.invoiceDueDates[0].TRY * 1, _this.invoiceDueDates[1].TRY * 1, _this.invoiceDueDates[2].TRY * 1, _this.invoiceDueDates[3].TRY * 1];
            var vlUSD = [_this.invoiceDueDates[0].USD * 1, _this.invoiceDueDates[1].USD * 1, _this.invoiceDueDates[2].USD * 1, _this.invoiceDueDates[3].USD * 1];
            var vlEUR = [_this.invoiceDueDates[0].EUR * 1, _this.invoiceDueDates[1].EUR * 1, _this.invoiceDueDates[2].EUR * 1, _this.invoiceDueDates[3].EUR * 1];
            var vlGBP = [_this.invoiceDueDates[0].GBP * 1, _this.invoiceDueDates[1].GBP * 1, _this.invoiceDueDates[2].GBP * 1, _this.invoiceDueDates[3].GBP * 1];
            _this.series2.push(vlTRY);
            _this.series2.push(vlUSD);
            _this.series2.push(vlEUR);
            _this.series2.push(vlGBP);
            console.log(_this.series2, "series2");
        });
    };
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getInvoiceDueDates();
        this.getInvoiceDueDates2();
        this.getCustomerInvoiceDueDates();
        this.resolveAfter2Seconds(1).then(function (value) {
            //this.getInvoiceDueDates2();
            _this.tableData = {
                headerRow: ['ID', 'Name', 'Salary', 'Country', 'City'],
                dataRows: [
                    ['US', 'USA', '2.920	', '53.23%'],
                    ['DE', 'Germany', '1.300', '20.43%'],
                    ['AU', 'Australia', '760', '10.35%'],
                    ['GB', 'United Kingdom	', '690', '7.87%'],
                    ['RO', 'Romania', '600', '5.94%'],
                    ['BR', 'Brasil', '550', '4.34%']
                ]
            };
            var mapData = {
                "AU": 760,
                "BR": 550,
                "CA": 120,
                "DE": 1300,
                "FR": 540,
                "GB": 690,
                "GE": 200,
                "IN": 200,
                "RO": 600,
                "RU": 300,
                "US": 2920,
            };
            $('#worldMap').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                zoomOnScroll: false,
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 0.9,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    }
                },
                series: {
                    regions: [{
                            values: mapData,
                            scale: ["#AAAAAA", "#444444"],
                            normalizeFunction: 'polynomial'
                        }]
                },
            });
            _this.emailChartType = _lbd_lbd_chart_lbd_chart_component__WEBPACK_IMPORTED_MODULE_1__["ChartType"].Pie;
            _this.emailChartData = {
                labels: ['62%', '32%', '6%'],
                series: [62, 32, 6]
            };
            _this.emailChartLegendItems = [
                { title: 'Open', imageClass: 'fa fa-circle text-info' },
                { title: 'Bounce', imageClass: 'fa fa-circle text-danger' },
                { title: 'Unsubscribe', imageClass: 'fa fa-circle text-warning' }
            ];
            _this.hoursChartType = _lbd_lbd_chart_lbd_chart_component__WEBPACK_IMPORTED_MODULE_1__["ChartType"].Line;
            _this.hoursChartData = {
                labels: ['9:00AM', '12:00AM', '3:00PM', '6:00PM', '9:00PM', '12:00PM', '3:00AM', '6:00AM'],
                series: [
                    [287, 385, 490, 492, 554, 586, 698, 695, 752, 788, 846, 944],
                    [67, 152, 143, 240, 287, 335, 435, 437, 539, 542, 544, 647],
                    [23, 113, 67, 108, 190, 239, 307, 308, 439, 410, 410, 509]
                ]
            };
            _this.hoursChartOptions = {
                low: 0,
                high: 800,
                showArea: false,
                height: '245px',
                axisX: {
                    showGrid: false,
                },
                lineSmooth: chartist__WEBPACK_IMPORTED_MODULE_2__["Interpolation"].simple({
                    divisor: 3
                }),
                showLine: true,
                showPoint: true,
            };
            _this.hoursChartResponsive = [
                ['screen and (max-width: 640px)', {
                        axisX: {
                            labelInterpolationFnc: function (value) {
                                return value[0];
                            }
                        }
                    }]
            ];
            _this.hoursChartLegendItems = [
                { title: 'Open', imageClass: 'fa fa-circle text-info' },
                { title: 'Click', imageClass: 'fa fa-circle text-danger' },
                { title: 'Click Second Time', imageClass: 'fa fa-circle text-warning' }
            ];
            /*
            this.activityChartType = ChartType.Bar;
            this.activityChartData = {
              labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
              series: [
                [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895],
                [412, 243, 280, 580, 453, 353, 300, 364, 368, 410, 636, 695]
              ]
            };
            this.activityChartOptions = {
              seriesBarDistance: 20,
              axisX: {
                showGrid: false
              },
              height: '245px'
            };
            this.activityChartResponsive = [
              ['screen and (max-width: 640px)', {
                seriesBarDistance: 5,
                axisX: {
                  labelInterpolationFnc: function (value) {
                    return value[0];
                  }
                }
              }]
            ];
            this.activityChartLegendItems = [
              { title: 'Tesla Model S', imageClass: 'fa fa-circle text-info' },
              { title: 'BMW 5 Series', imageClass: 'fa fa-circle text-danger' }
            ];
            */
            // Invoices Graphs - 1 
            _this.activityChartType = _lbd_lbd_chart_lbd_chart_component__WEBPACK_IMPORTED_MODULE_1__["ChartType"].Bar;
            _this.activityChartData = {
                labels: ['Upcoming', '1-30 days', '31-60 days', '61-90 days', '90 days'],
                series: _this.series
            };
            _this.activityChartOptions = {
                seriesBarDistance: 10,
                axisX: {
                    showGrid: true
                },
                height: '240px'
            };
            _this.activityChartResponsive = [
                ['screen and (max-width: 640px)', {
                        seriesBarDistance: 10,
                        axisX: {
                            labelInterpolationFnc: function (value) {
                                return value[0];
                            }
                        }
                    }]
            ];
            _this.activityChartLegendItems = [
                { title: 'TRY', imageClass: 'fa fa-circle text-info' },
                { title: 'USD', imageClass: 'fa fa-circle text-danger' },
                { title: 'EUR', imageClass: 'fa fa-circle text-warning' },
                { title: 'GBP', imageClass: 'fa fa-circle text-success' }
            ];
            // Invoices Graphs - 2
            _this.activityChartType2 = _lbd_lbd_chart_lbd_chart_component__WEBPACK_IMPORTED_MODULE_1__["ChartType"].Bar;
            _this.activityChartData2 = {
                labels: ['Upcoming', '1-30 days', '31-60 days', '61-90 days', '90 days'],
                series: _this.series2
            };
            // Invoices Graphs - 3
            _this.activityChartLegendItems3 = [
                { title: 'Bills', imageClass: 'fa fa-circle text-info' },
                { title: 'İnvoices', imageClass: 'fa fa-circle text-danger' }
            ];
            _this.activityChartType3 = _lbd_lbd_chart_lbd_chart_component__WEBPACK_IMPORTED_MODULE_1__["ChartType"].Bar;
            _this.activityChartData3 = {
                labels: ['3Months', '2Months', 'LastMonth', 'Cumulative'],
                series: _this.series3
            };
            console.log("graph completed");
            _this.tasks = [
                { title: 'Invoice EFD2019000000001 is due today', checked: false, check_number: 'checkbox1' },
                { title: 'Lines From Great Russian Literature? Or E-mails From My Boss?', checked: true, check_number: 'checkbox2' },
                {
                    title: 'Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit',
                    checked: true, check_number: 'checkbox3'
                },
                { title: 'Create 4 Invisible User Experiences you Never Knew About', checked: false, check_number: 'checkbox4' },
                { title: 'Read \'Following makes Medium better\'', checked: false, check_number: 'checkbox5' },
                { title: 'Unfollow 5 enemies from twitter', checked: false, check_number: 'checkbox6' },
            ];
        });
    };
    DashboardComponent.prototype.resolveAfter2Seconds = function (x) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                resolve(x);
            }, 600);
        });
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'dashboard-cmp',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/dashboard/dashboard.component.html")
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.module.ts":
/*!***********************************************!*\
  !*** ./src/app/dashboard/dashboard.module.ts ***!
  \***********************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _lbd_lbd_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../lbd/lbd.module */ "./src/app/lbd/lbd.module.ts");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _dashboard_routing__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard.routing */ "./src/app/dashboard/dashboard.routing.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_dashboard_routing__WEBPACK_IMPORTED_MODULE_6__["DashboardRoutes"]),
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _lbd_lbd_module__WEBPACK_IMPORTED_MODULE_4__["LbdModule"]
            ],
            declarations: [_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.routing.ts":
/*!************************************************!*\
  !*** ./src/app/dashboard/dashboard.routing.ts ***!
  \************************************************/
/*! exports provided: DashboardRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutes", function() { return DashboardRoutes; });
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/dashboard/dashboard.component.ts");

var DashboardRoutes = [{
        path: '',
        children: [{
                path: 'dashboard',
                component: _dashboard_component__WEBPACK_IMPORTED_MODULE_0__["DashboardComponent"]
            }]
    }];


/***/ })

}]);
//# sourceMappingURL=dashboard-dashboard-module.js.map