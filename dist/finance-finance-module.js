(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["finance-finance-module"],{

/***/ "./src/app/finance/aggr/aggr.component.html":
/*!**************************************************!*\
  !*** ./src/app/finance/aggr/aggr.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <form method=\"post\" class=\"form-horizontal\">\n                  <div class=\"card\">\n                      <div class=\"content\">\n                            <h4 class=\"title\">{{\"Financial Aggrement\"| translate}}\n                                    <span style='float:right;'>\n                                        <a (click)=\"excelExport()\" style='color:gray;margin-right:10px;font-size:16px;'><i class='fa fa-file-excel-o'></i></a>\n                                    </span>\n                                </h4>\n                                <p class=\"category\">{{\"Select Your Report Parameters\" | translate |titlecase}} </p>\n                                <!-- <legend></legend> -->\n                                <br>     \n\n                          <fieldset>\n                              <div class=\"form-group\">\n                                  <label class=\"col-sm-2 control-label\">Customer</label>\n                                  <div class=\"col-sm-10\">\n                                      <select name=\"customerID\" [(ngModel)]=\"customerID\" class=\"selectpicker selectCustomer\"\n                                          data-title=\"Select To\" data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                          <option value=\"0\">All</option>\n                                          <option *ngFor=\"let customer of customers\" [value]=\"customer.ID\">{{customer.customer | titlecase}}</option>\n                                      </select>\n                                  </div>\n                              </div>\n                          </fieldset>\n                          <fieldset>\n                              <div class=\"form-group\">\n                                  <label class=\"col-sm-2 control-label\">Start Date</label>\n                                  <div class=\"col-sm-2\">\n                                      <input type=\"text\" [(ngModel)]=\"invoiceStartDate\" name=\"invoiceStartDate\" class=\"form-control datetimepicker invoiceStartDate\"\n                                          placeholder=\"Pick Date Time\" />\n                                  </div>\n                                  <label class=\"col-sm-2 control-label\">End Date</label>\n                                  <div class=\"col-sm-2\">\n                                      <input type=\"text\" [(ngModel)]=\"invoiceEndDate\" name=\"invoiceEndDate\" class=\"form-control datetimepicker invoiceEndDate\"\n                                          placeholder=\"Pick Date Time\" />\n                                  </div>\n                                  <label class=\"col-sm-2 control-label\">Currency</label>\n                                  <div class=\"col-sm-2\">\n                                      <select name=\"invoiceCurrency\" [(ngModel)]=\"invoiceCurrency\"\n                                          class=\"selectpicker selectCurrency\" data-title=\"Currency\" data-style=\"btn-default btn-block\"\n                                          data-menu-style=\"dropdown-blue\">\n                                          <option [value]=\"1\">TRY</option>\n                                          <option [value]=\"2\">USD</option>\n                                          <option [value]=\"3\">EUR</option>\n                                          <option [value]=\"4\">GBP</option>\n                                      </select>\n                                  </div>\n                              </div>\n                          </fieldset>\n                          <fieldset>\n                          <div class=\"content\">\n                              <div style='float:right;'>\n                                <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                                <button style='margin-right:5px;' class=\"btn btn-success btn-fill btn-wd\"\n                                    (click)=\"clickReport()\">REPORT</button>\n                            </div>\n                        </div>\n                          </fieldset>\n                      </div>\n                      <div class=\"content\">\n                            <h4 class=\"title\">{{\"Details\"| translate}}</h4>\n                            <legend></legend>\n                      <div class=\"fresh-datatables\">\n                          <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\"\n                              width=\"100%\" style=\"width:100%\">\n                              <thead>\n                                  <tr>\n                                      <th class='text-center'>{{\"Invoice Date\" | translate | uppercase}}</th>\n                                      <th class=\"text-left\">{{\"Invoice#\" | translate | uppercase}}</th>\n                                      <th class='text-left'>{{\"Customer\" | translate | uppercase}}</th>\n                                      <th class=\"text-right\">{{\"Owed\" | translate | uppercase}}</th>\n                                      <th class=\"text-right\">{{\"VAT\" | translate | uppercase}}</th>\n                                      <th class=\"text-center\"></th>\n                                      <th class=\"text-right\">{{\"Due\" | translate | uppercase}}</th>\n                                      <th class=\"text-right\">{{\"VAT\" | translate | uppercase}}</th>\n                                      <th class=\"text-center\"></th>\n                                      <th class=\"text-center\">{{\"Payment Date\" | translate | uppercase}}</th>\n                                      <th class=\"text-center\">{{\"Payment\" | translate | uppercase}}</th>      \n                                      <th class=\"text-center\">{{\"Amount-Payment\" | translate | uppercase}}</th>   \n                                  </tr>\n                              </thead>\n                              <tbody>\n                                  <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                      <td class='text-center'>{{row.invoiceDate}}</td>\n                                      <td class='text-left'><a href=\"#/sales/invoice/{{row.ID}}?invoiceType={{row.invoiceType}}\" target=\"_blank\">{{row.invoiceNumber}}</a></td>\n                                      <td class='text-left'>{{row.customer | titlecase}}</td>\n                                      <td class='text-right'>{{row.amount1 | number}}</td>\n                                      <td class='text-right'>{{row.vatAmount1 | number}}</td>\n                                      <td class='text-center'>{{row.currency}}</td>\n                                      <td class='text-right'>{{row.amount2*-1 | number}}</td>\n                                      <td class='text-right'>{{row.vatAmount2*-1 | number}}</td>\n                                      <td class='text-center'>{{row.currency}}</td>\n                                      <td class='text-center'>{{row.paymentDate}}</td>\n                                      <td class='text-right'>{{row.paymentAmount}}</td>\n                                      <td class='text-right' [ngStyle]=\"{'font-weight':(row.paymentAmount>0) ? 'bold' : ''}\">{{row.pdif}}</td>\n                                  </tr>\n                              </tbody>\n                              <tfoot>\n                                  <tr>\n                                        <th>{{\"TOTALS\"|translate}}</th>\n                                        <th></th>\n                                        <th></th>\n                                        <th class='text-right'>{{totals.amount1 | number}}</th>\n                                        <th class='text-right'>{{totals.amount1Vat | number}}</th>\n                                        <th></th>\n                                        <th class='text-right'>{{totals.amount2 | number}}</th>\n                                        <th class='text-right'>{{totals.amount2Vat | number}}</th>\n                                        <th></th>\n                                        <th></th>\n                                  </tr>\n                              </tfoot>\n                          </table>\n                      </div>\n                      </div>\n                  </div>\n              </form>\n          </div>\n      </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/finance/aggr/aggr.component.scss":
/*!**************************************************!*\
  !*** ./src/app/finance/aggr/aggr.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvYWdnci9hZ2dyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/finance/aggr/aggr.component.ts":
/*!************************************************!*\
  !*** ./src/app/finance/aggr/aggr.component.ts ***!
  \************************************************/
/*! exports provided: AggrComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AggrComponent", function() { return AggrComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_providers_excel_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/providers/excel.service */ "./src/app/providers/excel.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AggrComponent = /** @class */ (function () {
    function AggrComponent(apiService, activeRoute, excelService, datePipe) {
        var _this = this;
        this.apiService = apiService;
        this.activeRoute = activeRoute;
        this.excelService = excelService;
        this.datePipe = datePipe;
        this.customers = [];
        this.reports = [];
        this.invoiceCurrency = 1;
        this.totals = [];
        var routeParams = this.activeRoute.snapshot.params;
        this.activeRoute.queryParams
            .subscribe(function (params) {
            // Defaults to 0 if no query param provided.
            _this.customerID = +params['customerID'] || 0;
        });
        this.businessID = localStorage.getItem("businessID");
        var now = new Date();
        this.invoiceStartDate = this.datePipe.transform(now, "dd-MM-yyyy");
        this.invoiceEndDate = this.datePipe.transform(now, "dd-MM-yyyy");
    }
    AggrComponent.prototype.ngOnInit = function () {
        this.dataTable = {
            headerRow: ['#', 'Date', 'Invoice Number', 'Description', 'Customer', 'Amount1', 'vatAmount1', 'Amount2', 'vatAmount2'],
            footerRow: ['Row Number', 'Date', 'Invoice Number', 'Description', 'Customer', 'Amount1', 'vatAmount1', 'Amount2', 'vatAmount2'],
            dataRows: []
        };
        this.getCustomer(0);
        $('.datetimepicker').datetimepicker({
            locale: 'tr',
            format: 'DD-MM-YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    };
    AggrComponent.prototype.excelExport = function () {
        var obj = this.reports;
        this.excelService.exportAsExcelFile(obj, 'aggr');
    };
    AggrComponent.prototype.getCustomer = function (customerID) {
        var _this = this;
        this.apiService.getData(this.businessID + "/0/" + customerID, "customers/").then(function (result) {
            console.log(result);
            _this.customers = result;
            _this.resolveAfter2Seconds(20).then(function (value) {
                _this.spInit();
                $(".selectCustomer").selectpicker('refresh');
                if (_this.customerID > 0) {
                    $(".selectCustomer").selectpicker('val', _this.customerID);
                }
            });
        });
    };
    AggrComponent.prototype.spInit = function () {
        console.log("Init Select");
        $(".selectCustomer").selectpicker();
    };
    AggrComponent.prototype.resolveAfter2Seconds = function (x) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                resolve(x);
            }, 2000);
        });
    };
    AggrComponent.prototype.clickCancel = function () {
    };
    AggrComponent.prototype.clickReport = function () {
        var _this = this;
        this.invoiceStartDate = $('.invoiceStartDate').val();
        this.invoiceEndDate = $('.invoiceEndDate').val();
        this.apiService.getData(this.businessID + "/" + this.customerID + "/" + this.invoiceStartDate + "/" + this.invoiceEndDate + '/' + this.invoiceCurrency, "aggr/").then(function (result) {
            console.log(result);
            _this.reports = result;
            var amount1 = 0;
            var amount1Vat = 0;
            var amount2 = 0;
            var amount2Vat = 0;
            _this.reports.forEach(function (element) {
                amount1 = amount1 + (element.amount1 * 1);
                amount2 = amount2 + (element.amount2 * 1);
                amount1Vat = amount1Vat + (element.vatAmount1 * 1);
                amount2Vat = amount2Vat + (element.vatAmount2 * 1);
            });
            _this.totals["amount1"] = amount1.toFixed(2);
            _this.totals["amount2"] = amount2.toFixed(2);
            _this.totals["amount1Vat"] = amount1Vat.toFixed(2);
            _this.totals["amount2Vat"] = amount2Vat.toFixed(2);
            console.log(_this.totals, "totals");
            _this.dataTable = {
                headerRow: ['#', 'Date', 'Invoice Number', 'Description', 'Customer', 'Amount1', 'vatAmount1', 'Amount2', 'vatAmount2'],
                footerRow: ['Row Number', 'Date', 'Invoice Number', 'Description', 'Customer', 'Amount1', 'vatAmount1', 'Amount2', 'vatAmount2'],
                dataRows: _this.reports
            };
        });
    };
    AggrComponent.prototype.ngAfterViewInit = function () {
        $(".selectpicker").selectpicker();
        $(".selectCurrency").selectpicker('val', 1);
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: " ",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                paginate: {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                }
            }
        });
    };
    AggrComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-aggr',
            template: __webpack_require__(/*! ./aggr.component.html */ "./src/app/finance/aggr/aggr.component.html"),
            styles: [__webpack_require__(/*! ./aggr.component.scss */ "./src/app/finance/aggr/aggr.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], app_providers_excel_service__WEBPACK_IMPORTED_MODULE_3__["ExcelService"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"]])
    ], AggrComponent);
    return AggrComponent;
}());



/***/ }),

/***/ "./src/app/finance/babs/babs.component.html":
/*!**************************************************!*\
  !*** ./src/app/finance/babs/babs.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <form method=\"post\" class=\"form-horizontal\">\n                  <div class=\"card\">\n                      <div class=\"content\">\n                            <h4 class=\"title\">{{\"BABS\"| translate}}\n                                    <span style='float:right;'>\n                                        <a title=\"Excel Export\" style=\"cursor:pointer;\" (click)=\"excelExport()\" style='color:gray;margin-right:10px;font-size:16px;'><i class='fa fa-file-excel-o'></i></a>\n                                    </span>\n                                    <span style='float:right;'>\n                                        <a title=\"Select Term\" style=\"cursor:pointer;\" (click)=\"selectTerm()\" style='color:gray;margin-right:10px;font-size:16px;'><i class='fa fa-calendar'></i></a>\n                                    </span>\n                                </h4>   \n                  \n                      </div>\n                      <div class=\"content\">\n                            \n                      <div class=\"fresh-datatables\">\n                          <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\"\n                              width=\"100%\" style=\"width:100%\">\n                              <thead>\n                                  <tr>\n                                      <th class='text-center'>{{\"Term\" | translate | uppercase}}</th>\n                                      <th class='text-left'>{{\"Customer\" | translate | uppercase}}</th>\n                                      <th class=\"text-right\">{{\"Form Type\" | translate | uppercase}}</th>\n                                      <th class=\"text-right\">{{\"Invoice Count\" | translate | uppercase}}</th>\n                                      <th class=\"text-right\">{{\"Amount\" | translate | uppercase}}</th>\n                                      <th class=\"text-center\"></th>\n                                      <th class=\"text-center\">{{\"Response Date\" | translate | uppercase}}</th>\n                                      <th class=\"text-center\">{{\"Response\" | translate | uppercase}}</th>   \n                                      <th></th>   \n                                  </tr>\n                              </thead>\n                              <tbody>\n                                  <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                      <td class='text-center'>{{row.term}}</td>\n                                      <td class='text-left'>{{row.customer | titlecase}}</td>\n                                      <td class='text-center'>{{row.formType | uppercase}}</td>\n                                      <td class='text-right'>{{row.invoiceCount | number}}</td>\n                                      <td class='text-right'>{{row.amount | number}}</td>\n                                      <td></td>\n                                      <td class='text-center'>{{row.responseDate}}</td>\n                                      <td class='text-center'><span *ngIf=\"row.responseText!='' ? true : false\" class=\"btn btn-xs \">{{row.responseText | uppercase}}</span></td>\n                                      <td class='text-center'><a *ngIf=\"row.responseFile!='' ? true : false\" href=\"https://crmapi.efdigitalcodes.com/Uploads/babs/{{row.responseFile}}\" target=\"_blank\"><i class=\"fa fa-paperclip\"></i></a></td>\n                                  </tr>\n                              </tbody>\n                             \n                          </table>\n                      </div>\n                      </div>\n                  </div>\n              </form>\n          </div>\n      </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/finance/babs/babs.component.scss":
/*!**************************************************!*\
  !*** ./src/app/finance/babs/babs.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvYmFicy9iYWJzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/finance/babs/babs.component.ts":
/*!************************************************!*\
  !*** ./src/app/finance/babs/babs.component.ts ***!
  \************************************************/
/*! exports provided: BabsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BabsComponent", function() { return BabsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var app_providers_excel_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/providers/excel.service */ "./src/app/providers/excel.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _term_term_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./term/term.component */ "./src/app/finance/babs/term/term.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BabsComponent = /** @class */ (function () {
    function BabsComponent(apiService, excelService, modalService) {
        this.apiService = apiService;
        this.excelService = excelService;
        this.modalService = modalService;
        this.bb = [];
        this.businessID = localStorage.getItem("businessID");
    }
    BabsComponent.prototype.ngOnInit = function () {
        this.babs();
    };
    BabsComponent.prototype.excelExport = function () {
        var obj = this.bb;
        this.excelService.exportAsExcelFile(obj, 'babs');
    };
    BabsComponent.prototype.selectTerm = function () {
        var modalRef = this.modalService.open(_term_term_component__WEBPACK_IMPORTED_MODULE_4__["TermComponent"], { "backdrop": "static" });
        modalRef.result.then(function (data) {
            // on close
            console.log(data, "result data");
        }, function () {
            // on dismiss
        });
    };
    BabsComponent.prototype.babs = function () {
        var _this = this;
        this.apiService.getData(this.businessID, "babs/").then(function (result) {
            console.log(result);
            _this.bb = result;
            _this.dataTable = {
                headerRow: ['#', 'Term', 'Customer', 'Form Type', 'Invoice Count', 'Amount', 'Response Date', 'Response'],
                footerRow: ['Row Number', 'Term', 'Customer', 'Form Type', 'Invoice Count', 'Amount', 'Response Date', 'Response'],
                dataRows: _this.bb
            };
        });
    };
    BabsComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: " ",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                paginate: {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                }
            }
        });
    };
    BabsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-babs',
            template: __webpack_require__(/*! ./babs.component.html */ "./src/app/finance/babs/babs.component.html"),
            styles: [__webpack_require__(/*! ./babs.component.scss */ "./src/app/finance/babs/babs.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], app_providers_excel_service__WEBPACK_IMPORTED_MODULE_2__["ExcelService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]])
    ], BabsComponent);
    return BabsComponent;
}());



/***/ }),

/***/ "./src/app/finance/balance/balance.component.html":
/*!********************************************************!*\
  !*** ./src/app/finance/balance/balance.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <form method=\"post\" class=\"form-horizontal\">\n          <div class=\"card\">\n            <div class=\"content\">\n              <h4 class=\"title\">{{\"Balance\"| translate}}\n                <span style='float:right;'>\n                  <a title=\"Excel Export\" (click)=\"excelExport()\" style='color:gray;margin-right:10px;font-size:16px;'><i\n                      class='fa fa-file-excel-o'></i></a>\n                </span>\n              </h4>\n              <p class=\"category\">{{\"Select Your Balance Parameters\" | translate |titlecase}} </p>\n              <!-- <legend></legend> -->\n              <br>\n              <fieldset>\n                <div class=\"form-group\">\n                  <label class=\"col-sm-2 control-label\">Start Date</label>\n                  <div class=\"col-sm-2\">\n                    <input type=\"text\" [(ngModel)]=\"invoiceStartDate\" name=\"invoiceStartDate\"\n                      class=\"form-control datetimepicker invoiceStartDate\" placeholder=\"Pick Date Time\" />\n                  </div>\n                  <label class=\"col-sm-2 control-label\">End Date</label>\n                  <div class=\"col-sm-2\">\n                    <input type=\"text\" [(ngModel)]=\"invoiceEndDate\" name=\"invoiceEndDate\"\n                      class=\"form-control datetimepicker invoiceEndDate\" placeholder=\"Pick Date Time\" />\n                  </div>\n                  <label class=\"col-sm-2 control-label\">Currency</label>\n                  <div class=\"col-sm-2\">\n                    <select name=\"invoiceCurrency\" [(ngModel)]=\"invoiceCurrency\" class=\"selectpicker selectCurrency\"\n                      data-title=\"Currency\" data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                      <option [value]=\"1\">TRY</option>\n                      <option [value]=\"2\">USD</option>\n                      <option [value]=\"3\">EUR</option>\n                      <option [value]=\"4\">GBP</option>\n                    </select>\n                  </div>\n                </div>\n              </fieldset>\n              <fieldset>\n                <div class=\"content\">\n                  <div style='float:right;'>\n                    <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\"\n                      (click)=\"clickCancel()\">CANCEL</button>\n                    <button style='margin-right:5px;' class=\"btn btn-success btn-fill btn-wd\"\n                      (click)=\"clickReport()\">REPORT</button>\n                  </div>\n                </div>\n              </fieldset>\n            </div>\n            <div class=\"content\">\n              <h4 class=\"title\">{{\"Details\"| translate}}</h4>\n              <legend></legend>\n              <div class=\"fresh-datatables\">\n                <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\"\n                  style=\"width:100%\">\n                  <thead>\n                    <tr>\n                      <th class='text-center'>{{\"Account\" | translate | uppercase}}</th>\n                      <th class=\"text-left\">{{\"SubAccount\" | translate | uppercase}}</th>\n                      <th class='text-left'>{{\"AccountName\" | translate | uppercase}}</th>\n                      <th class=\"text-right\">{{\"Currency\" | translate | uppercase}}</th>\n                      <th class=\"text-right\">{{\"Amount\" | translate | uppercase}}</th>\n                      <th class=\"text-center\"></th>\n                      <th class=\"text-center\">{{\"Amount (Selected Currency)\" | translate | uppercase}}</th>\n                      <th></th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                    <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                      <td class='text-center'>{{row.account}}</td>\n                      <td class='text-left'>{{row.subAccount}}</td>\n                      <td class='text-left'>{{row.accName | titlecase}}</td>\n                      <td class='text-right'>{{row.currency | uppercase}}</td>\n                      <td class='text-right'>{{row.amount | number}}</td>\n                      <td class='text-center'></td>\n                      <td class='text-right'>{{row.amounts | number}}</td>\n                      <td> <a (click)=\"showDetail(row.ID,row.invoiceCurrency)\"><i\n                            class=\"fa fa-search\"></i></a></td>\n                    </tr>\n                  </tbody>\n                  <tfoot>\n                    <tr>\n                      <th>{{\"TOTALS\"|translate}}</th>\n                      <th></th>\n                      <th></th>\n                      <th></th>\n                      <th class='text-right'>{{totals.amount1 | number}}</th>\n                      <th></th>\n                      <th class='text-right'>{{totals.amount2 | number}}</th>\n                      <th></th>\n                    </tr>\n                  </tfoot>\n                </table>\n              </div>\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/finance/balance/balance.component.scss":
/*!********************************************************!*\
  !*** ./src/app/finance/balance/balance.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvYmFsYW5jZS9iYWxhbmNlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/finance/balance/balance.component.ts":
/*!******************************************************!*\
  !*** ./src/app/finance/balance/balance.component.ts ***!
  \******************************************************/
/*! exports provided: BalanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BalanceComponent", function() { return BalanceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_providers_excel_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/providers/excel.service */ "./src/app/providers/excel.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _balance_details_balance_details_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../balance-details/balance-details.component */ "./src/app/finance/balance-details/balance-details.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var BalanceComponent = /** @class */ (function () {
    function BalanceComponent(apiService, activeRoute, excelService, modalService, datePipe) {
        this.apiService = apiService;
        this.activeRoute = activeRoute;
        this.excelService = excelService;
        this.modalService = modalService;
        this.datePipe = datePipe;
        this.reports = [];
        this.invoiceCurrency = 1;
        this.totals = [];
        this.businessID = localStorage.getItem("businessID");
        var now = new Date();
        this.invoiceStartDate = this.datePipe.transform(now, "dd-MM-yyyy");
        this.invoiceEndDate = this.datePipe.transform(now, "dd-MM-yyyy");
    }
    BalanceComponent.prototype.ngOnInit = function () {
        this.dataTable = {
            headerRow: ['#', 'Account', 'SubAccount', 'Account Name', 'Currency', 'Amount', 'Amount(Currency)'],
            footerRow: ['Row Number', 'Account', 'SubAccount', 'Account Name', 'Currency', 'Amount', 'Amount(Currency)'],
            dataRows: []
        };
        $('.datetimepicker').datetimepicker({
            locale: 'tr',
            format: 'DD-MM-YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    };
    BalanceComponent.prototype.excelExport = function () {
        var obj = this.reports;
        this.excelService.exportAsExcelFile(obj, 'balance');
    };
    BalanceComponent.prototype.spInit = function () {
        console.log("Init Select");
        $(".selectCustomer").selectpicker();
    };
    BalanceComponent.prototype.resolveAfter2Seconds = function (x) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                resolve(x);
            }, 2000);
        });
    };
    BalanceComponent.prototype.clickCancel = function () {
    };
    BalanceComponent.prototype.showDetail = function (rowID, ic) {
        var modalRef = this.modalService.open(_balance_details_balance_details_component__WEBPACK_IMPORTED_MODULE_5__["BalanceDetailsComponent"], { "size": "lg", "backdrop": "static" });
        modalRef.componentInstance.ID = rowID;
        modalRef.componentInstance.startDate = this.invoiceStartDate;
        modalRef.componentInstance.endDate = this.invoiceEndDate;
        modalRef.componentInstance.currency = ic;
        modalRef.result.then(function (data) {
            // on close
            console.log(data, "result data");
            if (data > 0) {
                //this.invoices[index].paymentID = data;
            }
        }, function () {
            // on dismiss
        });
    };
    BalanceComponent.prototype.clickReport = function () {
        var _this = this;
        this.invoiceStartDate = $('.invoiceStartDate').val();
        this.invoiceEndDate = $('.invoiceEndDate').val();
        this.apiService.getData(this.businessID + "/" + this.invoiceStartDate + "/" + this.invoiceEndDate + '/' + this.invoiceCurrency, "balance/").then(function (result) {
            console.log(result);
            _this.reports = result;
            var amount1 = 0;
            var amount2 = 0;
            _this.reports.forEach(function (element) {
                amount1 = amount1 + (element.amount * 1);
                amount2 = amount2 + (element.amounts * 1);
            });
            _this.totals["amount1"] = amount1.toFixed(2);
            _this.totals["amount2"] = amount2.toFixed(2);
            console.log(_this.totals, "totals");
            _this.dataTable = {
                headerRow: ['#', 'Account', 'SubAccount', 'Account Name', 'Currency', 'Amount', 'Amount(Currency)'],
                footerRow: ['Row Number', 'Account', 'SubAccount', 'Account Name', 'Currency', 'Amount', 'Amount(Currency)'],
                dataRows: _this.reports
            };
        });
    };
    BalanceComponent.prototype.ngAfterViewInit = function () {
        $(".selectpicker").selectpicker();
        $(".selectCurrency").selectpicker('val', 1);
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: " ",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                paginate: {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                }
            }
        });
    };
    BalanceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-balance',
            template: __webpack_require__(/*! ./balance.component.html */ "./src/app/finance/balance/balance.component.html"),
            styles: [__webpack_require__(/*! ./balance.component.scss */ "./src/app/finance/balance/balance.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], app_providers_excel_service__WEBPACK_IMPORTED_MODULE_3__["ExcelService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["DatePipe"]])
    ], BalanceComponent);
    return BalanceComponent;
}());



/***/ }),

/***/ "./src/app/finance/bank/bank.component.html":
/*!**************************************************!*\
  !*** ./src/app/finance/bank/bank.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <form method=\"post\" class=\"form-horizontal\">\n                    <h4 class=\"title\">New/Edit Bank</h4>\n                    <div class=\"card\">\n                        <!-- mains will be here -->\n                        <div class=\"header\">\n                            <legend>Bank Info Card</legend>\n                        </div>\n                        <div class=\"content\">\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Name</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"name\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"bank.name\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Bank ID</label>\n                                    <div class=\"col-sm-10\">\n                                            <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"bankID\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"bank.bankID\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Swift</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"SWIFT\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"bank.SWIFT\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Phone</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"phone\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"bank.phone\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Email</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"kepEmail\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"bank.kepEmail\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                    <div class=\"form-group\">\n                                        <label class=\"col-sm-2 control-label\">Address</label>\n                                        <div class=\"col-sm-10\">\n                                            <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"address\"\n                                                [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"bank.address\">\n                                        </div>\n                                    </div>\n                                </fieldset>\n                                <fieldset>\n                                        <div class=\"form-group\">\n                                            <label class=\"col-sm-2 control-label\">Country</label>\n                                            <div class=\"col-sm-10\">\n                                                    <select name=\"countryID\" [(ngModel)]=\"bank.countryID\" class=\"selectpicker selectCountry\" \n                                                    data-title=\"Select Country\" data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                                    <option selected *ngFor=\"let country of countries\" [value]=\"country.ID\">{{country.name}}</option>\n                                                </select>\n                                            </div>\n                                        </div>\n                                    </fieldset>\n                        </div>\n                    </div>\n                    <div class=\"content\">\n                        <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"deleteBank()\">DELETE</button>\n                        <div style='float:right;'>\n                            <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                            <button class=\"btn btn-success btn-fill btn-wd\" (click)=\"saveBank()\">SAVE</button>\n                        </div>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n"

/***/ }),

/***/ "./src/app/finance/bank/bank.component.scss":
/*!**************************************************!*\
  !*** ./src/app/finance/bank/bank.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvYmFuay9iYW5rLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/finance/bank/bank.component.ts":
/*!************************************************!*\
  !*** ./src/app/finance/bank/bank.component.ts ***!
  \************************************************/
/*! exports provided: BankComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankComponent", function() { return BankComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BankComponent = /** @class */ (function () {
    function BankComponent(apiService, activeRoute, router) {
        this.apiService = apiService;
        this.activeRoute = activeRoute;
        this.router = router;
        this.bankAccount = [];
        this.bank = [];
        this.countries = [];
        var routeParams = this.activeRoute.snapshot.params;
        this.bankID = routeParams.id;
        this.businessID = localStorage.getItem("businessID");
    }
    BankComponent.prototype.ngOnInit = function () {
        this.getCountries();
    };
    BankComponent.prototype.deleteBank = function () {
        var _this = this;
        this.apiService.deleteData(this.bank.bankID, "bank/delete/").then(function (result) {
            console.log(result);
            _this.router.navigateByUrl("/finance/banks");
        });
    };
    BankComponent.prototype.clickCancel = function () {
        this.router.navigateByUrl("/finance/banks");
    };
    BankComponent.prototype.saveBank = function () {
        var _this = this;
        this.apiService.postData(this.bank.bankID, "bank/save/", JSON.stringify(this.bank)).then(function (result) {
            console.log(result);
            _this.router.navigateByUrl("/finance/banks");
        });
    };
    BankComponent.prototype.getBank = function (bankID) {
        var _this = this;
        if (bankID > 0) {
            this.apiService.getData(bankID, "banks/").then(function (result) {
                _this.bank = result[0];
                _this.spInit();
            });
        }
        else {
            this.bank = { "ID": 0, "SWIFT": "", "address": "", "bankID": 0, "country": "", "countryID": 0, "kepEmail": "", "name": "", "phone": "" };
            this.spInit();
        }
    };
    BankComponent.prototype.getCountries = function () {
        var _this = this;
        this.apiService.getData("", "country").then(function (result) {
            _this.countries = result;
            _this.getBank(_this.bankID);
        });
    };
    BankComponent.prototype.spInit = function () {
        var _this = this;
        setTimeout(function () {
            $(".selectCountry").selectpicker('val', _this.bank.countryID);
        }, 500);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BankComponent.prototype, "accountID", void 0);
    BankComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bank',
            template: __webpack_require__(/*! ./bank.component.html */ "./src/app/finance/bank/bank.component.html"),
            styles: [__webpack_require__(/*! ./bank.component.scss */ "./src/app/finance/bank/bank.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], BankComponent);
    return BankComponent;
}());



/***/ }),

/***/ "./src/app/finance/bankAccount/bankAccount.component.html":
/*!****************************************************************!*\
  !*** ./src/app/finance/bankAccount/bankAccount.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <form method=\"post\" class=\"form-horizontal\">\n                    <h4 class=\"title\">New/Edit Bank Account</h4>\n                    <div class=\"card\">\n                        <!-- mains will be here -->\n                        <div class=\"header\">\n                            <legend>Bank Account Card</legend>\n                        </div>\n                        <div class=\"content\">\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Name</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"account\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"bankAccount.account\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Bank</label>\n                                    <div class=\"col-sm-10\">\n                                            <select name=\"bankID\" [(ngModel)]=\"bankAccount.bankID\" class=\"selectpicker selectBank\" (change)=\"getBankBranches(bankAccount.bankID)\"\n                                            data-title=\"Select Bank\" data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                            <option selected *ngFor=\"let bank of banks\" [value]=\"bank.bankID\">{{bank.name}}</option>\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Branch</label>\n                                    <div class=\"col-sm-10\">\n                                        <select name=\"bankID\" [(ngModel)]=\"bankAccount.branchID\" class=\"selectpicker selectBranch\" \n                                            data-title=\"Select Branch\" data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                            <option selected *ngFor=\"let branch of branches\" [value]=\"branch.branchID\">{{branch.branch}}</option>\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Account Number</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"accountID\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"bankAccount.accountID\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">IBAN</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"IBAN\"\n                                            [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"bankAccount.IBAN\">\n                                    </div>\n                                </div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Currency</label>\n                                    <div class=\"col-sm-10\">\n                                        <select name=\"currencyID\" [(ngModel)]=\"bankAccount.currencyID\" class=\"selectpicker selectcurrency\"\n                                            data-title=\"Currency\" data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                            <option selected [value]=\"1\">TRY</option>\n                                            <option selected [value]=\"2\">USD</option>\n                                            <option selected [value]=\"3\">EUR</option>\n                                            <option selected [value]=\"4\">GBP</option>\n                                        </select>\n                                    </div>\n                                </div>\n                            </fieldset>\n                        </div>\n                    </div>\n                    <div class=\"content\">\n                        <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"deleteAccount()\">DELETE</button>\n                        <div style='float:right;'>\n                            <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                            <button class=\"btn btn-success btn-fill btn-wd\" (click)=\"saveAccount()\">SAVE</button>\n                        </div>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n"

/***/ }),

/***/ "./src/app/finance/bankAccount/bankAccount.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/finance/bankAccount/bankAccount.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvYmFua0FjY291bnQvYmFua0FjY291bnQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/finance/bankAccount/bankAccount.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/finance/bankAccount/bankAccount.component.ts ***!
  \**************************************************************/
/*! exports provided: BankAccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankAccountComponent", function() { return BankAccountComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BankAccountComponent = /** @class */ (function () {
    function BankAccountComponent(apiService, activeRoute, router) {
        this.apiService = apiService;
        this.activeRoute = activeRoute;
        this.router = router;
        this.bankID = 0;
        this.bankAccount = [];
        this.banks = [];
        this.branches = [];
        var routeParams = this.activeRoute.snapshot.params;
        this.bankID = routeParams.id;
        this.businessID = localStorage.getItem("businessID");
    }
    BankAccountComponent.prototype.ngOnInit = function () {
        // New record control
        if (this.bankID > 0) {
            this.getBankAccount(this.bankID);
        }
        else {
            this.bankAccount = { "IBAN": "", "ID": 0, "account": "", "accountID": "", "bank": "", "bankAccountID": 0, "bankID": 0, "currency": "", "currencyID": 0 };
        }
        console.log(this.bankAccount, "bankaccount");
        this.getBank(0);
    };
    BankAccountComponent.prototype.getBankAccount = function (bankID) {
        var _this = this;
        this.apiService.getData((this.businessID + "/" + bankID + "/1"), "accounts/").then(function (result) {
            _this.bankAccount = result[0];
        });
    };
    BankAccountComponent.prototype.getBankBranches = function (bankID) {
        var _this = this;
        this.apiService.getData(bankID, "bankBranches/").then(function (result) {
            _this.branches = result;
            _this.spInit();
        });
    };
    BankAccountComponent.prototype.deleteAccount = function () {
        var _this = this;
        this.apiService.deleteData(this.bankID, "account/delete/").then(function (result) {
            console.log(result);
            _this.router.navigateByUrl("/finance/bankAccounts");
        });
    };
    BankAccountComponent.prototype.clickCancel = function () {
        this.router.navigateByUrl("/finance/bankAccounts");
    };
    BankAccountComponent.prototype.saveAccount = function () {
        var _this = this;
        this.bankAccount.branchID = 0;
        this.bankAccount.asID = this.businessID;
        this.apiService.postData(this.bankAccount.ID, "account/save/", JSON.stringify(this.bankAccount)).then(function (result) {
            console.log(result);
            _this.router.navigateByUrl("/finance/bankAccounts");
        });
    };
    BankAccountComponent.prototype.getBank = function (bankID) {
        var _this = this;
        this.apiService.getData(bankID, "banks/").then(function (result) {
            _this.banks = result;
            if (_this.bankAccount.bankID > 0) {
                _this.getBankBranches(_this.bankAccount.bankID);
            }
            else {
                _this.spInit();
            }
        });
    };
    BankAccountComponent.prototype.spInit = function () {
        var _this = this;
        console.log("spInit");
        setTimeout(function () {
            $(".selectBank").selectpicker('val', _this.bankAccount.bankID);
            $(".selectcurrency").selectpicker('val', _this.bankAccount.currencyID);
            $(".selectBranch").selectpicker('val', _this.bankAccount.branchID);
            $(".selectBranch").selectpicker('refresh');
        }, 500);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BankAccountComponent.prototype, "accountID", void 0);
    BankAccountComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bankAccount',
            template: __webpack_require__(/*! ./bankAccount.component.html */ "./src/app/finance/bankAccount/bankAccount.component.html"),
            styles: [__webpack_require__(/*! ./bankAccount.component.scss */ "./src/app/finance/bankAccount/bankAccount.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], BankAccountComponent);
    return BankAccountComponent;
}());



/***/ }),

/***/ "./src/app/finance/bankAccounts/bankAccounts.component.html":
/*!******************************************************************!*\
  !*** ./src/app/finance/bankAccounts/bankAccounts.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"content\">\n                          <h4 class=\"title\">{{\"bank accounts\" | translate |titlecase}} \n                                  <span style='float:right;'>\n                                        \n                                        <a routerLink='/finance/bankAccount/0' style='color:gray;margin-right:10px;font-size:16px;'><i class='fa fa-plus-square-o'></i></a>\n                                  </span>\n                              </h4>\n                              <p class=\"category\"><a routerLink='/finance/banks/'>{{\"Click for List of Banks\" | translate |titlecase}} </a></p>\n                          <!-- <legend></legend> -->\n                          <br>\n                      <div class=\"toolbar\">\n                          \n                          <!--        Here you can write extra buttons/actions for the toolbar              -->\n                      </div>\n                      <div class=\"fresh-datatables\">\n                          <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                              <thead>\n                                  <tr>\n                                    <th>{{ dataTable.headerRow[0] }}</th>\n                                    <th class='text-left'>{{ dataTable.headerRow[1] | translate }}</th>\n                                    <th class='text-left'>{{ dataTable.headerRow[2] }}</th>\n                                    <th class='text-center'>{{ dataTable.headerRow[3] }}</th>\n                                    <th class='text-right'>{{ dataTable.headerRow[4] }}</th>\n                                    <th class='text-center'>{{ dataTable.headerRow[5] }}</th>\n                                    <th class='text-right'>{{ dataTable.headerRow[6] }}</th>\n                                    <th class='text-right'>{{ dataTable.headerRow[7] }}</th>\n                                  </tr>\n                              </thead>\n                              <tbody>\n                                      <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                          <td><span class=\"dot {{row.color}}\"></span></td>\n                                          <td class='text-left'><a routerLink='/finance/bankAccount/{{row.bankAccountID}}'>{{row.account}}</a></td>\n                                          <td class='text-left'>{{row.bank| uppercase}}</td>\n                                          <td class='text-center'><span class=\"btn btn-outline-primary btn-xs\">{{row.accountID | uppercase}}</span></td>\n                                          <td class='text-right'>{{row.IBAN}}</td>\n                                          <td class='text-center'><span class=\"btn btn-success btn-xs\">{{row.currency}}</span></td>\n                                          <td class='text-right'>{{row.closingBalance | currency: row.currency }}</td>\n                                          <td class=\"text-right tableicon\">\n                                                <a routerLink=\"/finance/bankAccount/{{row.bankAccountID}}\"><i class=\"fa fa-search\"> </i>&nbsp;</a>\n                                                <a routerLink=\"/finance/transactions/{{row.bankAccountID}}\"><i class=\"fa fa-bars\"> </i>&nbsp;</a>\n                                            </td>\n                                      </tr>\n                                  </tbody>\n                          </table>\n                      </div>\n                  </div>\n                  <!-- end content-->\n              </div>\n              <!--  end card  -->\n          </div>\n          <!-- end col-md-12 -->\n      </div>\n      <!-- end row -->\n  </div>\n  </div>\n  "

/***/ }),

/***/ "./src/app/finance/bankAccounts/bankAccounts.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/finance/bankAccounts/bankAccounts.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvYmFua0FjY291bnRzL2JhbmtBY2NvdW50cy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/finance/bankAccounts/bankAccounts.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/finance/bankAccounts/bankAccounts.component.ts ***!
  \****************************************************************/
/*! exports provided: BankAccountsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankAccountsComponent", function() { return BankAccountsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BankAccountsComponent = /** @class */ (function () {
    function BankAccountsComponent(apiService) {
        this.apiService = apiService;
        this.customers = [];
        this.balances = [];
        /*
         this.dataTable = {
             headerRow: ['#', 'Bank', 'Account', 'Amount'],
             footerRow: ['#', 'Bank', 'Account', 'Amount'],
             dataRows: []
         };
         */
        this.businessID = localStorage.getItem("businessID");
        this.getAccounts(this.businessID + "/0/1");
    }
    BankAccountsComponent.prototype.ngOnInit = function () {
    };
    BankAccountsComponent.prototype.getAccounts = function (accountID) {
        var _this = this;
        this.apiService.getData(accountID, "accountBalances/").then(function (result) {
            _this.customers = result;
            _this.dataTable = {
                headerRow: ['#', 'Name', 'Bank', 'Account#', 'IBAN', 'Currency', 'Balance', 'Actions'],
                footerRow: ['#', 'Name', 'Bank', 'Account#', 'IBAN', 'Currency', 'Balance', 'Actions'],
                dataRows: _this.customers
            };
            console.log(_this.dataTable);
        });
    };
    BankAccountsComponent.prototype.getAccountBalances = function (accountID) {
        var _this = this;
        this.apiService.getData(accountID, "accountBalances/").then(function (result) {
            _this.balances = result;
            console.log(_this.balances, 'balances');
        });
    };
    BankAccountsComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }
        });
        /*
        var table = $('#datatables').DataTable();
    
        // Edit record
        table.on( 'click', '.edit', function () {
            var $tr = $(this).closest('tr');
    
            var data = table.row($tr).data();
            alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
        } );
    
        // Delete a record
        table.on( 'click', '.remove', function (e) {
            var $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        } );
    
        //Like record
        table.on( 'click', '.like', function () {
            alert('You clicked on Like button');
        });
        */
    };
    BankAccountsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bankAccounts',
            template: __webpack_require__(/*! ./bankAccounts.component.html */ "./src/app/finance/bankAccounts/bankAccounts.component.html"),
            styles: [__webpack_require__(/*! ./bankAccounts.component.scss */ "./src/app/finance/bankAccounts/bankAccounts.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], BankAccountsComponent);
    return BankAccountsComponent;
}());



/***/ }),

/***/ "./src/app/finance/banks/banks.component.html":
/*!****************************************************!*\
  !*** ./src/app/finance/banks/banks.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"content\">\n                          <h4 class=\"title\">{{\"banks\" | translate |titlecase}} \n                                  <span style='float:right;'>\n                                          <a routerLink='/finance/bank/0' style='color:gray;margin-right:10px;font-size:16px;'><i class='fa fa-plus-square-o'></i></a>\n                                  </span>\n                              </h4>\n                              <p class=\"category\">{{\"These are available banks in Your Country\" | translate |titlecase}}</p>\n                          <!-- <legend></legend> -->\n                          <br>\n                      <div class=\"toolbar\">\n                          \n                          <!--        Here you can write extra buttons/actions for the toolbar              -->\n                      </div>\n                      <div class=\"fresh-datatables\">\n                          <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                              <thead>\n                                  <tr>\n                                    <th>{{ dataTable.headerRow[0] }}</th>\n                                    <th class='text-left'>{{ dataTable.headerRow[1] }}</th>\n                                    <th class='text-left'>{{ dataTable.headerRow[2] }}</th>\n                                    <th class='text-center'>{{ dataTable.headerRow[3] }}</th>\n                                    <th class='text-right'>{{ dataTable.headerRow[4] }}</th>\n                                    <th class='text-center'>{{ dataTable.headerRow[5] }}</th>\n                                    <th class='text-right'>{{ dataTable.headerRow[6] }}</th>\n                                  </tr>\n                              </thead>\n                              <tbody>\n                                      <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                          <td>{{i+1}}</td>\n                                          <td class='text-left'><a routerLink='/finance/bank/{{row.bankID}}'>{{row.name | uppercase }}</a></td>\n                                          <td class='text-left'>{{row.phone}}</td>\n                                          <td class='text-center'><span class=\"btn btn-outline-primary btn-xs\">{{row.SWIFT | uppercase}}</span></td>\n                                          <td class='text-right'>{{row.country}}</td>\n                                          <td class='text-center'>{{row.kepEmail}}</td>\n                                          <td class=\"text-right tableicon\">\n                                                \n                                                <a href=\"mailto:{{row.kepEmail}}?Subject=Merhaba\"><i class=\"fa fa-envelope-o\"> </i>&nbsp;</a>\n                                                <a routerLink=\"/finance/bank/{{row.bankID}}\"><i class=\"fa fa-search\"> </i>&nbsp;</a>\n                                            </td>\n                                      </tr>\n                                  </tbody>\n                          </table>\n                      </div>\n                  </div>\n                  <!-- end content-->\n              </div>\n              <!--  end card  -->\n          </div>\n          <!-- end col-md-12 -->\n      </div>\n      <!-- end row -->\n  </div>\n  </div>\n  "

/***/ }),

/***/ "./src/app/finance/banks/banks.component.scss":
/*!****************************************************!*\
  !*** ./src/app/finance/banks/banks.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvYmFua3MvYmFua3MuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/finance/banks/banks.component.ts":
/*!**************************************************!*\
  !*** ./src/app/finance/banks/banks.component.ts ***!
  \**************************************************/
/*! exports provided: BanksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BanksComponent", function() { return BanksComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BanksComponent = /** @class */ (function () {
    function BanksComponent(apiService) {
        this.apiService = apiService;
        this.customers = [];
        /*
        this.dataTable = {
            headerRow: ['#', 'Bank', 'Account', 'Amount'],
            footerRow: ['#', 'Bank', 'Account', 'Amount'],
            dataRows: []
        };
        */
        this.businessID = localStorage.getItem("businessID");
        this.getBanks("0");
    }
    BanksComponent.prototype.ngOnInit = function () {
    };
    BanksComponent.prototype.getBanks = function (bankID) {
        var _this = this;
        this.apiService.getData(bankID, "banks/").then(function (result) {
            _this.customers = result;
            _this.dataTable = {
                headerRow: ['#', 'Name', 'Country', 'SWIFT', 'Phone', 'Email', 'Actions'],
                footerRow: ['#', 'Name', 'Country', 'SWIFT', 'Phone', 'Email', 'Actions'],
                dataRows: _this.customers
            };
            console.log(_this.dataTable);
        });
    };
    BanksComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 50,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }
        });
        /*
        var table = $('#datatables').DataTable();
    
        // Edit record
        table.on( 'click', '.edit', function () {
            var $tr = $(this).closest('tr');
    
            var data = table.row($tr).data();
            alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
        } );
    
        // Delete a record
        table.on( 'click', '.remove', function (e) {
            var $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        } );
    
        //Like record
        table.on( 'click', '.like', function () {
            alert('You clicked on Like button');
        });
        */
    };
    BanksComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-banks',
            template: __webpack_require__(/*! ./banks.component.html */ "./src/app/finance/banks/banks.component.html"),
            styles: [__webpack_require__(/*! ./banks.component.scss */ "./src/app/finance/banks/banks.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], BanksComponent);
    return BanksComponent;
}());



/***/ }),

/***/ "./src/app/finance/cash/cash.component.html":
/*!**************************************************!*\
  !*** ./src/app/finance/cash/cash.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"content\">\n                          <h4 class=\"title\">{{\"cashaccount\" | translate |titlecase}} \n                                  <span style='float:right;'>\n                                          <a routerLink='/bank'  style='color:gray;margin-right:10px;'><i class='fa fa-plus-square-o'></i></a>\n                                          <!-- (click)=\"openModal(row.ID)\" -->\n                                  </span>\n                              </h4>\n                              <p class=\"category\">{{\"These are your cash accounts\" | translate |titlecase}}</p>\n                          <!-- <legend></legend> -->\n                          <br>\n                      <div class=\"toolbar\">\n                          \n                          <!--        Here you can write extra buttons/actions for the toolbar              -->\n                      </div>\n                      <div class=\"fresh-datatables\">\n                          <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                              <thead>\n                                  <tr>\n                                    <th>{{ dataTable.headerRow[0] }}</th>\n                                    <th class='text-left'>{{ dataTable.headerRow[1] | translate }}</th>\n                                    <th class='text-left'></th>\n                                    <th class='text-center'></th>\n                                    <th class='text-right'></th>\n                                    <th class='text-center'>{{ dataTable.headerRow[5] }}</th>\n                                    <th class='text-right'>{{ dataTable.headerRow[6] }}</th>\n                                  </tr>\n                              </thead>\n                              <tbody>\n                                      <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                          <td>{{i+1}}</td>\n                                          <td class='text-left'>{{row.account}}</td>\n                                          <td class='text-left'>{{row.bank}}</td>\n                                          <td class='text-center'></td>\n                                          <td class='text-right'></td>\n                                          <td class='text-center'><span class=\"btn btn-success btn-xs\">{{row.currency}}</span></td>\n                                          <td class=\"text-right tableicon\">\n                                                <a routerLink=\"/finance/transactions/{{row.ID}}\"><i class=\"fa fa-navicon\"> </i>&nbsp;</a>\n                                            </td>\n                                      </tr>\n                                  </tbody>\n                          </table>\n                      </div>\n                  </div>\n                  <!-- end content-->\n              </div>\n              <!--  end card  -->\n          </div>\n          <!-- end col-md-12 -->\n      </div>\n      <!-- end row -->\n  </div>\n  </div>\n  "

/***/ }),

/***/ "./src/app/finance/cash/cash.component.scss":
/*!**************************************************!*\
  !*** ./src/app/finance/cash/cash.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvY2FzaC9jYXNoLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/finance/cash/cash.component.ts":
/*!************************************************!*\
  !*** ./src/app/finance/cash/cash.component.ts ***!
  \************************************************/
/*! exports provided: CashComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CashComponent", function() { return CashComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_finance_bank_bank_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/finance/bank/bank.component */ "./src/app/finance/bank/bank.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CashComponent = /** @class */ (function () {
    function CashComponent(apiService, router, modalService) {
        this.apiService = apiService;
        this.router = router;
        this.modalService = modalService;
        this.customers = [];
        /*
        this.dataTable = {
            headerRow: ['#', 'Bank', 'Account', 'Amount'],
            footerRow: ['#', 'Bank', 'Account', 'Amount'],
            dataRows: []
        };
        */
    }
    CashComponent.prototype.ngOnInit = function () {
        this.businessID = localStorage.getItem("businessID");
        this.getAccounts(this.businessID + "/0/0");
    };
    CashComponent.prototype.getAccounts = function (accountID) {
        var _this = this;
        this.apiService.getData(accountID, "accounts/").then(function (result) {
            _this.customers = result;
            _this.dataTable = {
                headerRow: ['#', 'Name', 'Bank', 'Account#', 'IBAN', 'Currency', 'Actions'],
                footerRow: ['#', 'Name', 'Bank', 'Account#', 'IBAN', 'Currency', 'Actions'],
                dataRows: _this.customers
            };
            console.log(_this.dataTable);
        });
    };
    CashComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }
        });
        /*
        var table = $('#datatables').DataTable();
    
        // Edit record
        table.on( 'click', '.edit', function () {
            var $tr = $(this).closest('tr');
    
            var data = table.row($tr).data();
            alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
        } );
    
        // Delete a record
        table.on( 'click', '.remove', function (e) {
            var $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        } );
    
        //Like record
        table.on( 'click', '.like', function () {
            alert('You clicked on Like button');
        });
        */
    };
    CashComponent.prototype.openModal = function (accountID) {
        var modalRef = this.modalService.open(app_finance_bank_bank_component__WEBPACK_IMPORTED_MODULE_4__["BankComponent"], { "backdrop": "static" });
        modalRef.componentInstance.title = 'Account';
        modalRef.componentInstance.ID = accountID;
    };
    CashComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cash',
            template: __webpack_require__(/*! ./cash.component.html */ "./src/app/finance/cash/cash.component.html"),
            styles: [__webpack_require__(/*! ./cash.component.scss */ "./src/app/finance/cash/cash.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"]])
    ], CashComponent);
    return CashComponent;
}());



/***/ }),

/***/ "./src/app/finance/finance.module.ts":
/*!*******************************************!*\
  !*** ./src/app/finance/finance.module.ts ***!
  \*******************************************/
/*! exports provided: FinanceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinanceModule", function() { return FinanceModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _finance_routing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./finance.routing */ "./src/app/finance/finance.routing.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _banks_banks_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./banks/banks.component */ "./src/app/finance/banks/banks.component.ts");
/* harmony import */ var _bank_bank_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./bank/bank.component */ "./src/app/finance/bank/bank.component.ts");
/* harmony import */ var _cash_cash_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./cash/cash.component */ "./src/app/finance/cash/cash.component.ts");
/* harmony import */ var _transactions_transactions_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./transactions/transactions.component */ "./src/app/finance/transactions/transactions.component.ts");
/* harmony import */ var _bankAccount_bankAccount_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./bankAccount/bankAccount.component */ "./src/app/finance/bankAccount/bankAccount.component.ts");
/* harmony import */ var _bankAccounts_bankAccounts_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./bankAccounts/bankAccounts.component */ "./src/app/finance/bankAccounts/bankAccounts.component.ts");
/* harmony import */ var _aggr_aggr_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./aggr/aggr.component */ "./src/app/finance/aggr/aggr.component.ts");
/* harmony import */ var _balance_balance_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./balance/balance.component */ "./src/app/finance/balance/balance.component.ts");
/* harmony import */ var _babs_babs_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./babs/babs.component */ "./src/app/finance/babs/babs.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var FinanceModule = /** @class */ (function () {
    function FinanceModule() {
    }
    FinanceModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_finance_routing__WEBPACK_IMPORTED_MODULE_4__["FinanceRoutes"]),
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"]
            ],
            declarations: [
                _banks_banks_component__WEBPACK_IMPORTED_MODULE_6__["BanksComponent"],
                _bank_bank_component__WEBPACK_IMPORTED_MODULE_7__["BankComponent"],
                _cash_cash_component__WEBPACK_IMPORTED_MODULE_8__["CashComponent"],
                _transactions_transactions_component__WEBPACK_IMPORTED_MODULE_9__["TransactionsComponent"],
                _bankAccount_bankAccount_component__WEBPACK_IMPORTED_MODULE_10__["BankAccountComponent"],
                _bankAccounts_bankAccounts_component__WEBPACK_IMPORTED_MODULE_11__["BankAccountsComponent"],
                _aggr_aggr_component__WEBPACK_IMPORTED_MODULE_12__["AggrComponent"],
                _balance_balance_component__WEBPACK_IMPORTED_MODULE_13__["BalanceComponent"],
                _babs_babs_component__WEBPACK_IMPORTED_MODULE_14__["BabsComponent"]
            ]
        })
    ], FinanceModule);
    return FinanceModule;
}());



/***/ }),

/***/ "./src/app/finance/finance.routing.ts":
/*!********************************************!*\
  !*** ./src/app/finance/finance.routing.ts ***!
  \********************************************/
/*! exports provided: FinanceRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinanceRoutes", function() { return FinanceRoutes; });
/* harmony import */ var _bank_bank_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bank/bank.component */ "./src/app/finance/bank/bank.component.ts");
/* harmony import */ var _banks_banks_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./banks/banks.component */ "./src/app/finance/banks/banks.component.ts");
/* harmony import */ var _bankAccounts_bankAccounts_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bankAccounts/bankAccounts.component */ "./src/app/finance/bankAccounts/bankAccounts.component.ts");
/* harmony import */ var _bankAccount_bankAccount_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bankAccount/bankAccount.component */ "./src/app/finance/bankAccount/bankAccount.component.ts");
/* harmony import */ var _cash_cash_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./cash/cash.component */ "./src/app/finance/cash/cash.component.ts");
/* harmony import */ var _transactions_transactions_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./transactions/transactions.component */ "./src/app/finance/transactions/transactions.component.ts");
/* harmony import */ var _auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../auth/auth-guard.service */ "./src/app/auth/auth-guard.service.ts");
/* harmony import */ var _aggr_aggr_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./aggr/aggr.component */ "./src/app/finance/aggr/aggr.component.ts");
/* harmony import */ var _balance_balance_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./balance/balance.component */ "./src/app/finance/balance/balance.component.ts");
/* harmony import */ var _babs_babs_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./babs/babs.component */ "./src/app/finance/babs/babs.component.ts");










var FinanceRoutes = [
    {
        path: '',
        children: [{
                path: 'banks',
                component: _banks_banks_component__WEBPACK_IMPORTED_MODULE_1__["BanksComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    },
    {
        path: 'bank/:id',
        component: _bank_bank_component__WEBPACK_IMPORTED_MODULE_0__["BankComponent"],
        canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
    },
    {
        path: '',
        children: [{
                path: 'bank',
                component: _bank_bank_component__WEBPACK_IMPORTED_MODULE_0__["BankComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    },
    {
        path: '',
        children: [{
                path: 'bankAccounts',
                component: _bankAccounts_bankAccounts_component__WEBPACK_IMPORTED_MODULE_2__["BankAccountsComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    },
    {
        path: 'bankAccount/:id',
        component: _bankAccount_bankAccount_component__WEBPACK_IMPORTED_MODULE_3__["BankAccountComponent"]
    },
    {
        path: '',
        children: [{
                path: 'bank',
                component: _bankAccount_bankAccount_component__WEBPACK_IMPORTED_MODULE_3__["BankAccountComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    },
    {
        path: '',
        children: [{
                path: 'cash',
                component: _cash_cash_component__WEBPACK_IMPORTED_MODULE_4__["CashComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    },
    {
        path: '',
        children: [{
                path: 'aggr',
                component: _aggr_aggr_component__WEBPACK_IMPORTED_MODULE_7__["AggrComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    },
    {
        path: '',
        children: [{
                path: 'babs',
                component: _babs_babs_component__WEBPACK_IMPORTED_MODULE_9__["BabsComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    },
    {
        path: '',
        children: [{
                path: 'balance',
                component: _balance_balance_component__WEBPACK_IMPORTED_MODULE_8__["BalanceComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    },
    {
        path: '',
        children: [{
                path: 'transactions',
                component: _transactions_transactions_component__WEBPACK_IMPORTED_MODULE_5__["TransactionsComponent"],
                canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]]
            }]
    }, {
        path: 'transactions/:id',
        component: _transactions_transactions_component__WEBPACK_IMPORTED_MODULE_5__["TransactionsComponent"]
    }
];


/***/ }),

/***/ "./src/app/finance/transactions/transactions.component.html":
/*!******************************************************************!*\
  !*** ./src/app/finance/transactions/transactions.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n    <div class=\"row\">\n        <div class=\"col-md-12\">\n            <div class=\"card\">\n                <div class=\"content\">\n                        <h4 class=\"title\">{{\"transactions\" | translate |titlecase}} \n                                <span style='float:right;font-size:16px;'>\n                                    <a title=\"Excel Export\" (click)=\"excelExport()\" style='color:gray;margin-right:10px;font-size:16px;'><i class='fa fa-file-excel-o'></i></a>\n                                        \n                                </span>\n                            </h4>\n                            <p class=\"category\">{{\"These are your bank transactions\" | translate |titlecase}}</p>\n                        <!-- <legend></legend> -->\n                        <br>\n                        <fieldset>\n                            <div class=\"form-group\">\n                                <label class=\"col-sm-2 control-label\" style=\"padding-top:10px;\">Start Date</label>\n                                <div class=\"col-sm-2\">\n                                    <input type=\"text\" [(ngModel)]=\"invoiceStartDate\" name=\"invoiceStartDate\" class=\"form-control datetimepicker invoiceStartDate\"\n                                        placeholder=\"Pick Date Time\" />\n                                </div>\n                                <label class=\"col-sm-2 control-label\" style=\"padding-top:10px;\">End Date</label>\n                                <div class=\"col-sm-2\">\n                                    <input type=\"text\" [(ngModel)]=\"invoiceEndDate\" name=\"invoiceEndDate\" class=\"form-control datetimepicker invoiceEndDate\"\n                                        placeholder=\"Pick Date Time\" />\n                                </div>\n                                <div class=\"col-sm-4\">\n                                <button style='margin-right:5px;' class=\"btn btn-success btn-fill btn-wd\"\n                                (click)=\"clickReport()\">REPORT</button>\n                                </div>\n                            </div>\n                        </fieldset>\n\n                        <hr>\n                    <div class=\"fresh-datatables\">\n                        <table id=\"datatables\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\" style=\"width:100%\">\n                            <thead>\n                                <tr>\n                                  <th>#</th>\n                                  <th>Date</th>\n                                  <th>Account</th>\n                                  <th>Description</th>\n                                  <th class='text-right'>Amount</th>\n                                  <th class='text-center'></th>\n                                  <th class='text-right'></th>\n                                </tr>\n                            </thead>\n                            <tbody>\n                                    <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                        <td><span class=\"dot {{row.color}}\"></span></td>\n                                        <td>{{row.tranDate | date:'d/M/y'}}</td>\n                                        <td><a routerLink='/bank/{{row.bankID}}'><span class=\"btn btn-outline-primary btn-xs\">{{row.bank}} - {{row.accountNumber}}</span></a></td>\n                                        <td>{{row.description}}</td>                                        \n                                        <td class='text-right'>{{row.transaction | number}}</td>\n                                        <td class='text-center'><span class=\"btn btn-success btn-xs\">{{row.currency}}</span></td>  \n                                        <td class='text-right tableicon'>\n                                            <a title='{{row.notes}}'(click)=\"openModal(row.ID,i)\">\n                                                <i ngClass=\"{{row.notes != NULL ? ' fa fa-sticky-note note' : 'fa fa-sticky-note-o'}}\"></i>&nbsp;\n                                            </a>\n                                            <a alt='Link An Invoice' (click)=\"openModalInvoice(row.ID)\">\n                                                <i ngClass=\"{{row.relatedInvoiceID != NULL ? ' fa fa-link linkedInvoice' : 'fa fa-chain-broken'}}\"></i>&nbsp;\n                                            </a>\n                                            \n                                        </td>\n                                    </tr>\n                                </tbody>\n                        </table>\n                    </div>\n                </div>\n                <!-- end content-->\n            </div>\n            <!--  end card  -->\n        </div>\n        <!-- end col-md-12 -->\n    </div>\n    <!-- end row -->\n</div>\n</div>\n"

/***/ }),

/***/ "./src/app/finance/transactions/transactions.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/finance/transactions/transactions.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".note {\n  color: orange !important;\n  visibility: visible !important;\n  font-size: 17px; }\n\n.linkedInvoice {\n  color: #447DF7 !important;\n  visibility: visible !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9kZXZlbG9wZXIyMDIvRG9jdW1lbnRzL2lvbmljcHJvamVjdHMvZWZjcm0vc3JjL2FwcC9maW5hbmNlL3RyYW5zYWN0aW9ucy90cmFuc2FjdGlvbnMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBdUI7RUFDdkIsK0JBQThCO0VBQzlCLGdCQUFlLEVBQ2xCOztBQUNEO0VBQ0ksMEJBQXdCO0VBQ3hCLCtCQUE4QixFQUNqQyIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvdHJhbnNhY3Rpb25zL3RyYW5zYWN0aW9ucy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ub3Rle1xuICAgIGNvbG9yOm9yYW5nZSAhaW1wb3J0YW50O1xuICAgIHZpc2liaWxpdHk6IHZpc2libGUgIWltcG9ydGFudDtcbiAgICBmb250LXNpemU6IDE3cHg7IFxufVxuLmxpbmtlZEludm9pY2V7XG4gICAgY29sb3I6IzQ0N0RGNyAhaW1wb3J0YW50O1xuICAgIHZpc2liaWxpdHk6IHZpc2libGUgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/finance/transactions/transactions.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/finance/transactions/transactions.component.ts ***!
  \****************************************************************/
/*! exports provided: TransactionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsComponent", function() { return TransactionsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _note_note_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../note/note.component */ "./src/app/finance/note/note.component.ts");
/* harmony import */ var _invoice_id_invoice_id_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../invoice-id/invoice-id.component */ "./src/app/finance/invoice-id/invoice-id.component.ts");
/* harmony import */ var app_providers_excel_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/providers/excel.service */ "./src/app/providers/excel.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TransactionsComponent = /** @class */ (function () {
    function TransactionsComponent(apiService, activeRoute, modalService, excelService, datePipe) {
        this.apiService = apiService;
        this.activeRoute = activeRoute;
        this.modalService = modalService;
        this.excelService = excelService;
        this.datePipe = datePipe;
        this.transactions = [];
        this.accountID = 0;
        var routeParams = this.activeRoute.snapshot.params;
        if (routeParams.id > 0) {
            this.accountID = routeParams.id;
        }
        var now = new Date();
        this.invoiceStartDate = this.datePipe.transform(now, "dd-MM-yyyy");
        this.invoiceEndDate = this.datePipe.transform(now, "dd-MM-yyyy");
        /*
        this.dataTable = {
          headerRow: ['#', 'Date', 'Description', 'Amount'],
          footerRow: ['#', 'Date', 'Description', 'Amount'],
          dataRows: []
        };
        */
        console.log(this.accountID);
    }
    TransactionsComponent.prototype.ngOnInit = function () {
        this.businessID = localStorage.getItem("businessID");
        this.clickReport();
        $('.datetimepicker').datetimepicker({
            locale: 'tr',
            format: 'DD-MM-YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    };
    TransactionsComponent.prototype.getBankTransactions = function (parameters) {
        var _this = this;
        this.apiService.getData(parameters, "transactions/").then(function (result) {
            _this.transactions = result;
            _this.dataTable = {
                headerRow: ['#', 'Date', 'Description', 'Amount'],
                footerRow: ['#', 'Date', 'Description', 'Amount'],
                dataRows: _this.transactions
            };
            console.log(_this.dataTable);
        });
    };
    TransactionsComponent.prototype.openModal = function (transactionID, index) {
        var _this = this;
        var modalRef = this.modalService.open(_note_note_component__WEBPACK_IMPORTED_MODULE_4__["NoteComponent"], { "backdrop": "static" });
        modalRef.componentInstance.transactionID = transactionID;
        modalRef.result.then(function (data) {
            // on close
            console.log(data, "result data");
            if (data["notes"] != "") {
                _this.transactions[index].notes = data["notes"];
            }
            else {
                _this.transactions[index].notes = null;
            }
        }, function (reason) {
            // on dismiss
        });
    };
    TransactionsComponent.prototype.openModalInvoice = function (transactionID) {
        var modalRef = this.modalService.open(_invoice_id_invoice_id_component__WEBPACK_IMPORTED_MODULE_5__["InvoiceIDComponent"], { "backdrop": "static" });
        modalRef.componentInstance.transactionID = transactionID;
        modalRef.componentInstance.startDate = this.startDate;
        modalRef.componentInstance.endDate = this.endDate;
    };
    TransactionsComponent.prototype.excelExport = function () {
        var obj = this.transactions;
        this.excelService.exportAsExcelFile(obj, 'transactions');
    };
    TransactionsComponent.prototype.ngAfterViewInit = function () {
        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }
        });
    };
    TransactionsComponent.prototype.clickReport = function () {
        this.startDate = $('.invoiceStartDate').val();
        this.endDate = $('.invoiceEndDate').val();
        if (this.startDate == "" || this.endDate == "") {
            var sdate = new Date();
            this.startDate = this.datePipe.transform(sdate, "yyyy-MM-dd");
            this.endDate = this.startDate;
        }
        this.getBankTransactions(this.businessID + "/" + this.accountID + "/" + this.startDate + "/" + this.endDate);
    };
    TransactionsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-transactions',
            template: __webpack_require__(/*! ./transactions.component.html */ "./src/app/finance/transactions/transactions.component.html"),
            styles: [__webpack_require__(/*! ./transactions.component.scss */ "./src/app/finance/transactions/transactions.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"], app_providers_excel_service__WEBPACK_IMPORTED_MODULE_6__["ExcelService"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"]])
    ], TransactionsComponent);
    return TransactionsComponent;
}());



/***/ })

}]);
//# sourceMappingURL=finance-finance-module.js.map