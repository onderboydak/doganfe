(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./admin/admin.module": [
		"./src/app/admin/admin.module.ts",
		"admin-admin-module"
	],
	"./costs/costs.module": [
		"./src/app/costs/costs.module.ts",
		"default~costs-costs-module~dashboard-dashboard-module~sales-sales-module",
		"costs-costs-module"
	],
	"./dashboard/dashboard.module": [
		"./src/app/dashboard/dashboard.module.ts",
		"default~costs-costs-module~dashboard-dashboard-module~sales-sales-module",
		"dashboard-dashboard-module"
	],
	"./finance/finance.module": [
		"./src/app/finance/finance.module.ts",
		"finance-finance-module"
	],
	"./hr/hr.module": [
		"./src/app/hr/hr.module.ts",
		"hr-hr-module"
	],
	"./pages/pages.module": [
		"./src/app/pages/pages.module.ts",
		"pages-pages-module"
	],
	"./sales/sales.module": [
		"./src/app/sales/sales.module.ts",
		"default~costs-costs-module~dashboard-dashboard-module~sales-sales-module",
		"sales-sales-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet>\n    <div id=\"loader\"><img src=\"assets/img/loader-preview.svg\" alt=\"loading\" *ngIf=\"loading\"></div>\n</router-outlet>\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(router, translate) {
        this.router = router;
        this.translate = translate;
        this.loading = false;
        this.translate.addLangs(["en", "tr"]);
        this.translate.setDefaultLang('tr');
        //let browserLang = this.translate.getBrowserLang();
        var browserLang = "tr";
        this.translate.use(browserLang.match(/en|tr/) ? browserLang : 'tr');
        console.log(browserLang);
        if (!localStorage.getItem("userID")) {
            this.router.navigate(["/pages/login"]);
        }
    }
    AppComponent.prototype.ngOnInit = function () {
        console.log("ngOnInit start");
        this.loading = true;
    };
    AppComponent.prototype.ngOnDestroy = function () {
        console.log("ngOnDestroy start");
        this.loading = false;
    };
    AppComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        console.log("ngAfterViewInit start");
        setTimeout(function () {
            _this.loading = false;
        }, 3000);
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: HttpLoaderFactory, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/fesm5/ngx-translate-http-loader.js");
/* harmony import */ var _sidebar_sidebar_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./sidebar/sidebar.module */ "./src/app/sidebar/sidebar.module.ts");
/* harmony import */ var _shared_fixedplugin_fixedplugin_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./shared/fixedplugin/fixedplugin.module */ "./src/app/shared/fixedplugin/fixedplugin.module.ts");
/* harmony import */ var _shared_footer_footer_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./shared/footer/footer.module */ "./src/app/shared/footer/footer.module.ts");
/* harmony import */ var _shared_navbar_navbar_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./shared/navbar/navbar.module */ "./src/app/shared/navbar/navbar.module.ts");
/* harmony import */ var _shared_pagesnavbar_pagesnavbar_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./shared/pagesnavbar/pagesnavbar.module */ "./src/app/shared/pagesnavbar/pagesnavbar.module.ts");
/* harmony import */ var _layouts_admin_admin_layout_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./layouts/admin/admin-layout.component */ "./src/app/layouts/admin/admin-layout.component.ts");
/* harmony import */ var _layouts_auth_auth_layout_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./layouts/auth/auth-layout.component */ "./src/app/layouts/auth/auth-layout.component.ts");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _providers_api_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _pagenotfound_pagenotfound_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./pagenotfound/pagenotfound.component */ "./src/app/pagenotfound/pagenotfound.component.ts");
/* harmony import */ var _sales_invoice_payment_invoice_payment_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./sales/invoice-payment/invoice-payment.component */ "./src/app/sales/invoice-payment/invoice-payment.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _tax_revenue_revenue_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./tax/revenue/revenue.component */ "./src/app/tax/revenue/revenue.component.ts");
/* harmony import */ var _tax_payroll_payroll_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./tax/payroll/payroll.component */ "./src/app/tax/payroll/payroll.component.ts");
/* harmony import */ var _tax_vat_vat_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./tax/vat/vat.component */ "./src/app/tax/vat/vat.component.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./auth/auth-guard.service */ "./src/app/auth/auth-guard.service.ts");
/* harmony import */ var _finance_note_note_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./finance/note/note.component */ "./src/app/finance/note/note.component.ts");
/* harmony import */ var _finance_invoice_id_invoice_id_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./finance/invoice-id/invoice-id.component */ "./src/app/finance/invoice-id/invoice-id.component.ts");
/* harmony import */ var _sales_approve_modal_approve_modal_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./sales/approve-modal/approve-modal.component */ "./src/app/sales/approve-modal/approve-modal.component.ts");
/* harmony import */ var _pages_upload_upload_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./pages/upload/upload.component */ "./src/app/pages/upload/upload.component.ts");
/* harmony import */ var _costs_accounting_accounting_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./costs/accounting/accounting.component */ "./src/app/costs/accounting/accounting.component.ts");
/* harmony import */ var _providers_excel_service__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./providers/excel.service */ "./src/app/providers/excel.service.ts");
/* harmony import */ var _finance_balance_details_balance_details_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./finance/balance-details/balance-details.component */ "./src/app/finance/balance-details/balance-details.component.ts");
/* harmony import */ var _finance_babs_term_term_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./finance/babs/term/term.component */ "./src/app/finance/babs/term/term.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

 // this is needed!

































function HttpLoaderFactory(httpClient) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_7__["TranslateHttpLoader"](httpClient, "../assets/i18n/", ".json");
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(_app_routing__WEBPACK_IMPORTED_MODULE_15__["AppRoutes"], { useHash: true }),
                _angular_http__WEBPACK_IMPORTED_MODULE_3__["HttpModule"],
                _sidebar_sidebar_module__WEBPACK_IMPORTED_MODULE_8__["SidebarModule"],
                _shared_navbar_navbar_module__WEBPACK_IMPORTED_MODULE_11__["NavbarModule"],
                _shared_footer_footer_module__WEBPACK_IMPORTED_MODULE_10__["FooterModule"],
                _shared_fixedplugin_fixedplugin_module__WEBPACK_IMPORTED_MODULE_9__["FixedPluginModule"],
                _shared_pagesnavbar_pagesnavbar_module__WEBPACK_IMPORTED_MODULE_12__["PagesnavbarModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_16__["HttpClientModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_18__["NgbModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_18__["NgbTypeaheadModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"].withConfig({ warnOnNgModelWithFormControl: 'never' }),
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateLoader"],
                        useFactory: HttpLoaderFactory,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_16__["HttpClient"]]
                    }
                })
            ],
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _layouts_admin_admin_layout_component__WEBPACK_IMPORTED_MODULE_13__["AdminLayoutComponent"],
                _layouts_auth_auth_layout_component__WEBPACK_IMPORTED_MODULE_14__["AuthLayoutComponent"],
                _pagenotfound_pagenotfound_component__WEBPACK_IMPORTED_MODULE_19__["PagenotfoundComponent"],
                _sales_invoice_payment_invoice_payment_component__WEBPACK_IMPORTED_MODULE_20__["InvoicePaymentComponent"],
                _tax_revenue_revenue_component__WEBPACK_IMPORTED_MODULE_22__["RevenueComponent"],
                _tax_payroll_payroll_component__WEBPACK_IMPORTED_MODULE_23__["PayrollComponent"],
                _finance_note_note_component__WEBPACK_IMPORTED_MODULE_27__["NoteComponent"],
                _finance_invoice_id_invoice_id_component__WEBPACK_IMPORTED_MODULE_28__["InvoiceIDComponent"],
                _sales_approve_modal_approve_modal_component__WEBPACK_IMPORTED_MODULE_29__["ApproveModalComponent"],
                _tax_vat_vat_component__WEBPACK_IMPORTED_MODULE_24__["VatComponent"],
                _pages_upload_upload_component__WEBPACK_IMPORTED_MODULE_30__["UploadComponent"],
                _costs_accounting_accounting_component__WEBPACK_IMPORTED_MODULE_31__["AccountingComponent"],
                _finance_balance_details_balance_details_component__WEBPACK_IMPORTED_MODULE_33__["BalanceDetailsComponent"],
                _finance_babs_term_term_component__WEBPACK_IMPORTED_MODULE_34__["TermComponent"]
            ],
            providers: [_providers_api_service__WEBPACK_IMPORTED_MODULE_17__["ApiService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_16__["HttpClient"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_18__["NgbActiveModal"], _angular_common__WEBPACK_IMPORTED_MODULE_21__["DatePipe"], _auth_auth_service__WEBPACK_IMPORTED_MODULE_25__["AuthService"], _auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_26__["AuthGuardService"], _providers_excel_service__WEBPACK_IMPORTED_MODULE_32__["ExcelService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
            entryComponents: [
                _sales_invoice_payment_invoice_payment_component__WEBPACK_IMPORTED_MODULE_20__["InvoicePaymentComponent"], _finance_note_note_component__WEBPACK_IMPORTED_MODULE_27__["NoteComponent"], _finance_invoice_id_invoice_id_component__WEBPACK_IMPORTED_MODULE_28__["InvoiceIDComponent"], _sales_approve_modal_approve_modal_component__WEBPACK_IMPORTED_MODULE_29__["ApproveModalComponent"], _pages_upload_upload_component__WEBPACK_IMPORTED_MODULE_30__["UploadComponent"], _costs_accounting_accounting_component__WEBPACK_IMPORTED_MODULE_31__["AccountingComponent"], _finance_balance_details_balance_details_component__WEBPACK_IMPORTED_MODULE_33__["BalanceDetailsComponent"], _finance_babs_term_term_component__WEBPACK_IMPORTED_MODULE_34__["TermComponent"]
            ]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: AppRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutes", function() { return AppRoutes; });
/* harmony import */ var _layouts_admin_admin_layout_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./layouts/admin/admin-layout.component */ "./src/app/layouts/admin/admin-layout.component.ts");
/* harmony import */ var _layouts_auth_auth_layout_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./layouts/auth/auth-layout.component */ "./src/app/layouts/auth/auth-layout.component.ts");


var AppRoutes = [{
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: '',
        component: _layouts_admin_admin_layout_component__WEBPACK_IMPORTED_MODULE_0__["AdminLayoutComponent"],
        children: [{
                path: '',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'sales',
                loadChildren: './sales/sales.module#SalesModule'
            }, {
                path: 'costs',
                loadChildren: './costs/costs.module#CostsModule'
            }, {
                path: 'finance',
                loadChildren: './finance/finance.module#FinanceModule'
            }, {
                path: 'admin',
                loadChildren: './admin/admin.module#AdminModule'
            }, {
                path: 'hr',
                loadChildren: './hr/hr.module#HrModule'
            }]
    }, {
        path: '',
        component: _layouts_auth_auth_layout_component__WEBPACK_IMPORTED_MODULE_1__["AuthLayoutComponent"],
        children: [{
                path: 'pages',
                loadChildren: './pages/pages.module#PagesModule'
            }]
    }
];


/***/ }),

/***/ "./src/app/auth/auth-guard.service.ts":
/*!********************************************!*\
  !*** ./src/app/auth/auth-guard.service.ts ***!
  \********************************************/
/*! exports provided: AuthGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuardService", function() { return AuthGuardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthGuardService = /** @class */ (function () {
    function AuthGuardService(auth, router, apiService) {
        this.auth = auth;
        this.router = router;
        this.apiService = apiService;
        this.userID = localStorage.getItem("userID");
        this.userType = localStorage.getItem("userType");
        this.action = JSON.parse(localStorage.getItem("action"));
    }
    AuthGuardService.prototype.canActivate = function (nextPage, onPage) {
        console.log(nextPage.url, " ", onPage.url, " yesss");
        console.log(this.action);
        var act;
        if (!this.auth.isAuthenticated() && this.userType != 1) {
            this.router.navigate(['pages/login']);
            return false;
        }
        if (this.userType != 1) {
            console.log(this.action, "action");
            var forFilter = this.action;
            act = forFilter.filter(function (ac) { return ac.menu == onPage.url.substring(1); });
            if (act.length > 0) {
                act = act[0]["action"];
            }
        }
        console.log(act, "action", this.userType, "userType");
        return (act > 0 || this.userType == 1);
    };
    AuthGuardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], app_providers_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"]])
    ], AuthGuardService);
    return AuthGuardService;
}());



/***/ }),

/***/ "./src/app/auth/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/auth/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AuthService = /** @class */ (function () {
    function AuthService() {
    }
    // ...
    AuthService.prototype.isAuthenticated = function () {
        //const token = localStorage.getItem('token');
        // Check whether the token is expired and return
        // true or false
        return true;
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/costs/accounting/accounting.component.html":
/*!************************************************************!*\
  !*** ./src/app/costs/accounting/accounting.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n            <form method=\"post\" class=\"form-horizontal\">\n                <h4 class=\"title text-center\">Invoice #{{invoice.invoiceNumber}}</h4>\n                    <!-- mains will be here -->\n                    <div class=\"header text-center\" >\n                        <legend style=\"border:none !important;\">{{\"Select Account ?\" | translate}}</legend>\n                    </div>\n                    <fieldset>\n                        <div class=\"form-group\">\n                           \n                            <label class=\"col-sm-4 control-label\">Account</label>\n                            <div class=\"col-sm-8\">\n                                    <select #account name=\"account\" [(ngModel)]=\"invoice.account\" class=\"selectpicker selectAccount\" \n                                    data-title=\"Select To\" data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                                    <option *ngFor=\"let coa of chartOfAccounts\" [value]=\"(coa.account+'.'+coa.subAccount)\">{{coa.account+\".\"+coa.subAccount+\" \"+coa.accName+\" (\"+coa.masterAccName+\")\"}}</option>\n                                </select>\n                            </div>\n                        </div>\n                    </fieldset>\n                    <fieldset>\n                        <div class=\"form-group\">\n                            <label class=\"col-sm-4 control-label\"></label>\n                            <div class=\"col-sm-8\" *ngIf=\"invoice.invoiceRule>0\" style=\"color:red;\"> Rule Exists!&nbsp;<a style=\"cursor:pointer;color:red;\" title=\"Delete Rule\" (click)=\"removeRule(invoice.invoiceRule)\"><i class=\"fa fa-remove\"></i></a></div>\n                            <div class=\"col-sm-8 checkbox\" *ngIf=\"!invoice.invoiceRule>0\">\n                                <input id=\"rule\" type=\"checkbox\"  name=\"rule\" [(ngModel)]=\"rule\"> \n                                <label for=\"rule\">\n                                    Add Rule\n                                </label>\n                            </div>\n                           \n                        </div>\n                    </fieldset>\n                <div class=\"content text-right\" >\n                  <div style='margin-bottom: 40px;'>\n                      <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                      <button class=\"btn btn-success btn-fill btn-wd\" (click)=\"postAccount()\" >SAVE</button>\n  \n                  </div>\n              </div>\n            </form>\n            </div>\n        </div>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/costs/accounting/accounting.component.scss":
/*!************************************************************!*\
  !*** ./src/app/costs/accounting/accounting.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Nvc3RzL2FjY291bnRpbmcvYWNjb3VudGluZy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/costs/accounting/accounting.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/costs/accounting/accounting.component.ts ***!
  \**********************************************************/
/*! exports provided: AccountingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountingComponent", function() { return AccountingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AccountingComponent = /** @class */ (function () {
    function AccountingComponent(apiService, activeModal) {
        this.apiService = apiService;
        this.activeModal = activeModal;
        this.chartOfAccounts = [];
        this.invoice = [];
        this.rule = false;
        this.businessID = localStorage.getItem("businessID");
    }
    AccountingComponent.prototype.ngOnInit = function () {
        this.getInvoice(this.invoiceID);
        this.getChartOfAccounts();
        this.spInit();
    };
    AccountingComponent.prototype.postAccount = function () {
        var _this = this;
        this.invoice["rule"] = this.rule;
        //console.log(this.invoice);
        this.apiService.postData(this.invoiceID, "/invoice/save/", JSON.stringify(this.invoice)).then(function (result) {
            var res = [];
            res["account"] = _this.invoice.account;
            _this.activeModal.close(res);
        });
    };
    AccountingComponent.prototype.resolveAfter2Seconds = function (x) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                resolve(x);
            }, 600);
        });
    };
    AccountingComponent.prototype.clickCancel = function () {
        this.activeModal.close();
    };
    AccountingComponent.prototype.getChartOfAccounts = function () {
        var _this = this;
        this.apiService.getData(this.businessID + "/700,740,770,760", "chartOfAccounts/").then(function (result) {
            _this.chartOfAccounts = result;
            console.log(_this.chartOfAccounts, "chartofaccounts");
            _this.resolveAfter2Seconds(50).then(function (value) {
                _this.spInit();
            });
        });
    };
    AccountingComponent.prototype.getInvoice = function (invoiceID) {
        var _this = this;
        this.apiService.getData(("2/" + this.businessID + "/" + invoiceID), "invoices/").then(function (result) {
            _this.invoice = result[0];
            _this.invoice.invoiceDetails = JSON.parse(_this.invoice.invoiceDetails);
        });
    };
    AccountingComponent.prototype.spInit = function () {
        console.log("Init Select");
        $(".selectAccount").selectpicker('val', this.invoice.account);
        $(".selectAccount").selectpicker('refresh');
    };
    AccountingComponent.prototype.removeRule = function (ruleID) {
        var _this = this;
        if (confirm("Are you sure ?")) {
            this.apiService.deleteData(ruleID, "/rule/delete/").then(function (result) {
                console.log(result);
                _this.invoice.invoiceRule = 0;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AccountingComponent.prototype, "invoiceID", void 0);
    AccountingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-accounting',
            template: __webpack_require__(/*! ./accounting.component.html */ "./src/app/costs/accounting/accounting.component.html"),
            styles: [__webpack_require__(/*! ./accounting.component.scss */ "./src/app/costs/accounting/accounting.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbActiveModal"]])
    ], AccountingComponent);
    return AccountingComponent;
}());



/***/ }),

/***/ "./src/app/finance/babs/term/term.component.html":
/*!*******************************************************!*\
  !*** ./src/app/finance/babs/term/term.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <form method=\"post\" class=\"form-horizontal\">\n          <h4 class=\"title text-center\">Term</h4>\n          <!-- mains will be here -->\n          <div class=\"header text-center\">\n            <legend style=\"border:none !important;\">{{\"Select term for BABS ?\" | translate}}</legend>\n          </div>\n          <div class=\"content text-center\">\n            <fieldset>\n              <div class=\"form-group\">\n                <label class=\"col-sm-2 control-label\">Yıl</label>\n                <div class=\"col-sm-4\">\n                  <select name=\"y\" [(ngModel)]=\"y\" class=\"selectpicker selectYear\" data-title=\"Year\"\n                    data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                    <option [value]=\"2019\">2019</option>\n                    <option [value]=\"2020\">2020</option>\n                    <option [value]=\"2021\">2021</option>\n                    <option [value]=\"2022\">2022</option>\n                    <option [value]=\"2023\">2023</option>\n                    <option [value]=\"2024\">2024</option>\n                  </select>\n                </div>\n                <label class=\"col-sm-1 control-label\">Ay</label>\n                <div class=\"col-sm-4\">\n                  <select name=\"m\" [(ngModel)]=\"m\" class=\"selectpicker selectMonth\" data-title=\"Month\"\n                    data-style=\"btn-default btn-block\" data-menu-style=\"dropdown-blue\">\n                    <option [value]=\"1\">Ocak</option>\n                    <option [value]=\"2\">Subat</option>\n                    <option [value]=\"3\">Mart</option>\n                    <option [value]=\"4\">Nisan</option>\n                    <option [value]=\"5\">Mayis</option>\n                    <option [value]=\"6\">Haziran</option>\n                    <option [value]=\"7\">Temmuz</option>\n                    <option [value]=\"8\">Agustos</option>\n                    <option [value]=\"9\">Eylul</option>\n                    <option [value]=\"10\">Ekim</option>\n                    <option [value]=\"11\">Kasim</option>\n                    <option [value]=\"12\">Aralik</option>\n                  </select>\n                </div>\n\n              </div>\n            </fieldset>\n            <fieldset>\n                <div class=\"form-group\">\n                    <label class=\"col-sm-4 control-label\">With Email ?</label>\n                    <div class=\"col-sm-4 checkbox\" style=\"padding-top: 10px;\">\n                        <input id=\"withMail\" type=\"checkbox\" [(ngModel)]=\"withMail\"\n                            name=\"withMail\" >\n                        <label for=\"withMail\">\n\n                        </label>\n                    </div>\n                </div>\n            </fieldset>\n            <div style='margin-bottom: 40px;'>\n              <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\"\n                (click)=\"clickCancel()\">CANCEL</button>\n              <button class=\"btn btn-success btn-fill btn-wd\" (click)=\"generateBABS()\">GENERATE</button>\n\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/finance/babs/term/term.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/finance/babs/term/term.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvYmFicy90ZXJtL3Rlcm0uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/finance/babs/term/term.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/finance/babs/term/term.component.ts ***!
  \*****************************************************/
/*! exports provided: TermComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermComponent", function() { return TermComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TermComponent = /** @class */ (function () {
    function TermComponent(activeModal, apiService) {
        this.activeModal = activeModal;
        this.apiService = apiService;
        this.withMail = false;
    }
    TermComponent.prototype.ngOnInit = function () {
        $(".selectpicker").selectpicker();
    };
    TermComponent.prototype.clickCancel = function () {
        this.activeModal.close();
    };
    TermComponent.prototype.generateBABS = function () {
        var _this = this;
        var mail = (this.withMail == true) ? 1 : 0;
        this.apiService.postData(this.y + "/" + this.m + "/" + mail, "babs/", null).then(function (result) {
            _this.activeModal.close();
        });
    };
    TermComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-term',
            template: __webpack_require__(/*! ./term.component.html */ "./src/app/finance/babs/term/term.component.html"),
            styles: [__webpack_require__(/*! ./term.component.scss */ "./src/app/finance/babs/term/term.component.scss")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"], app_providers_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]])
    ], TermComponent);
    return TermComponent;
}());



/***/ }),

/***/ "./src/app/finance/balance-details/balance-details.component.html":
/*!************************************************************************!*\
  !*** ./src/app/finance/balance-details/balance-details.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n            <form method=\"post\" class=\"form-horizontal\">\n                <h4 class=\"title\">Balance Details</h4>\n                <div class=\"card\">\n                    <!-- mains will be here -->\n                  \n                    <div class=\"content\" style=\"height:450px;overflow-y: scroll;\">       \n                        <div class=\"fresh-datatables\">\n                            <table id=\"datatablesd\" class=\"table table-no-bordered table-hover\" cellspacing=\"0\" width=\"100%\"\n                              style=\"width:100%\">\n                              <thead>\n                                <tr>\n                                  <th class='text-center'>{{\"Invoice Date\" | translate | uppercase}}</th>\n                                  <th class=\"text-left\">{{\"Description\" | translate | uppercase}}</th>\n                                  <th class=\"text-right\">{{\"Currency\" | translate | uppercase}}</th>\n                                  <th class=\"text-right\">{{\"Amount\" | translate | uppercase}}</th>\n                                </tr>\n                              </thead>\n                              <tbody>\n                                <tr *ngFor=\"let row of dataTable.dataRows; let i = index\">\n                                  <td class='text-center'>{{row.invoiceDate}}</td>\n                                  <td class='text-left'>{{row.operationDesc}}</td>\n                                  <td class='text-right'>{{row.currency | uppercase}}</td>\n                                  <td class='text-right'>{{row.amount | number}}</td>\n                                </tr>\n                              </tbody>\n                              <tfoot>\n                                <tr>\n                                  <th>{{\"TOTALS\"|translate}}</th>\n                                  <th></th>\n                                  <th></th>\n                                  <th class='text-right'>{{totals.amount1 | number}}</th>\n                                </tr>\n                              </tfoot>\n                            </table>\n                          </div>\n                    </div>\n                </div>\n                <div class=\"content\" >\n                    <div style='float:right;margin-bottom: 40px;'>\n                        <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                    </div>\n                </div>\n            </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/finance/balance-details/balance-details.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/finance/balance-details/balance-details.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvYmFsYW5jZS1kZXRhaWxzL2JhbGFuY2UtZGV0YWlscy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/finance/balance-details/balance-details.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/finance/balance-details/balance-details.component.ts ***!
  \**********************************************************************/
/*! exports provided: BalanceDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BalanceDetailsComponent", function() { return BalanceDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BalanceDetailsComponent = /** @class */ (function () {
    function BalanceDetailsComponent(apiService, activeModal) {
        this.apiService = apiService;
        this.activeModal = activeModal;
        this.invoiceCurrency = 1;
        this.totals = [];
        this.reports = [];
        this.businessID = localStorage.getItem("businessID");
    }
    BalanceDetailsComponent.prototype.ngOnInit = function () {
        this.dataTable = {
            headerRow: ['Invoice Date', 'Description', 'Currency', 'Amount'],
            footerRow: ['Invoice Date', 'Description', 'Currency', 'Amount'],
            dataRows: []
        };
        this.report();
    };
    BalanceDetailsComponent.prototype.report = function () {
        var _this = this;
        this.invoiceStartDate = this.startDate;
        this.invoiceEndDate = this.endDate;
        this.invoiceCurrency = this.currency;
        this.apiService.getData(this.businessID + "/" + this.invoiceStartDate + "/" + this.invoiceEndDate + '/' + this.invoiceCurrency + "/" + this.ID, "balanceDetails/").then(function (result) {
            console.log(result);
            _this.reports = result;
            var amount1 = 0;
            _this.reports.forEach(function (element) {
                amount1 = amount1 + (element.amount * 1);
            });
            _this.totals["amount1"] = amount1.toFixed(2);
            console.log(_this.totals, "totals");
            _this.dataTable = {
                headerRow: ['Invoice Date', 'Description', 'Currency', 'Amount'],
                footerRow: ['Invoice Date', 'Description', 'Currency', 'Amount'],
                dataRows: _this.reports
            };
        });
    };
    BalanceDetailsComponent.prototype.ngAfterViewInit = function () {
        $('#datatablesd').DataTable({
            "pagingType": "full",
            "pageLength": 100,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            paginate: false,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: " ",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                paginate: {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                }
            }
        });
    };
    BalanceDetailsComponent.prototype.clickCancel = function () {
        this.activeModal.close();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BalanceDetailsComponent.prototype, "ID", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BalanceDetailsComponent.prototype, "startDate", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BalanceDetailsComponent.prototype, "endDate", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], BalanceDetailsComponent.prototype, "currency", void 0);
    BalanceDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-balance-details',
            template: __webpack_require__(/*! ./balance-details.component.html */ "./src/app/finance/balance-details/balance-details.component.html"),
            styles: [__webpack_require__(/*! ./balance-details.component.scss */ "./src/app/finance/balance-details/balance-details.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbActiveModal"]])
    ], BalanceDetailsComponent);
    return BalanceDetailsComponent;
}());



/***/ }),

/***/ "./src/app/finance/invoice-id/invoice-id.component.html":
/*!**************************************************************!*\
  !*** ./src/app/finance/invoice-id/invoice-id.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n            <form method=\"post\" class=\"form-horizontal\">\n                <h4 class=\"title\">{{\"Related Invoice\" | translate}}</h4>\n                <div class=\"card\">\n                    <!-- mains will be here -->\n                    <div class=\"header\">\n                        <legend>{{\"InvoiceID\" | translate}}</legend>\n                    </div>\n                    <div class=\"content\">       \n                        <fieldset>\n                            <div class=\"form-group\">\n                                <div class=\"col-sm-12\">\n                                    <input type=\"text\" placeholder=\"Invoice ID\" class=\"form-control\" name=\"relatedInvoiceID\"\n                                    [ngModelOptions]=\"{standalone: true}\" [(ngModel)]=\"transaction.relatedInvoiceID\">\n                                </div>\n                            </div>\n                            <a target=\"_blank\" \n                                *ngIf=\"transaction.relatedInvoiceID != NULL \"\n                                href=\"https://crmapi.efdigitalcodes.com/{{transaction.pdfPath}}\">\n                                <i class=\"fa fa-file-pdf-o\"></i>\n                                {{\"Click for the invoice\"|translate}}</a>\n                        <br>\n                        </fieldset>\n                    </div>\n                </div>\n                <div class=\"content\" >\n                    <div style='float:right;margin-bottom: 40px;'>\n                        <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                        <button class=\"btn btn-success btn-fill btn-wd\" (click)=\"saveInvoiceID()\" >SAVE</button>\n                    </div>\n                </div>\n            </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/finance/invoice-id/invoice-id.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/finance/invoice-id/invoice-id.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2UvaW52b2ljZS1pZC9pbnZvaWNlLWlkLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/finance/invoice-id/invoice-id.component.ts":
/*!************************************************************!*\
  !*** ./src/app/finance/invoice-id/invoice-id.component.ts ***!
  \************************************************************/
/*! exports provided: InvoiceIDComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceIDComponent", function() { return InvoiceIDComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var InvoiceIDComponent = /** @class */ (function () {
    function InvoiceIDComponent(apiService, activeModal) {
        this.apiService = apiService;
        this.activeModal = activeModal;
        this.transaction = [];
        this.businessID = localStorage.getItem("businessID");
    }
    InvoiceIDComponent.prototype.ngOnInit = function () {
        console.log(this.transactionID, "Transaction ID");
        this.getTransaction();
    };
    InvoiceIDComponent.prototype.clickCancel = function () {
        this.activeModal.close();
    };
    InvoiceIDComponent.prototype.saveInvoiceID = function () {
        var _this = this;
        console.log("Click Save");
        this.apiService.postData(this.transactionID, "transaction/save/", JSON.stringify(this.transaction)).then(function (result) {
            console.log(result);
            _this.activeModal.close();
        });
    };
    InvoiceIDComponent.prototype.getTransaction = function () {
        var _this = this;
        console.log(this.transactionID, "Transaction ID");
        var parameters = this.businessID + "/0/" + this.startDate + "/" + this.endDate + "/" + this.transactionID;
        this.apiService.getData(parameters, "transactions/").then(function (result) {
            _this.transaction = result[0];
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], InvoiceIDComponent.prototype, "transactionID", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], InvoiceIDComponent.prototype, "startDate", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], InvoiceIDComponent.prototype, "endDate", void 0);
    InvoiceIDComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-invoice-id',
            template: __webpack_require__(/*! ./invoice-id.component.html */ "./src/app/finance/invoice-id/invoice-id.component.html"),
            styles: [__webpack_require__(/*! ./invoice-id.component.scss */ "./src/app/finance/invoice-id/invoice-id.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbActiveModal"]])
    ], InvoiceIDComponent);
    return InvoiceIDComponent;
}());



/***/ }),

/***/ "./src/app/finance/note/note.component.html":
/*!**************************************************!*\
  !*** ./src/app/finance/note/note.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n            <form method=\"post\" class=\"form-horizontal\">\n                <h4 class=\"title\">Note</h4>\n                <div class=\"card\">\n                    <!-- mains will be here -->\n                    <div class=\"header\">\n                        <legend>{{\"Note\" | translate}}</legend>\n                    </div>\n                    <div class=\"content\">       \n                        <fieldset>\n                            <div class=\"form-group\">\n                                <div class=\"col-sm-12\">\n                                    <textarea rows=\"3\" class=\"form-control\" placeholder=\"Notes\" [(ngModel)]=\"transaction.notes\"\n                                    name=\"notes\"></textarea>\n                                </div>\n                            </div>\n                        </fieldset>\n                    </div>\n                </div>\n                <div class=\"content\" >\n                    <div style='float:right;margin-bottom: 40px;'>\n                        <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                        <button class=\"btn btn-success btn-fill btn-wd\" (click)=\"saveNote()\" >SAVE</button>\n                    </div>\n                </div>\n            </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/finance/note/note.component.scss":
/*!**************************************************!*\
  !*** ./src/app/finance/note/note.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpbmFuY2Uvbm90ZS9ub3RlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/finance/note/note.component.ts":
/*!************************************************!*\
  !*** ./src/app/finance/note/note.component.ts ***!
  \************************************************/
/*! exports provided: NoteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoteComponent", function() { return NoteComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NoteComponent = /** @class */ (function () {
    function NoteComponent(apiService, activeModal) {
        this.apiService = apiService;
        this.activeModal = activeModal;
        this.transaction = [];
        this.businessID = localStorage.getItem("businessID");
    }
    NoteComponent.prototype.ngOnInit = function () {
        console.log(this.transactionID, "Transaction ID");
        this.getTransaction();
    };
    NoteComponent.prototype.clickCancel = function () {
        this.activeModal.close();
    };
    NoteComponent.prototype.saveNote = function () {
        var _this = this;
        console.log("Click Save");
        this.apiService.postData(this.transactionID, "transaction/save/", JSON.stringify(this.transaction)).then(function (result) {
            console.log(result);
            _this.activeModal.close(result);
        });
    };
    NoteComponent.prototype.getTransaction = function () {
        var _this = this;
        console.log(this.transactionID, "Transaction ID");
        var parameters = this.businessID + "/0/" + this.transactionID;
        this.apiService.getData(parameters, "transactions/").then(function (result) {
            _this.transaction = result[0];
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NoteComponent.prototype, "transactionID", void 0);
    NoteComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-note',
            template: __webpack_require__(/*! ./note.component.html */ "./src/app/finance/note/note.component.html"),
            styles: [__webpack_require__(/*! ./note.component.scss */ "./src/app/finance/note/note.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"]])
    ], NoteComponent);
    return NoteComponent;
}());



/***/ }),

/***/ "./src/app/layouts/admin/admin-layout.component.html":
/*!***********************************************************!*\
  !*** ./src/app/layouts/admin/admin-layout.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"wrapper\">\n    <div class=\"sidebar\" data-color=\"red\" data-image=\"\">\n        <sidebar-cmp></sidebar-cmp>\n        <!-- Removed Background image from sidenav\n            <div class=\"sidebar-background\" style=\"background-image: url(assets/img/full-screen-image-3.jpg)\"></div>\n        -->\n    </div>\n    <div class=\"main-panel\">\n        <navbar-cmp></navbar-cmp>\n        <router-outlet></router-outlet>\n        <div *ngIf=\"!isMap()\">\n          <footer-cmp></footer-cmp>\n        </div>\n    </div>\n</div>\n<fixedplugin-cmp></fixedplugin-cmp>\n"

/***/ }),

/***/ "./src/app/layouts/admin/admin-layout.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/layouts/admin/admin-layout.component.ts ***!
  \*********************************************************/
/*! exports provided: AdminLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminLayoutComponent", function() { return AdminLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs_add_operator_filter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/filter */ "./node_modules/rxjs-compat/_esm5/add/operator/filter.js");
/* harmony import */ var _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/navbar/navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminLayoutComponent = /** @class */ (function () {
    function AdminLayoutComponent(router, location) {
        this.router = router;
        this.location = location;
    }
    AdminLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._router = this.router.events.filter(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; }).subscribe(function (event) {
            //   this.url = event.url;
            _this.navbar.sidebarClose();
        });
        var isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
        if (isWindows) {
            // if we are on windows OS we activate the perfectScrollbar function
            var $main_panel = $('.main-panel');
            $main_panel.perfectScrollbar();
        }
    };
    AdminLayoutComponent.prototype.isMap = function () {
        if (window.location.pathname.indexOf("/maps/fullscreen") !== -1) {
            return true;
        }
        else {
            return false;
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('sidebar'),
        __metadata("design:type", Object)
    ], AdminLayoutComponent.prototype, "sidebar", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__["NavbarComponent"]),
        __metadata("design:type", _shared_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__["NavbarComponent"])
    ], AdminLayoutComponent.prototype, "navbar", void 0);
    AdminLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__(/*! ./admin-layout.component.html */ "./src/app/layouts/admin/admin-layout.component.html")
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"]])
    ], AdminLayoutComponent);
    return AdminLayoutComponent;
}());



/***/ }),

/***/ "./src/app/layouts/auth/auth-layout.component.html":
/*!*********************************************************!*\
  !*** ./src/app/layouts/auth/auth-layout.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <pagesnavbar-cmp></pagesnavbar-cmp>\n  <router-outlet></router-outlet>\n  <fixedplugin-cmp></fixedplugin-cmp>\n"

/***/ }),

/***/ "./src/app/layouts/auth/auth-layout.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/layouts/auth/auth-layout.component.ts ***!
  \*******************************************************/
/*! exports provided: AuthLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthLayoutComponent", function() { return AuthLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs_add_operator_filter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/filter */ "./node_modules/rxjs-compat/_esm5/add/operator/filter.js");
/* harmony import */ var _shared_pagesnavbar_pagesnavbar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/pagesnavbar/pagesnavbar.component */ "./src/app/shared/pagesnavbar/pagesnavbar.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthLayoutComponent = /** @class */ (function () {
    function AuthLayoutComponent(router, location) {
        this.router = router;
        this.location = location;
    }
    AuthLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._router = this.router.events.filter(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; }).subscribe(function (event) {
            //   this.url = event.url;
            _this.pagesnavbar.sidebarClose();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_shared_pagesnavbar_pagesnavbar_component__WEBPACK_IMPORTED_MODULE_4__["PagesnavbarComponent"]),
        __metadata("design:type", _shared_pagesnavbar_pagesnavbar_component__WEBPACK_IMPORTED_MODULE_4__["PagesnavbarComponent"])
    ], AuthLayoutComponent.prototype, "pagesnavbar", void 0);
    AuthLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__(/*! ./auth-layout.component.html */ "./src/app/layouts/auth/auth-layout.component.html")
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"]])
    ], AuthLayoutComponent);
    return AuthLayoutComponent;
}());



/***/ }),

/***/ "./src/app/pagenotfound/pagenotfound.component.html":
/*!**********************************************************!*\
  !*** ./src/app/pagenotfound/pagenotfound.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  pagenotfound works!\n</p>\n"

/***/ }),

/***/ "./src/app/pagenotfound/pagenotfound.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/pagenotfound/pagenotfound.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2Vub3Rmb3VuZC9wYWdlbm90Zm91bmQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pagenotfound/pagenotfound.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pagenotfound/pagenotfound.component.ts ***!
  \********************************************************/
/*! exports provided: PagenotfoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagenotfoundComponent", function() { return PagenotfoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PagenotfoundComponent = /** @class */ (function () {
    function PagenotfoundComponent() {
    }
    PagenotfoundComponent.prototype.ngOnInit = function () {
    };
    PagenotfoundComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pagenotfound',
            template: __webpack_require__(/*! ./pagenotfound.component.html */ "./src/app/pagenotfound/pagenotfound.component.html"),
            styles: [__webpack_require__(/*! ./pagenotfound.component.scss */ "./src/app/pagenotfound/pagenotfound.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PagenotfoundComponent);
    return PagenotfoundComponent;
}());



/***/ }),

/***/ "./src/app/pages/upload/upload.component.html":
/*!****************************************************!*\
  !*** ./src/app/pages/upload/upload.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n            <form method=\"post\" class=\"form-horizontal\">\n                <h4 class=\"title\">File Description</h4>\n                <div class=\"card\">\n                    <!-- mains will be here -->\n                  \n                    <div class=\"content\">       \n                        <fieldset>\n                            <div class=\"form-group\">\n                                <label class=\"col-sm-4 control-label\">Description</label>\n                                <div class=\"col-sm-8\">\n\n                                    <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"description\" #description\n                                     >\n                                </div>\n                            </div>\n                        </fieldset>\n                        <fieldset>\n                            <div class=\"form-group\">\n                                <label class=\"col-sm-4 control-label\"></label>\n                                <div class=\"col-sm-8\">\n                                    <input type=\"file\" accept=\"application/pdf\" name=\"uf\" style=\"display:none;\" #fileInput (change)=\"onFileChange($event)\"/>\n                                    <a #fileLink (click)='addFile()' title='Add File' style='color:gray;margin-right:10px;cursor:pointer;'>Select File</a>\n                                </div>\n                            </div>\n                        </fieldset>\n                    </div>\n                </div>\n                <div class=\"content\" >\n                    <div style='float:right;margin-bottom: 40px;'>\n                        <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                        <button class=\"btn btn-success btn-fill btn-wd\" (click)=\"saveFile()\" >SAVE</button>\n                    </div>\n                </div>\n            </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/pages/upload/upload.component.scss":
/*!****************************************************!*\
  !*** ./src/app/pages/upload/upload.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VwbG9hZC91cGxvYWQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/upload/upload.component.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/upload/upload.component.ts ***!
  \**************************************************/
/*! exports provided: UploadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadComponent", function() { return UploadComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UploadComponent = /** @class */ (function () {
    function UploadComponent(apiService, activeModal) {
        this.apiService = apiService;
        this.activeModal = activeModal;
        this.userID = localStorage.getItem("userID");
    }
    UploadComponent.prototype.ngOnInit = function () {
    };
    UploadComponent.prototype.clickCancel = function () {
        this.activeModal.close();
    };
    UploadComponent.prototype.addFile = function () {
        var event = new MouseEvent('click', { bubbles: false });
        this.fileInput.nativeElement.dispatchEvent(event);
    };
    UploadComponent.prototype.onFileChange = function (event) {
        this.fileLink.nativeElement.text = event.target.files[0].name;
    };
    UploadComponent.prototype.saveFile = function () {
        var _this = this;
        var reader = new FileReader();
        if (this.fileInput.nativeElement.files && this.fileInput.nativeElement.files.length > 0) {
            var file = this.fileInput.nativeElement.files[0];
            reader.readAsDataURL(file);
            reader.onload = function () {
                var file = {
                    "fileName": _this.fileInput.nativeElement.files[0].name,
                    "fileType": _this.fileInput.nativeElement.files[0].type,
                    "table": _this.table,
                    "userID": _this.userID,
                    "description": _this.description.nativeElement.value,
                    "file": reader.result.toString().split(",")[1]
                };
                _this.apiService.postData(_this.table + "/" + _this.ID, "files/upload/", JSON.stringify(file)).then(function (result) {
                    console.log(result);
                    document.location.reload();
                });
                console.log(_this.fileInput.nativeElement.files[0].name);
                console.log(_this.fileInput.nativeElement.files[0].type);
                console.log(reader.result.toString().split(",")[1]);
            };
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], UploadComponent.prototype, "fileInput", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('description'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], UploadComponent.prototype, "description", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('fileLink'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], UploadComponent.prototype, "fileLink", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], UploadComponent.prototype, "ID", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], UploadComponent.prototype, "table", void 0);
    UploadComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-upload',
            template: __webpack_require__(/*! ./upload.component.html */ "./src/app/pages/upload/upload.component.html"),
            styles: [__webpack_require__(/*! ./upload.component.scss */ "./src/app/pages/upload/upload.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbActiveModal"]])
    ], UploadComponent);
    return UploadComponent;
}());



/***/ }),

/***/ "./src/app/providers/api.service.ts":
/*!******************************************!*\
  !*** ./src/app/providers/api.service.ts ***!
  \******************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//let apiURL = "https://localhost/efcrmapi/V1/";
//let apiURL = "http://localhost/efcrmapi/V1/";
var apiURL = "https://crmapi.efdigitalcodes.com/V1/";
var apiUser = "onderboydak@eglencefabrikasi.com";
var apiPass = "1234";
var ApiService = /** @class */ (function () {
    function ApiService(http) {
        this.http = http;
    }
    ApiService.prototype.postData = function (parameters, func, body) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', 'Basic ' + btoa(apiUser + ':' + apiPass));
            var url = apiURL + func + parameters;
            console.log(body, "Body");
            _this.http.post(url, body, { headers: headers }).subscribe(function (res) {
                console.info("postData", res);
                resolve(res);
            }, function (err) {
                resolve(err);
                //this.app.getActiveNav().push(ErrorPage);
            });
        });
    };
    ApiService.prototype.getData = function (parameters, func) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', 'Basic ' + btoa(apiUser + ':' + apiPass));
            var url = apiURL + func + parameters;
            console.log(url);
            _this.http.get(url, { headers: headers }).subscribe(function (res) {
                console.info("getData", res);
                resolve(res);
            }, function (err) {
                resolve(err);
                //this.app.getActiveNav().push(ErrorPage);
            });
        });
    };
    ApiService.prototype.deleteData = function (parameters, func) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', 'Basic ' + btoa(apiUser + ':' + apiPass));
            var url = apiURL + func + parameters;
            console.log(url);
            _this.http.delete(url, { headers: headers }).subscribe(function (res) {
                console.info("deleteData", res);
                resolve(res);
            }, function (err) {
                resolve(err);
                //this.app.getActiveNav().push(ErrorPage);
            });
        });
    };
    ApiService.prototype.mysql2date = function (timestamp) {
        //function parses mysql datetime string and returns javascript Date object
        //input has to be in this format: 2007-06-05 15:26:02
        var regex = /^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9]) (?:([0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/;
        var parts = timestamp.replace(regex, "$1 $2 $3 $4 $5 $6").split(' ');
        return new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
    };
    ApiService.prototype.jsonToTable = function (jsonValue) {
        for (var _i = 0, jsonValue_1 = jsonValue; _i < jsonValue_1.length; _i++) {
            var child = jsonValue_1[_i];
            return child[0];
        }
        ;
    };
    ApiService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/providers/excel.service.ts":
/*!********************************************!*\
  !*** ./src/app/providers/excel.service.ts ***!
  \********************************************/
/*! exports provided: ExcelService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExcelService", function() { return ExcelService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! xlsx */ "./node_modules/xlsx/xlsx.js");
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
var EXCEL_EXTENSION = '.xlsx';
var ExcelService = /** @class */ (function () {
    function ExcelService() {
    }
    ExcelService.prototype.exportAsExcelFile = function (json, excelFileName) {
        var worksheet = xlsx__WEBPACK_IMPORTED_MODULE_2__["utils"].json_to_sheet(json);
        var workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        var excelBuffer = xlsx__WEBPACK_IMPORTED_MODULE_2__["write"](workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    };
    ExcelService.prototype.saveAsExcelFile = function (buffer, fileName) {
        var data = new Blob([buffer], { type: EXCEL_TYPE });
        file_saver__WEBPACK_IMPORTED_MODULE_1__["saveAs"](data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    };
    ExcelService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ExcelService);
    return ExcelService;
}());



/***/ }),

/***/ "./src/app/sales/approve-modal/approve-modal.component.html":
/*!******************************************************************!*\
  !*** ./src/app/sales/approve-modal/approve-modal.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n          <form method=\"post\" class=\"form-horizontal\">\n              <h4 class=\"title text-center\">Invoice #{{invoice.invoiceNumber}}</h4>\n                  <!-- mains will be here -->\n                  <div class=\"header text-center\" >\n                      <legend style=\"border:none !important;\">{{\"Would you like to approve this invoice?\" | translate}}</legend>\n                  </div>\n              <div class=\"content text-center\" >\n                <div style='margin-bottom: 40px;'>\n                    <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                    <button style='margin-right:5px;' class=\"btn btn-danger btn-fill btn-wd\" (click)=\"approveInvoice(2)\" >REJECT</button>\n                    <button class=\"btn btn-success btn-fill btn-wd\" (click)=\"approveInvoice(3)\" >APPROVE</button>\n\n                </div>\n            </div>\n          </form>\n          </div>\n      </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/sales/approve-modal/approve-modal.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/sales/approve-modal/approve-modal.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NhbGVzL2FwcHJvdmUtbW9kYWwvYXBwcm92ZS1tb2RhbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/sales/approve-modal/approve-modal.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/sales/approve-modal/approve-modal.component.ts ***!
  \****************************************************************/
/*! exports provided: ApproveModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApproveModalComponent", function() { return ApproveModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApproveModalComponent = /** @class */ (function () {
    function ApproveModalComponent(apiService, activeModal) {
        this.apiService = apiService;
        this.activeModal = activeModal;
        this.invoice = [];
        this.businessID = localStorage.getItem("businessID");
    }
    ApproveModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this.invoiceID, "inv id");
        this.apiService.getData('0/' + this.businessID + '/' + this.invoiceID, "invoices/").then(function (result) {
            _this.invoice = result[0];
        });
    };
    ApproveModalComponent.prototype.clickCancel = function () {
        this.activeModal.close();
    };
    ApproveModalComponent.prototype.approveInvoice = function (approve) {
        var _this = this;
        // 3 - Approve  2-Reject
        var st = this.invoice.statusID;
        if (approve == 3 && this.invoice.statusID == 0) {
            this.invoice.statusID = 1;
        }
        else if (approve == 3 && this.invoice.statusID == 1) {
            if (this.invoice.invoiceType == 1) {
                this.invoice.statusID = 3;
            }
            else {
                this.invoice.statusID = 1000;
            }
        }
        else {
            this.invoice.statusID = approve;
        }
        this.apiService.postData(this.invoiceID, "/invoice/save/", JSON.stringify(this.invoice)).then(function (result) {
            var res = [];
            if (approve == 3) {
                if (_this.invoice.invoiceType == 1) {
                    res["statusID"] = 3;
                    res["status"] = "Approved";
                }
                else {
                    res["statusID"] = 1000;
                    res["status"] = "Completed";
                }
            }
            else if (approve == 2) {
                res["statusID"] = 2;
                res["status"] = "Rejected";
            }
            else {
                res["statusID"] = 0;
                res["status"] = "";
            }
            _this.activeModal.close(res);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ApproveModalComponent.prototype, "invoiceID", void 0);
    ApproveModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-approve-modal',
            template: __webpack_require__(/*! ./approve-modal.component.html */ "./src/app/sales/approve-modal/approve-modal.component.html"),
            styles: [__webpack_require__(/*! ./approve-modal.component.scss */ "./src/app/sales/approve-modal/approve-modal.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbActiveModal"]])
    ], ApproveModalComponent);
    return ApproveModalComponent;
}());



/***/ }),

/***/ "./src/app/sales/invoice-payment/invoice-payment.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/sales/invoice-payment/invoice-payment.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n            <form method=\"post\" class=\"form-horizontal\">\n                <h4 class=\"title\">{{paymentType}} payment for this invoice #{{invoice.invoiceNumber}}</h4>\n                <div class=\"card\">\n                    <!-- mains will be here -->\n                    <div class=\"header\">\n                        <legend>{{\"Payment\" | translate}}</legend>\n                    </div>\n                    <div class=\"content\">       \n                        <fieldset>\n                            <div class=\"form-group\">\n                                <label class=\"col-sm-4 control-label\">Amount</label>\n                                <div class=\"col-sm-8\">\n                                    <input type=\"text\" placeholder=\"\" class=\"form-control\" name=\"amount\"\n                                    [ngModelOptions]=\"{standalone: true}\" [value]=\"invoice.payment.amount\" [(ngModel)]=\"invoice.payment.amount\">\n                                </div>\n                            </div>\n                        </fieldset>\n                        <fieldset>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-4 control-label\">Payment Date</label>\n                                    <div class=\"col-sm-8\">\n                                        <input type=\"text\" [(ngModel)]=\"invoice.payment.paymentDate\" name=\"paymentDate\" class=\"form-control datetimepicker\"\n                                            placeholder=\"Pick Date Time\" [ngModelOptions]=\"{standalone: true}\" [value]=\"invoice.payment.paymentDate\"/>\n                                    </div>\n                                </div>\n                            </fieldset>\n                        <fieldset>\n                            <div class=\"form-group\">\n                                <label class=\"col-sm-4 control-label\">Payment Method</label>\n                                <div class=\"col-sm-8\">\n                                    <select name=\"paymentMethod\" [(ngModel)]=\"invoice.payment.paymentMethod\" class=\"selectpicker selectpaymentMethod\"\n                                        data-title=\"Payment Method\" data-style=\"btn-default btn-block\"\n                                        data-menu-style=\"dropdown-blue\">\n                                        <option [value]=\"1\">Cash</option>\n                                        <option [value]=\"2\">Credit Card</option>\n                                        <option [value]=\"3\">Bank</option>\n                                    </select>\n                                </div>\n                            </div>\n                        </fieldset>\n                        <fieldset>\n                            <div class=\"form-group\">\n                                <label class=\"col-sm-4 control-label\">Payment Account</label>\n                                <div class=\"col-sm-8\">\n                                    <select name=\"paymentAccount\" [(ngModel)]=\"invoice.payment.paymentAccount\" class=\"selectpicker selectpaymentAccount\"\n                                        data-title=\"Payment Account\" data-style=\"btn-default btn-block\"\n                                        data-menu-style=\"dropdown-blue\">\n                                        <option [value]=\"1\">Cash Register</option>\n                                    </select>\n                                </div>\n                            </div>\n                        </fieldset>\n                        <fieldset>\n                            <div class=\"form-group\">\n                                <label class=\"col-sm-4 control-label\">Notes</label>\n                                <div class=\"col-sm-8\">\n                                    <textarea rows=\"3\" class=\"form-control\" placeholder=\"Notes\" [(ngModel)]=\"invoice.payment.notes\"\n                                        name=\"notes\"></textarea>\n\n                                </div>\n                            </div>\n                        </fieldset>\n                    </div>\n                </div>\n                <div class=\"content\" >\n                    <div style='float:right;margin-bottom: 40px;'>\n                        <button style='margin-right:5px;' class=\"btn btn-default btn-fill btn-wd\" (click)=\"clickCancel()\">CANCEL</button>\n                        <button class=\"btn btn-success btn-fill btn-wd\" (click)=\"savePayment()\" >SAVE</button>\n                    </div>\n                </div>\n            </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/sales/invoice-payment/invoice-payment.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/sales/invoice-payment/invoice-payment.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NhbGVzL2ludm9pY2UtcGF5bWVudC9pbnZvaWNlLXBheW1lbnQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/sales/invoice-payment/invoice-payment.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/sales/invoice-payment/invoice-payment.component.ts ***!
  \********************************************************************/
/*! exports provided: InvoicePaymentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicePaymentComponent", function() { return InvoicePaymentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InvoicePaymentComponent = /** @class */ (function () {
    function InvoicePaymentComponent(apiService, activeModal, datePipe) {
        this.apiService = apiService;
        this.activeModal = activeModal;
        this.datePipe = datePipe;
        this.invoice = [];
        var now = new Date();
        this.fd = this.datePipe.transform(now, 'dd-MM-yyyy').toString();
        console.log(this.fd);
        this.invoice.payment = [{
                "invoiceID": 0,
                "paymentDate": this.fd,
                "amount": 0,
                "paymentAccount": 0,
                "paymentMethod": 0,
                "notes": ""
            }];
        console.log(this.invoice.payment);
        this.businessID = localStorage.getItem("businessID");
    }
    InvoicePaymentComponent.prototype.ngOnInit = function () {
        this.getInvoice(0);
        $('.datetimepicker').datetimepicker({
            locale: 'tr',
            format: 'DD-MM-YYYY',
            date: this.invoice.paymentDate,
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
        //console.log(this.fd,"fd");
        $(".selectpicker").selectpicker();
    };
    InvoicePaymentComponent.prototype.getInvoice = function (invoiceType) {
        var _this = this;
        console.log(this.invoiceID, "Invoice ID");
        this.apiService.getData((invoiceType + "/" + this.businessID + "/" + this.invoiceID), "invoices/").then(function (result) {
            _this.invoice = result[0];
            /*
            this.invoice.invoiceDetails = JSON.parse(this.invoice.invoiceDetails);
            console.log(this.invoice, "Invoıce");
            */
            _this.invoice.payment = JSON.parse(_this.invoice.payment)[0];
            if (_this.invoice.payment.invoiceID == 0) {
                _this.invoice.payment.amount = _this.invoice.totalAmount;
            }
        });
    };
    InvoicePaymentComponent.prototype.clickCancel = function () {
        this.activeModal.close();
    };
    InvoicePaymentComponent.prototype.savePayment = function () {
        var _this = this;
        console.log("Click Save");
        this.invoice.payment.paymentDate = $('.datetimepicker').val();
        console.log(JSON.stringify(this.invoice.payment));
        this.apiService.postData(this.invoice.ID, "invoice/payment/save/", JSON.stringify(this.invoice.payment)).then(function (result) {
            console.log(result);
            _this.activeModal.close(result);
        });
    };
    InvoicePaymentComponent.prototype.ngAfterViewInit = function () {
        console.log('AfterViewInit');
        $(".datetimepicker").datetimepicker({ data: this.invoice.paymentDate });
        //Picker actiginda secilmis oluyor ama neden secili goster miyor.
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], InvoicePaymentComponent.prototype, "invoiceID", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], InvoicePaymentComponent.prototype, "paymentType", void 0);
    InvoicePaymentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-invoice-payment',
            template: __webpack_require__(/*! ./invoice-payment.component.html */ "./src/app/sales/invoice-payment/invoice-payment.component.html"),
            styles: [__webpack_require__(/*! ./invoice-payment.component.scss */ "./src/app/sales/invoice-payment/invoice-payment.component.scss")]
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbActiveModal"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["DatePipe"]])
    ], InvoicePaymentComponent);
    return InvoicePaymentComponent;
}());



/***/ }),

/***/ "./src/app/shared/fixedplugin/fixedplugin.component.html":
/*!***************************************************************!*\
  !*** ./src/app/shared/fixedplugin/fixedplugin.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\n<div class=\"fixed-plugin\">\n    <div class=\"dropdown\">\n        <a href=\"#\" data-toggle=\"dropdown\">\n        <i class=\"fa fa-cog fa-2x\"> </i>\n        </a>\n        <ul class=\"dropdown-menu\">\n            <li class=\"header-title\">Configuration</li>\n            <li class=\"adjustments-line\">\n                <a href=\"javascript:void(0)\" class=\"switch-trigger\">\n                    <p *ngIf=\"isPages()\">Sidebar Image</p>\n                    <p *ngIf=\"!isPages()\"> Background Image</p>\n                    <bSwitch\n                      [switch-on-color]=\"'primary'\"\n                      [(ngModel)]=\"state\"\n                      [ngClass]=\"['switch','switch-sidebar-image']\"\n                      (changeState)=\"onChange($event)\">\n                    </bSwitch>\n                    <div class=\"clearfix\"></div>\n                </a>\n            </li>\n\t\t\t<li class=\"adjustments-line\" *ngIf=\"isPages()\">\n                <a href=\"javascript:void(0)\" class=\"switch-trigger\">\n                    <p>Sidebar Mini</p>\n                    <bSwitch\n                      [switch-on-color]=\"'primary'\"\n                      [ngClass]=\"['switch','switch-sidebar-mini']\"\n                      (changeState)=\"onChange1($event)\">\n                    </bSwitch>\n                    <div class=\"clearfix\"></div>\n                </a>\n            </li>\n\t\t\t<li class=\"adjustments-line\" *ngIf=\"isPages()\">\n                <a href=\"javascript:void(0)\" class=\"switch-trigger\">\n                    <p>Fixed Navbar</p>\n                    <bSwitch\n                      [switch-on-color]=\"'primary'\"\n                      [ngClass]=\"['switch','switch-navbar-fixed']\"\n                      (changeState)=\"onChange2($event)\">\n                    </bSwitch>\n                    <div class=\"clearfix\"></div>\n                </a>\n            </li>\n            <li class=\"adjustments-line\">\n                <a href=\"javascript:void(0)\" class=\"switch-trigger\">\n                    <p>Filters</p>\n                    <div class=\"pull-right\">\n                        <span class=\"badge filter\" data-color=\"black\"></span>\n                        <span class=\"badge filter badge-azure\" data-color=\"azure\"></span>\n                        <span class=\"badge filter badge-green\" data-color=\"green\"></span>\n                        <span class=\"badge filter badge-orange\" data-color=\"orange\"></span>\n                        <span class=\"badge filter badge-red active\" data-color=\"red\"></span>\n                        <span class=\"badge filter badge-purple\" data-color=\"purple\"></span>\n                    </div>\n                    <div class=\"clearfix\"></div>\n                </a>\n            </li>\n            <li class=\"header-title\">Sidebar Images</li>\n            <li>\n                <a class=\"img-holder switch-trigger\" href=\"javascript:void(0)\">\n                    <img src=\"../../../assets/img/full-screen-image-1.jpg\">\n                </a>\n            </li>\n            <li>\n                <a class=\"img-holder switch-trigger\" href=\"javascript:void(0)\">\n                    <img src=\"../../../assets/img/full-screen-image-2.jpg\">\n                </a>\n            </li>\n            <li class=\"active\">\n                <a class=\"img-holder switch-trigger\" href=\"javascript:void(0)\">\n                    <img src=\"../../../assets/img/full-screen-image-3.jpg\">\n                </a>\n            </li>\n            <li>\n                <a class=\"img-holder switch-trigger\" href=\"javascript:void(0)\">\n                    <img src=\"../../../assets/img/full-screen-image-4.jpg\">\n                </a>\n            </li>\n\n        </ul>\n    </div>\n</div>\n-->"

/***/ }),

/***/ "./src/app/shared/fixedplugin/fixedplugin.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/shared/fixedplugin/fixedplugin.component.ts ***!
  \*************************************************************/
/*! exports provided: FixedPluginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FixedPluginComponent", function() { return FixedPluginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var misc = {
    navbar_menu_visible: 0,
    active_collapse: true,
    disabled_collapse_init: 0
};
var FixedPluginComponent = /** @class */ (function () {
    function FixedPluginComponent(location) {
        this.background_image = true;
        this.state = true;
        this.location = location;
    }
    FixedPluginComponent.prototype.ngOnInit = function () {
        var $sidebar = $('.sidebar');
        var $sidebar_img_container = $sidebar.find('.sidebar-background');
        var $full_page = $('.full-page');
        var $sidebar_responsive = $('body > .navbar-collapse');
        var window_width = $(window).width();
        if (window_width > 767) {
            if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                $('.fixed-plugin .dropdown').addClass('open');
            }
        }
        $('.fixed-plugin a').click(function (event) {
            // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
            if ($(this).hasClass('switch-trigger')) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
                else if (window.event) {
                    window.event.cancelBubble = true;
                }
            }
        });
        $('.fixed-plugin .badge').click(function () {
            var $full_page_background = $('.full-page-background');
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var new_color = $(this).data('color');
            if ($sidebar.length != 0) {
                $sidebar.attr('data-color', new_color);
            }
            if ($full_page.length != 0) {
                $full_page.attr('data-color', new_color);
            }
            if ($sidebar_responsive.length != 0) {
                $sidebar_responsive.attr('data-color', new_color);
            }
        });
        $('.fixed-plugin .img-holder').click(function () {
            var $full_page_background = $('.full-page-background');
            $(this).parent('li').siblings().removeClass('active');
            $(this).parent('li').addClass('active');
            var new_image = $(this).find("img").attr('src');
            if ($sidebar_img_container.length != 0) {
                $sidebar_img_container.fadeOut('fast', function () {
                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $sidebar_img_container.fadeIn('fast');
                });
            }
            if ($full_page_background.length != 0) {
                $full_page_background.fadeOut('fast', function () {
                    $full_page_background.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.fadeIn('fast');
                });
            }
            if ($sidebar_responsive.length != 0) {
                $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
            }
        });
    };
    FixedPluginComponent.prototype.onChange = function ($event) {
        var $sidebar = $('.sidebar');
        var $sidebar_img_container = $sidebar.find('.sidebar-background');
        var $full_page = $('.full-page');
        var $full_page_background = $('.full-page-background');
        var $sidebar_responsive = $('body > .navbar-collapse');
        if ($event.currentValue) {
            if ($sidebar_img_container.length != 0) {
                $sidebar_img_container.fadeIn('fast');
                $sidebar.attr('data-image', '#');
            }
            if ($full_page_background.length != 0) {
                $full_page_background.fadeIn('fast');
                $full_page.attr('data-image', '#');
            }
            this.background_image = true;
        }
        else {
            if ($sidebar_img_container.length != 0) {
                $sidebar.removeAttr('data-image');
                $sidebar_img_container.fadeOut('fast');
            }
            if ($full_page_background.length != 0) {
                $full_page.removeAttr('data-image', '#');
                $full_page_background.fadeOut('fast');
            }
            this.background_image = false;
        }
    };
    FixedPluginComponent.prototype.onChange1 = function ($event) {
        var $body = $('body');
        if (misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            misc.sidebar_mini_active = false;
        }
        else {
            $('.sidebar .collapse').collapse('hide').on('hidden.bs.collapse', function () {
                $(this).css('height', 'auto');
            });
            setTimeout(function () {
                $('body').addClass('sidebar-mini');
                $('.sidebar .collapse').css('height', 'auto');
                misc.sidebar_mini_active = true;
            }, 300);
        }
        // we simulate the window Resize so the charts will get updated in realtime.
        var simulateWindowResize = setInterval(function () {
            window.dispatchEvent(new Event('resize'));
        }, 180);
        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function () {
            clearInterval(simulateWindowResize);
        }, 1000);
    };
    FixedPluginComponent.prototype.onChange2 = function ($event) {
        var $nav = $('nav.navbar').first();
        if ($nav.hasClass('navbar-fixed')) {
            $nav.removeClass('navbar-fixed').prependTo('.main-panel');
        }
        else {
            $nav.prependTo('.wrapper').addClass('navbar-fixed');
        }
    };
    FixedPluginComponent.prototype.isPages = function () {
        if (this.location.prepareExternalUrl(this.location.path()) == '/pages/login' || this.location.prepareExternalUrl(this.location.path()) == '/pages/register' ||
            this.location.prepareExternalUrl(this.location.path()) == '/pages/lock') {
            return false;
        }
        else {
            return true;
        }
    };
    FixedPluginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'fixedplugin-cmp',
            template: __webpack_require__(/*! ./fixedplugin.component.html */ "./src/app/shared/fixedplugin/fixedplugin.component.html")
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"]])
    ], FixedPluginComponent);
    return FixedPluginComponent;
}());



/***/ }),

/***/ "./src/app/shared/fixedplugin/fixedplugin.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/shared/fixedplugin/fixedplugin.module.ts ***!
  \**********************************************************/
/*! exports provided: FixedPluginModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FixedPluginModule", function() { return FixedPluginModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _fixedplugin_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fixedplugin.component */ "./src/app/shared/fixedplugin/fixedplugin.component.ts");
/* harmony import */ var jw_bootstrap_switch_ng2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jw-bootstrap-switch-ng2 */ "./node_modules/jw-bootstrap-switch-ng2/fesm5/jw-bootstrap-switch-ng2.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var FixedPluginModule = /** @class */ (function () {
    function FixedPluginModule() {
    }
    FixedPluginModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], jw_bootstrap_switch_ng2__WEBPACK_IMPORTED_MODULE_4__["JwBootstrapSwitchNg2Module"]],
            declarations: [_fixedplugin_component__WEBPACK_IMPORTED_MODULE_3__["FixedPluginComponent"]],
            exports: [_fixedplugin_component__WEBPACK_IMPORTED_MODULE_3__["FixedPluginComponent"]]
        })
    ], FixedPluginModule);
    return FixedPluginModule;
}());



/***/ }),

/***/ "./src/app/shared/footer/footer.component.html":
/*!*****************************************************!*\
  !*** ./src/app/shared/footer/footer.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<footer class=\"footer\">\n    <div class=\"container-fluid\">\n        <nav class=\"pull-left\">\n                <ul>\n                        <li>\n                            <a href=\"#\">\n                                Web\n                            </a>\n                        </li>\n                        <li>\n                            <a href=\"#\">\n                                Factory Floor\n                            </a>\n                        </li>\n                        <li>\n                            <a href=\"#\">\n                                EF Codes\n                            </a>\n                        </li>\n                        <li>\n                            <a href=\"#\">\n                               Distronaut\n                            </a>\n                        </li>\n                    </ul>\n        </nav>\n        <p class=\"copyright pull-right\">\n            &copy; {{test | date: 'yyyy'}} <a href=\"http://www.eglencefabrikasi.com\">Eglence Fabrikasi</a>\n        </p>\n    </div>\n</footer>\n"

/***/ }),

/***/ "./src/app/shared/footer/footer.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/footer/footer.component.ts ***!
  \***************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
        this.test = new Date();
    }
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'footer-cmp',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/shared/footer/footer.component.html")
        })
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/shared/footer/footer.module.ts":
/*!************************************************!*\
  !*** ./src/app/shared/footer/footer.module.ts ***!
  \************************************************/
/*! exports provided: FooterModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterModule", function() { return FooterModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _footer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./footer.component */ "./src/app/shared/footer/footer.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FooterModule = /** @class */ (function () {
    function FooterModule() {
    }
    FooterModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
            declarations: [_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]],
            exports: [_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]]
        })
    ], FooterModule);
    return FooterModule;
}());



/***/ }),

/***/ "./src/app/shared/navbar/navbar.component.html":
/*!*****************************************************!*\
  !*** ./src/app/shared/navbar/navbar.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<nav #navbar class=\"navbar navbar-default\">\n    <div class=\"container-fluid\">\n        <div class=\"navbar-minimize\">\n            <button id=\"minimizeSidebar\" class=\"btn btn-danger btn-fill btn-round btn-icon\">\n                <i class=\"fa fa-ellipsis-v visible-on-sidebar-regular\"></i>\n                <i class=\"fa fa-navicon visible-on-sidebar-mini\"></i>\n            </button>\n        </div>\n        <div class=\"navbar-header\">\n            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" (click)=\"sidebarToggle()\">\n                <span class=\"sr-only\">Toggle navigation</span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n            </button>\n            <!-- <a class=\"navbar-brand\">{{getTitle()}}</a> -->\n        </div>\n        <div class=\"collapse navbar-collapse\">\n            <div class=\"\" *ngIf=\"isMobileMenu()\">\n                <form class=\"navbar-form navbar-left navbar-search-form\" role=\"search\">\n                    <div class=\"input-group\">\n                        <span class=\"input-group-addon\"><i class=\"fa fa-search\"></i></span>\n                        <input type=\"text\" value=\"\" class=\"form-control\" placeholder=\"Search...\">\n                    </div>\n                </form>\n\n                <ul class=\"nav navbar-nav navbar-right\">\n                    <li style=\"padding: 18px;\">\n                        {{user.firstName + \" \" + user.lastName}}\n                       <!--  <a ruterLinkActive = \"active\" [routerLink]=\"['/charts']\">\n                            <i class=\"fa fa-line-chart\"></i>\n                            <p>Stats</p>\n                        </a> -->\n                    </li>\n                    <li class=\"dropdown dropdown-with-icons\">\n                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                            <i class=\"fa fa-list\"></i>\n                            <p class=\"hidden-md hidden-lg\">\n                                More\n                                <b class=\"caret\"></b>\n                            </p>\n                        </a>\n                        <ul class=\"dropdown-menu dropdown-with-icons\">\n                            <li>\n                                <a href=\"#\">\n                                    <i class=\"pe-7s-mail\"></i> Messages\n                                </a>\n                            </li>\n                            <li>\n                                <a href=\"#\">\n                                    <i class=\"pe-7s-help1\"></i> Help Center\n                                </a>\n                            </li>\n                            <li>\n                                <a href=\"#/admin/user/{{user.ID}}\">\n                                    <i class=\"pe-7s-tools\"></i> Settings\n                                </a>\n                            </li>\n                            <li class=\"divider\"></li>\n                            <li>\n                                <a href=\"#\">\n                                    <i class=\"pe-7s-lock\"></i> Lock Screen\n                                </a>\n                            </li>\n                            <li>\n                                <a (click)=\"logout()\" class=\"text-danger\" style=\"cursor: pointer;\">\n                                    <i class=\"pe-7s-close-circle\"></i>\n                                    Log out\n                                </a>\n                            </li>\n                        </ul>\n                    </li>\n\n                </ul>\n            </div>\n        </div>\n    </div>\n</nav>\n"

/***/ }),

/***/ "./src/app/shared/navbar/navbar.component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/navbar/navbar.component.ts ***!
  \***************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.././sidebar/sidebar.component */ "./src/app/sidebar/sidebar.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var misc = {
    navbar_menu_visible: 0,
    active_collapse: true,
    disabled_collapse_init: 0,
};
var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(location, renderer, element, router, apiService) {
        this.renderer = renderer;
        this.element = element;
        this.router = router;
        this.apiService = apiService;
        this.user = [];
        this.location = location;
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        this.getUser();
        this.listTitles = _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_1__["ROUTES"].filter(function (listTitle) { return listTitle; });
        var navbar = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        if ($('body').hasClass('sidebar-mini')) {
            misc.sidebar_mini_active = true;
        }
        $('#minimizeSidebar').click(function () {
            var $btn = $(this);
            if (misc.sidebar_mini_active == true) {
                $('body').removeClass('sidebar-mini');
                misc.sidebar_mini_active = false;
            }
            else {
                setTimeout(function () {
                    $('body').addClass('sidebar-mini');
                    misc.sidebar_mini_active = true;
                }, 300);
            }
            // we simulate the window Resize so the charts will get updated in realtime.
            var simulateWindowResize = setInterval(function () {
                window.dispatchEvent(new Event('resize'));
            }, 180);
            // we stop the simulation of Window Resize after the animations are completed
            setTimeout(function () {
                clearInterval(simulateWindowResize);
            }, 1000);
        });
    };
    NavbarComponent.prototype.isMobileMenu = function () {
        if ($(window).width() < 991) {
            return false;
        }
        return true;
    };
    NavbarComponent.prototype.sidebarOpen = function () {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');
        this.sidebarVisible = true;
    };
    NavbarComponent.prototype.sidebarClose = function () {
        var body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    NavbarComponent.prototype.sidebarToggle = function () {
        // var toggleButton = this.toggleButton;
        // var body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible == false) {
            this.sidebarOpen();
        }
        else {
            this.sidebarClose();
        }
    };
    NavbarComponent.prototype.getTitle = function () {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(2);
        }
        for (var item = 0; item < this.listTitles.length; item++) {
            var parent = this.listTitles[item];
            if (parent.path === titlee) {
                return parent.title;
            }
            else if (parent.children) {
                var children_from_url = titlee.split("/")[2];
                for (var current = 0; current < parent.children.length; current++) {
                    if (parent.children[current].path === children_from_url) {
                        return parent.children[current].title;
                    }
                }
            }
        }
        return 'Dashboard';
    };
    NavbarComponent.prototype.getPath = function () {
        // console.log(this.location);
        return this.location.prepareExternalUrl(this.location.path());
    };
    NavbarComponent.prototype.logout = function () {
        localStorage.clear();
        this.router.navigate(["/pages/login"]);
    };
    NavbarComponent.prototype.getUser = function () {
        var _this = this;
        var asID = localStorage.getItem("businessID");
        var userID = localStorage.getItem("userID");
        this.apiService.getData(asID + "/" + userID, "users/").then(function (result) {
            _this.user = result[0];
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("navbar-cmp"),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "button", void 0);
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'navbar-cmp',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/shared/navbar/navbar.component.html")
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], app_providers_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/shared/navbar/navbar.module.ts":
/*!************************************************!*\
  !*** ./src/app/shared/navbar/navbar.module.ts ***!
  \************************************************/
/*! exports provided: NavbarModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarModule", function() { return NavbarModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _navbar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./navbar.component */ "./src/app/shared/navbar/navbar.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
            declarations: [_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"]],
            exports: [_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"]]
        })
    ], NavbarModule);
    return NavbarModule;
}());



/***/ }),

/***/ "./src/app/shared/pagesnavbar/pagesnavbar.component.html":
/*!***************************************************************!*\
  !*** ./src/app/shared/pagesnavbar/pagesnavbar.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav #pagesnavbar class=\"navbar navbar-primary navbar-transparent navbar-absolute\">\n    <div class=\"container\">\n        <div class=\"navbar-header\">\n            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navigation-example-2\" (click)=\"sidebarToggle()\">\n                <span class=\"sr-only\">Toggle navigation</span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n            </button>\n            <a class=\"navbar-brand\" routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">EF CRM</a>\n        </div>\n        <div class=\"collapse navbar-collapse\">\n            <ul class=\"nav navbar-nav navbar-right\">\n                <li>\n                    <a style=\"display: none;\" routerLinkActive=\"active\" [routerLink]=\"['/pages/login']\">\n                        <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>\n                        <p>Login</p>\n                    </a>\n                </li>\n                <li>\n                    <a style=\"display: none;\" routerLinkActive=\"active\" [routerLink]=\"['/pages/register']\">\n                        <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i>\n                        <p>Register</p>\n                    </a>\n                </li>\n                <li>\n                    <a style=\"display: none;\" routerLinkActive=\"active\" [routerLink]=\"['/pages/lock']\">\n                        <i class=\"fa fa-lock\" aria-hidden=\"true\"></i>\n                        <p>Lock Screen</p>\n                    </a>\n                </li>\n            </ul>\n        </div>\n    </div>\n</nav>\n"

/***/ }),

/***/ "./src/app/shared/pagesnavbar/pagesnavbar.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/shared/pagesnavbar/pagesnavbar.component.ts ***!
  \*************************************************************/
/*! exports provided: PagesnavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesnavbarComponent", function() { return PagesnavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PagesnavbarComponent = /** @class */ (function () {
    function PagesnavbarComponent(location, renderer, element) {
        this.renderer = renderer;
        this.element = element;
        this.location = location;
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }
    PagesnavbarComponent.prototype.ngOnInit = function () {
        var navbar = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        console.log(this.location.prepareExternalUrl(this.location.path()));
    };
    PagesnavbarComponent.prototype.sidebarOpen = function () {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');
        this.sidebarVisible = true;
    };
    PagesnavbarComponent.prototype.sidebarClose = function () {
        var body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    PagesnavbarComponent.prototype.sidebarToggle = function () {
        // var toggleButton = this.toggleButton;
        // var body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible == false) {
            this.sidebarOpen();
        }
        else {
            this.sidebarClose();
        }
    };
    PagesnavbarComponent.prototype.isLogin = function () {
        if (this.location.prepareExternalUrl(this.location.path()) === 'pages/login') {
            return true;
        }
        return false;
    };
    PagesnavbarComponent.prototype.isLock = function () {
        if (this.location.prepareExternalUrl(this.location.path()) === 'pages/lock') {
            return true;
        }
        return false;
    };
    PagesnavbarComponent.prototype.isRegister = function () {
        if (this.location.prepareExternalUrl(this.location.path()) === 'pages/register') {
            return true;
        }
        return false;
    };
    PagesnavbarComponent.prototype.getPath = function () {
        // console.log(this.location);
        return this.location.prepareExternalUrl(this.location.path());
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("pagesnavbar-cmp"),
        __metadata("design:type", Object)
    ], PagesnavbarComponent.prototype, "button", void 0);
    PagesnavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'pagesnavbar-cmp',
            template: __webpack_require__(/*! ./pagesnavbar.component.html */ "./src/app/shared/pagesnavbar/pagesnavbar.component.html")
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], PagesnavbarComponent);
    return PagesnavbarComponent;
}());



/***/ }),

/***/ "./src/app/shared/pagesnavbar/pagesnavbar.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/shared/pagesnavbar/pagesnavbar.module.ts ***!
  \**********************************************************/
/*! exports provided: PagesnavbarModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesnavbarModule", function() { return PagesnavbarModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _pagesnavbar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pagesnavbar.component */ "./src/app/shared/pagesnavbar/pagesnavbar.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PagesnavbarModule = /** @class */ (function () {
    function PagesnavbarModule() {
    }
    PagesnavbarModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
            ],
            declarations: [_pagesnavbar_component__WEBPACK_IMPORTED_MODULE_2__["PagesnavbarComponent"]],
            exports: [_pagesnavbar_component__WEBPACK_IMPORTED_MODULE_2__["PagesnavbarComponent"]]
        })
    ], PagesnavbarModule);
    return PagesnavbarModule;
}());



/***/ }),

/***/ "./src/app/sidebar/sidebar.component.html":
/*!************************************************!*\
  !*** ./src/app/sidebar/sidebar.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"logo\">\n\t<a href=\"http://www.eglencefabrikasi.com\" class=\"simple-text logo-mini\">\n\t\t<div class=\"logo-img\">\n\t\t\t<img src=\"../../assets/img/EFlogo.png\"/>\n\t\t</div>\n\t</a>\n\t<a href=\"http://www.eglencefabrikasi.com\" class=\"simple-text logo-normal\">\n\t\tEF CRM\n\t</a>\n</div>\n\n<div class=\"sidebar-wrapper\">\n    <div class=\"user\">\n\t\t<div class=\"info\">\n\t\t\t<a data-toggle=\"collapse\" href=\"#collapseSideBar\" class=\"collapsed\">\n\t\t\t\t<span>\n\t\t\t\t\t{{accountingSupplier}}\n\t\t\t\t\t<b class=\"caret\"></b>\n\t\t\t\t</span>\n\t\t\t</a>\n\t\t\t<div class=\"collapse\" id=\"collapseSideBar\">\n\t\t\t\t<ul class=\"nav\">\n\t\t\t\t\t<li *ngFor=\"let item of accountingSuppliers\">\n\t\t\t\t\t\t<a (click)=\"selectAS(item.ID)\">\n\t\t\t\t\t\t\t<span class=\"sidebar-mini\">MP</span>\n\t\t\t\t\t\t\t<span class=\"sidebar-normal\">{{item.supplier}}</span>\n\t\t\t\t\t\t</a>\n                    </li>\n                    <!-- <li>\n                        <a (click)=\"addNew()\">\n                            <span class=\"sidebar-mini\">A</span>\n                            <span class=\"sidebar-normal\">{{'add new business' |translate |titlecase}}</span>\n                        </a>\n                    </li> -->\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<div *ngIf=\"isNotMobileMenu()\">\n\t\t<form class=\"navbar-form navbar-left navbar-search-form\" role=\"search\">\n            <div class=\"input-group\">\n                <span class=\"input-group-addon\"><i class=\"fa fa-search\"></i></span>\n                <input class=\"form-control\" placeholder=\"Search...\" type=\"text\" value=\"\">\n            </div>\n        </form>\n\t\t<ul class=\"nav nav-mobile-menu\">\n            <li>\n               \n                <a routerLinkActive = \"active\" [routerLink]=\"['/charts']\">\n                    <i class=\"fa fa-line-chart\"></i>\n                    <p>Stats</p>\n                </a> \n            </li>\n\n            <li class=\"dropdown\">\n                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" aria-expanded=\"false\">\n                    <i class=\"fa fa-gavel\"></i>\n                    <p class=\"hidden-md hidden-lg\">\n                        Actions\n                        <b class=\"caret\"></b>\n                    </p>\n                </a>\n                <ul class=\"dropdown-menu\">\n                    <li><a href=\"#\">Create New Post</a></li>\n                    <li><a href=\"#\">Manage Something</a></li>\n                    <li><a href=\"#\">Do Nothing</a></li>\n                    <li><a href=\"#\">Submit to live</a></li>\n                    <li class=\"divider\"></li>\n                    <li><a href=\"#\">Another Action</a></li>\n                </ul>\n            </li>\n\n            <li class=\"dropdown\">\n                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" aria-expanded=\"false\">\n                    <i class=\"fa fa-bell-o\"></i>\n                    <span class=\"notification\">5</span>\n                    <p class=\"hidden-md hidden-lg\">\n                        Notifications\n                        <b class=\"caret\"></b>\n                    </p>\n                </a>\n                <ul class=\"dropdown-menu\">\n                    <li><a href=\"#\">Notification 1</a></li>\n                    <li><a href=\"#\">Notification 2</a></li>\n                    <li><a href=\"#\">Notification 3</a></li>\n                    <li><a href=\"#\">Notification 4</a></li>\n                    <li><a href=\"#\">Another notification</a></li>\n                </ul>\n            </li>\n\n            <li class=\"dropdown dropdown-with-icons\">\n                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" aria-expanded=\"false\">\n                    <i class=\"fa fa-list\"></i>\n                    <p class=\"hidden-md hidden-lg\">\n                        More\n                        <b class=\"caret\"></b>\n                    </p>\n                </a>\n                <ul class=\"dropdown-menu dropdown-with-icons\">\n                    <li>\n                        <a href=\"#\">\n                            <i class=\"pe-7s-mail\"></i> Messages\n                        </a>\n                    </li>\n                    <li>\n                        <a href=\"#\">\n                            <i class=\"pe-7s-help1\"></i> Help Center\n                        </a>\n                    </li>\n                    <li>\n                        <a href=\"#\">\n                            <i class=\"pe-7s-tools\"></i> Settings\n                        </a>\n                    </li>\n                    <li class=\"divider\"></li>\n                    <li>\n                        <a href=\"#\">\n                            <i class=\"pe-7s-lock\"></i> Lock Screen\n                        </a>\n                    </li>\n                    <li>\n                        <a class=\"text-danger\" href=\"#\">\n                            <i class=\"pe-7s-close-circle\"></i>\n                            Log out\n                        </a>\n                    </li>\n                </ul>\n            </li>\n\n        </ul>\n    </div>\n        <ul class=\"nav\">\n            <li routerLinkActive=\"active\" *ngFor=\"let menuitem of menuItems\">\n                <!--If is a single link-->\n                <a [routerLink]=\"[menuitem.path]\" *ngIf=\"menuitem.type === 'link'\">\n                    <i class=\"{{menuitem.icontype}}\"></i>\n                    <p>{{menuitem.title|translate|titlecase}}</p>\n                </a>\n                <!--If it have a submenu-->\n                <a data-toggle=\"collapse\" href=\"#{{menuitem.title}}\" *ngIf=\"menuitem.type === 'sub'\">\n                    <i class=\"{{menuitem.icontype}}\"></i>\n                    <p>{{menuitem.title|translate|titlecase}}<b class=\"caret\"></b></p>\n                </a>\n\n                <!--Display the submenu items-->\n                <div id=\"{{menuitem.title}}\" class=\"collapse\" *ngIf=\"menuitem.type === 'sub'\">\n                    <ul class=\"nav\">\n                        <li routerLinkActive=\"active\" *ngFor=\"let childitem of menuitem.children\">\n                            <a [routerLink]=\"[menuitem.path, childitem.path]\">\n                                <span class=\"sidebar-mini\">{{childitem.ab|translate|titlecase}}</span>\n                                <span class=\"sidebar-normal\">{{childitem.title|translate|titlecase}}</span>\n                            </a>\n                        </li>\n                    </ul>\n                </div>\n            </li>\n        </ul>\n\n</div>\n"

/***/ }),

/***/ "./src/app/sidebar/sidebar.component.ts":
/*!**********************************************!*\
  !*** ./src/app/sidebar/sidebar.component.ts ***!
  \**********************************************/
/*! exports provided: ROUTES, SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/providers/api.service */ "./src/app/providers/api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//Menu Items
var ROUTES = [];
/*[{
    path: '/dashboard',
    title: 'dashboard',
    type: 'link',
    icontype: 'fa fa-home'
}, {
    path: '/sales',
    title: 'sales',
    type: 'sub',
    icontype: 'fa fa-bell-o',
    children: [
        { path: 'invoices', title: 'invoices', ab: 'I' },
        { path: 'customers', title: 'customers', ab: 'C' },
        { path: 'products', title: 'products/services', ab: 'P' }
    ]
}, {
    path: '/costs',
    title: 'purchases',
    type: 'sub',
    icontype: 'fa fa-credit-card',
    children: [
        { path: 'invoices', title: 'bills', ab: 'O' },
        { path: 'expenses', title: 'receipts/expenses', ab: 'R' },
        { path: 'suppliers', title: 'vendors', ab: 'V' },
        { path: 'products', title: 'products/services', ab: 'P' }
    ]
}, {
    path: '/finance',
    title: 'finance',
    type: 'sub',
    icontype: 'fa fa-calculator',
    children: [
        { path: 'cash', title: 'cashaccount', ab: 'C' },
        { path: 'bankAccounts', title: 'bankAccounts', ab: 'A' },
        { path: 'banks', title: 'banks', ab: 'B' },
        { path: 'transactions', title: 'transactions', ab: 'T' }
    ]
},
{
    path: '/hr',
    title: 'humanresources',
    type: 'sub',
    icontype: 'fa fa-archive',
    children: [
        { path: 'team', title: 'team', ab: 'T' },
        { path: 'payroll', title: 'payroll', ab: 'P' }
    ]
},
{
    path: '/tax',
    title: 'tax',
    type: 'sub',
    icontype: 'fa fa-thumbs-o-down',
    children: [
        { path: 'revenue', title: 'revenue', ab: 'R' },
        { path: 'payroll', title: 'payrolltax', ab: 'P' },
        { path: 'vat', title: 'vat', ab: 'V' }
    ]
}
];
*/
var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(apiService, router) {
        this.apiService = apiService;
        this.router = router;
        this.accountingSuppliers = [];
        this.menuResult = [];
    }
    SidebarComponent.prototype.isNotMobileMenu = function () {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
    SidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userID = localStorage.getItem("userID");
        this.apiService.getData(userID, "menus/").then(function (result) {
            var isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
            _this.menuResult = result;
            //ROUTES.push(this.menuResult);
            console.log(_this.menuResult, "menus result");
            for (var i = 0; i < _this.menuResult.length; i++) {
                _this.menuResult[i].children = JSON.parse(_this.menuResult[i].children.replace(/\\/g, ""));
                ROUTES.push(_this.menuResult[i]);
            }
            console.log(ROUTES, "route");
            _this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
            isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
            if (isWindows) {
                // if we are on windows OS we activate the perfectScrollbar function
                $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();
                $('html').addClass('perfect-scrollbar-on');
            }
            else {
                $('html').addClass('perfect-scrollbar-off');
            }
            _this.getAccountingSuppliers();
        });
    };
    SidebarComponent.prototype.ngAfterViewInit = function () {
        var $sidebarParent = $('.sidebar .nav > li.active .collapse li.active > a').parent().parent().parent();
        var collapseId = $sidebarParent.siblings('a').attr("href");
        $(collapseId).collapse("show");
    };
    SidebarComponent.prototype.getAccountingSuppliers = function () {
        var _this = this;
        var ownerID = localStorage.getItem("ownerID");
        var userID = localStorage.getItem("userID");
        this.apiService.getData(ownerID + "/" + userID, "userAccountingSuppliers/").then(function (result) {
            _this.accountingSuppliers = result;
            var pf = _this.accountingSuppliers.filter(function (x) { return x.uaID != null; });
            //console.log(pf,"useraccountsuppliers");
            _this.accountingSuppliers = pf;
            if (localStorage.getItem("businessID")) {
                _this.accountingSupplierID = localStorage.getItem("businessID");
            }
            else {
                localStorage.setItem("businessID", result[0].ID);
                _this.accountingSupplierID = result[0].ID;
            }
            console.log(_this.accountingSupplierID, "SupplıerID");
            _this.getSelectedAccountingSuppliers();
        });
    };
    SidebarComponent.prototype.getSelectedAccountingSuppliers = function () {
        var _this = this;
        this.apiService.getData(this.accountingSupplierID, "accountingSuppliers/").then(function (result) {
            _this.accountingSupplier = result[0].supplier;
        });
    };
    SidebarComponent.prototype.selectAS = function (accountingSupplierID) {
        localStorage.setItem("businessID", accountingSupplierID);
        console.log(localStorage.getItem("businessID"));
        console.log(this.router.url);
        document.location.reload();
    };
    SidebarComponent.prototype.addNew = function () {
        console.log("Add New");
        this.router.navigateByUrl("admin/company/0");
    };
    SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'sidebar-cmp',
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/sidebar/sidebar.component.html"),
        }),
        __metadata("design:paramtypes", [app_providers_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/sidebar/sidebar.module.ts":
/*!*******************************************!*\
  !*** ./src/app/sidebar/sidebar.module.ts ***!
  \*******************************************/
/*! exports provided: SidebarModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarModule", function() { return SidebarModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _sidebar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sidebar.component */ "./src/app/sidebar/sidebar.component.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var SidebarModule = /** @class */ (function () {
    function SidebarModule() {
    }
    SidebarModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateModule"]],
            declarations: [_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["SidebarComponent"]],
            exports: [_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["SidebarComponent"]]
        })
    ], SidebarModule);
    return SidebarModule;
}());



/***/ }),

/***/ "./src/app/tax/payroll/payroll.component.html":
/*!****************************************************!*\
  !*** ./src/app/tax/payroll/payroll.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  payroll works!\n</p>\n"

/***/ }),

/***/ "./src/app/tax/payroll/payroll.component.scss":
/*!****************************************************!*\
  !*** ./src/app/tax/payroll/payroll.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RheC9wYXlyb2xsL3BheXJvbGwuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/tax/payroll/payroll.component.ts":
/*!**************************************************!*\
  !*** ./src/app/tax/payroll/payroll.component.ts ***!
  \**************************************************/
/*! exports provided: PayrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayrollComponent", function() { return PayrollComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PayrollComponent = /** @class */ (function () {
    function PayrollComponent() {
    }
    PayrollComponent.prototype.ngOnInit = function () {
    };
    PayrollComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-payroll',
            template: __webpack_require__(/*! ./payroll.component.html */ "./src/app/tax/payroll/payroll.component.html"),
            styles: [__webpack_require__(/*! ./payroll.component.scss */ "./src/app/tax/payroll/payroll.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PayrollComponent);
    return PayrollComponent;
}());



/***/ }),

/***/ "./src/app/tax/revenue/revenue.component.html":
/*!****************************************************!*\
  !*** ./src/app/tax/revenue/revenue.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  revenue works!\n</p>\n"

/***/ }),

/***/ "./src/app/tax/revenue/revenue.component.scss":
/*!****************************************************!*\
  !*** ./src/app/tax/revenue/revenue.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RheC9yZXZlbnVlL3JldmVudWUuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/tax/revenue/revenue.component.ts":
/*!**************************************************!*\
  !*** ./src/app/tax/revenue/revenue.component.ts ***!
  \**************************************************/
/*! exports provided: RevenueComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RevenueComponent", function() { return RevenueComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RevenueComponent = /** @class */ (function () {
    function RevenueComponent() {
    }
    RevenueComponent.prototype.ngOnInit = function () {
    };
    RevenueComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-revenue',
            template: __webpack_require__(/*! ./revenue.component.html */ "./src/app/tax/revenue/revenue.component.html"),
            styles: [__webpack_require__(/*! ./revenue.component.scss */ "./src/app/tax/revenue/revenue.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], RevenueComponent);
    return RevenueComponent;
}());



/***/ }),

/***/ "./src/app/tax/vat/vat.component.html":
/*!********************************************!*\
  !*** ./src/app/tax/vat/vat.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  vat works!\n</p>\n"

/***/ }),

/***/ "./src/app/tax/vat/vat.component.scss":
/*!********************************************!*\
  !*** ./src/app/tax/vat/vat.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RheC92YXQvdmF0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/tax/vat/vat.component.ts":
/*!******************************************!*\
  !*** ./src/app/tax/vat/vat.component.ts ***!
  \******************************************/
/*! exports provided: VatComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VatComponent", function() { return VatComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var VatComponent = /** @class */ (function () {
    function VatComponent() {
    }
    VatComponent.prototype.ngOnInit = function () {
    };
    VatComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-vat',
            template: __webpack_require__(/*! ./vat.component.html */ "./src/app/tax/vat/vat.component.html"),
            styles: [__webpack_require__(/*! ./vat.component.scss */ "./src/app/tax/vat/vat.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], VatComponent);
    return VatComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"]);


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/developer202/Documents/ionicprojects/efcrm/src/main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/*!************************!*\
  !*** stream (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map