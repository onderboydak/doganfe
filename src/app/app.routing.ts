import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';


export const AppRoutes: Routes = [{
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: '',
        component: AdminLayoutComponent,
        children: [{
            path: '',
            loadChildren: './dashboard/dashboard.module#DashboardModule'
        },
        {
            path: 'sales',
            loadChildren: './sales/sales.module#SalesModule'

        },{
            path: 'costs',
            loadChildren: './costs/costs.module#CostsModule'
        },{
            path: 'finance',
            loadChildren: './finance/finance.module#FinanceModule'
        },{
            path: 'services',
            loadChildren: './services/services.module#ServicesModule'
        },{
            path: 'admin',
            loadChildren: './admin/admin.module#AdminModule'
        },{
            path: 'hr',
            loadChildren: './hr/hr.module#HrModule'
        }]
        },{
            path: '',
            component: AuthLayoutComponent,
            children: [{
                path: 'pages',
                loadChildren: './pages/pages.module#PagesModule'
            }]
        }
];
