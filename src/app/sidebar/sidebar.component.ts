import { Component, OnInit, AfterViewInit, AfterViewChecked, AfterContentInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { Router } from '@angular/router';

declare var $: any;
//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    // icon: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [];


@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent {
    public menuItems: any = [];
    accountingSuppliers: any = [];
    accountingSupplierID: any;
    accountingSupplier: any;
    menuResult:any=[];
    balance:string="";
    //ROUTES:RouteInfo[]=[];


    constructor(public apiService: ApiService, public router: Router) { }

    isNotMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    }

    ngOnInit() {
        let userID = localStorage.getItem("userID");
        this.apiService.getData(userID, "menus/").then((result) => {
            var isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
            this.menuResult=result;
            //ROUTES.push(this.menuResult);

           console.log( this.menuResult,"menus result");

           for(var i = 0;i<this.menuResult.length;i++) { 
               this.menuResult[i].children=JSON.parse(this.menuResult[i].children.replace(/\\/g, ""));
                ROUTES.push(this.menuResult[i]);
           }
          
            console.log(ROUTES,"route");
            this.menuItems = ROUTES.filter(menuItem => menuItem);

            isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;

            if (isWindows) {
                // if we are on windows OS we activate the perfectScrollbar function
                $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();
                $('html').addClass('perfect-scrollbar-on');
            } else {
                $('html').addClass('perfect-scrollbar-off');
            }

            this.getAccountingSuppliers();
           
        });
    }
    ngAfterViewInit() {
        var $sidebarParent = $('.sidebar .nav > li.active .collapse li.active > a').parent().parent().parent();

        var collapseId = $sidebarParent.siblings('a').attr("href");

        $(collapseId).collapse("show");
    }

    
    getAccountingSuppliers() {
        let ownerID = localStorage.getItem("ownerID");
        let userID=localStorage.getItem("userID");
        this.apiService.getData(ownerID+"/"+userID, "userAccountingSuppliers/").then((result) => {
            this.accountingSuppliers = result;
            let pf:any = this.accountingSuppliers.filter(x => x.uaID != null);
            //console.log(pf,"useraccountsuppliers");
            this.accountingSuppliers=pf;

            if (localStorage.getItem("businessID")) {
                this.accountingSupplierID = localStorage.getItem("businessID");
            } else {
                localStorage.setItem("businessID", result[0].ID);
                this.accountingSupplierID = result[0].ID;

            }
            console.log(this.accountingSupplierID, "SupplıerID");
            this.getSelectedAccountingSuppliers();
        });
    }

    getSelectedAccountingSuppliers() {
        this.apiService.getData(this.accountingSupplierID, "accountingSuppliers/").then((result) => {
            this.accountingSupplier = result[0].supplier;
            this.balance=result[0].balance;
        });
    }

    selectAS(accountingSupplierID: any) {
        localStorage.setItem("businessID", accountingSupplierID);
        //console.log(localStorage.getItem("businessID"));
        //console.log(this.router.url);
        document.location.reload();

    }

    addNew() {
        console.log("Add New");
        this.router.navigateByUrl("admin/company/0");
    }
}
