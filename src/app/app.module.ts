import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent }   from './app.component';
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { SidebarModule } from './sidebar/sidebar.module';
import { FixedPluginModule } from './shared/fixedplugin/fixedplugin.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { PagesnavbarModule} from './shared/pagesnavbar/pagesnavbar.module';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AppRoutes } from './app.routing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ApiService } from './providers/api.service';

import {NgbModule, NgbTypeaheadModule, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { InvoicePaymentComponent } from './sales/invoice-payment/invoice-payment.component';
import { DatePipe } from '@angular/common';
import { RevenueComponent } from './tax/revenue/revenue.component';
import { PayrollComponent } from './tax/payroll/payroll.component';
import { VatComponent } from './tax/vat/vat.component';
import { AuthService } from './auth/auth.service';
import { AuthGuardService } from './auth/auth-guard.service';
import { NoteComponent } from './finance/note/note.component';
import { InvoiceIDComponent } from './finance/invoice-id/invoice-id.component';
import { ApproveModalComponent } from './sales/approve-modal/approve-modal.component';
import { UploadComponent } from './pages/upload/upload.component';
import { AccountingComponent } from './costs/accounting/accounting.component';
import { ExcelService } from './providers/excel.service';
import { BalanceDetailsComponent } from './finance/balance-details/balance-details.component';
import { TermComponent } from './finance/babs/term/term.component';
import { NgxHmCarouselModule } from 'ngx-hm-carousel';
import { BrowserModule } from '@angular/platform-browser';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

export function HttpLoaderFactory(httpClient: HttpClient) {
    return new TranslateHttpLoader(httpClient, "../assets/i18n/", ".json");
  }

@NgModule({
    imports:      [
        BrowserAnimationsModule,
        FormsModule,
        RouterModule.forRoot(AppRoutes,{useHash:true}),
        HttpModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        FixedPluginModule,
        PagesnavbarModule,
        HttpClientModule,
        NgxHmCarouselModule,
        NgbModule,
        BrowserModule,
        NgbTypeaheadModule,
        ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
          }) 
    ],
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        AuthLayoutComponent,
        PagenotfoundComponent,
        InvoicePaymentComponent,
        RevenueComponent,
        PayrollComponent,
        NoteComponent,
        InvoiceIDComponent,
        ApproveModalComponent,
        VatComponent,
        UploadComponent,
        AccountingComponent,
        BalanceDetailsComponent,
        TermComponent
       
            ],
    providers: [ApiService,HttpClient ,NgbActiveModal,DatePipe,AuthService,AuthGuardService,ExcelService],
    bootstrap:    [ AppComponent ],
    entryComponents: [
        InvoicePaymentComponent,NoteComponent,InvoiceIDComponent,ApproveModalComponent,UploadComponent,AccountingComponent,BalanceDetailsComponent,TermComponent
    ],
    schemas:[NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
