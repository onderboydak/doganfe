import { Component } from '@angular/core';
import { TableData } from '../lbd/lbd-table/lbd-table.component';
import { LegendItem, ChartType } from '../lbd/lbd-chart/lbd-chart.component';
import { Task } from '../lbd/lbd-task-list/lbd-task-list.component';

import * as Chartist from 'chartist';
import { ApiService } from 'app/providers/api.service';
import { TranslateService } from '@ngx-translate/core';

declare var $:any;

@Component({
  selector: 'dashboard-cmp',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent {
    public tableData: TableData;
    public emailChartType: ChartType;
    public emailChartData: any;
    public emailChartLegendItems: LegendItem[];

    public hoursChartType: ChartType;
    public hoursChartData: any;
    public hoursChartOptions: any;
    public hoursChartResponsive: any[];
    public hoursChartLegendItems: LegendItem[];

    public activityChartType: ChartType;
    public activityChartType2: ChartType;
    public activityChartType3: ChartType;
    public activityChartData: any;
    public activityChartData2: any;
    public activityChartData3: any;
    public activityChartOptions: any;
    public activityChartResponsive: any[];
    public activityChartLegendItems: LegendItem[];
    public activityChartLegendItems3: LegendItem[];

    public tasks: Task[];
    invoiceDueDates:any=[];
    invoiceDueDates3:any=[];
    businessID:any;
    series:any=[];
    series2:any=[];
    series3:any=[];

    currentIndex = 0;
    speed = 5000;
    infinite = true;
    direction = 'right';
    directionToggle = true;
    autoplay = true;
   /*
    avatars = '12345'.split('').map((x, i) => {
      const num = i;
      return {
        url: `https://picsum.photos/600/400/?${num}`,
        title: `${num}`
      };
    });
    */
   avatars:any=[{"url":"","title":""}];

    constructor(public apiService: ApiService,private translate:TranslateService) {
      this.businessID = localStorage.getItem("businessID");
      console.log(this.avatars,"avatarsmaın");
    }

    click(i:any) {
      alert(`${i}`);
    }

   

    getCustomerInvoiceDueDates() {
      this.apiService.getData(this.businessID + "/0", "invoiceCustomerDueDates/").then((result) => {
          this.invoiceDueDates3 = result;
         
             
              let vlM1 = [ this.invoiceDueDates3[0].M3*1,this.invoiceDueDates3[0].M2*1,this.invoiceDueDates3[0].M1*1,this.invoiceDueDates3[0].M0*1];
              let vlM0 = [ this.invoiceDueDates3[1].M3*1,this.invoiceDueDates3[1].M2*1,this.invoiceDueDates3[1].M1*1,this.invoiceDueDates3[1].M0*1];
              this.series3.push(vlM1);
              this.series3.push(vlM0);

               console.log(this.series3,"series 3");
              
      });
      
  }

    getInvoiceDueDates() {
        this.apiService.getData(this.businessID + "/1", "invoiceDueDates/").then((result) => {
            this.invoiceDueDates = result;
           
                let vlTRY = [ this.invoiceDueDates[0].TRY*1,this.invoiceDueDates[1].TRY*1,this.invoiceDueDates[2].TRY*1,this.invoiceDueDates[3].TRY*1];
                let vlUSD = [ this.invoiceDueDates[0].USD*1,this.invoiceDueDates[1].USD*1,this.invoiceDueDates[2].USD*1,this.invoiceDueDates[3].USD*1];
                let vlEUR = [ this.invoiceDueDates[0].EUR*1,this.invoiceDueDates[1].EUR*1,this.invoiceDueDates[2].EUR*1,this.invoiceDueDates[3].EUR*1];
                let vlGBP = [ this.invoiceDueDates[0].GBP*1,this.invoiceDueDates[1].GBP*1,this.invoiceDueDates[2].GBP*1,this.invoiceDueDates[3].GBP*1];
                this.series.push(vlTRY);
                this.series.push(vlUSD);
                this.series.push(vlEUR);
                this.series.push(vlGBP);

                 console.log(this.series,"series");
                
        });
        
    }
    getInvoiceDueDates2() {
      this.apiService.getData(this.businessID + "/2", "invoiceDueDates/").then((result) => {
          this.invoiceDueDates = result;
         
              let vlTRY = [ this.invoiceDueDates[0].TRY*1,this.invoiceDueDates[1].TRY*1,this.invoiceDueDates[2].TRY*1,this.invoiceDueDates[3].TRY*1];
              let vlUSD = [ this.invoiceDueDates[0].USD*1,this.invoiceDueDates[1].USD*1,this.invoiceDueDates[2].USD*1,this.invoiceDueDates[3].USD*1];
              let vlEUR = [ this.invoiceDueDates[0].EUR*1,this.invoiceDueDates[1].EUR*1,this.invoiceDueDates[2].EUR*1,this.invoiceDueDates[3].EUR*1];
              let vlGBP = [ this.invoiceDueDates[0].GBP*1,this.invoiceDueDates[1].GBP*1,this.invoiceDueDates[2].GBP*1,this.invoiceDueDates[3].GBP*1];
              this.series2.push(vlTRY);
              this.series2.push(vlUSD);
              this.series2.push(vlEUR);
              this.series2.push(vlGBP);

               console.log(this.series2,"series2");

               
               
      });
      
  }

    getTranslate(txt:string) {
      return this.translate.instant(txt);
    }

    getBanners() {
      this.apiService.getData("", "showBanners").then((result) => {
        this.avatars = result;
        console.log(this.avatars,"avatars");
      });
    }

    ngOnInit(){
     
      this.getInvoiceDueDates();
      this.getInvoiceDueDates2();
      this.getCustomerInvoiceDueDates();
      this.getBanners();

      this.resolveAfter2Seconds(1).then(value => {

      //this.getInvoiceDueDates2();
         this.emailChartType = ChartType.Pie;
         this.emailChartData = {
           labels: ['62%', '32%', '6%'],
           series: [62, 32, 6]
         };
         this.emailChartLegendItems = [
           { title: 'Open', imageClass: 'fa fa-circle text-info' },
           { title: 'Bounce', imageClass: 'fa fa-circle text-danger' },
           { title: 'Unsubscribe', imageClass: 'fa fa-circle text-warning' }
         ];

         this.hoursChartType = ChartType.Line;
         this.hoursChartData = {
           labels: ['9:00AM', '12:00AM', '3:00PM', '6:00PM', '9:00PM', '12:00PM', '3:00AM', '6:00AM'],
           series: [
             [287, 385, 490, 492, 554, 586, 698, 695, 752, 788, 846, 944],
             [67, 152, 143, 240, 287, 335, 435, 437, 539, 542, 544, 647],
             [23, 113, 67, 108, 190, 239, 307, 308, 439, 410, 410, 509]
           ]
         };
         this.hoursChartOptions = {
           low: 0,
           high: 800,
           showArea: false,
           height: '245px',
           axisX: {
             showGrid: false,
           },
           lineSmooth: Chartist.Interpolation.simple({
             divisor: 3
           }),
           showLine: true,
           showPoint: true,
         };
         this.hoursChartResponsive = [
           ['screen and (max-width: 640px)', {
             axisX: {
               labelInterpolationFnc: function (value) {
                 return value[0];
               }
             }
           }]
         ];
         this.hoursChartLegendItems = [
           { title: 'Open', imageClass: 'fa fa-circle text-info' },
           { title: 'Click', imageClass: 'fa fa-circle text-danger' },
           { title: 'Click Second Time', imageClass: 'fa fa-circle text-warning' }
         ];
        
        // Invoices Graphs - 1 

        this.activityChartType = ChartType.Bar;
        this.activityChartData = {
          labels: ['Upcoming', '1-30 days', '31-60 days', '61-90 days', '90 days'],
          series: this.series
        };
        this.activityChartOptions = {
          seriesBarDistance: 10,
          axisX: {
            showGrid: true
          },
          height: '240px'
        };
        this.activityChartResponsive = [
          ['screen and (max-width: 640px)', {
             seriesBarDistance: 10,
            axisX: {
              labelInterpolationFnc: function (value) {
                return value[0];
              }
            }
          }]
        ];
        this.activityChartLegendItems = [
          { title: 'TRY', imageClass: 'fa fa-circle text-info' },
          { title: 'USD', imageClass: 'fa fa-circle text-danger' },
          { title: 'EUR', imageClass: 'fa fa-circle text-warning' },
          { title: 'GBP', imageClass: 'fa fa-circle text-success' }
        ];

         // Invoices Graphs - 2

        this.activityChartType2 = ChartType.Bar;
        this.activityChartData2 = {
          labels: ['Upcoming', '1-30 days', '31-60 days', '61-90 days', '90 days'],
          series: this.series2
        };
        
         // Invoices Graphs - 3
         this.activityChartLegendItems3 = [
          { title: 'Bills', imageClass: 'fa fa-circle text-info' },
          { title: 'İnvoices', imageClass: 'fa fa-circle text-danger' }
        ];

         this.activityChartType3 = ChartType.Bar;
         this.activityChartData3 = {
           labels: ['3Months', '2Months', 'LastMonth', 'Cumulative'],
           series: this.series3
         };

        console.log("graph completed");

         this.tasks = [
           { title: 'Invoice EFD2019000000001 is due today', checked: false, check_number:'checkbox1' },
           { title: 'Lines From Great Russian Literature? Or E-mails From My Boss?', checked: true, check_number:'checkbox2'  },
           {
             title: 'Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit',
             checked: true, check_number:'checkbox3'
           },
           { title: 'Create 4 Invisible User Experiences you Never Knew About', checked: false, check_number:'checkbox4'  },
           { title: 'Read \'Following makes Medium better\'', checked: false, check_number:'checkbox5'  },
           { title: 'Unfollow 5 enemies from twitter', checked: false, check_number:'checkbox6'  },
         ];
        });
    }

    resolveAfter2Seconds(x) {
      return new Promise(resolve => {
        setTimeout(() => {
          resolve(x);
        }, 600);
      });
    }
}
