import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TaxRoutes } from './tax.routing';

import { TranslateModule } from '@ngx-translate/core';

import { PayrollComponent } from './payroll/payroll.component';
import { RevenueComponent } from './revenue/revenue.component';
import { VatComponent } from './vat/vat.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(TaxRoutes),
        FormsModule,
        ReactiveFormsModule,
        TranslateModule
    ],
    declarations: [
        PayrollComponent,
        RevenueComponent,
        VatComponent
    ]
})

export class TaxModule {}
