import { Routes } from '@angular/router';

import { PayrollComponent } from './payroll/payroll.component';
import { RevenueComponent } from './revenue/revenue.component';
import { VatComponent } from './vat/vat.component';
import { AuthGuardService as AuthGuard } from '../auth/auth-guard.service';


export const TaxRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'team',
            component: RevenueComponent,
            canActivate: [AuthGuard]
    }]},   
    
    {
        path: '',
        children: [{
            path: 'vat',
            component: VatComponent,
            canActivate: [AuthGuard]
        }]}, 
    
    {
        path: '',
        children: [{
            path: 'payroll',
            component: PayrollComponent,
            canActivate: [AuthGuard]
    }]}
];


