import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SalesRoutes } from './sales.routing';

import { CustomersComponent } from './customers/customers.component';
import { CustomerComponent } from './customer/customer.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { InvoicesOutComponent } from './invoices-out/invoices-out.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { TranslateModule } from '@ngx-translate/core';
import { LbdModule } from 'app/lbd/lbd.module';
import { ArchieveComponent } from './archieve/archieve.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(SalesRoutes),
        FormsModule,
        CurrencyMaskModule,
        ReactiveFormsModule,
        TranslateModule,
        LbdModule
    ],
    declarations: [
        CustomersComponent,
        CustomerComponent,
        InvoiceComponent,
        InvoicesOutComponent,
        ProductsComponent,
        ProductComponent,
        ArchieveComponent
    ]
})

export class SalesModule {}
