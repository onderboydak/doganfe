import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-approve-modal',
  templateUrl: './approve-modal.component.html',
  styleUrls: ['./approve-modal.component.scss']
})
export class ApproveModalComponent implements OnInit {
  businessID: any;
  invoice: any = [];
  @Input() public invoiceID: any;
  constructor(public apiService: ApiService, public activeModal: NgbActiveModal) {
    this.businessID = localStorage.getItem("businessID");
   

  }

  ngOnInit() {
    console.log(this.invoiceID,"inv id");
    this.apiService.getData('0/' + this.businessID + '/' + this.invoiceID, "invoices/").then((result) => {
      this.invoice = result[0];
    });
  }
  clickCancel() {
    this.activeModal.close();
  }

  approveInvoice(approve: any) {
    // 3 - Approve  2-Reject
    let st =  this.invoice.statusID;
    if (approve==3 && this.invoice.statusID==0) {
      this.invoice.statusID=1;
    }
    else if (approve==3 && this.invoice.statusID==1) {
      if (this.invoice.invoiceType==1) {
        this.invoice.statusID=3;
      } else {
        this.invoice.statusID=1000;
      }
    } else {
      this.invoice.statusID = approve;
    }
   
    this.apiService.postData(this.invoiceID, "/invoice/save/", JSON.stringify(this.invoice)).then((result) => {
      let res: any = [];
      if (approve == 3) {
        if (this.invoice.invoiceType==1) {
          res["statusID"] = 3;
          res["status"] = "Approved";
        } else {
          res["statusID"] = 1000;
          res["status"] = "Completed";
        }
      
      } else if (approve == 2) {
        res["statusID"]  = 2;
        res["status"]  = "Rejected";
      } else {
        res["statusID"]  = 0;
        res["status"]  = "";
      }
      this.activeModal.close(res);
    });
  }

}
