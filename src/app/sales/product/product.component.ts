import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { Router, ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})

export class ProductComponent implements OnInit {
  product:any=[];
  productID:any;
  businessID:any;
  isVendor:any;
  chartOfAccounts :any=[];

  @ViewChild('accountBuy') accountBuy: ElementRef;
  @ViewChild('accountSell') accountSell: ElementRef;


  constructor(public apiService:ApiService,public router:Router,private activeRoute: ActivatedRoute) { 
    const routeParams = this.activeRoute.snapshot.params;
    this.productID = routeParams.id;

    this.activeRoute.queryParams
    .subscribe(params => {
      // Defaults to 0 if no query param provided.
      this.isVendor = +params['isVendor'] || 0;
    });


    console.log(this.productID,"productID");
    this.businessID=localStorage.getItem("businessID");
    this.getChartOfAccounts();
    this.getProduct(this.businessID+"/"+this.isVendor+"/"+this.productID);
    
  }

  ngOnInit() {
    $(".selectpicker").selectpicker();
  }

  getProduct(productID: any) {
    if (this.productID>0) {
      this.apiService.getData(productID, "products/").then((result) => {
        this.product = result[0];
        console.log(this.product,"product");
        this.spInit();
      });
    } else {
      this.product={ "ID": 0, "asID": this.businessID, "product": "", "price": 0, "description": "", "account": "", "taxRate": 0, "isVendor": this.isVendor,"currency": 0 };
      console.log(this.product);
      
    }
  }

  getChartOfAccounts() {
    this.apiService.getData(this.businessID+"/600,601", "chartOfAccounts/").then((result) => {
      this.chartOfAccounts = result;
      console.log(this.chartOfAccounts,"chartofaccounts");
    });
  }

  saveProduct() {
    this.apiService.postData(this.product.ID, "product/save/", JSON.stringify(this.product)).then((result) => {
      console.log(result);
      if (this.isVendor==1) {
        this.router.navigateByUrl("/sales/products");
      } else {
        this.router.navigateByUrl("/costs/products");
      }
    });
  }

  deleteProduct() {
    this.apiService.deleteData(this.product.ID, "product/delete/").then((result) => {
      console.log(result);
      if (this.isVendor==1) {
        this.router.navigateByUrl("/sales/products");
      } else {
        this.router.navigateByUrl("/costs/products");
      }
    });
  }

  clickCancel () {
    if (this.isVendor==1) {
      this.router.navigateByUrl("/sales/products");
    } else {
      this.router.navigateByUrl("/costs/products");
    }
  }

  spInit() {
    console.log("Init Select");
    $(".selectisVendor").selectpicker('val', this.product.isVendor);
    $(".selecttaxRate").selectpicker('val', this.product.taxRate);
    $(".selectCurrency").selectpicker('val', this.product.currency);
    $(".selectAccountSell").selectpicker('val', this.product.accountSell);
    $(".selectAccountSell").selectpicker('refresh');
  }

  clearValues() {
    console.log(this.product.isVendor);
    if (this.product.isVendor==2) {
      this.accountBuy.nativeElement.value="";
    } 
    if (this.product.isVendor==1) {
      this.accountSell.nativeElement.value="";
    }
  }
}
