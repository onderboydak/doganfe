import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';


declare var $: any;

declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows: [];
}


@Component({
    selector: 'app-customers',
    templateUrl: './customers.component.html',
    styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {

    public dataTable: DataTable;
    public customers: any = [];
    businessID: any;

    constructor(public apiService: ApiService) {
        /*
          this.dataTable = {
              headerRow: ['#', 'Name', 'Country', 'Type'],
              footerRow: ['#', 'Name', 'Country', 'Type'],
              dataRows: []
          };
      */

    }

    ngOnInit() {
        this.businessID = localStorage.getItem("businessID");
        this.getCustomers(this.businessID + "/1/0");
    }

    getCustomers(customerID: any) {
        this.apiService.getData(customerID, "customers/").then((result) => {
            this.customers = result;
            console.log("Customers", this.customers);

            this.dataTable = {
                headerRow: ['#', 'Name', 'Country', 'Type'],
                footerRow: ['#', 'Name', 'Country', 'Type'],
                dataRows: this.customers
            };

            console.log(this.dataTable);
        });

    }
    ngAfterViewInit() {

        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }

        });


    }
}
