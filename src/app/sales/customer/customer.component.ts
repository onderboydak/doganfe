import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {
  customerID: any;
  customer: any = [];
  businessID:any;
  isVendor:any;
  customerInvoices:any=[];
  isVendor2:boolean=false;
  banks:any=[];

  customerForm: FormGroup;
  constructor(public apiService: ApiService, private activeRoute: ActivatedRoute,public router: Router,private fb: FormBuilder) {
    const routeParams = this.activeRoute.snapshot.params;
    this.customerID = routeParams.id;
    this.activeRoute.queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.isVendor = +params['isVendor'] || 0;
      });

      

    console.log(this.customerID,"customerID");
    this.businessID=localStorage.getItem("businessID");
    this.customer = {"ID": 0,"address": "","buildingNumber": "","city": "","commerceAuthorityNo": "","isVendor":this.isVendor,"asID":this.businessID,
                  "country": "","customer": "","customerContact": "","customerContactEmail": "","customerShortName": "",
                  "customerType": 1,"district": "","dueDateOptions": null,"email": "","phone": "","vatAuthority": "","vatNumber": "","pbox":"","bankID":0,"IBAN":""};
    
    // form validation
    this.customerForm = this.fb.group({
      customerShortName: ['', Validators.required],
      customer:['', Validators.required],
      dueDateOptions:['', Validators.required],
      vatAuthority:['', Validators.required],
      vatNumber:['', Validators.required],
      commerceAuthorityNo:[],
      buildingNumber:[],
      address:[],
      pbox:[],
      district:['', Validators.required],
      city:['', Validators.required],
      country:['', Validators.required],
      email:[],
      phone:[],
      bankID:[],
      IBAN:[],
      customerContact:[],
      customerContactEmail:['', Validators.required]
    });
                 
  }

  get f() {
    return this.customerForm.controls;
  }

  getCustomerInvoices() {
    this.apiService.getData(this.customer.asID + "/"+this.customerID, "invoiceCustomerSummary/").then((result) => {
      this.customerInvoices = result;
     let m3Total = this.customerInvoices[0].M3*1-this.customerInvoices[1].M3*1;
     let m2Total = this.customerInvoices[0].M2*1-this.customerInvoices[1].M2*1;
     let m1Total = this.customerInvoices[0].M1*1-this.customerInvoices[1].M1*1;
     let m0Total = this.customerInvoices[0].M0*1-this.customerInvoices[1].M0*1;

     this.customerInvoices[2].M3=m3Total.toFixed(2);
     this.customerInvoices[2].M2=m2Total.toFixed(2);
     this.customerInvoices[2].M1=m1Total.toFixed(2);
     this.customerInvoices[2].M0=m0Total.toFixed(2);
    });
  }

  onCtChange (ct:any) {
      this.customer.customerType=ct;
  }

  getBanks() {
    this.apiService.getData("", "banks").then((result) => {
      this.banks = result;
      this.customer.bankID=parseInt(this.customer.bankID);
      console.log(this.banks,"banks");
      setTimeout(function(){
        $(".selectBank").selectpicker('refresh');
       
      },300);
      $(".selectBank").selectpicker('val', this.customer.bankID);
    });
  }

  ngOnInit() {
    if (this.customerID>0) {
      this.getCustomer(this.businessID+"/0/"+this.customerID);
    } else {
      $(".selectdueDateOptions").selectpicker();
    }
    this.getCustomerInvoices();
    this.getBanks();
  }

  getCustomer(customerID: any) {
    if (this.customerID>0) {
      this.apiService.getData(customerID, "customers/").then((result) => {
        this.customer = result[0];
        console.log(this.customer,"customer");
        this.isVendor2=(this.customer.isVendor==3) ? true : false;
        console.log(this.isVendor2);
        $(".selectdueDateOptions").selectpicker('val', this.customer.dueDateOptions);
      });
    } else {
      this.customer.ID=0;
    }
  }

  saveCustomer() {
    console.log(this.customerForm.invalid);
   
    if (this.customerForm.invalid) {
      return;
    }

  
  // console.log(this.isVendor2,"isVendor2"); 
  // console.log($("#isVendor2").val(),"isVendor2");
  this.isVendor2=$("#isVendor2").prop('checked');
  console.log(this.isVendor2,"isVendor2"); 
  if (this.isVendor2===true) {
      this.customer.isVendor=3;
   } else {
     this.customer.isVendor=this.isVendor;
   }
   //this.customer.bankID = parseInt($('.selectBank').val());
   console.log(this.customer.bankID,"customeree");

    this.apiService.postData(this.customer.ID, "/customer/save/", JSON.stringify(this.customer)).then((result) => {
      console.log(result);
      if (this.customer.isVendor==1 || this.customer.isVendor==3) {
         this.router.navigateByUrl("/sales/customers");
      } else {
        this.router.navigateByUrl("/costs/suppliers");
      }
    });
  }

  deleteCustomer() {
    this.apiService.deleteData(this.customer.ID, "/customer/delete/").then((result) => {
      if (this.customer.isVendor==1) {
        this.router.navigateByUrl("/sales/customers");
     } else {
       this.router.navigateByUrl("/costs/suppliers");
     }
    });
  }

  clickCancel () {
    if (this.customer.isVendor==1) {
      this.router.navigateByUrl("/sales/customers");
   } else {
     this.router.navigateByUrl("/costs/suppliers");
   }
  }

  setBankID (e:any,index:any)  {
    //console.log(e.target.options[e.target['options'].selectedIndex].getAttribute('id'),index);
    this.customer.bankID=e.target.options[e.target['options'].selectedIndex].getAttribute('id');
 }
}
