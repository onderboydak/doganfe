import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountingComponent } from 'app/costs/accounting/accounting.component';
import { TranslateService } from '@ngx-translate/core';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})

export class InvoiceComponent implements OnInit {

  customers: any = [];
  accountingSuppliers: any = [];
  name: any;

  invoice: any = [];
  invoiceID: any;
  totals: any = [];
  closeResult: string;
  invoiceType: any = 0;
  businessID: any;
  selectedCustomer: any = [];
  customerID: any = 0;
  products: any = [];
  masterProducts: any = [];


  @ViewChild('idContent') idContent: ElementRef;
  @ViewChild('fileInput') fileInput: ElementRef;
  //invoiceForm: FormGroup;

  constructor(public apiService: ApiService,
    private activeRoute: ActivatedRoute, public router: Router, public render: Renderer2, private modalService: NgbModal, private translate: TranslateService) {
    const routeParams = this.activeRoute.snapshot.params;
    this.activeRoute.queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.invoiceType = +params['invoiceType'] || 0;
        this.customerID = +params['customerID'] || 0;
      });
    this.invoiceID = routeParams.id;
    this.businessID = localStorage.getItem("businessID");

    console.log(routeParams);
    console.log(this.invoiceType, "invoiceType");
    console.log(this.customerID, "customerID");

    if (!this.invoiceID) {
      this.invoiceID = 0;
    }
    this.getProducts();

    this.invoice = { "ID": 0, "asID": this.businessID, "invoiceDate": "", "customerID": this.customerID, "description": "", "notes": "", "tags": "", "statusID": 0, "dueDateOptions": 0 };

  }

  resolveAfter2Seconds(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 2000);
    });
  }



  ngOnInit() {
    /*
    this.invoiceForm = this.fb.group({
      customerID: ['', Validators.min(1)]
    });
    */
    this.getCustomers(0);


    if (this.invoiceID > 0) {
      console.log(this.invoiceID);
      this.getInvoice(this.invoiceID, 0);
    } else {
      this.invoice.invoiceDetails = [];
      this.invoice.statusID = 0;
    }

    this.resolveAfter2Seconds(40).then(value => {
      this.spInit();
      $(".selectCustomer").selectpicker('refresh');
    });

    $('.datetimepicker').datetimepicker({
      locale: 'tr',
      format: 'DD-MM-YYYY HH:mm',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });

    $('.selectperiodLastDate').datetimepicker({
      locale: 'tr',
      format: 'DD-MM-YYYY',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });
  }

  getTranslate(txt:string) {
    return this.translate.instant(txt);
  }

  openModal(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = 'Closed with: ${result}';
    }, (reason) => {
      this.closeResult = 'Dismissed ';
    });
    console.log(this.closeResult);
  }

  getTotals() {

    this.totals.unit = this.invoice.invoiceDetails.reduce((sum, item) => sum + (item.unit * 1), 0);
    this.totals.amount = this.invoice.invoiceDetails.reduce((sum, item) => sum + (item.totalAmount * 1), 0);
    this.totals.vatAmount = this.invoice.invoiceDetails.reduce((sum, item) => sum + (item.vatAmount * 1), 0);
    this.totals.total = (this.totals.amount * 1) + (this.totals.vatAmount * 1);

    console.log(this.totals.total, ",", this.totals.amount, ",", this.totals.vatAmount, "total amounts");
    /*this.invoice.invoiceDetails.forEach((e:any) => {
      console.log(e.amount);
    
      this.totals.amount =  Number(this.totals.amount) + Number(e.amount);
      console.log( this.totals.amount);
      this.totals.vatAmount =  this.totals.vatAmount + Number(e.vatAmount);
      this.totals.total =  this.totals.total + Number(e.totalAmount);
      this.totals.discount =  this.totals.discount + Number(e.discountAmount);
  });
  */
  }

  getCustomer(customerID: any) {

    this.apiService.getData(this.businessID + "/1/" + customerID, "customers/").then((result) => {
      this.selectedCustomer = result[0];
      $(".selectdueDateOptions").selectpicker('val', this.selectedCustomer.dueDateOptions);
      this.invoice.dueDateOptions = this.selectedCustomer.dueDateOptions;
      this.getSuppliers(0);
    });
  }

  getCustomers(customerID: any) {
    if (this.invoiceType > 0) {
      this.invoice.invoiceType = this.invoiceType; // Satıs Faturası
    }
    this.apiService.getData(this.businessID + "/" + this.invoice.invoiceType + "/" + customerID, "customers/").then((result) => {
      this.customers = result;
      console.log(this.customers,"customers");
    });
  }

  getSuppliers(supplierID: any) {
    this.apiService.getData(supplierID, "accountingSuppliers/").then((result) => {
      this.accountingSuppliers = result;



    });
  }

  ngAfterViewInit() {

  }
  getInvoice(invoiceID: any, invoiceType: any) {
    this.apiService.getData((invoiceType + "/" + this.businessID + "/" + invoiceID), "invoices/").then((result) => {
      this.invoice = result[0];
      this.invoice.autoGenerate = parseInt(this.invoice.autoGenerate);
      this.invoice.isProforma = parseInt(this.invoice.isProforma);

      this.invoice.invoiceDetails = JSON.parse(this.invoice.invoiceDetails);
      if (this.invoice.invoiceDetails.length > 0) {
        this.getTotals();
        console.log(this.invoice.currency, "currency");
        if (this.invoice.currency != "") {
          let pf = this.masterProducts.filter(x => x.currency == this.invoice.currency);
          console.log(pf);
          this.products = pf;
          this.resolveAfter2Seconds(20).then(value => {
            $(".selectProduct").selectpicker('refresh');
          });
        }
      }
    });
  }

  spInit() {
    console.log("Init Select");
    $(".selectpicker").selectpicker();

    $(".selectAS").selectpicker('val', this.invoice.asID);
    $(".selectCustomer").selectpicker('val', this.invoice.customerID);
    $(".selectCurrency").selectpicker('val', this.invoice.invoiceCurrency);
    $(".selectinvoiceClass").selectpicker('val', this.invoice.invoiceClass);
    $(".selectdueDateOptions").selectpicker('val', this.invoice.dueDateOptions);
    $(".selectInvoicePeriod").selectpicker('val', this.invoice.invoicePeriod);
    $(".selectVatRate").selectpicker();

    //$(".selectProduct").selectpicker();

    this.invoice.invoiceDetails.forEach(element => {
      let si = ".rowd" + element.rowNumber;
      //console.log(si, element.item);
      $(".rowd" + element.rowNumber).selectpicker('val', element.item);
    });

    //$(".selectProduct").selectpicker("val", "GP Manganez AA Kalem Pil 12 Adet (GP15G R6)");
  }

  addInvoiceDetail() {
    console.log(this.invoice, "invoice");
    if (this.invoice.invoiceDetails === null) {
      this.invoice.invoiceDetails = [];
    }

    let rn = this.invoice.invoiceDetails.length + 1;
    let id = { "rowNumber": rn, "item": "", "unit": 1, "amount": 0, "discountRate": 0, "vatRate": 18, "vatAmount": 0, "totalAmount": 0, "discountAmount": 0,"productID":0 };



    this.invoice.invoiceDetails.push(id);
    this.resolveAfter2Seconds(20).then(value => {
      $(".selectVatRate").selectpicker();
      $(".selectProduct").selectpicker();
    });
  }

  uploadInvoice(invoiceID: any) {
    let event = new MouseEvent('click', { bubbles: false });
    this.invoiceID = invoiceID;
    this.fileInput.nativeElement.dispatchEvent(event);
  }

  onFileChange(event) {
    let reader = new FileReader();
    if (this.fileInput.nativeElement.files && this.fileInput.nativeElement.files.length > 0) {
      let file = this.fileInput.nativeElement.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {

        let file = {
          "fileName": this.fileInput.nativeElement.files[0].name,
          "fileType": this.fileInput.nativeElement.files[0].type,
          "table": "invoices",
          "userID": localStorage.getItem("userID"),
          "description": "Invoice",
          "file": reader.result.toString().split(",")[1]
        };

        this.apiService.postData("invoices/" + this.invoiceID, "files/upload/", JSON.stringify(file)).then((result) => {
          console.log(result);
        });
        /*
           console.log(this.fileInput.nativeElement.files[0].name);
         console.log(this.fileInput.nativeElement.files[0].type);
         console.log(reader.result.toString().split(",")[1]);
        */
      };
    }
  }


  removeItem(rowNumber: any) {
    let ndx = this.invoice.invoiceDetails.findIndex(x => x.rowNumber == rowNumber);
    this.invoice.invoiceDetails.splice(ndx, 1);
  }

  clickCancel() {
    if (this.invoice.invoiceType == 2) {
      this.router.navigateByUrl("/costs/invoices");
    } else {
      this.router.navigateByUrl("/sales/invoices");
    }
  }

  clickSave() {
    /*
    console.log("Click Save");
    if (this.invoiceForm.invalid) {
      return;
    }
    */

    if (this.invoiceType > 0) {
      this.invoice.invoiceType = this.invoiceType; // Satıs Faturası
    }
    this.invoice.asID = this.businessID;
    this.invoice.tags = this.invoice.supplier;
    this.invoice.invoiceDate = $('.datetimepicker').val();
    this.invoice.periodLastDate = $('.selectperiodLastDate').val();

    this.apiService.postData(this.invoice.ID, "/invoice/save/", JSON.stringify(this.invoice)).then((result) => {
      console.log(result);
      if (this.invoice.invoiceType == 2) {
        this.router.navigateByUrl("/costs/invoices");
      } else {
        this.router.navigateByUrl("/sales/invoices");
      }
    });

  }

  clickSaveAsInvoice() {
    console.log("Click Save");
    if (this.invoiceType > 0) {
      this.invoice.invoiceType = this.invoiceType; // Satıs Faturası
    }
    this.invoice.isProforma = 0;
    this.invoice.asID = this.businessID;
    this.invoice.tags = this.invoice.supplier;
    this.invoice.invoiceDate = $('.datetimepicker').val();
    this.apiService.postData(this.invoice.ID, "/invoice/save/", JSON.stringify(this.invoice)).then((result) => {
      console.log(result);
      if (this.invoice.invoiceType == 2) {
        this.router.navigateByUrl("/costs/invoices");
      } else {
        this.router.navigateByUrl("/sales/invoices");
      }
    });
  }
  deleteFile(fileID: any) {
    this.apiService.deleteData(fileID, "/files/delete/").then((result) => {
      console.log(result);
      this.invoice.pdfPath = null;
    });
  }

  clickDelete(content: any) {
    //this.openModal(content);
    if (confirm("Are you sure ?")) {
      this.apiService.deleteData(this.invoice.ID, "/invoice/delete/").then((result) => {
        console.log(result);
        if (this.invoice.invoiceType == 2) {
          this.router.navigateByUrl("/costs/invoices");
        } else {
          this.router.navigateByUrl("/sales/invoices");
        }
      });
    }

  }

  changeCurrency(e: any) {
    this.invoice.currency = e.target['options'][e.target['options'].selectedIndex].text;
    console.log(e.target['options'][e.target['options'].selectedIndex].value, "selected currency");
    let pf = this.masterProducts.filter(x => x.currency == e.target['options'][e.target['options'].selectedIndex].text);
    console.log(pf);
    this.products = pf;
    this.resolveAfter2Seconds(20).then(value => {
      $(".selectProduct").selectpicker('refresh');
    });



  }

  unitChanged(e: any, rowNumber: any) {
    console.log("unit changed", e, rowNumber);
    this.calculate(rowNumber);
  }

  amountChanged(e: any, rowNumber: any) {
    console.log("amount changed", e);
    this.calculate(rowNumber);
  }

  discountChanged(e: any, rowNumber: any) {
    console.log("discount changed", e);
    this.calculate(rowNumber);
  }

  vatChanged(e: any, rowNumber: any) {
    console.log("vat changed", e);
    this.calculate(rowNumber);
  }

  calculate(rowNumber: any) {
    let idr = this.invoice.invoiceDetails.filter(x => x.rowNumber == rowNumber)[0];
    idr.vatAmount = (idr.unit * idr.amount * idr.vatRate / 100).toFixed(2);
    if (idr.discount > 0) {
      idr.totalAmount = (idr.unit * idr.amount * ((100 - idr.discount) / 100)).toFixed(2);
    } else {
      idr.totalAmount = (idr.unit * idr.amount).toFixed(2);
    }
    this.getTotals();
  }

  getProducts() {
    //console.log(this.businessID + "/3/0/" + this.invoiceID,"prodicys");

    this.apiService.getData((this.businessID + "/" + this.invoiceType + "/0/" + this.invoiceID), "products/").then((result) => {
      this.products = result;
      this.masterProducts = result;
      console.log(this.products,"products");
    });
  }

  postAccount() {
    console.log(this.invoiceID, "invoice ID");
    let modalRef = this.modalService.open(AccountingComponent, { "backdrop": "static" });
    modalRef.componentInstance.invoiceID = this.invoiceID;

    modalRef.result.then((data) => {
      console.log(data, "result data");

    }, (reason) => {
      // on dismiss
    });
  }

  setProductID (e:any,index:any)  {
     //console.log(e.target.options[e.target['options'].selectedIndex].getAttribute('id'),index);
     this.invoice.invoiceDetails[index].productID=e.target.options[e.target['options'].selectedIndex].getAttribute('id');
  }
  
}
