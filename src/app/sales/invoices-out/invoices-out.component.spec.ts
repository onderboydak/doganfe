import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicesOutComponent } from './invoices-out.component';

describe('InvoicesComponent', () => {
  let component: InvoicesOutComponent;
  let fixture: ComponentFixture<InvoicesOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicesOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicesOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
