import { Component, OnInit } from '@angular/core';


import * as printJS from "print-js";
import { ApiService } from 'app/providers/api.service';
import { Router } from '@angular/router';


declare var $: any;



declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows: [];
}

@Component({
  selector: 'app-archieve',
  templateUrl: './archieve.component.html',
  styleUrls: ['./archieve.component.scss']
})
export class ArchieveComponent implements OnInit {
  public dataTable: DataTable;
  public invoices: any = [];
  businessID: any;
  userType: any;
  checkAll:boolean=false;

  constructor(public apiService: ApiService, public router: Router) {
    this.businessID = localStorage.getItem("businessID");
    this.userType = localStorage.getItem("userType");
   }

  ngOnInit() {
    this.getInvoices('1/' + this.businessID + '/0/1');
  }


  getInvoices(invoiceID: any) {
    this.apiService.getData(invoiceID, "invoices/").then((result) => {
        this.invoices = result;

        this.dataTable = {
            headerRow: ['#', 'Due', 'Invoice Number', 'Customer', 'Issued', 'Amount', 'Status', 'Action'],
            footerRow: ['Row Number', 'Due Date', 'Description', 'Customer', 'ßIssue Date', 'Amount', 'Status', 'Action'],
            dataRows: this.invoices
        };

    });

  }


  ngAfterViewInit() {
    $('#datatables').DataTable({
        'columnDefs': [ {
            'targets': [0,-1,-2], /* column index */
            'orderable': false /* true or false */
         }],
        "pagingType": "full",
        "pageLength": 25,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Search records",
            lengthMenu: "_MENU_ records per page",
            zeroRecords: "None found",
            info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
            infoEmpty: "No record",
            infoFiltered: "(filtered from _MAX_ records)",
            paginate: {
                "next": "<i class='fa fa-angle-right'></i>",
                "previous": "<i class='fa fa-angle-left'></i>",
                "first": "<i class='fa fa-angle-double-left'></i>",
                "last": "<i class='fa fa-angle-double-right'></i>",
            }
        }
    });
  }


  calculateDays(startTime: any, endTime: any) {

    let date2: string = endTime;

    let diffInMs: number = Date.parse(date2) - Date.parse(Date());
    let diffInHours: number = diffInMs / 1000 / 60 / 60 / 24;
    if (isNaN(diffInHours)) {
        diffInHours = 9999;
    }
    return Math.round(diffInHours);
}

printInvoices() {
  $(".selectedRows").each(function(){
          if ($(this).prop("checked")) {
              printJS({printable:$(this).val(), type:'pdf', showModal:true});
          }
  });
}
}
