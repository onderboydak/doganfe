import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';

declare var $: any;

@Component({
  selector: 'app-invoice-payment',
  templateUrl: './invoice-payment.component.html',
  styleUrls: ['./invoice-payment.component.scss']
})
export class InvoicePaymentComponent implements OnInit {
  invoice: any = [];
  businessID: any;
  @Input() public invoiceID: any;
  @Input() public paymentType: any;
  fd: any;

  constructor(public apiService: ApiService, public activeModal: NgbActiveModal, private datePipe: DatePipe) {
    let now = new Date();
    this.fd = this.datePipe.transform(now, 'dd-MM-yyyy').toString();
    console.log(this.fd);
    this.invoice.payment = [{
      "invoiceID": 0,
      "paymentDate": this.fd,
      "amount": 0,
      "paymentAccount": 0,
      "paymentMethod": 0,
      "notes": ""
    }];
    console.log(this.invoice.payment);
    this.businessID = localStorage.getItem("businessID");

  }

  ngOnInit() {

    this.getInvoice(0);


    $('.datetimepicker').datetimepicker({
      locale: 'tr',
      format: 'DD-MM-YYYY',
      date: this.invoice.paymentDate,
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });

    //console.log(this.fd,"fd");

    $(".selectpicker").selectpicker();



  }

  getInvoice(invoiceType: any) {
    console.log(this.invoiceID, "Invoice ID");
    this.apiService.getData((invoiceType + "/" + this.businessID + "/" + this.invoiceID), "invoices/").then((result) => {
      this.invoice = result[0];
      /*
      this.invoice.invoiceDetails = JSON.parse(this.invoice.invoiceDetails);
      console.log(this.invoice, "Invoıce");
      */
      this.invoice.payment = JSON.parse(this.invoice.payment)[0];
      if (this.invoice.payment.invoiceID == 0) {
        this.invoice.payment.amount = this.invoice.totalAmount;
      }



    });
  }
  clickCancel() {
    this.activeModal.close();
  }
  savePayment() {
    console.log("Click Save");
    this.invoice.payment.paymentDate = $('.datetimepicker').val();
    console.log(JSON.stringify(this.invoice.payment));
    this.apiService.postData(this.invoice.ID, "invoice/payment/save/", JSON.stringify(this.invoice.payment)).then((result) => {
      console.log(result);
      this.activeModal.close(result);
    });
  }

  ngAfterViewInit() {
    console.log('AfterViewInit');

    $(".datetimepicker").datetimepicker({ data: this.invoice.paymentDate });
    //Picker actiginda secilmis oluyor ama neden secili goster miyor.

  }

}
