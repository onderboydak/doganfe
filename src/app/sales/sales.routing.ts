import { Routes } from '@angular/router';
import { CustomersComponent } from './customers/customers.component';
import { CustomerComponent } from './customer/customer.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { ProductsComponent } from './products/products.component';
import { InvoicesOutComponent } from './invoices-out/invoices-out.component';
import { ProductComponent } from './product/product.component';
import { AuthGuardService as AuthGuard } from '../auth/auth-guard.service';
import { ArchieveComponent } from './archieve/archieve.component';

export const SalesRoutes: Routes = [
    {
        path: '',
        children: [ {
            path: 'invoices',
            component: InvoicesOutComponent,
            canActivate: [AuthGuard] 
    }]},{
        path: 'invoice/:id',
        component: InvoiceComponent,
        canActivate: [AuthGuard] 
    },{
        path: '',
        children: [ {
            path: 'invoice',
            component: InvoiceComponent,
            canActivate: [AuthGuard] 
    }]},{
        path: '',
        children: [ {
            path: 'customers',
            component: CustomersComponent,
            canActivate: [AuthGuard] 
    }]},{
        path: 'customer/:id',
        component: CustomerComponent,
        canActivate: [AuthGuard] 
    },{
        path: '',
        children: [ {
            path: 'customer',
            component: CustomerComponent,
            canActivate: [AuthGuard] 
    }]},
    {
    path: '',
    children: [{
        path: 'products',
        component: ProductsComponent,
        canActivate: [AuthGuard] 
    }]},{
        path: 'product/:id',
        component: ProductComponent,
        canActivate: [AuthGuard] 
    },{
        path: '',
        children: [ {
            path: 'product',
            component: ProductComponent,
            canActivate: [AuthGuard] 
    }]},{
        path: '',
        children: [ {
            path: 'archieves',
            component: ArchieveComponent,
            canActivate: [AuthGuard] 
    }]}
    ];
