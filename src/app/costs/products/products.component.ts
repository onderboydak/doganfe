import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';

declare var $:any;

declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows:[];
}


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

 

  public dataTable: DataTable;
  public products: any = [];
  businessID:any;

  constructor(public apiService: ApiService) { 
      /*
    this.dataTable = {
        headerRow: ['#', 'Name', 'Price'],
        footerRow: ['#', 'Name', 'Price'],
        dataRows: []
    };
    */

  }

  ngOnInit() {
    this.businessID=localStorage.getItem("businessID");
    this.getProducts(this.businessID+"/2/0");
}

getProducts(productID: any) {
    this.apiService.getData(productID, "products/").then((result) => {
        this.products = result;

        this.dataTable = {
            headerRow: ['#', 'Name', 'Price'],
            footerRow: ['#', 'Name', 'Price'],
            dataRows: this.products
        };

        console.log(this.dataTable);
    });

}
  ngAfterViewInit(){

    $('#datatables').DataTable({
        "pagingType": "full",
        "pageLength": 25,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
        lengthMenu: "_MENU_ records per page",
        zeroRecords: "None found",
        info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
        infoEmpty: "No record",
        infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                  },
        }

    });
    
    /*
    var table = $('#datatables').DataTable();

    // Edit record
    table.on( 'click', '.edit', function () {
        var $tr = $(this).closest('tr');

        var data = table.row($tr).data();
        alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
    } );

    // Delete a record
    table.on( 'click', '.remove', function (e) {
        var $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
    } );

    //Like record
    table.on( 'click', '.like', function () {
        alert('You clicked on Like button');
    });
    */
}
}
