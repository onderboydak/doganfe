import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

declare var $: any;

@Component({
  selector: 'app-accounting',
  templateUrl: './accounting.component.html',
  styleUrls: ['./accounting.component.scss']
})
export class AccountingComponent implements OnInit {
  @Input() public invoiceID: any;
  chartOfAccounts: any = [];
  businessID: any;
  invoice: any = [];
  rule: boolean = false;

  constructor(private apiService: ApiService, public activeModal: NgbActiveModal) {
    this.businessID = localStorage.getItem("businessID");
  }

  ngOnInit() {
    this.getInvoice(this.invoiceID);
    this.getChartOfAccounts();
    this.spInit();
  }


  postAccount() {
    this.invoice["rule"] = this.rule;
    //console.log(this.invoice);
    this.apiService.postData(this.invoiceID, "/invoice/save/", JSON.stringify(this.invoice)).then((result) => {
      let res: any = [];
      res["account"] = this.invoice.account;

      this.activeModal.close(res);

    });

  }

  resolveAfter2Seconds(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 600);
    });
  }

  clickCancel() {
    this.activeModal.close();
  }

  getChartOfAccounts() {
    this.apiService.getData(this.businessID + "/700,740,770,760", "chartOfAccounts/").then((result) => {
      this.chartOfAccounts = result;
      console.log(this.chartOfAccounts, "chartofaccounts");

      this.resolveAfter2Seconds(50).then(value => {
        this.spInit();
      });
    });
  }

  getInvoice(invoiceID: any) {
    this.apiService.getData(("2/" + this.businessID + "/" + invoiceID), "invoices/").then((result) => {
      this.invoice = result[0];
      this.invoice.invoiceDetails = JSON.parse(this.invoice.invoiceDetails);
    });
  }

  spInit() {
    console.log("Init Select");
    $(".selectAccount").selectpicker('val', this.invoice.account);
    $(".selectAccount").selectpicker('refresh');
  }

  removeRule(ruleID:any) {
    if (confirm("Are you sure ?")) {
      this.apiService.deleteData(ruleID, "/rule/delete/").then((result) => {
        console.log(result);
        this.invoice.invoiceRule = 0;
      });
    }
  }

}
