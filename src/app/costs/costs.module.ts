import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CostsRoutes } from './costs.routing';

import { SuppliersComponent } from './suppliers/suppliers.component'
import { ExpensesComponent } from './expenses/expenses.component';
import { SupplierComponent } from './supplier/supplier.component';
import { ExpenseComponent } from './expense/expense.component';
import { ExpenseTabComponent } from './expense-tab/expense-tab.component';
import { ProductsComponent } from './products/products.component';
import { InvoicesInComponent } from './invoices-in/invoices-in.component';
import { TranslateModule } from '@ngx-translate/core';
import { LbdModule } from 'app/lbd/lbd.module';
import { ArchieveCComponent } from './archieve-c/archieve-c.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(CostsRoutes),
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        LbdModule
    ],
    declarations: [
        SuppliersComponent,
        ExpensesComponent,
        SupplierComponent,
        ExpenseComponent,
        ExpenseTabComponent,
        ProductsComponent,
        InvoicesInComponent,
        ArchieveCComponent
    ]
})

export class CostsModule {}
