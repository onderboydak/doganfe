import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var $: any;


@Component({
    moduleId: module.id,
    selector: 'app-expense',
    templateUrl: './expense.component.html',
    styleUrls: ['./expense.component.scss']
})

export class ExpenseComponent {
    expenseID: any;
    businessID: any;
    expense:any=[];
    expenseCategories:any=[];

    constructor(public apiService: ApiService,public activeRoute:ActivatedRoute,public router:Router) { 
        const routeParams = this.activeRoute.snapshot.params;
        this.expenseID = routeParams.id;
        console.log(this.expenseID,"expenseID");
        this.businessID=localStorage.getItem("businessID");
        this.getExpenseCategories();
        this.getExpense(this.businessID+"/"+this.expenseID);
        

    }
    getExpenseCategories (){
      this.apiService.getData(this.businessID, "expenseCategories/").then((result) => {
        this.expenseCategories = result;
        console.log(this.expenseCategories,"expenseCategories");
        $(".selectexpenseCategory").selectpicker('refresh');
      });
    }

    getExpense(expenseID: any) {
        if (this.expenseID>0) {
          this.apiService.getData(expenseID, "expenses/").then((result) => {
            this.expense = result[0];
            console.log(this.expense,"expense");
            this.spInit();
          });
        } else {
            this.expense={ "ID": 0, "merchant": "", "expenseNumber": "","expenseDate": "", "expenseAccount": 0, "expenseCategory": 1, "currency": 1, "amount": 0,"vatRate": 18, "vatAmount": 0, "totalAmount": 0,"notes":"","asID":this.businessID,"status":1 };
            console.log(this.expense);
        }
      }

    ngOnInit() {
        $('.datetimepicker').datetimepicker({
            locale: 'tr',
            format: 'DD-MM-YYYY HH:mm',
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fa fa-trash',
              close: 'fa fa-remove'
            }
          });

          this.resolveAfter2Seconds(20).then(value => {
            this.spInit();      
          });

         
    }

    ngAfterViewInit() {

    }

  spInit () {
    console.log("Sp Init");
    $(".selectpicker").selectpicker('refresh');
    $(".selectPaymentAccount").selectpicker('val', this.expense.expenseAccount);
    $(".selectexpenseCategory").selectpicker('val', this.expense.expenseCategory);
    $(".selectCurrency").selectpicker('val', this.expense.currency);
    $(".selectvatRate").selectpicker('val', this.expense.vatRate);

   
  }  

  resolveAfter2Seconds(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 1000);
    });
  }

  saveExpense() {
    this.apiService.postData(this.expense.ID, "/expense/save/", JSON.stringify(this.expense)).then((result) => {
      console.log(result);
    });
  }

  deleteExpense() {
    this.apiService.deleteData(this.expense.ID, "/expense/delete/").then((result) => {
      console.log(result);
    });
  }

  clickCancel () {
    this.router.navigateByUrl("/costs/expenses");
  }

  changeCurrency(event:any) {
      console.log("Change Currency");
  }

  vatChanged(event:any) {
      if (this.businessID==1 || this.businessID==2 ) {
          this.expense.vatAmount=(this.expense.totalAmount-this.expense.totalAmount/((event/100)+1)).toFixed(2);
          this.expense.amount=(this.expense.totalAmount/((event/100)+1)).toFixed(2);
      }
  }
}