import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'app/providers/api.service';

declare var $: any;

declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows: [];
}

@Component({
    moduleId: module.id,
    selector: 'app-expenses',
    templateUrl: './expenses.component.html',
    styleUrls: ['./expenses.component.scss']
})

export class ExpensesComponent implements OnInit {
    @ViewChild('fileInput') fileInput:ElementRef;
    public dataTable: DataTable;
    public expenses: any = [];
    businessID: any;

    constructor(public apiService: ApiService) {
        /*
        this.dataTable = {
            headerRow: ['#', 'Status', 'Date', 'Merchant', 'Category', 'Account', 'Total', 'Total Amount'],
            footerRow: ['Row Number', 'Status', 'Date', 'Merchant', 'Category', 'Account', 'Total', 'Total Amount'],
            dataRows:[]
        };
        */
    }


    ngOnInit() {
        this.businessID = localStorage.getItem("businessID");
        this.getExpenses(this.businessID + '/0');
    }

    uploadFile(){
        let event = new MouseEvent('click', {bubbles: false});
        this.fileInput.nativeElement.dispatchEvent(event);
    }

    onFileChange(event) {
        let reader = new FileReader();
        if(event.target.files && event.target.files.length > 0) {
          let file = event.target.files[0];
          reader.readAsDataURL(file);
          reader.onload = () => {

            let file ={
                        "fileName":this.fileInput.nativeElement.files[0].name,
                        "fileType":this.fileInput.nativeElement.files[0].type,
                        "file":reader.result.toString().split(",")[1]
            };

            this.apiService.postData(this.businessID, "receipts/import/", JSON.stringify(file)).then((result) => {
                console.log(result);
                document.location.reload();
              });
            console.log(this.fileInput.nativeElement.files[0].name);
            console.log(this.fileInput.nativeElement.files[0].type);
            console.log(reader.result.toString().split(",")[1]);
            /*
            this.fileInput.nativeElement.setValue({
              filename: file.name,
              filetype: file.type,
              value: reader.result //.split(',')[1]
            })
            */
          };
        }
      }

    getExpenses(expenseID: any) {

        this.apiService.getData(expenseID, "expenses/").then((result) => {
            this.expenses = result;

            this.dataTable = {
                headerRow: ['#', 'Status', 'Date', 'Merchant', 'Category', 'Account', 'Total', 'Total Amount'],
                footerRow: ['Row Number', 'Status', 'Date', 'Merchant', 'Category', 'Account', 'Total', 'Total Amount'],
                dataRows: this.expenses
            };

            console.log(this.expenses, "expenses");
        });

    }

    ngAfterViewInit() {


        $('#datatables').DataTable({
            "pagingType": "full",
            "pageLength": 25,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ records per page",
                zeroRecords: "None found",
                info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
                infoEmpty: "No record",
                infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                },
            }

        });

    }

}
