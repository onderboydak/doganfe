import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchieveCComponent } from './archieve-c.component';

describe('ArchieveCComponent', () => {
  let component: ArchieveCComponent;
  let fixture: ComponentFixture<ArchieveCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchieveCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchieveCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
