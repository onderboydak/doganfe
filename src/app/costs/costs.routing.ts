import { Routes } from '@angular/router';

import { ExpensesComponent } from './expenses/expenses.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { ExpenseComponent } from './expense/expense.component';
import { SupplierComponent } from './supplier/supplier.component';
import { ProductsComponent } from './products/products.component';
import { InvoicesInComponent } from './invoices-in/invoices-in.component';
import { AuthGuardService as AuthGuard } from '../auth/auth-guard.service';
import { ArchieveCComponent } from './archieve-c/archieve-c.component';


export const CostsRoutes: Routes = [
{
    path: '',
    children: [ {
        path: 'invoices',
        component: InvoicesInComponent,
        canActivate: [AuthGuard] 
}]},   
{
    path: '',
    children: [ {
        path: 'expenses',
        component: ExpensesComponent,
        canActivate: [AuthGuard] 
}]},{
    path: 'expense/:id',
    component: ExpenseComponent
},{
    path: '',
    children: [ {
        path: 'expense',
        component: ExpenseComponent
}]},{
    path: '',
    children: [ {
        path: 'suppliers',
        component: SuppliersComponent,
        canActivate: [AuthGuard] 
}]},{
    path: '',
    children: [{
        path: 'supplier',
        component: SupplierComponent
    }]
},
{
    path: '',
    children: [{
        path: 'products',
        component: ProductsComponent,
        canActivate: [AuthGuard] 
    }]
},{
    path: '',
    children: [ {
        path: 'archieves',
        component: ArchieveCComponent,
        canActivate: [AuthGuard] 
}]}
];


