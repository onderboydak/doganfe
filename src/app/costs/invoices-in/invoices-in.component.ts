import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { Router } from '@angular/router';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { InvoicePaymentComponent } from 'app/sales/invoice-payment/invoice-payment.component';
import { ChartType, LegendItem } from 'app/lbd/lbd-chart/lbd-chart.component';
import { ApproveModalComponent } from 'app/sales/approve-modal/approve-modal.component';
import { AccountingComponent } from '../accounting/accounting.component';
import * as printJS from "print-js";
import { TranslateService } from '@ngx-translate/core';

declare var $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: [];
}

@Component({
  moduleId: module.id,
  selector: 'app-invoices-in',
  templateUrl: './invoices-in.component.html',
  styleUrls: ['./invoices-in.component.scss']
})

export class InvoicesInComponent implements OnInit {
  @ViewChild('fileInput') fileInput: ElementRef;

  public dataTable: DataTable;
  public invoices: any = [];
  businessID: any;
  invoiceDueDates: any = [];
  userType: any;
  showHideAccor: any = 0;
  series: any = [];
  userID: any;
  invoiceID: any;
  totals: any = [];
  checkAll:boolean=false;

  public activityChartType: ChartType;
  public activityChartData: any;
  public activityChartOptions: any;
  public activityChartResponsive: any[];
  public activityChartLegendItems: LegendItem[];

  constructor(public apiService: ApiService, public router: Router, private modalService: NgbModal, private translate: TranslateService) {
    this.userType = localStorage.getItem("userType");
    this.userID = localStorage.getItem("userID");
    /*
     this.dataTable = {
       headerRow: ['#', 'Due', 'Invoice Number', 'Customer', 'Issued', 'Amount', 'Status','Action'],
       footerRow: ['Row Number', 'Due Date', 'Description', 'Customer', 'Issue Date', 'Amount', 'Status','Action'],
       dataRows: []
     };
 */

  }

  showAccord() {
    this.showHideAccor = (this.showHideAccor == 1) ? 0 : 1;
  }

  ngOnInit() {
    this.businessID = localStorage.getItem("businessID");
    this.getInvoices('2/' + this.businessID + '/0');
    this.getInvoiceDueDates();

    // graph

    this.activityChartType = ChartType.Bar;
    this.activityChartData = {
      labels: ['Upcoming', '1-30 days', '31-60 days', '61-90 days', '90 days'],
      series: this.series
    };
    this.activityChartOptions = {
      seriesBarDistance: 10,
      axisX: {
        showGrid: true
      },
      height: '240px'
    };
    this.activityChartResponsive = [
      ['screen and (max-width: 640px)', {
        seriesBarDistance: 10,
        axisX: {
          labelInterpolationFnc: function (value) {
            return value[0];
          }
        }
      }]
    ];
    this.activityChartLegendItems = [
      { title: 'TRY', imageClass: 'fa fa-circle text-info' },
      { title: 'USD', imageClass: 'fa fa-circle text-danger' },
      { title: 'EUR', imageClass: 'fa fa-circle text-warning' },
      { title: 'GBP', imageClass: 'fa fa-circle text-success' }
    ];


  }

  getTranslate(txt:string) {
    return this.translate.instant(txt);
  }

  calculateDays(startTime: any, endTime: any) {


    let date1: string = startTime;
    let date2: string = endTime;

    let diffInMs: number = Date.parse(date2) - Date.parse(Date());
    let diffInHours: number = diffInMs / 1000 / 60 / 60 / 24;

    if (isNaN(diffInHours)) {
      diffInHours = 9999;
    }
    return Math.round(diffInHours);

  }

  getInvoices(invoiceID: any) {

    this.apiService.getData(invoiceID, "invoices/").then((result) => {
      this.invoices = result;
      for (var i=0; i<this.invoices.length; i++)
      {
          this.invoices[i].isArchieve = parseInt(this.invoices[i].isArchieve, 10);
      }
      this.dataTable = {
        headerRow: ['#', 'Due', 'Invoice Number', 'Customer', 'Issued', 'Amount', 'Status', 'Action'],
        footerRow: ['Row Number', 'Due Date', 'Description', 'Customer', 'ßIssue Date', 'Amount', 'Status', 'Action'],

        dataRows: this.invoices
      };

      //console.log(this.dataTable);
    });

  }

  selectAll() {
    let ck:number=0;
    if (!this.checkAll) {
       ck=1;
    } else {
        ck=0;
    }
    
    for (var i=0; i<this.invoices.length; i++)
    {
        this.invoices[i].isArchieve = ck;
    }
}

  callInvoice(invoiceType: any) {
    this.router.navigate(["/sales/invoice"], { queryParams: { invoiceType: invoiceType } });
  }


  ngAfterViewInit() {

    $('#datatablesIn').DataTable({
      'columnDefs': [ {
        'targets': [0,-1,-2], /* column index */
        'orderable': false /* true or false */
     }],
      "pagingType": "full",
      "pageLength": 25,
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
        lengthMenu: "_MENU_ records per page",
        zeroRecords: "None found",
        info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
        infoEmpty: "No record",
        infoFiltered: "(filtered from _MAX_ records)",
        "paginate": {
          "next": "<i class='fa fa-angle-right'></i>",
          "previous": "<i class='fa fa-angle-left'></i>",
          "first": "<i class='fa fa-angle-double-left'></i>",
          "last": "<i class='fa fa-angle-double-right'></i>",
        },
      }

    });
  }

  cloneInvoice(invoiceID: any) {
    this.apiService.postData(invoiceID, "invoice/clone/", null).then((result) => {
      console.log(result, "clone");
      let nInvoiceID = result;

      if (nInvoiceID > 0) {
        this.getInvoices('2/' + this.businessID + '/0');
      }
    });

  }

  getInvoiceDueDates() {
    this.apiService.getData(this.businessID + "/2", "invoiceDueDates/").then((result) => {
      this.invoiceDueDates = result;

      let vlTRY = [this.invoiceDueDates[0].TRY * 1, this.invoiceDueDates[1].TRY * 1, this.invoiceDueDates[2].TRY * 1, this.invoiceDueDates[3].TRY * 1];
      let vlUSD = [this.invoiceDueDates[0].USD * 1, this.invoiceDueDates[1].USD * 1, this.invoiceDueDates[2].USD * 1, this.invoiceDueDates[3].USD * 1];
      let vlEUR = [this.invoiceDueDates[0].EUR * 1, this.invoiceDueDates[1].EUR * 1, this.invoiceDueDates[2].EUR * 1, this.invoiceDueDates[3].EUR * 1];
      let vlGBP = [this.invoiceDueDates[0].GBP * 1, this.invoiceDueDates[1].GBP * 1, this.invoiceDueDates[2].GBP * 1, this.invoiceDueDates[3].GBP * 1];

      this.totals = {
        "totalTRY": this.invoiceDueDates[0].TRY * 1 + this.invoiceDueDates[1].TRY * 1 + this.invoiceDueDates[2].TRY * 1 + this.invoiceDueDates[3].TRY * 1,
        "totalUSD": this.invoiceDueDates[0].USD * 1 + this.invoiceDueDates[1].USD * 1 + this.invoiceDueDates[2].USD * 1 + this.invoiceDueDates[3].USD * 1,
        "totalEUR": this.invoiceDueDates[0].EUR * 1 + this.invoiceDueDates[1].EUR * 1 + this.invoiceDueDates[2].EUR * 1 + this.invoiceDueDates[3].EUR * 1,
        "totalGBP": this.invoiceDueDates[0].GBP * 1 + this.invoiceDueDates[1].GBP * 1 + this.invoiceDueDates[2].GBP * 1 + this.invoiceDueDates[3].GBP * 1
      };

      console.log(this.totals, "inv due dates");


      this.series.push(vlTRY);
      this.series.push(vlUSD);
      this.series.push(vlEUR);
      this.series.push(vlGBP);

      console.log(this.series, "series");
    });
  }

  uploadInvoice(invoiceID: any) {
    let event = new MouseEvent('click', { bubbles: false });
    this.invoiceID = invoiceID;
    this.fileInput.nativeElement.dispatchEvent(event);
  }

  onFileChange(event) {
    let reader = new FileReader();
    if (this.fileInput.nativeElement.files && this.fileInput.nativeElement.files.length > 0) {
      let file = this.fileInput.nativeElement.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {

        let file = {
          "fileName": this.fileInput.nativeElement.files[0].name,
          "fileType": this.fileInput.nativeElement.files[0].type,
          "table": "invoices",
          "userID": this.userID,
          "description": "Invoice",
          "file": reader.result.toString().split(",")[1]
        };

        this.apiService.postData("invoices/" + this.invoiceID, "files/upload/", JSON.stringify(file)).then((result) => {
          console.log(result);
        });
        /*
           console.log(this.fileInput.nativeElement.files[0].name);
         console.log(this.fileInput.nativeElement.files[0].type);
         console.log(reader.result.toString().split(",")[1]);
        */
      };
    }
  }

  openModal(invoiceID: any, index: any) {
    console.log(invoiceID, "invoice ID");
    let modalRef = this.modalService.open(InvoicePaymentComponent, { "backdrop": "static" });
    modalRef.componentInstance.title = 'Payment';
    //modalRef.componentInstance.ID = invoiceID;
    modalRef.componentInstance.invoiceID = invoiceID;
    modalRef.componentInstance.paymentType = "Record";

    modalRef.result.then((data) => {
      // on close
      console.log(data, "result data");
      if (data > 0) {
        this.invoices[index].paymentID = data;
      }
    }, (reason) => {
      // on dismiss
    });


  }

  approveInvoice(invoiceID: any, index: any) {
    let modalRef = this.modalService.open(ApproveModalComponent, { "backdrop": "static" });
    modalRef.componentInstance.invoiceID = invoiceID;

    modalRef.result.then((data) => {
      // on close
      console.log(data, "result data");
      if (data) {
        if (data.statusID > 0) {
          this.invoices[index].statusID = data.statusID;
          this.invoices[index].status = data.status;
        }
      }
    }, () => {
      // on dismiss
    });

  }

  openAccountingModal(invoiceID: any, index: any) {
    console.log(invoiceID, "invoice ID");
    let modalRef = this.modalService.open(AccountingComponent, { "backdrop": "static" });
    modalRef.componentInstance.invoiceID = invoiceID;
    modalRef.result.then((data) => {
      // on close
      //console.log(data, "result data");
      if (data.account != "") {
        this.invoices[index].account = data.account;
        //console.log(data.account, "result data account");
      }
    }, (reason) => {
      // on dismiss
    });
  }

  printInvoices() {
    let ci:any = this.invoices.filter(x=> x.isArchieve == 1);
    ci.forEach(element => {
            printJS({printable:element.pdfPath, type:'pdf', showModal:true});
    });
    
}

sendArchieves() {
    let ci:any = this.invoices.filter(x=> x.isArchieve == 1);
    ci.forEach((element,index) => {
        this.apiService.postData (element.ID+"/1", "/invoice/archieve/",null).then((result) => {
            console.log(result,index);
            this.invoices.splice(this.invoices.findIndex(e => e.ID === element.ID),1);
         });       
    });

}

}
