import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicesInComponent } from './invoices-in.component';

describe('InvoicesInComponent', () => {
  let component: InvoicesInComponent;
  let fixture: ComponentFixture<InvoicesInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicesInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicesInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
