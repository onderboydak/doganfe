import { Component, AfterViewInit, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements AfterViewInit {
  public loading: boolean = false;
  constructor(public router: Router, public translate: TranslateService) {
   
    this.translate.addLangs(["EN", "TR"]);
   // this.translate.setDefaultLang('tr');
    
    let browserLang = this.translate.getBrowserLang();
    //let browserLang = "tr";
    let lang = (localStorage.getItem("lang")!="") ? localStorage.getItem("lang") : browserLang;
    this.translate.setDefaultLang(lang);
    console.log(lang,"dil2");
    this.translate.use(browserLang.match(/EN|TR/) ? lang : 'TR');

    if (!localStorage.getItem("userID")) {
      this.router.navigate(["/pages/login"]);
    }

  }

  ngOnInit() {
    console.log("ngOnInit start");
    this.loading = true;
  }

  ngOnDestroy() {
    console.log("ngOnDestroy start");
    this.loading = false;
  }

  ngAfterViewInit() {
    console.log("ngAfterViewInit start");
    setTimeout(()=>{
      this.loading = false;
    },3000);
  }
  

}
