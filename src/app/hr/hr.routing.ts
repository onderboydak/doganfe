import { Routes } from '@angular/router';

import { MemberComponent } from './member/member.component';
import { PayrollComponent } from './payroll/payroll.component';
import { TeamComponent } from './team/team.component';
import { AuthGuardService as AuthGuard } from '../auth/auth-guard.service';


export const HrRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'team',
            component: TeamComponent,
            canActivate: [AuthGuard] 
    }]},   
    
    {
        path: '',
        children: [{
            path: 'member',
            component: MemberComponent,
            canActivate: [AuthGuard] 
        }]}, 
    {
        path: 'member/:id',
        component: MemberComponent,
        canActivate: [AuthGuard] 
    },
    
    {
        path: '',
        children: [{
            path: 'payroll',
            component: PayrollComponent,
            canActivate: [AuthGuard] 
    }]},
    {
        path: 'payroll/:id',
        component: PayrollComponent,
        canActivate: [AuthGuard] 
    },
];


