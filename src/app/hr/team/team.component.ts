import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { Router } from '@angular/router';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MemberComponent } from '../member/member.component';


declare var $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: [];
}

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  public dataTable: DataTable;
  public members: any = [];
  businessID:any;
  userID:any;
  userType:any;

  constructor(public apiService: ApiService,public router : Router,private modalService: NgbModal) { 
    this.userType=localStorage.getItem("userType");
   
    /*
    this.dataTable = {
      headerRow: ['#', 'Name', 'LastName', 'TCKN', 'Title', 'Department', 'cellPhone','Birthday','Salary'],
      footerRow: ['Row Number', 'Name', 'LastName', 'TCKN', 'Title', 'Department', 'cellPhone','Birthday','Salary'],
      dataRows: []
    };
    */
  }

  ngOnInit() {
    this.businessID=localStorage.getItem("businessID");
    this.userID=localStorage.getItem("userID");
    
    this.getMembers(this.businessID+'/0');
  }

  getMembers(employeeID: any) {

    this.apiService.getData(employeeID, "members/").then((result) => {
      this.members = result;

      this.dataTable = {
        headerRow: ['#', 'Name', 'LastName', 'TCKN', 'Title', 'Department', 'cellPhone','Birthday','Salary'],
        footerRow: ['Row Number', 'Name', 'LastName', 'TCKN', 'Title', 'Department', 'cellPhone','Birthday','Salary'],
        dataRows: this.members
      };

      //console.log(this.dataTable);
    });

  }

  /*
  callInvoice(invoiceType:any) {
    this.router.navigate(["/sales/invoice"],{ queryParams: { invoiceType: invoiceType } });
  }

*/
  ngAfterViewInit() {

    $('#datatablesIn').DataTable({
      "pagingType": "full",
      "pageLength": 25,
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
        lengthMenu: "_MENU_ records per page",
        zeroRecords: "None found",
        info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
        infoEmpty: "No record",
        infoFiltered: "(filtered from _MAX_ records)",
        "paginate": {
          "next": "<i class='fa fa-angle-right'></i>",
          "previous": "<i class='fa fa-angle-left'></i>",
          "first": "<i class='fa fa-angle-double-left'></i>",
          "last": "<i class='fa fa-angle-double-right'></i>",
        },
      }

    });
  }
/* 
 callMember (memberID:any){
    this.router.navigate(["/hr/member/"+memberID]);
 } */

}
