import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HrRoutes } from './hr.routing';

import { TranslateModule } from '@ngx-translate/core';

import { MemberComponent } from './member/member.component';
import { PayrollComponent } from './payroll/payroll.component';
import { TeamComponent } from './team/team.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(HrRoutes),
        FormsModule,
        ReactiveFormsModule,
        TranslateModule
    ],
    declarations: [
        MemberComponent,
        PayrollComponent,
        TeamComponent
    ]
})

export class HrModule {}
