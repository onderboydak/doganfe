import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadComponent } from 'app/pages/upload/upload.component';

declare var $: any;

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})
export class MemberComponent implements OnInit {
  @ViewChild('fileInput') fileInput:ElementRef;

  memberID: any;
  employee: any = [];
  businessID:any;
  userType:any;
  files:any=[];


  constructor(public apiService: ApiService, private activeRoute: ActivatedRoute,public router: Router, private modalService: NgbModal) { 

    const routeParams = this.activeRoute.snapshot.params;
    this.memberID = routeParams.id;

    console.log(this.memberID,"memberID");
    this.businessID=localStorage.getItem("businessID");
    this.userType=localStorage.getItem("userType");

    console.log(this.userType,"userType");
    
    if (this.memberID>0) {
       this.getMember(this.memberID);
       this.getFiles(this.memberID);
    } else {
      this.employee={"ID":"0","TCKN":"","name":"","lastName":"","email":"","homePhone":"","cellPhone":"","birthday":"","title":"","startDate":"","salary":"0","isNet":"1","school":"","department":"","socialSecDoc":"","SSKNo":"","idDoc":"","gradDoc":"","asID":this.businessID,"userID":"0","isMarried":"0","numberOfKids":"0","isSpouseWorking":"0","payrollInfo":""};
    }


  }

  getFiles (memberID:any) {
    this.apiService.getData(memberID+"/members", "files/").then((result) => {
      this.files = result;
      console.log(this.files,"files");

    });
  }
  
  getMember(memberID:any) {
    if (this.memberID>0) {
      this.apiService.getData(this.businessID+"/"+memberID, "members/").then((result) => {
        this.employee = result[0];
        console.log(this.employee,"member");
        $(".selectpicker").selectpicker('val',this.employee.numberOfKids);

      });
    } else {
      this.employee.ID=0;
    }
  }

  onInChange(ct:any) {
      this.employee.isNet=ct;
  }

  addFile() {

        let modalRef = this.modalService.open(UploadComponent, { "backdrop": "static" });
        modalRef.componentInstance.ID = this.memberID;
        modalRef.componentInstance.table = "members";

        modalRef.result.then((data) => {
            // on close
            console.log(data, "result data");
            if (data > 0) {
                //this.invoices[index].paymentID = data;
            }
        }, () => {
            // on dismiss
        });

  }

  deleteFile(fileID:any,index:any) {
    if (confirm("Are you sure ?")) {
      this.apiService.deleteData(fileID, "files/delete/").then((result) => {
          console.log(result);
          this.files.splice(index, 1);
      });
  };
  }
  

  onImChange(ct:any) {
      this.employee.isMarried=ct;
  }

  onIsChange(ct:any) {
      this.employee.isSpouseWorking=ct;
  }

  ngOnInit() {
    this.spInit();
  }

  clickCancel() {
    this.router.navigateByUrl("/hr/team");
  }

  saveEmployee() {
    console.log(this.employee,"employee");
    this.employee.asID=this.businessID;
    this.employee.startDate=$(".pickerStartDate").val();
    this.employee.birthday=$(".pickerBirthday").val();
    this.apiService.postData(this.memberID, "member/save/", JSON.stringify(this.employee)).then((result) => {
      console.log(result);
      this.router.navigateByUrl("/hr/team");


    });
  }

  deleteEmployee() {
    this.apiService.deleteData(this.memberID, "/member/delete/").then((result) => {
      console.log(result);
      this.router.navigateByUrl("/hr/team");
    });
  }

  spInit() {
    $('.datetimepicker').datetimepicker({
      locale: 'tr',
      format: 'DD-MM-YYYY',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });


  }

}
