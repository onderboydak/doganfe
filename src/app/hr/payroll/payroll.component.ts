import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute } from '@angular/router';

declare var $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: [];
}

@Component({
  selector: 'app-payroll',
  templateUrl: './payroll.component.html',
  styleUrls: ['./payroll.component.scss']
})
export class PayrollComponent implements OnInit {
  public dataTable: DataTable;

  payroll:any = [];
  employeeID:any;
  businessID:any;
  ep:any=[];
  userType:any;
  semployeeID:any;

  constructor(public apiService: ApiService, private activeRoute: ActivatedRoute) {
    this.semployeeID=localStorage.getItem("employeeID");
    this.businessID=localStorage.getItem("businessID");
    this.userType=localStorage.getItem("userType");

    const routeParams = this.activeRoute.snapshot.params;
    this.employeeID = routeParams.id;
    if (this.userType==1 || this.userType==3 || this.employeeID==this.semployeeID) {
      this.getPayroll();
    }
   }

  ngOnInit() {

  }

  getPayroll() {
    var months = new Array(12);
        months[0] = "January";
        months[1] = "February";
        months[2] = "March";
        months[3] = "April";
        months[4] = "May";
        months[5] = "June";
        months[6] = "July";
        months[7] = "August";
        months[8] = "September";
        months[9] = "October";
        months[10] = "November";
        months[11] = "December";


    this.apiService.getData("0/"+this.employeeID, "members/").then((result) => {

      this.payroll = result[0];

      //console.log(this.payroll, 'payroll');

      let pi = this.payroll.payrollInfo;
      let piArray = pi.split(";");
      
      let rowc  =0;
      let mc=1;
      let eps:any=[];
      
      piArray.forEach(element => {
          rowc++;
          
          if (rowc==1) {
            eps=[];
            eps["Month"] = mc;
            eps["MonthName"] = months[mc-1];
            eps["Salary"]=this.payroll.salary;
          }
          eps["A"+rowc]=element;
          if (rowc==14) {
            rowc=0;
            mc++;
            this.ep.push(eps);
          }
          console.log(element, 'element');
      });
      console.log(this.ep);
      this.ep[12]["MonthName"]="Total";

      this.dataTable = {
        headerRow: ['#', 'Net', 'SSK Isci', 'Issizlik Isci', 'Aylik Gelir Vergisi', 'Damga Vergisi', 'Kumulatif Vergi Matrahi','Brut','Asgari Gecim Indirimi','Toplam Ele Gecen','SSK Isveren','Issizlik Isveren','Toplam Maliyet'],
        footerRow: ['#', 'Net', 'SSK Isci', 'Issizlik Isci', 'Aylik Gelir Vergisi', 'Damga Vergisi', 'Kumulatif Vergi Matrahi','Brut','Asgari Gecim Indirimi','Toplam Ele Gecen','SSK Isveren','Issizlik Isveren','Toplam Maliyet'],
        dataRows: this.ep
      };

      //console.log(this.dataTable);
    });

  }
  ngAfterViewInit() {

    $('#datatables').DataTable({
      "pagingType": "full",
      "pageLength": 25,
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
        lengthMenu: "_MENU_ records per page",
        zeroRecords: "None found",
        info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
        infoEmpty: "No record",
        infoFiltered: "(filtered from _MAX_ records)",
        "paginate": {
          "next": "<i class='fa fa-angle-right'></i>",
          "previous": "<i class='fa fa-angle-left'></i>",
          "first": "<i class='fa fa-angle-double-left'></i>",
          "last": "<i class='fa fa-angle-double-right'></i>",
        },
      }

    });
  }

  //https://www.verginet.net/maas2009.aspx?tip=1&yil=2019&mDurum=1&eCalisiyormu=1&cocukSayisi=&sgkDis=true&m1=13.000&m2=13.000&m3=13.000&m4=13.000&m5=13.000&m6=13.000&m7=13.000&m8=13.000&m9=13.000&m10=13.000&m11=13.000&m12=13.000
  //https://www.verginet.net/maas2009.aspx?tip=1&yil=2019&mDurum=1&eCalisiyormu=1&cocukSayisi=2&sgkDis=true&m1=13.000&m2=13.000&m3=13.000&m4=13.000&m5=13.000&m6=13.000&m7=13.000&m8=13.000&m9=13.000&m10=13.000&m11=13.000&m12=13.000
}
