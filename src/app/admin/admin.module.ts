import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminRoutes } from './admin.routing';

import { TranslateModule } from '@ngx-translate/core';
import { CompanyComponent } from './company/company.component';
import { CompaniesComponent } from './companies/companies.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { XsltfilesComponent } from './xsltfiles/xsltfiles.component';
import { XsltfileComponent } from './xsltfile/xsltfile.component';
import { BannersComponent } from './banners/banners.component';
import { BannerComponent } from './banners/banner/banner.component';
import { ContractsComponent } from './contracts/contracts.component';
import { ContractComponent } from './contracts/contract/contract.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminRoutes),
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        CKEditorModule
    ],
    declarations: [
       
    CompanyComponent,
       
    CompaniesComponent,
       
    UsersComponent,
       
    UserComponent,
       
    XsltfilesComponent,

    BannersComponent,
       
    XsltfileComponent,
       
    BannerComponent,
       
    ContractsComponent,
       
    ContractComponent],
    schemas:[NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA]
})

export class AdminModule {}
