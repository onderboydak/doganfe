import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';

declare var $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: [];
}

@Component({
  selector: 'app-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.scss']
})
export class ContractsComponent implements OnInit {
  public dataTable: DataTable;
  contracts:any=[];
  constructor(public apiService: ApiService) { }

  ngOnInit() {
  this.getContracts();
  }

  getContracts() {
    this.apiService.getData("", "contracts").then((result) => {
        this.contracts = result;
        //console.log(this.contracts,"contracts");
        this.dataTable = {
            headerRow: ['#', 'Title', 'Date','isActive'],
            footerRow: ['#', 'Title', 'Date','isActive'],
            dataRows: this.contracts
        };

    });
  }
}
