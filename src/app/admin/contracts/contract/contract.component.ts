import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { DatePipe } from '@angular/common';


declare var $: any;

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent implements OnInit {
  public contract: any = [];
  contractID:any;
  public ClassicEditorBuild = ClassicEditorBuild;

  constructor(public apiService: ApiService,private activeRoute: ActivatedRoute,public router: Router,private datePipe: DatePipe) {
    const routeParams = this.activeRoute.snapshot.params;
    this.contractID = routeParams.id;

    let now = new Date();
    this.contract.contractDate=this.datePipe.transform(now, "dd-MM-yyyy");  
   }

  ngOnInit() {
    if (this.contractID>0) {
      this.getContract();
    } else {
      this.contract={"ID":0,"contractTitle":"","contract":"","contractDate":this.contract.contractDate,"userID":localStorage.getItem("userID"),"isActive":"0"};

    }

    $('.dates').datetimepicker({
      locale: 'tr',
      format: 'DD-MM-YYYY',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });
  }

  getContract() {
    this.apiService.getData(this.contractID, "contracts/").then((result) => {
        this.contract = result[0];
        console.log(this.contract,"contract");
    });
  }

  clickCancel() {
    this.router.navigateByUrl("/admin/contracts");
  }

  deleteContract() {
    if (confirm("Are you sure ?")) {
      this.apiService.deleteData(this.contractID, "/contract/delete/").then((result) => {     
        this.router.navigateByUrl("/admin/contracts");
      });
    }
  }

  saveContract() {
    console.log(this.contract,"contract");
    this.contract.contractDate = $('#contractDate').val();
    this.contract.isActive = (this.contract.isActive==true) ? 1 : 0;
    this.apiService.postData(this.contractID, "/contract/save/", JSON.stringify(this.contract)).then((result) => {
      this.router.navigateByUrl("/admin/contracts");
    });
  }
}
