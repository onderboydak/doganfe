import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';

declare var $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: [];
}


@Component({
  selector: 'app-banners',
  templateUrl: './banners.component.html',
  styleUrls: ['./banners.component.scss']
})
export class BannersComponent implements OnInit {
  public dataTable: DataTable;
  public banners: any = [];
  constructor(public apiService: ApiService) { }

  ngOnInit() {
    console.log("init");
    this.getBanners();
  }
  getBanners() {
    this.apiService.getData("", "banners").then((result) => {
        this.banners = result;
        console.log(this.banners,"banners");
        this.dataTable = {
            headerRow: ['#', 'Banner', 'Start Time','End Time'],
            footerRow: ['#', 'Banner', 'Start Time','End Time'],
            dataRows: this.banners
        };

    });
  }

}
