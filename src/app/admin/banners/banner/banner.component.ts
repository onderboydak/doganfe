import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  public banner: any = [];
  bannerID:any;
  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(public apiService: ApiService,private activeRoute: ActivatedRoute,public router: Router) { 
    const routeParams = this.activeRoute.snapshot.params;
    this.bannerID = routeParams.id;

  }

  ngOnInit() {
    if (this.bannerID>0) {
      this.getBanner();
    } else {
      this.banner={"ID":0,"banner":"","photo":0,"startTime":"","endTime":"","fileName":""};
    }

    $('.dates').datetimepicker({
      locale: 'tr',
      format: 'DD-MM-YYYY HH:mm',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });
  }

  getBanner() {
    this.apiService.getData(this.bannerID, "banners/").then((result) => {
        this.banner = result[0];
        console.log(this.banner,"banner");
    });
  }

  clickCancel() {
    this.router.navigateByUrl("/admin/banners");
  }

  deleteBanner() {
    if (confirm("Are you sure ?")) {
      this.apiService.deleteData(this.bannerID, "/banner/delete/").then((result) => {     
        this.router.navigateByUrl("/admin/banners");
      });
    }
  }

  saveBanner() {
    this.apiService.postData(this.bannerID, "/banner/save/", JSON.stringify(this.banner)).then((result) => {
      this.router.navigateByUrl("/admin/banners");
    });
  }

  uploadBanner(bannerID: any) {
    let event = new MouseEvent('click', { bubbles: false });
    this.bannerID = bannerID;
    this.fileInput.nativeElement.dispatchEvent(event);
  }

  onFileChange(event) {
    let reader = new FileReader();
    if (this.fileInput.nativeElement.files && this.fileInput.nativeElement.files.length > 0) {
      let file = this.fileInput.nativeElement.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {

        let file = {
          "fileName": this.fileInput.nativeElement.files[0].name,
          "fileType": this.fileInput.nativeElement.files[0].type,
          "table": "banner",
          "userID": localStorage.getItem("userID"),
          "description": "Banner",
          "file": reader.result.toString().split(",")[1]
        };

        this.apiService.postData("banners/" + this.banner.ID, "files/upload/", JSON.stringify(file)).then((result) => {
          console.log(result);
          if (this.bannerID>0) {
            this.getBanner();
          }
        });
       
      };
    }
  }

}
