import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XsltfilesComponent } from './xsltfiles.component';

describe('XsltfilesComponent', () => {
  let component: XsltfilesComponent;
  let fixture: ComponentFixture<XsltfilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XsltfilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XsltfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
