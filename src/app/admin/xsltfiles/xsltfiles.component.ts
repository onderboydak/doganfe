import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';

declare var $: any;

declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows: [];
}

@Component({
  selector: 'app-xsltfiles',
  templateUrl: './xsltfiles.component.html',
  styleUrls: ['./xsltfiles.component.scss']
})
export class XsltfilesComponent implements OnInit {
  public dataTable: DataTable;
  public xsltfiles: any = [];
  businessID: any;

  constructor(public apiService: ApiService) { }

  ngOnInit() {
    this.businessID = localStorage.getItem("businessID");
    this.getXsltfiles();
  }

  getXsltfiles() {
    let userType:number = parseInt(localStorage.getItem("userType"));
    let bsID:any=this.businessID;
    if (userType==1 || userType==100) {
        bsID=0;
    }

    this.apiService.getData(bsID, "xsltfiles/").then((result) => {
        this.xsltfiles = result;

        this.dataTable = {
            headerRow: ['#', 'Supplier', 'Template', 'Xsltfile'],
            footerRow: ['#', 'Supplier', 'Template', 'Xsltfile'],
            dataRows: this.xsltfiles
        };

        console.log(this.dataTable);
    });
  }

  ngAfterViewInit() {

    $('#datatables').DataTable({
        "pagingType": "full",
        "pageLength": 25,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Search records",
            lengthMenu: "_MENU_ records per page",
            zeroRecords: "None found",
            info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
            infoEmpty: "No record",
            infoFiltered: "(filtered from _MAX_ records)",
            "paginate": {
                "next": "<i class='fa fa-angle-right'></i>",
                "previous": "<i class='fa fa-angle-left'></i>",
                "first": "<i class='fa fa-angle-double-left'></i>",
                "last": "<i class='fa fa-angle-double-right'></i>",
            },
        }

    });


}


}
