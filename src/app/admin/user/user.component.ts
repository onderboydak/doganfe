import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


declare var $: any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public user: any = [];
  businessID: any;
  userID: any;
  userForm: FormGroup;
  userTypes:any=[];
  ownerID:any;
  userType:any;


  constructor(public apiService: ApiService, private activeRoute: ActivatedRoute, public router: Router, private fb: FormBuilder) {
    const routeParams = this.activeRoute.snapshot.params;
    this.userID = routeParams.id;
    this.businessID = localStorage.getItem("businessID");
    this.ownerID = localStorage.getItem("ownerID");
    this.userType=localStorage.getItem("userType");
    
    this.user = {
      "ID": 0, "email": "", "password": "", "userType": 0, "firstName": "","clang":"tr",
      "lastName": "", "phone": "","ownerID":this.ownerID
    };

    this.userForm = this.fb.group({
      email: ['', Validators.required],
      password: [],
      userType: [],
      firstName: [],
      lastName: [],
      phone: [],
      clang:[]
    });

  }

  ngOnInit() {
    if (this.userID>0) {
      this.getUser(); 
    }
    this.getUserTypes();
    this.resolveAfter2Seconds(50).then(value => {
      this.spInit();
      $(".selectuserType").selectpicker('refresh');

    });
  }

  getUser() {
    this.apiService.getData(this.businessID + "/" + this.userID, "users/").then((result) => {
      this.user = result[0];
      this.getUserAccountSuppliers();
     

    });
  }

  getUserAccountSuppliers() {
    //let localUserID = localStorage.getItem("userID");
    let ownerID = localStorage.getItem("ownerID");

    this.apiService.getData(ownerID + "/" + this.userID, "userAccountingSuppliers/").then((result) => {
      this.user.AS = result;
      console.log(this.user.AS,"useras");


    });
  }

  getUserTypes () {
    this.apiService.getData("", "userTypes").then((result) => {
      this.userTypes = result;
      $(".selectuserType").selectpicker();
     

    });
  }

  get f() {
    return this.userForm.controls;
  }

  saveUser() {
    console.log(this.userForm.invalid);

    if (this.userForm.invalid) {
      return;
    }

    this.user.clang = $(".selectclang").selectpicker('val');
    this.apiService.postData(this.user.ID, "user/save/", JSON.stringify(this.user)).then((result) => {
      console.log(result);
      this.router.navigateByUrl("/admin/users");

    });
  }

  checkAS (asID:any,ind:any) {
      this.user.AS[ind].uaID=this.user.AS[ind].uaID>0 ? 0 : 1;
  }

  deleteUser() {
    this.apiService.deleteData(this.user.ID, "user/delete/").then((result) => {

      this.router.navigateByUrl("/admin/users");

    });
  }

  clickCancel() {
    this.router.navigateByUrl("/admin/users");
  }

  spInit() {
    console.log("Init Select");
    console.log( this.user.clang,"user language");
    $(".selectuserType").selectpicker('refresh');
    $(".selectclang").selectpicker('val', this.user.clang);
    //$(".selectuserType").selectpicker('val', this.user.userType);
  }

  resolveAfter2Seconds(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 1000);
    });
  }

}
