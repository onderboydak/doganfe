import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XsltfileComponent } from './xsltfile.component';

describe('XsltfileComponent', () => {
  let component: XsltfileComponent;
  let fixture: ComponentFixture<XsltfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XsltfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XsltfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
