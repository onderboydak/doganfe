import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-xsltfile',
  templateUrl: './xsltfile.component.html',
  styleUrls: ['./xsltfile.component.scss']
})
export class XsltfileComponent implements OnInit {
  xsltfile: any = [];
  companies: any = [];
  xsltID: any;
  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(public apiService: ApiService, private activeRoute: ActivatedRoute,public router: Router) {
    const routeParams = this.activeRoute.snapshot.params;
    this.xsltID = routeParams.id;
  }

  ngOnInit() {
    this.getCompanies();
    console.log(this.xsltID, "xsltid");
    if (this.xsltID > 0) {
      this.getXsltfiles();
    } else {
      this.xsltfile={ "ID": 0, "template": "", "xsltfile": 0, "asID": 0};
    }

  }
  getCompanies() {
    this.apiService.getData("0", "accountingSuppliers/").then((result) => {
      this.companies = result;
      setTimeout(() => {
        $(".selectCompany").selectpicker('refresh');
      }, 500);
    });
  }
  getXsltfiles() {
    this.apiService.getData( "0/" + this.xsltID, "xsltfiles/").then((result) => {
      this.xsltfile = result[0];
      console.log(this.xsltfile,"xslt file");
      $(".selectCompany").selectpicker('val', this.xsltfile.asID);

    });
  }
  uploadXSLT(xsltID: any) {
    let event = new MouseEvent('click', { bubbles: false });
    this.fileInput.nativeElement.dispatchEvent(event);
  }

  clickDelete(content: any) {
    if (confirm("Are you sure ?")) {
      this.apiService.deleteData(this.xsltID, "/xsltfile/delete/").then((result) => {
        console.log(result);
        this.router.navigateByUrl("/admin/xsltfiles");
      });
    }

  }


  clickCancel() {
      this.router.navigateByUrl("/admin/xsltfiles");
    
  }

  clickSave() {
    this.apiService.postData(this.xsltID, "/xsltfile/save/", JSON.stringify(this.xsltfile)).then((result) => {
      console.log(result);
      this.router.navigateByUrl("/admin/xsltfiles");
    });
  }

  onFileChange(event) {
    let reader = new FileReader();
    if (this.fileInput.nativeElement.files && this.fileInput.nativeElement.files.length > 0) {
      let file = this.fileInput.nativeElement.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {

        let file = {
          "fileName": this.fileInput.nativeElement.files[0].name,
          "fileType": this.fileInput.nativeElement.files[0].type,
          "table": "xsltfiles",
          "userID": localStorage.getItem("userID"),
          "description": "xsltfile",
          "file": reader.result.toString().split(",")[1]
        };

        
      
        this.apiService.postData("xsltfiles/" + this.xsltID, "files/upload/", JSON.stringify(file)).then((result) => {
          console.log(result);
          this.xsltfile.xsltfile=result;
        });
        /*
           console.log(this.fileInput.nativeElement.files[0].name);
         console.log(this.fileInput.nativeElement.files[0].type);
         console.log(reader.result.toString().split(",")[1]);
        */
      };
    }
  }



}
