import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare var $: any;


@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  companyID:any;
  company:any=[];
  userType:any;
  companyForm: FormGroup;
  channels:any=[];
  distributors:any=[];
  sellers:any=[];

  constructor(public apiService: ApiService, private activeRoute: ActivatedRoute,public router: Router,private fb: FormBuilder) {
    const routeParams = this.activeRoute.snapshot.params;
    this.companyID = routeParams.id;
   
    this.company={ID:0,supplierShortName:"",supplier:"",supplierType:1,tckVKNNo:"",address:"",vatNumber:"",vatAuthority:"",
    commerceAuthorityNo:"",phone:"",email:"",supplierContact:"",supplierContactEmail:"",buildingNumber:"",district:"",city:"",
    country:"",invoicePreFix:"",apiUrl:"",apiUser:"",apiPass:"",notes:"",ownerID:0,autoInvoiceNumber:0,invoiceNumber:""  };

    this.userType=localStorage.getItem("userType");


    // Validation

    this.companyForm = this.fb.group({
      supplierShortName: ['', Validators.required],
      supplier:[],
      supplierType:[],
      tckVKNNo:[],
      address:[],
      vatNumber:[],
      vatAuthority:[],
      commerceAuthorityNo:[],
      phone:[],
      email:[],
      supplierContact:[],
      supplierContactEmail:[],
      buildingNumber:[],
      district:[],
      city:[],
      country:[],
      invoicePreFix:[],
      autoInvoiceNumber:[],
      invoiceNumber:[],
      apiUrl:[],
      apiUser:[],
      apiPass:[],
      notes:[]
    });


   }

  ngOnInit() {
    if (this.companyID>0) {
      this.getCompany();
      
    } 
  }

  getacwithsupplierType(supplierType,parentID) {
    this.apiService.getData(parentID+"/"+supplierType, "accountingSuppliersWithSupplierType/").then((result) => {
      switch (supplierType) {
        case 2:
          this.channels=result;
          this.resolveAfter2Seconds(10).then(value => {
            $(".selectChannel").selectpicker('refresh');
            $(".selectChannel").selectpicker('val', this.company.channelID);
          });
          break;
        case 3:
          this.distributors=result;
          this.resolveAfter2Seconds(10).then(value => {
            $(".selectDistributor").selectpicker('refresh');
            $(".selectDistributor").selectpicker('val', this.company.distributorID);
          });
          break;
        case 4:
          this.sellers=result;
          this.resolveAfter2Seconds(10).then(value => {
            $(".selectSeller").selectpicker('refresh');
            $(".selectSeller").selectpicker('val', this.company.sellerID);
          });
          break;
      }
    });
  }

  spInit () {
    $(".selectpicker").selectpicker();
    //$(".selectChannel").selectpicker('val', this.company.channelID);
    //$(".selectDistributor").selectpicker('val', this.company.distributorID);
    //$(".selectSeller").selectpicker('val', this.company.sellerID);
    $(".selectSupplierType").selectpicker('val', this.company.supplierType);
  }


  getCompany () {
    this.apiService.getData(this.companyID, "accountingSuppliers/").then((result) => {
      this.company = result[0];
      this.spInit();
      //this.resolveAfter2Seconds(20).then(value => {
         this.getacwithsupplierType(2,0);
      //});
      this.resolveAfter2Seconds(20).then(value => {
        this.getacwithsupplierType(3,this.company.channelID);
      });
      this.resolveAfter2Seconds(20).then(value => {
        this.getacwithsupplierType(4,this.company.distributorID);
      });
    });
  }

  resolveAfter2Seconds(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 2000);
    });
  }

  ngAfterViewInit() {
    $(".selectpicker").selectpicker();
  }
  clickCancel() {
    this.router.navigateByUrl("/admin/companies");

  }

  saveCompany() {
    console.log(this.companyForm.invalid);
   
    if (this.companyForm.invalid) {
      return;
    }
   
    this.apiService.postData(this.company.ID, "accountingSuppliers/save/", JSON.stringify(this.company)).then((result) => {
      console.log(result);
      this.router.navigateByUrl("/admin/companies");
      
    });
  }

  deleteCompany() {
    this.apiService.deleteData(this.company.ID, "accountingSuppliers/delete/").then((result) => {
        this.router.navigateByUrl("/admin/companies");
     
    });
  }

}
