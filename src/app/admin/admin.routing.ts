import { Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../auth/auth-guard.service';
import { CompanyComponent } from './company/company.component';
import { CompaniesComponent } from './companies/companies.component';
import { UserComponent } from './user/user.component';
import { UsersComponent } from './users/users.component';
import { XsltfilesComponent } from './xsltfiles/xsltfiles.component';
import { XsltfileComponent } from './xsltfile/xsltfile.component';
import { BannersComponent } from './banners/banners.component';
import { BannerComponent } from './banners/banner/banner.component';
import { ContractsComponent } from './contracts/contracts.component';
import { ContractComponent } from './contracts/contract/contract.component';


export const AdminRoutes: Routes = [ 
{
    path: '',
    children: [ {
        path: 'company',
        component: CompanyComponent,
        canActivate: [AuthGuard] 
}]},{
    path: 'company/:id',
    component: CompanyComponent
},
{
    path: '',
    children: [ {
        path: 'companies',
        component: CompaniesComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: '',
    children: [ {
        path: 'user',
        component: UserComponent,
        canActivate: [AuthGuard] 
}]},{
    path: 'user/:id',
    component: UserComponent
},
{
    path: '',
    children: [ {
        path: 'users',
        component: UsersComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: '',
    children: [ {
        path: 'panelcompanies',
        component: CompaniesComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: '',
    children: [ {
        path: 'xsltfiles',
        component: XsltfilesComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: 'xsltfile/:id',
    component: XsltfileComponent
},
{
    path: '',
    children: [ {
        path: 'banners',
        component: BannersComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: 'banner/:id',
    component: BannerComponent
},
{
    path: '',
    children: [ {
        path: 'contracts',
        component: ContractsComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: 'contract/:id',
    component: ContractComponent
}
];


