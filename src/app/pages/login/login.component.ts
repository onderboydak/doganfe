import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'login-cmp',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
    test: Date = new Date();
    user: any = [];
    notLoggedIn: number = 0;
    action :any=[];
    showContract:boolean=false;
    contract:any=[];
    approved:boolean=false;

    @ViewChild('email') email: ElementRef;
    @ViewChild('password') password: ElementRef;


    constructor(public apiService: ApiService, public router: Router, public translate: TranslateService) { }

    checkFullPageBackgroundImage() {
        var $page = $('.full-page');
        var image_src = $page.data('image');

        if (image_src !== undefined) {
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
            $page.append(image_container);
        }
    };

    ngOnInit() {
        this.checkFullPageBackgroundImage();

        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    }

    login() {
        console.log("Login");
        this.apiService.getData(this.email.nativeElement.value + "/" + this.password.nativeElement.value, "users/login/").then((result) => {
            this.user = result;
            if (this.user.ID > 0) {
                this.apiService.getData(this.user.ID, "userRoles/").then((result) => {
                    this.action = result;
                    localStorage.setItem("userID", this.user.ID);
                    localStorage.setItem("ownerID", this.user.ownerID);
                    localStorage.setItem("userType", this.user.userType);
                    localStorage.setItem("action", JSON.stringify(this.action));
                    localStorage.setItem("employeeID", this.user.employeeID);
                    localStorage.setItem("businessID", this.user.asID);
                    localStorage.setItem("lang", this.user.clang);
                    this.translate.setDefaultLang(this.user.clang);
                    
                    this.translate.use(this.user.clang);

                    console.log(this.user, "user",this.action,"action");
                    this.notLoggedIn = 0;
                    
                    if (parseInt(this.user.contractID)!=parseInt(this.user.activeContract)) {
                        this.showContract=true;
                        this.apiService.getData(this.user.activeContract, "contracts/").then((result) => {
                                this.contract=result[0];
                        });

                    } else {
                        this.router.navigate(["/dashboard"]);
                    }
                });
            } else {
                this.notLoggedIn = 1;
            }

        });
    }

    saveContract() {
        this.user.contractID=this.user.activeContract;
        this.apiService.postData(this.user.ID, "user/save/", JSON.stringify(this.user)).then((result) => {
            this.router.navigate(["/dashboard"]);
          });
    }
}
