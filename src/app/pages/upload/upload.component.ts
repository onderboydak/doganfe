import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  @ViewChild('fileInput') fileInput:ElementRef;
  @ViewChild('description') description:ElementRef;
  @ViewChild('fileLink') fileLink:ElementRef;

  @Input() public ID: any;
  @Input() public table: any;
  userID:any;

  constructor(public apiService: ApiService, public activeModal: NgbActiveModal) { 
    this.userID = localStorage.getItem("userID");
  }
  ngOnInit() {
  }

  clickCancel() {
    this.activeModal.close();
  }

  addFile() {
    let event = new MouseEvent('click', {bubbles: false});
    this.fileInput.nativeElement.dispatchEvent(event);
  }

  onFileChange(event){
      this.fileLink.nativeElement.text=event.target.files[0].name;
  }

  saveFile() {
    let reader = new FileReader();
    if(this.fileInput.nativeElement.files && this.fileInput.nativeElement.files.length > 0) {
      let file = this.fileInput.nativeElement.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {

        let file ={
                    "fileName":this.fileInput.nativeElement.files[0].name,
                    "fileType":this.fileInput.nativeElement.files[0].type,
                    "table":this.table,
                    "userID":this.userID,
                    "description":this.description.nativeElement.value,
                    "file":reader.result.toString().split(",")[1]
        };

        this.apiService.postData(this.table+"/"+this.ID, "files/upload/", JSON.stringify(file)).then((result) => {
            console.log(result);
            document.location.reload();
          });
        console.log(this.fileInput.nativeElement.files[0].name);
        console.log(this.fileInput.nativeElement.files[0].type);
        console.log(reader.result.toString().split(",")[1]);
       
      };
    }
  }

}
