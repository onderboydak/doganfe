import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

//let apiURL = "https://localhost/efcrmapi/V1/";
let apiURL = "https://localhost/doganapi/V1/";
//let apiURL = "https://crmapi.efdigitalcodes.com/V1/";
//let apiUser = "onderboydak@eglencefabrikasi.com";
let apiUser = "onderboydak@eglencefabrikasi.com";
let apiPass = "1234";


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  postData(parameters: any, func: String,body :string) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders().set('Authorization', 'Basic ' + btoa(apiUser + ':' + apiPass));
       let url = apiURL + func + parameters ;
       console.log(body,"Body");
       this.http.post(url,body, { headers: headers }).subscribe(res => {
        console.info("postData", res);
        resolve(res);
      }, (err) => {
        resolve(err);
        //this.app.getActiveNav().push(ErrorPage);
        
      });
    });
  }
  

  getData(parameters: string, func: String) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders().set('Authorization', 'Basic ' + btoa(apiUser + ':' + apiPass));
      let url = apiURL + func + parameters;
      console.log(url);
      this.http.get(url, { headers: headers }).subscribe(res => {
        console.info("getData", res);
        resolve(res);
      }, (err) => {
        resolve(err);
        //this.app.getActiveNav().push(ErrorPage);
      });
    });
  }
  
  deleteData(parameters: string, func: String) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders().set('Authorization', 'Basic ' + btoa(apiUser + ':' + apiPass));
      let url = apiURL + func + parameters;
      console.log(url);
      this.http.delete(url, { headers: headers }).subscribe(res => {
        console.info("deleteData", res);
        resolve(res);
      }, (err) => {
        resolve(err);
        //this.app.getActiveNav().push(ErrorPage);
      });
    });
  }


  mysql2date(timestamp) {
    //function parses mysql datetime string and returns javascript Date object
    //input has to be in this format: 2007-06-05 15:26:02
    let regex = /^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9]) (?:([0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/;
    let parts = timestamp.replace(regex, "$1 $2 $3 $4 $5 $6").split(' ');
    return new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
  }
  
  jsonToTable (jsonValue:any) {
    for(let child of jsonValue){
      return child[0];
    };
  }
}
