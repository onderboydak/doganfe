import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute } from '@angular/router';
import { ExcelService } from 'app/providers/excel.service';
import { DatePipe } from '@angular/common';

declare var $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: [];
}

@Component({
  selector: 'app-aggr',
  templateUrl: './aggr.component.html',
  styleUrls: ['./aggr.component.scss']
})
export class AggrComponent implements OnInit {
  public dataTable: DataTable;
  businessID: any;
  customers: any = [];
  customerID: any;
  reports: any = [];

  invoiceStartDate: any;
  invoiceEndDate: any;
  invoiceCurrency:any=1;
  totals:any=[];


  constructor(private apiService: ApiService,private activeRoute: ActivatedRoute,private excelService:ExcelService,private datePipe: DatePipe) {
    const routeParams = this.activeRoute.snapshot.params;
    this.activeRoute.queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.customerID = +params['customerID'] || 0;
      });
    this.businessID = localStorage.getItem("businessID");
    let now = new Date();
    this.invoiceStartDate=this.datePipe.transform(now, "dd-MM-yyyy");
    this.invoiceEndDate=this.datePipe.transform(now, "dd-MM-yyyy");  

  }

  ngOnInit() {
    this.dataTable = {
      headerRow: ['#', 'Date', 'Invoice Number', 'Description', 'Customer', 'Amount1', 'vatAmount1', 'Amount2', 'vatAmount2'],
      footerRow: ['Row Number', 'Date', 'Invoice Number', 'Description', 'Customer', 'Amount1', 'vatAmount1', 'Amount2', 'vatAmount2'],
      dataRows: []
    };
    
    this.getCustomer(0);

    $('.datetimepicker').datetimepicker({
      locale: 'tr',
      format: 'DD-MM-YYYY',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });
  }

  excelExport() {
    var obj = this.reports;
    this.excelService.exportAsExcelFile(obj, 'aggr');
  }

  getCustomer(customerID: any) {
    this.apiService.getData(this.businessID + "/0/" + customerID, "customers/").then((result) => {
      console.log(result);
      this.customers = result;
      this.resolveAfter2Seconds(20).then(value => {
        this.spInit();
        $(".selectCustomer").selectpicker('refresh');
        if (this.customerID>0) {
          $(".selectCustomer").selectpicker('val', this.customerID);
        }
      });
    });
  }

  spInit() {
    console.log("Init Select");
    $(".selectCustomer").selectpicker();
  }

  resolveAfter2Seconds(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 2000);
    });
  }

  clickCancel() {

  }

  clickReport() {
    this.invoiceStartDate = $('.invoiceStartDate').val();
    this.invoiceEndDate = $('.invoiceEndDate').val();
    this.apiService.getData(this.businessID + "/" + this.customerID + "/" + this.invoiceStartDate + "/" + this.invoiceEndDate+'/'+this.invoiceCurrency, "aggr/").then((result) => {
      console.log(result);
      this.reports = result;

      let amount1:number=0;
      let amount1Vat:number=0;
      let amount2:number=0;
      let amount2Vat:number=0;

      this.reports.forEach(element => {
          amount1=amount1+(element.amount1*1);
          amount2=amount2+(element.amount2*1);
          amount1Vat=amount1Vat+(element.vatAmount1*1);
          amount2Vat=amount2Vat+(element.vatAmount2*1);

      });

      this.totals["amount1"]=amount1.toFixed(2);
      this.totals["amount2"]=amount2.toFixed(2);
      this.totals["amount1Vat"]=amount1Vat.toFixed(2);
      this.totals["amount2Vat"]=amount2Vat.toFixed(2);

      console.log(this.totals,"totals");

      this.dataTable = {
        headerRow: ['#', 'Date', 'Invoice Number', 'Description', 'Customer', 'Amount1', 'vatAmount1', 'Amount2', 'vatAmount2'],
        footerRow: ['Row Number', 'Date', 'Invoice Number', 'Description', 'Customer', 'Amount1', 'vatAmount1', 'Amount2', 'vatAmount2'],
        dataRows: this.reports
      };

    });
  }

  ngAfterViewInit() {
    $(".selectpicker").selectpicker();
    $(".selectCurrency").selectpicker('val', 1);

    $('#datatables').DataTable({
      "pagingType": "full",
      "pageLength": 25,
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
        lengthMenu: "_MENU_ records per page",
        zeroRecords: " ",
        info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
        infoEmpty: "No record",
        infoFiltered: "(filtered from _MAX_ records)",
        paginate: {
          "next": "<i class='fa fa-angle-right'></i>",
          "previous": "<i class='fa fa-angle-left'></i>",
          "first": "<i class='fa fa-angle-double-left'></i>",
          "last": "<i class='fa fa-angle-double-right'></i>",
        }
      }
    });
  }

}
