import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AggrComponent } from './aggr.component';

describe('AggrComponent', () => {
  let component: AggrComponent;
  let fixture: ComponentFixture<AggrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AggrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AggrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
