import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';

declare var $:any;

declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows:[];
}


@Component({
  selector: 'app-bankAccounts',
  templateUrl: './bankAccounts.component.html',
  styleUrls: ['./bankAccounts.component.scss']
})

export class BankAccountsComponent implements OnInit {

  public dataTable: DataTable;
  public customers: any = [];
  public balances: any = [];
  businessID:any;

  constructor(public apiService: ApiService) { 
   /*
    this.dataTable = {
        headerRow: ['#', 'Bank', 'Account', 'Amount'],
        footerRow: ['#', 'Bank', 'Account', 'Amount'],
        dataRows: []
    };
    */
    this.businessID=localStorage.getItem("businessID");
    this.getAccounts(this.businessID+"/0/1");


  }

  ngOnInit() {
   
}

getAccounts(accountID: any) {
    this.apiService.getData(accountID, "accountBalances/").then((result) => {
        this.customers = result;

        this.dataTable = {
            headerRow: ['#', 'Name', 'Bank', 'Account', 'IBAN', 'Currency', 'Balance', 'Actions'],
            footerRow: ['#', 'Name', 'Bank', 'Account', 'IBAN', 'Currency', 'Balance', 'Actions'],
            dataRows: this.customers
        };

        console.log(this.dataTable);
    });

}

getAccountBalances(accountID: any) {
    this.apiService.getData(accountID, "accountBalances/").then((result) => {
        this.balances = result;

        console.log(this.balances,'balances');
    });

}

  ngAfterViewInit(){

    $('#datatables').DataTable({
        "pagingType": "full",
        "pageLength": 25,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
        lengthMenu: "_MENU_ records per page",
        zeroRecords: "None found",
        info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
        infoEmpty: "No record",
        infoFiltered: "(filtered from _MAX_ records)",
                "paginate": {
                    "next": "<i class='fa fa-angle-right'></i>",
                    "previous": "<i class='fa fa-angle-left'></i>",
                    "first": "<i class='fa fa-angle-double-left'></i>",
                    "last": "<i class='fa fa-angle-double-right'></i>",
                  },
        }

    });
    
    /*
    var table = $('#datatables').DataTable();

    // Edit record
    table.on( 'click', '.edit', function () {
        var $tr = $(this).closest('tr');

        var data = table.row($tr).data();
        alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
    } );

    // Delete a record
    table.on( 'click', '.remove', function (e) {
        var $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
    } );

    //Like record
    table.on( 'click', '.like', function () {
        alert('You clicked on Like button');
    });
    */
}
}
