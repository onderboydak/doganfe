import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

declare var $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: [];
}

@Component({
  selector: 'app-balance-details',
  templateUrl: './balance-details.component.html',
  styleUrls: ['./balance-details.component.scss']
})
export class BalanceDetailsComponent implements OnInit {
  public dataTable: DataTable;
  @Input() public ID: any;
  @Input() public startDate: any;
  @Input() public endDate: any;
  @Input() public currency: any;

  invoiceStartDate: any;
  invoiceEndDate: any;
  invoiceCurrency: any = 1;
  totals: any = [];
  businessID:any;
  reports: any = [];

  constructor(public apiService: ApiService, public activeModal: NgbActiveModal) { 
    this.businessID = localStorage.getItem("businessID");
  }

  ngOnInit() {
    this.dataTable = {
      headerRow: ['Invoice Date', 'Description', 'Currency', 'Amount'],
      footerRow: ['Invoice Date', 'Description', 'Currency', 'Amount'],
      dataRows: []
    };

    this.report();

  }

  report() {
    this.invoiceStartDate = this.startDate;
    this.invoiceEndDate = this.endDate;
    this.invoiceCurrency=this.currency;

    this.apiService.getData(this.businessID + "/" + this.invoiceStartDate + "/" + this.invoiceEndDate + '/' + this.invoiceCurrency+"/"+this.ID, "balanceDetails/").then((result) => {
      console.log(result);
      this.reports = result;

      let amount1: number = 0;

      this.reports.forEach(element => {
        amount1 = amount1 + (element.amount * 1);

      });

      this.totals["amount1"] = amount1.toFixed(2);


      console.log(this.totals, "totals");

      this.dataTable = {
        headerRow: [ 'Invoice Date', 'Description', 'Currency', 'Amount'],
        footerRow: [ 'Invoice Date', 'Description', 'Currency', 'Amount'],
        dataRows: this.reports
      };

    });
  }

  ngAfterViewInit() {
  
    $('#datatablesd').DataTable({
      "pagingType": "full",
      "pageLength": 100,
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      paginate:false,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
        lengthMenu: "_MENU_ records per page",
        zeroRecords: " ",
        info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
        infoEmpty: "No record",
        infoFiltered: "(filtered from _MAX_ records)",
        paginate: {
          "next": "<i class='fa fa-angle-right'></i>",
          "previous": "<i class='fa fa-angle-left'></i>",
          "first": "<i class='fa fa-angle-double-left'></i>",
          "last": "<i class='fa fa-angle-double-right'></i>",
        }
      }
    });
  }

  clickCancel() {
    this.activeModal.close();
  }

}
