import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BabsComponent } from './babs.component';

describe('BabsComponent', () => {
  let component: BabsComponent;
  let fixture: ComponentFixture<BabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
