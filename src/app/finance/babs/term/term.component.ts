import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'app/providers/api.service';

declare var $: any;

@Component({
  selector: 'app-term',
  templateUrl: './term.component.html',
  styleUrls: ['./term.component.scss']
})
export class TermComponent implements OnInit {
  m:any;
  y:any;
  withMail:boolean=false;

  constructor(public activeModal: NgbActiveModal,private apiService: ApiService) { }

  ngOnInit() {
    $(".selectpicker").selectpicker();
  }

  clickCancel() {
    this.activeModal.close();
  }

  generateBABS() {
    var mail = (this.withMail==true) ? 1 : 0;
    this.apiService.postData(this.y+"/"+this.m+"/"+mail, "babs/", null).then((result) => {
      this.activeModal.close();
    });
  }


}
