import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ExcelService } from 'app/providers/excel.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TermComponent } from './term/term.component';

declare var $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: [];
}

@Component({
  selector: 'app-babs',
  templateUrl: './babs.component.html',
  styleUrls: ['./babs.component.scss']
})
export class BabsComponent implements OnInit {
  public dataTable: DataTable;
  businessID: any;
  bb:any=[];

  constructor(private apiService: ApiService,private excelService:ExcelService,private modalService: NgbModal) {
    this.businessID = localStorage.getItem("businessID");

   }

  ngOnInit() {
    this.babs();    
  }

  excelExport() {
    var obj = this.bb;
    this.excelService.exportAsExcelFile(obj, 'babs');
  }

  selectTerm () {
    let modalRef = this.modalService.open(TermComponent, { "backdrop": "static" });

        modalRef.result.then((data) => {
            // on close
            console.log(data, "result data");
           
        }, () => {
            // on dismiss
        });
  }

  babs() {
    this.apiService.getData(this.businessID , "babs/").then((result) => {
      console.log(result);
      this.bb = result;
      this.dataTable = {
        headerRow: ['#', 'Term', 'Customer', 'Form Type', 'Invoice Count', 'Amount', 'Response Date', 'Response'],
        footerRow: ['Row Number', 'Term', 'Customer', 'Form Type', 'Invoice Count', 'Amount', 'Response Date', 'Response'],
        dataRows: this.bb
      };
      
    });
  }

  ngAfterViewInit() {
    $('#datatables').DataTable({
      "pagingType": "full",
      "pageLength": 25,
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
        lengthMenu: "_MENU_ records per page",
        zeroRecords: " ",
        info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
        infoEmpty: "No record",
        infoFiltered: "(filtered from _MAX_ records)",
        paginate: {
          "next": "<i class='fa fa-angle-right'></i>",
          "previous": "<i class='fa fa-angle-left'></i>",
          "first": "<i class='fa fa-angle-double-left'></i>",
          "last": "<i class='fa fa-angle-double-right'></i>",
        }
      }
    });
  }
}
