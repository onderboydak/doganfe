import { Routes } from '@angular/router';

import { BankComponent } from './bank/bank.component';
import { BanksComponent } from './banks/banks.component';

import { BankAccountsComponent } from './bankAccounts/bankAccounts.component';
import { BankAccountComponent } from './bankAccount/bankAccount.component';

import { CashComponent } from './cash/cash.component';
import { TransactionsComponent } from './transactions/transactions.component';

import { AuthGuardService as AuthGuard } from '../auth/auth-guard.service';
import { AggrComponent } from './aggr/aggr.component';
import { BalanceComponent } from './balance/balance.component';
import { BabsComponent } from './babs/babs.component';


export const FinanceRoutes: Routes = [
{
    path: '',
    children: [ {
        path: 'banks',
        component: BanksComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: 'bank/:id',
    component: BankComponent,
    canActivate: [AuthGuard] 
}, 
{
    path: '',
    children: [ {
        path: 'bank',
        component: BankComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: '',
    children: [ {
        path: 'bankAccounts',
        component: BankAccountsComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: 'bankAccount/:id',
    component: BankAccountComponent
}, 
{
    path: '',
    children: [ {
        path: 'bank',
        component: BankAccountComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: '',
    children: [ {
        path: 'cash',
        component: CashComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: '',
    children: [ {
        path: 'aggr',
        component: AggrComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: '',
    children: [ {
        path: 'babs',
        component: BabsComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: '',
    children: [ {
        path: 'balance',
        component: BalanceComponent,
        canActivate: [AuthGuard] 
}]},
{
    path: '',
    children: [ {
        path: 'transactions',
        component: TransactionsComponent,
        canActivate: [AuthGuard] 
}]},{
    path: 'transactions/:id',
    component: TransactionsComponent
}
];


