import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NoteComponent } from '../note/note.component';
import { InvoiceIDComponent } from '../invoice-id/invoice-id.component';
import { ExcelService } from 'app/providers/excel.service';
import { DatePipe } from '@angular/common';

declare var $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: [];
}

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {
  public dataTable: DataTable;
  businessID: any;
  public transactions: any = [];
  public accountID: any = 0;
  startDate: any;
  endDate: any;
  invoiceStartDate:any;
  invoiceEndDate:any;

  constructor(public apiService: ApiService, private activeRoute: ActivatedRoute, private modalService: NgbModal, private excelService: ExcelService, private datePipe: DatePipe) {
    const routeParams = this.activeRoute.snapshot.params;
    if (routeParams.id > 0) {
      this.accountID = routeParams.id;
    }
    let now = new Date();
    this.invoiceStartDate=this.datePipe.transform(now, "dd-MM-yyyy");
    this.invoiceEndDate=this.datePipe.transform(now, "dd-MM-yyyy");
    /*
    this.dataTable = {
      headerRow: ['#', 'Date', 'Description', 'Amount'],
      footerRow: ['#', 'Date', 'Description', 'Amount'],
      dataRows: []
    };
    */
    console.log(this.accountID);
  }

  ngOnInit() {
    this.businessID = localStorage.getItem("businessID");
    this.clickReport();

    $('.datetimepicker').datetimepicker({
      locale: 'tr',
      format: 'DD-MM-YYYY',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });
  }

  getBankTransactions(parameters: any) {
    this.apiService.getData(parameters, "transactions/").then((result) => {
      this.transactions = result;

      this.dataTable = {
        headerRow: ['#', 'Date', 'Description', 'Amount'],
        footerRow: ['#', 'Date', 'Description', 'Amount'],
        dataRows: this.transactions
      };

      console.log(this.dataTable);
    });
  }

  openModal(transactionID: any, index: any) {
    let modalRef = this.modalService.open(NoteComponent, { "backdrop": "static" });
    modalRef.componentInstance.transactionID = transactionID;
   
    modalRef.result.then((data) => {
      // on close

      console.log(data, "result data");
      if (data["notes"] != "") {
        this.transactions[index].notes = data["notes"];
      } else {
        this.transactions[index].notes = null;
      }
    }, (reason) => {
      // on dismiss
    });

  }
  openModalInvoice(transactionID: any) {
    let modalRef = this.modalService.open(InvoiceIDComponent, { "backdrop": "static" });
    modalRef.componentInstance.transactionID = transactionID;
    modalRef.componentInstance.startDate=this.startDate;
    modalRef.componentInstance.endDate=this.endDate;

  }

  excelExport() {
    var obj = this.transactions;
    this.excelService.exportAsExcelFile(obj, 'transactions');
  }

  ngAfterViewInit() {

    $('#datatables').DataTable({
      "pagingType": "full",
      "pageLength": 25,
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
        lengthMenu: "_MENU_ records per page",
        zeroRecords: "None found",
        info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
        infoEmpty: "No record",
        infoFiltered: "(filtered from _MAX_ records)",
        "paginate": {
          "next": "<i class='fa fa-angle-right'></i>",
          "previous": "<i class='fa fa-angle-left'></i>",
          "first": "<i class='fa fa-angle-double-left'></i>",
          "last": "<i class='fa fa-angle-double-right'></i>",
        },
      }

    });
  }

  clickReport() {
    this.startDate = $('.invoiceStartDate').val();
    this.endDate = $('.invoiceEndDate').val();

    if (this.startDate == "" || this.endDate == "") {
      var sdate = new Date();
      this.startDate = this.datePipe.transform(sdate, "yyyy-MM-dd");
      this.endDate = this.startDate;
    }

    this.getBankTransactions(this.businessID + "/" + this.accountID + "/" + this.startDate + "/" + this.endDate);
  }
}
