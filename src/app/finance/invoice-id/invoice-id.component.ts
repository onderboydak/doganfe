import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-invoice-id',
  templateUrl: './invoice-id.component.html',
  styleUrls: ['./invoice-id.component.scss']
})
export class InvoiceIDComponent implements OnInit {
  @Input() public transactionID: any;
  @Input() public startDate: any;
  @Input() public endDate: any;
  businessID: any;
  transaction: any = [];


  constructor(private apiService: ApiService, public activeModal: NgbActiveModal) {
    this.businessID = localStorage.getItem("businessID");
  

  }

  ngOnInit() {
    console.log(this.transactionID, "Transaction ID");
    this.getTransaction();
  }

  clickCancel() {
    this.activeModal.close();
  }
  saveInvoiceID() {
    console.log("Click Save");
    this.apiService.postData(this.transactionID, "transaction/save/", JSON.stringify(this.transaction)).then((result) => {
      console.log(result);
      this.activeModal.close();
    });
  }

  getTransaction() {
    console.log(this.transactionID, "Transaction ID");
    let parameters = this.businessID + "/0/" + this.startDate+"/"+this.endDate+"/"+this.transactionID;
    this.apiService.getData(parameters, "transactions/").then((result) => {
      this.transaction = result[0];

    });
  }

}
