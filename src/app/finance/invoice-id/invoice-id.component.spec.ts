import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceIDComponent } from './invoice-id.component';

describe('InvoiceIDComponent', () => {
  let component: InvoiceIDComponent;
  let fixture: ComponentFixture<InvoiceIDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceIDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceIDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
