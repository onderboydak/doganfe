import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute } from '@angular/router';
import { ExcelService } from 'app/providers/excel.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BalanceDetailsComponent } from '../balance-details/balance-details.component';
import { DatePipe } from '@angular/common';

declare var $: any;

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: [];
}

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss']
})
export class BalanceComponent implements OnInit {
  public dataTable: DataTable;
  businessID: any;
  reports: any = [];

  invoiceStartDate: any;
  invoiceEndDate: any;
  invoiceCurrency: any = 1;
  totals: any = [];
  dt:any;

  constructor(private apiService: ApiService, private activeRoute: ActivatedRoute, private excelService: ExcelService, private modalService: NgbModal,private datePipe: DatePipe) {
    this.businessID = localStorage.getItem("businessID");
    let now = new Date();
    this.invoiceStartDate=this.datePipe.transform(now, "dd-MM-yyyy");
    this.invoiceEndDate=this.datePipe.transform(now, "dd-MM-yyyy");
    
  }

  ngOnInit() {
    this.dataTable = {
      headerRow: ['#', 'Account', 'SubAccount', 'Account Name', 'Currency', 'Amount', 'Amount(Currency)'],
      footerRow: ['Row Number', 'Account', 'SubAccount', 'Account Name', 'Currency', 'Amount', 'Amount(Currency)'],
      dataRows: []
    };


    $('.datetimepicker').datetimepicker({
      locale: 'tr',
      format: 'DD-MM-YYYY',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });
  }

  excelExport() {
    var obj = this.reports;
    this.excelService.exportAsExcelFile(obj, 'balance');
  }


  spInit() {
    console.log("Init Select");
    $(".selectCustomer").selectpicker();
  }

  resolveAfter2Seconds(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 2000);
    });
  }

  clickCancel() {

  }

  showDetail(rowID:number,ic:number) {
    let modalRef = this.modalService.open(BalanceDetailsComponent, {  "size": "lg","backdrop": "static" });
    modalRef.componentInstance.ID = rowID;
    modalRef.componentInstance.startDate = this.invoiceStartDate;
    modalRef.componentInstance.endDate = this.invoiceEndDate;
    modalRef.componentInstance.currency = ic;

    modalRef.result.then((data) => {
        // on close
        console.log(data, "result data");
        if (data > 0) {
            //this.invoices[index].paymentID = data;
        }
    }, () => {
        // on dismiss
    });
  }


  clickReport() {
    this.invoiceStartDate = $('.invoiceStartDate').val();
    this.invoiceEndDate = $('.invoiceEndDate').val();
    this.apiService.getData(this.businessID + "/" + this.invoiceStartDate + "/" + this.invoiceEndDate + '/' + this.invoiceCurrency, "balance/").then((result) => {
      console.log(result);
      this.reports = result;

      let amount1: number = 0;
      let amount2: number = 0;

      this.reports.forEach(element => {
        amount1 = amount1 + (element.amount * 1);
        amount2 = amount2 + (element.amounts * 1);

      });

      this.totals["amount1"] = amount1.toFixed(2);
      this.totals["amount2"] = amount2.toFixed(2);


      console.log(this.totals, "totals");

      this.dataTable = {
        headerRow: ['#', 'Account', 'SubAccount', 'Account Name', 'Currency', 'Amount', 'Amount(Currency)'],
        footerRow: ['Row Number', 'Account', 'SubAccount', 'Account Name', 'Currency', 'Amount', 'Amount(Currency)'],
        dataRows: this.reports
      };

    });
  }

  ngAfterViewInit() {
    $(".selectpicker").selectpicker();
    $(".selectCurrency").selectpicker('val', 1);

    $('#datatables').DataTable({
      "pagingType": "full",
      "pageLength": 25,
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
        lengthMenu: "_MENU_ records per page",
        zeroRecords: " ",
        info: "_TOTAL_ Records - Page _PAGE_/_PAGES_",
        infoEmpty: "No record",
        infoFiltered: "(filtered from _MAX_ records)",
        paginate: {
          "next": "<i class='fa fa-angle-right'></i>",
          "previous": "<i class='fa fa-angle-left'></i>",
          "first": "<i class='fa fa-angle-double-left'></i>",
          "last": "<i class='fa fa-angle-double-right'></i>",
        }
      }
    });
  }

}
