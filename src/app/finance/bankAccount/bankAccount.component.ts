import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-bankAccount',
  templateUrl: './bankAccount.component.html',
  styleUrls: ['./bankAccount.component.scss']
})

export class BankAccountComponent implements OnInit {
  public bankID:any=0;
  businessID:any;
  bankAccount:any=[];
  banks:any=[];
  branches:any=[];


  @Input() public accountID:any;

  constructor(public apiService: ApiService, private activeRoute: ActivatedRoute,public router:Router) {
      const routeParams = this.activeRoute.snapshot.params;
      this.bankID = routeParams.id;
      this.businessID=localStorage.getItem("businessID");
      
     }

  ngOnInit() {
      
      // New record control
      if (this.bankID>0) {
        this.getBankAccount(this.bankID);
      } else {
         this.bankAccount={"IBAN": "","ID":0,"account": "","accountID": "","bank": "","bankAccountID":0,"bankID": 0,"currency": "","currencyID":0}; 
      }
      console.log(this.bankAccount,"bankaccount");
      this.getBank(0);   
  }

  getBankAccount (bankID:any) {
    this.apiService.getData((this.businessID+"/"+bankID+"/1"), "accounts/").then((result) => {
      this.bankAccount = result[0];    
      
    });

  }

  getBankBranches (bankID:any) {
    this.apiService.getData(bankID, "bankBranches/").then((result) => {
      this.branches = result;
      this.spInit();
    });
  }

  deleteAccount() {
    this.apiService.deleteData(this.bankID, "account/delete/").then((result) => {
      console.log(result);
      this.router.navigateByUrl("/finance/bankAccounts");
    });
  }
  clickCancel(){
    this.router.navigateByUrl("/finance/bankAccounts");
  }

  saveAccount(){
    this.bankAccount.branchID=0;
    this.bankAccount.asID=this.businessID;
    this.apiService.postData(this.bankAccount.ID, "account/save/", JSON.stringify(this.bankAccount)).then((result) => {
      console.log(result);
      this.router.navigateByUrl("/finance/bankAccounts");
    });
  }

  getBank(bankID:any) {
    this.apiService.getData(bankID, "banks/").then((result) => {
      this.banks = result;
      if (this.bankAccount.bankID>0) {
        this.getBankBranches(this.bankAccount.bankID);
      } else {
        this.spInit();
      }
    });
  }

  spInit() {
    console.log("spInit");
    setTimeout(()=> {
      $(".selectBank").selectpicker('val', this.bankAccount.bankID);
      $(".selectcurrency").selectpicker('val', this.bankAccount.currencyID);
      $(".selectBranch").selectpicker('val', this.bankAccount.branchID);
      $(".selectBranch").selectpicker('refresh');
      },500);
  }
}
