import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'app/providers/api.service';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {
  @Input() public transactionID:any;
  businessID:any;
  transaction:any=[];

  constructor(private apiService:ApiService, public activeModal : NgbActiveModal) { 
    this.businessID = localStorage.getItem("businessID");
    
  }

  ngOnInit() {
    console.log(this.transactionID,"Transaction ID");
    this.getTransaction();
  }

  clickCancel() {
    this.activeModal.close();
}
saveNote() {
  console.log("Click Save");
  this.apiService.postData(this.transactionID, "transaction/save/", JSON.stringify(this.transaction)).then((result) => {
    console.log(result);
    this.activeModal.close(result);
  });
}

getTransaction() {
  console.log(this.transactionID,"Transaction ID");
  let parameters = this.businessID + "/0/" + this.transactionID 
  this.apiService.getData(parameters, "transactions/").then((result) => {
    this.transaction = result[0];

  });
}

}
