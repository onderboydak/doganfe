import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FinanceRoutes } from './finance.routing';

import { TranslateModule } from '@ngx-translate/core';

import { BanksComponent } from './banks/banks.component';
import { BankComponent } from './bank/bank.component';

import { CashComponent } from './cash/cash.component';
import { TransactionsComponent } from './transactions/transactions.component';

import { BankAccountComponent } from './bankAccount/bankAccount.component';
import { BankAccountsComponent } from './bankAccounts/bankAccounts.component';
import { AggrComponent } from './aggr/aggr.component';
import { BalanceComponent } from './balance/balance.component';
import { BabsComponent } from './babs/babs.component';
import { TermComponent } from './babs/term/term.component';




@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(FinanceRoutes),
        FormsModule,
        ReactiveFormsModule,
        TranslateModule
    ],
    declarations: [
        BanksComponent,
        BankComponent,
        CashComponent,
        TransactionsComponent,
        BankAccountComponent,
        BankAccountsComponent,
        AggrComponent,
        BalanceComponent,
        BabsComponent
            ]
})

export class FinanceModule {}
