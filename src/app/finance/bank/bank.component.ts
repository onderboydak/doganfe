import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'app/providers/api.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})

export class BankComponent implements OnInit {
  public bankID: any;
  businessID: any;
  bankAccount: any = [];
  bank: any = [];
  countries: any = [];

  @Input() public accountID: any;

  constructor(public apiService: ApiService, private activeRoute: ActivatedRoute, public router: Router) {
    const routeParams = this.activeRoute.snapshot.params;
    this.bankID = routeParams.id;
    this.businessID = localStorage.getItem("businessID");

  }

  ngOnInit() {
    this.getCountries();

  }


  deleteBank() {
    this.apiService.deleteData(this.bank.bankID, "bank/delete/").then((result) => {
      console.log(result);
      this.router.navigateByUrl("/finance/banks");
    });
  }
  clickCancel() {
    this.router.navigateByUrl("/finance/banks");
  }

  saveBank() {
    this.apiService.postData(this.bank.bankID, "bank/save/", JSON.stringify(this.bank)).then((result) => {
      console.log(result);
      this.router.navigateByUrl("/finance/banks");
    });
  }

  getBank(bankID: any) {
    if (bankID > 0) {
      this.apiService.getData(bankID, "banks/").then((result) => {
        this.bank = result[0];
        this.spInit();
      });
    } else {
      this.bank = { "ID": 0, "SWIFT": "", "address": "", "bankID": 0, "country": "", "countryID": 0, "kepEmail": "", "name": "", "phone": "" };
      this.spInit();
    }
  }

  getCountries() {
    this.apiService.getData("", "country").then((result) => {
      this.countries = result;
      this.getBank(this.bankID);
    });
  }

  spInit() {
    setTimeout(() => {
      $(".selectCountry").selectpicker('val', this.bank.countryID);
    }, 500);
  }
}
