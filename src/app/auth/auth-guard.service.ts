import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { ApiService } from 'app/providers/api.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  userID: any;
  userType: any;
  action: any;
  canActive: boolean;
  constructor(public auth: AuthService, public router: Router, public apiService: ApiService) {
    this.userID = localStorage.getItem("userID");
    this.userType = localStorage.getItem("userType");
    this.action = JSON.parse(localStorage.getItem("action"));
    //console.log(this.action);
  }

  canActivate(nextPage: ActivatedRouteSnapshot, onPage: RouterStateSnapshot): boolean {
    console.log(nextPage.url, " ", onPage.url, " yesss");
    console.log(this.action);

    var act: any;
    if (!this.auth.isAuthenticated() && this.userType != 1) {
      this.router.navigate(['pages/login']);
      return false;
    }
    if (this.userType != 1) {
      //console.log(this.action,"action");
      //console.log(onPage.url.substring(1).split("?",1)[0]);
      var forFilter = this.action;
      act = forFilter.filter((ac: { menu: string; }) => onPage.url.substring(1).includes(ac.menu));
      if (act.length>0) {
        act = act[0]["action"];
      }
    }


    //console.log(act, "action", this.userType, "userType");
    return (act > 0 || this.userType == 1);

  }

}


