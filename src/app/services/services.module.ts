import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchCustomerComponent } from './search-customer/search-customer.component';

import { ServicesRoutes } from './services.routing';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [SearchCustomerComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ServicesRoutes),
    FormsModule,
    TranslateModule  
  ]
})
export class ServicesModule { }
