import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/providers/api.service';

@Component({
  selector: 'app-search-customer',
  templateUrl: './search-customer.component.html',
  styleUrls: ['./search-customer.component.scss']
})
export class SearchCustomerComponent implements OnInit {
  vkn:string="";
  title:string="";
  customer:any =[];
  constructor(public apiService:ApiService) {

   }

  ngOnInit() {
      
  }

  searchCustomer() {
    if (this.vkn=="") {
      this.vkn="null";
    }
    if (this.title=="") {
      this.title="null";
    }
    this.apiService.getData(this.vkn+"/"+this.title, "checkUser/").then((result) => {
     console.log(result);
      this.customer=result["USER"][0];
      console.log(this.customer,"customer");
    });
  }

}
