import { Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../auth/auth-guard.service';
import { SearchCustomerComponent } from './search-customer/search-customer.component';

export const ServicesRoutes: Routes = [
    {
        path: '',
        children: [ {
            path: 'searchCustomer',
            component: SearchCustomerComponent,
            canActivate: [AuthGuard] 
    }]}
]